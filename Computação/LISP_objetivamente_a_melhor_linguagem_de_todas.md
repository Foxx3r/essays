# LISP: objetivamente a melhor linguagem de todas
Antes de começar, quero dizer que minha visão não é enviesada. Eu odeio LISP porque não consigo me acostumar com a linguagem, tampouco preciso dela na academia (apenas de vez em quando).

LISP, o que eu posso dizer desta maravilhosa linguagem? Basicamente, LISP foi a segunda linguagem criada no mundo, a primeira linguagem a ter suporte a UTF-8, a primeira linguagem interpretada, a primeira linguagem a ter um GC, a primeira linguagem homoicônica, a primeira linguagem com computação simbólica, a primeira linguagem a ter if, a primeira linguagem a ter meta-programação, a primeira linguagem funcional, a primeira linguagem reflexiva, a pioneira em linguística e inteligência artificial, sem falar das LISPs machines, além de seu inventor ter inventado o time sharing, impulsionou a criação de DSLs (linguagens de domínio específico), pioneira em recursão, estruturas de dados, self-hosting compiler, tipagem dinâmica... E não para por aí. Se eu quisesse, faria um artigo inteiro sobre LISP e o John McCarthy.

LISP não é só importante por ter sido a primeira linguagem funcional, mas ela é um elemento importante para a ML, a mãe de Haskell e da programação funcional moderna.

LISP é basicamente a linguagem mais simples do mundo. Você pode criar um interpretador LISP apenas com 9 keywords:
```lisp
quote
atom
eq
cond
lambda
cons
label
car
cdr
```

Isso reflete na quantidade de dialetos da linguagem, que são infinitas. Na verdade, LISP é a linguagem mais avançada no quesito de metaprogramação; como tudo é uma lista, tudo é altamente manipulável, até mesmo a AST da linguagem, então você pode criar dialetos facilmente. Não há uma única coisa que você não possa mudar em LISP, é uma linguagem muito poderosa, não é a toa que Sussman e Abelson colocaram em sua capa de livro a imagem de um mago.

A décima regra de Greenspun afirma que toda linguagem em cima de C/Fortran não só carregam os bugs da linguagem (ou amenizam), mas também são mais lentos. Curiosamente, isso não acontece com LISP, porque em LISP, tudo é uma estrutura de dados. Você pode olhar para o programa como uma estrutura de dados, como uma lista de instruções, como átomos, como funções e parâmetros, como lambda-calculus e como lista de átomos. Ao escrever uma linguagem em LISP, você basicamente só está escrevendo o mesmo código sem nenhum truque por baixo. Curiosamente, isso só afeta LISP (e talvez Forth?), porque outras linguagens homoicônicas como Elixir e Julia sofrem desse mesmo problema de C/Fortran.
