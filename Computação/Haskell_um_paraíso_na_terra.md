# Haskell: um paraíso na terra
Haskell tem um GC incrivelmente eficiente, e tem os avanços mais modernos na área de laziness (eles usam STG machines, linguagens convencionais usam G machines), além de que curry simplifica bastante as soluções. List comprehensions (inclusive, list comprehension se originou em Haskell) e lambdas são as mais legíveis de todas, e você também pode fazer tail recursion sem nem mesmo alterar uma linha do código recursivo, usando strictness (ativado pela extensão `BangPatterns`). Haskell também tem o suporte mais avançado do mundo a type-level programming. E o parsec é um pai do parsing, ele é extremamente eficiente e por isso, criar linguagens em Haskell é bem simples. Haskell também pode ser otimizado de várias maneiras, eu já enumerei 15 formas uma vez para o meu amigo. Haskell também tem uma stdlib bem madura e completa, e é a linguagem funcional mais poderosa do mundo. Haskell também tem todas os tipos de lambda-calculus possíveis (exceto CC e tipos dependentes), além de ser feito para ser extensível. E quase não tem sintaxe.

Aliás, Haskell tem um modelo avançadíssimo de concorrência, aonde suas green threads são estáveis, você tem mutação real (para performance) e elas são mais leves que as `pthreads` de C. Além de que todo o modelo de concorrência de Haskell é baseado no modelo corporativo pesado de tolerância a falhas, o famoso STM. Além de que, monads são apenas variantes de async. E fazer FFI com C é bem simples.

Além de que a linguagem tem milhares de extensões (no formato `{-# LANGUAGE ExtensaoDesejada #-}`) que modificam a sintaxe ou semântica, extendem a linguagem e torna as coisas mais produtivas. Além de que é possível representar idéias abstratas, por isso que Haskell é tão usado por matemáticos - aliás, seu modelo de sistema de tipos é baseado na teoria das categorias, o auge em abstração da álgebra.

Além de milhares de recursos avançados, como lens, prisms, profunctors, generics, type families, arrows, polykinds, etc.

Uma lens pega um product type e "foca" em uma parte dele. Um Prism pega um sum type e valida se ele é o caso correto. Prisms também podem construir sum types a partir de valores. Por exemplo:
```hs
preview _Right (Right x) = Just x
preview _Left (Right x) = Nothing
review _Right x = Right x
review _Left x = Left x
```

E apesar dessa complexidade toda, Haskell é uma das linguagens mais simples de todas. A prova disso é como é fácil escrever um interpretador Haskell - sua lógica é bem simples e não demanda muito além do que é expresso na própria "sintaxe" (só não é simples se quisermos concorrer com o GHC ou mesmo replicar o Haskell 2010 ou prime, mas o 98 é até acessível para meras criaturas normais que não sabem pronunciar Ctulhu). Haskell tem basicamente as seguintes keywords:
```hs
module
import
qualified
hiding
let
do
where
case
type
newtype
data
class
instance
```

E os seguintes operadores built-in:
```hs
=
::
=>
->
<-
```
