# Python: um hype desnecessário
Há 3 anos, eu consegui resumir minhas críticas a Python no Telegram, porém, adicionei mais coisas à crítica (e removi duplicatas) com coisas que aprendi 2 anos atrás. Primeiramente, meu argumento não é clamando por uma linguagem "perfeita", minha crítica a Python não é sobre nenhum elemento individual (exceto alguns poucos, como performance, mas que são exclusos por outros fatores, como o uso), mas a minha crítica a Python se centra em que todo esse conjunto de críticas o tornam necessariamente ruim, para (quase) todos os casos, principalmente no que tange à engenharia do software por parte da organização que inicialmente arquitetou o interpretador Python. Vamos lá.
1. Performance horrível (sensível ao contexto, gramática precisa de backtracking, usa parser LL e a gramática é ambígua)
2. Performance horrível (bigint para tudo)
3. Performance horrível (GIL quase não deixa um objeto todo passar, principalmente por causa do stop the world, e o GC de Python não tem minor paralelo)
4. Performance horrível (sem JIT)
5. Performance horrível (fibras ultrapassando o bottleneck de Python)
6. Performance horrível (tudo é objeto)
7. Performance horrível (sem laziness)
8. Performance horrível (não consegue fazer auto inlinement e dificilmente consegue fazer auto-vetorização SIMD, assim não conseguindo aproveitar grandes features do x64)
9. Performance horrível (GC antigo, de 1970: mark-and-sweep)
10. Performance horrível (dynamic dispatching)
11. Performance horrível (async com complexidade assintótica de `O(log n)`)
12. Semântica horrível (só threadpools salvam concorrência em python)
13. Gambiarra (FFI com C - CPython realiza um longo e complexo processo de tradução/interpretação)
14. Gambiarra (AST horrível)
15. Complexo (sintaxe > semântica)
16. Inseguro (duck typing ruim)
17. Inseguro e performance horrível (muito suscetível a memory leak)

Eu considero que o PyPy é uma melhoria substancial de Python. Mesmo que não resolva todos os maiores problemas acima - apenas alguns -, ele torna a linguagem "usável" (entre aspas porque meu conceito de "usável" não é o mesmo do senso comum) para alguns sistemas.

Não que usar Python nesses sistemas seja impossível, afinal, em linguagem turing-completas é possível fazer quase qualquer coisa, e representar quase qualquer coisa. Portanto, esse ponto não invalida a crítica. É uma crítica pontual acerca do design da linguagem.

O argumento de "O programador faz a linguagem" aqui também não serve, tendo em vista que somos humanos - e humanos erram! A linguagem deve buscar reduzir os erros do programador, foi assim que C nasceu do Assembly e todos os ALGOLs posteriores (e mesmo Pascal ou linguagens funcionais - sistemas de tipos nasceram para que os compiladores pudessem saber o tamanho dos dados na memória e mais tarde encontrou mais utilidades ao descobrir-se que um sistema de tipos é um provador de teoremas, atingindo seu apogeu no mainstream funcional da década 2010, na qual as linguagens mainstreams perceberam que isso as daria maior confiabilidade (e eficiência). Note, eu não errei na "antropologia computacional" ao dizer que este é um fenômeno recente - de 2010 -, porque tipagem não é o mesmo que sistema de tipos). Não brincamos de seres perfeitos, nós fazemos aplicações reais. A maioria dos programadores sequer programam regularmente, eles são engenheiros, são cientistas, são físicos, são economistas, são arquitetos, e um comportamento errado devido à impureza de um código ou Undefined Behavior ou data race pode derrubar um avião, um foguete, ou mesmo explodir um laboratório químico. Lembre-se que mesmo a equipe do OpenSSL produziu a maior falha do ano - o Heartbleed -, mesmo sendo uma equipe altamente treinada, mesmo o código passando por milhares de revisores, por uma falha tosca e boba do C, que não seria possível acontecer caso a aplicação fosse em Rust. Todo cuidado é pouco e precisamos de metodologias de software rigorosas.

Estendendo um pouco o tópico, irei compartilhar meu segredo sobre metodologia de software:
- Programação funcional (de preferência, linguagem puramente funcional)
- Tipagem sempre explícita
- Tipos lineares (ou afins) (quando possível)
- Type-level programming
- Dependent types (quando possível, caso contrário, uso refinement types/contratos)
- Teoria das categorias (quando possível)
- Wishful thinking (com typeclasses e tipos de dados algébricos, às vezes parametrizados quando preciso de mais poder)
- Fearless concurrency (quando possível, caso não seja possível, tomo as precauções necessárias que aprendi com Rust e Java)
- Property testing com grupos de teste (quanto mais similar ao Tasty de Haskell, melhor) (ao invés de testes unitários)
- Formas de static analysis (sejam humanas ou não - não gosto de programar em equipe, então a maior parte da minha vida, em uma ocorrência de 1/20 projetos, eu não usei métodos humanos de análises estáticas)
- Métodos modernos, como strings como sendo clusters de grafema UTF-8
- Análise ciclomática
- Análise assintótica
- Sistema de controle de versionamento (de preferência, GitLab ou mesmo notabug nos casos mais simples)
- Laziness (quando possível) + strictness quando necessário
- Clean code (não dogmaticamente, aliás, livros de programação são sugestões, não manuais de regras) e evitar comentários
- Evitar design patterns (com exceção de wishful thinking?)
- Programação genérica (note, não é o mesmo que tipos genéricos, a programação genérica te permite carregar informações sobre a estrutura do seu tipo e sua implementação polimórfica, como RTTI, que em Haskell se chama `Typeable`)
- Programação dinâmica (otimização serial)
- Programação linear (otimização de funções - subset de programação dinâmica)
- Refatoração (envolvendo até mesmo formalização de código para matemática reversa (necessariamente construtiva))

E no final faço otimização (usando profiling) até onde não der mais. Além de claro, flags de compilação e configuração de OS/criptografia.

Ah, e 6 conselhos que quase passaram despercebidos.
1. Nunca use floating point para sistemas que necessitem de precisão aritmética (como sistemas bancários/contabilísticos)
2. Nunca use programação orientada a objetos (pois não casa com paralelismo)
3. Parseie, não valide! :)
4. Evite tratamento de exceções A TODO CUSTO e passe a usar option types (lembrando que nulls são o [erro de 1 bilhão de dólares](https://www.infoq.com/presentations/Null-References-The-Billion-Dollar-Mistake-Tony-Hoare/) da indústria de softwares)
5. Evite mutex ([por que?](https://accu.org/journals/overload/27/149/ignatchenko_2623/))
6. Evite MVC e outras gambiarras web. Use TEA - The Elm Architecture!