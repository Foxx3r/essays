# Por que não usar floating point em sistemas bancários
```py
>>> 1 / 3
0.3333333333333333
>>> 1/3 + 1/3 + 1/3
1.0
>>> a = 0.1
>>> for _ in range(0, 101):
...     a += 0.1
...
>>> a
10.19999999999998
```

Me digam, vocês acham que alguma dessas contas está correta? Não, porque na verdade:
```py
0.1 == 0.100000001490116119384765625
```

Isso não faz diferença alguma se for representação. Mas se for usar cálculo/operações, pode esquecer. O melhor tipo de dado para representar dinheiro no sistema monetário seria o `int` (na verdade, big int) ou `decimal` (na verdade, big decimal).

Um 662 seria 6,62, 80 seria 0,80 e por aí vai. Sempre dá para representar dinheiro como `int` e você não vai ter problema com isso. Qualquer dízima periódica pode sofrer deste problema, e isso é consequência do processador não saber interpretar 0.1, então ele precisa fazer uma aproximação. O computador que sabia interpretar era o IBM 1620.

Fontes:
- [The Floating-point Guide | What Every Programmer Should Know About Floating-Point Arithmetic](https://floating-point-gui.de)