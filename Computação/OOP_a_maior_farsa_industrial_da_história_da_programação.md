# OOP: a maior farsa industrial da história da programação
A seguir, um resumo das minhas críticas a OOP (Oriented Object Programming):
- OO é só programação procedural e com funções em structs (lentas e pesadas)
- Impura, tem side effects, desestimula programação funcional (o que implica em mais insegurança)
- É um conceito roubado e mal-compreendido de Bjarne Stroustroup sobre Alan Kay
- Você pode simular herança via super traits ou monkey/dynamic dispatching
- Abstração é possível sem OO
- Enums, structs, unions, hashtables, sum types, product types, etc podem servir como encapsulação
- Polimorfismo é possível com traits/typeclasses/módulos
- Os 4 pontos acima dizem muito do porque nunca conseguirem formalizar Orientação a Objetos: nunca conseguiriam, haveriam linguagens não-OO que seriam tratadas como OO
- Alignment de classes é impossível por elas serem lentas
- Muitos design patterns
- É comprovado academicamente que sincronização e herança não se dão bem

"Mas ao abandonar OO, você abandona o polimorfismo" diz a pessoa que não sabe nem que existem [6 tipos de polimorfismo](https://medium.com/red-ventures-br-tech/6-tipos-de-polimorfismo-7787080e8857).