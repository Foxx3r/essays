# PCRE: a regex que não é uma regex
A PCRE criada pelo criador do Perl que nunca estudou teoria da computação e criou uma expressão regular que não é uma expressão regular. PCRE permite backreferencing, lookahead, lookbehind, que o FA de regex seja indecidível, é lenta e além de não ser regular.

Na hierarquia de Chomsky, gramática regular está na hierarquia (3), enquanto PCRE está na hierarquia (1) (sensível ao contexto) ou hierarquia (2) (livre de contexto). PCRE é turing-completa, ou seja, pode rodar infinitamente, é indecidível e cai no problema da completude (ou melhor, problema da parada, em decorrência de ser turing-completo, sem necessidade), além de ser NP.

Um regex clássico (que é equivalente a um NFA com epsilon closures) é tão leve que a quantidade de uso da memória dele é a mesma quantidade de estados que ele gera, ou seja, `O(n)`. Já as implementações mais usadas hoje em dia (como a PCRE) são falsas regex, isso significa que elas não são regulares (não podem ser reconhecidas por uma máquina de estados finitos) e praticamente estourariam a memória dela de tanto consumo, aliás, uma regex real não faz escrita na memória, a PCRE sim. Isso quer dizer que a complexidade dessas falsas regex (que a gente usa no dia-a-dia) é `O(2ⁿ)`.

Essa coisa se popularizou porque na época havia um mito de que Perl era linguagem de sistema.