# Contra o MongoDB e um retorno aos clássicos
MongoDB é tão ruim, tão inútil e ainda por cima a empresa que o faz é tão incompetente e politicamente incorreta que mudaram a licença para impedir cada vez mais seu uso, principalmente no mundo cloud computing.

Conseguiram se fazer ejetar da distribuição Fedora:
>O banco de dados MongoDB mudou recentemente sua licença de AGPL para uma licença personalizada chamada Server Side Public License v1 (SSPL). Após revisar a nova licença, o Projeto Fedora determinou que o SSPL não é uma licença de software livre. Como as políticas de licenciamento do Fedora proíbem o projeto de distribuir software não-livre, o MongoDB seria impossível de atualizar para novas versões upstream e, portanto, está sendo removido do Fedora.

Resumo: gente assim tem mesmo que se ver fora do mercado.

Eventual consistency é um câncer. Torna a aplicação muito mais complexa. Poucos sistemas realmente precisam funcionar assim, mesmo distribuídos. Por exemplo, a Google criou o spanner justamente por isso. Isso anula a maior vantagem do mongo IMO (escalabilidade horizontal). Postgres tem replicação *com consistência*.

A outra "vantagem" é que o mongo não tem schema, o que para a maioria dos sistemas é uma desvantagem. Mas se você quiser isso, postgres tem suporte a colunas JSON. A meu ver, postgres é melhor nesse NoSQL business do que o próprio mongo.

E destruir os valores "morais" da ACID é quase um atentado à boas práticas de programação, porque NoSQL nega a ACID, como um hipster. Você vai estar abrindo dos 4 princípios da ACID:
- Atomicidade
- Consistência
- Isolação
- Durabilidade