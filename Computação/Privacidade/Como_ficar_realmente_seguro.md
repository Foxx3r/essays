# Como ficar realmente seguro
Não, não irei mandar você sair da internet, invadir os servidores da polícia e apagar as digitais da sua mão e os seus pés, o seu DNA, alvejar toda a sua casa, nem destruir seu HD. Este é um manual verdadeiro de como realmente ficar seguro na internet - afinal, somos socialistas, e todo corpo revolucionário precisa de seu hacker e que seu hacker esteja seguro.

Níveis de segurança:

**Wallets**

1. Contas em exchanges
2. Web & closed-source & 2of2 wallets
3. Mobile/Desktop open-source wallets
4. Ledger Nano S/X
5. Trezor One/T & Paper wallets
6. Nenhuma

**Hardware desktop**

1. Stock BIOS/UEFI FW
2. BIOS Mod & BIOS open-source com blobs
3. BIOS open-source + IME neutralizado (pré-Skylake somente)
4. BIOS open-source sem blobs
5. Open Hardware

**Mobile OS**

1. Stock ROM Android Gapps & Windows Phone
2. iOS sem jailbreak
3. Custom ROM Android Gapps & iOS jailbreak (uncover ou checkrain)
4. Custom ROM Android sem GApps & Linux não Android
5. Open Hardware & Graphene OS

**Desktop OS**

1. Windows
2. Binary-based Linux Lennartwares & MacOS
3. Binary-based Linux sem Lennartware (exceção LUKS)
4. FreeBSD
5. Source-based Linux & OpenBSD

Mas qual o sentido de usar um source-based além de puro saudosismo? Vou dar exemplos:
```
-fstack-protector
  Emite código extra para estouros de buffer, como ataques de destruição de pilha. Isso é feito adicionando uma variável de guarda às funções com objetos vulneráveis. Isso inclui funções que chamam "alloca" e funções com buffers maiores que 8 bytes. Os protetores são inicializados quando uma função é inserida e, em seguida, verificados quando a função é encerrada. Se uma verificação de proteção falhar, uma mensagem de erro será impressa e o programa será encerrado.
-fstack-protector-strong
  Como -fstack-protector, mas inclui funções adicionais a serem protegidas - aquelas que têm definições de array locais ou têm referências a endereços de frames locais
-fstack-protector-all
  Como -fstack-protector, exceto que todas as funções são protegidas
```

Com isso, daria para controlar o SSP (Stack-Smashing Protector, também conhecido como Canary), que é algo bem importante para a segurança.
```
-Wl, -z, relro, -z, now
   Ao gerar uma biblioteca executável ou compartilhada, marca para informar ao vinculador dinâmico para resolver todos os símbolos quando o programa for iniciado ou quando a biblioteca compartilhada for carregada por dlopen, em vez de adiar a resolução de chamada de função para o ponto em que a função é chamada pela primeira vez.
RELRO
   Cria um cabeçalho de segmento ELF "PT_GNU_RELRO" no objeto. Isso especifica um segmento de memória que deve ser tornado somente leitura após a realocação, se houver suporte.
```

RELRO é uma proteção contra Overflows que acontecem no segmento HEAP.
```
-fstack-clash-protection
   Gera código para evitar ataques de estilo de conflito de stack. Quando esta opção está habilitada, o compilador alocará apenas uma página de espaço de pilha por vez e cada página será acessada imediatamente após a alocação. Portanto, evita que as alocações pulem qualquer página de proteção de stack fornecida pelo sistema operacional.
-fsplit-stack
   Gere código para dividir automaticamente a pilha antes que ela transborde. O programa resultante tem uma pilha descontínua que só pode transbordar se o programa não for capaz de alocar mais memória. Isso é mais útil ao executar programas encadeados, pois não é mais necessário calcular um bom tamanho de stack a ser usado para cada encadeamento. Isso está atualmente implementado apenas para os destinos x86 executando GNU/Linux.
-pie
   Produz um executável independente de posição ligada dinamicamente em alvos que o suportam. Para resultados previsíveis, você também deve especificar o conjunto de opções de nomes usado para compilação (-fPIE, -fpie ou subopções de modelos) ao especificar esta opção de vinculador.
-fwrapv-pointer
   Esta opção instrui o compilador a assumir que o estouro aritmético do ponteiro na adição e subtração ocorre usando a representação de complemento de dois. Este sinalizador desativa algumas otimizações que presumem que o estouro de ponto é inválido.
-fstrict-overflow
   Esta opção implica em -fno-wrapv -fno-wrapv-pointer e, quando negada, implica em -fwrapv -fwrapv-pointer.
```

Entre muitas outras cflags onde você pode usar o poder que está em suas mãos, documentadas na man page do GCC e do LD. Sem falar na facilidade de modificar os sources, diminuir a surface de ataque (desabilitar tudo que não é necessário, useflags), tudo seguindo a filosofia minimalismo e KISS (Keep It Simple Stupid).

Por exemplo, porque seu sistema precisa ter suporte a LKMs (Loadable Kernel Modules), sendo que você pode embutir tudo diretamente no kernel? Suporte a LKMs permite que alguém carregue algum módulo que faça algum hooking (interceptação de syscalls) malicioso, por exemplo, e como tudo funciona sob syscalls, então para retirá-lo, você teria que formatar.

Mesma coisa com o ld_preload da GLibc, que permite a possibilidade de hookings (porém em user-space ao invés de kernel-space). Para desabilitar o ld_preload da GLibc (no caso, fazer com que a função do_preload retorne sempre 0) teria que editar o source e recompilar, e por aí vai. São N razões para usar source-based para segurança.

Se quiser, dê uma olhada no ClipOS (é basicamente um Gentoo com algumas modificações), foi desenvolvida pela ANSSI (Agence Nationale de la Sécurité des Systèmes d'Information). Ele tem GrSecurity/PaX (PAGEEXEC sem EMUTRAMP, ASLR/KASLR, etc), MAC desenvolvido por eles mesmo, AES LRW-BENBI (para FDE ao invés de XTS), swap criptografado, cflags habilitando SSP, PIE/PIC, RELRO entre outros mecanismos e etc. A ANSSI também é a desenvolvedora da libecc (que mostra bem o nível de paranóia deles) entre outros projetos (SmartPGP, Wookey, etc).

A libecc mantém um foco na capacidade de leitura e auditoria do código. O código é puro C99, sem alocação dinâmica e inclui pré/pós-asserções no código. Consequentemente, essa biblioteca é uma boa candidata para alvos incorporados (deve ser facilmente transportável em várias plataformas). Ela tem separação de camadas limpa para todas as abstrações e operações matemáticas necessárias. Uma tipagem forte (tão "forte" quanto o C99 permite, é claro) de objetos matemáticos foi usada em cada camada.

A biblioteca não foi projetada para quebrar recordes de desempenho, embora faça um trabalho decente. Da mesma forma, a área de cobertura da memória da biblioteca (em termos de uso de ROM e RAM) não é a menor possível (embora alguns esforços tenham sido feitos para limitá-la e caber em plataformas "comuns"). O núcleo da biblioteca libecc não tem dependência externa (nem mesmo a biblioteca libc padrão) para torná-lo portátil. Consulte a seção sobre portabilidade para obter mais informações.

Alguns truques algorítmicos em curvas primos específicas não são implementados: os mesmos algoritmos são usados para todas as curvas. Isso significa, por exemplo, que as curvas usando primos pseudo-Mersenne (como as curvas SECP do NIST) não serão mais rápidas do que curvas usando primos aleatórios genéricos (como curvas Brainpool), embora os primos pseudo-Mersenne possam se beneficiar de um algoritmo de redução dedicado, produzindo ordens de magnitude aritmética de campo mais rápida (cerca de cinco a dez vezes mais rápido).

Nota: libecc não usa nenhuma alocação de heap, e as únicas variáveis globais usadas são as constantes. Os dados constantes devem terminar na seção flash/EEPROM com um acesso somente leitura a eles: nenhuma memória RAM deve ser consumida por eles. Os dados de leitura/gravação são feitos apenas de variáveis locais na stack, portanto, o consumo de RAM (essencialmente feito de matrizes que representam objetos internos, como números, pontos em curvas, etc) deve ser razoavelmente constante entre as plataformas. No entanto, algumas plataformas que usam a arquitetura Harvard (em oposição à de Von Neumann) podem ter grandes limitações ao acessar a chamada "memória de programa" como dados. O Atmel AVR MCU de 8 bits é um exemplo. Compiladores e conjuntos de ferramentas para tais arquiteturas geralmente copiam dados somente leitura na RAM em tempo de execução e/ou fornecem maneiras não padronizadas de acessar dados somente leitura na memória de programa flash/EEPROM (por meio de macros, pragmas, funções específicas). O primeiro caso significa que o consumo de RAM aumentará para libecc em comparação com o uso apenas da pilha (por causa da cópia em tempo de execução). O segundo caso significa que o código libecc terá que ser adaptado à plataforma se o usuário quiser manter o uso de RAM em seu nível mínimo. Em qualquer caso, rastrear onde residem os dados qualificados const será importante quando a quantidade de RAM for uma questão crítica.

Uma coisa interessante que podemos fazer é comparar a ANSSI (FR) e a NSA (USA). Diferentemente da NSA, a ANSSI segue o máximo que pode a filosofia KISS/simplicidade (obs: em relação ao funcionamento e não a facilidade) e o minimalismo (não ficam desenvolvendo bloats complexos e não se envolvem com a RedHat).

Já o OpenBSD adota o minimalismo (ele possui somente 3 mi Lines Of Code (LOC), algumas centenas de syscalls, uma única Stack TCP/IP para drivers wireless e etc) e o KISS (Keep It Simple Stupid - exemplo: desenvolveram o `doas` para substituir o `sudo`).

| Distribuição | Número de syscalls |
|---           |---                 |
| Linux        | 22 mi              |
| FreeBSD      | ~550               |
| NetBSD       | ~480               |
| OpenBSD      | ~330 syscalls      |

Quando falamos de kernel Linux vs kernel OpenBSD (sem falarmos muito da userland) então basicamente a discussão irá girar em torno de:
- seccomp × pledge + unveil
- sys_prctl (LSM Hooks)
- Wireless stack
- reallocarray, strlcpy, entre outras funções da libbsd
- PaX/GrSecurity × Retguard + W^X

Em ordem para ter uma segurança minimamente decente, no seu sistema operacional você deve configurar:
- MAC (Mandatory Access Control): AppArmor ou Tomoyo (eu prefiro este último) por exemplo
- FDE (Full-Disk Encryption): LUKS, plain dm-crypt ou truecrypt para GNU/Linux e softraid para OpenBSD
- Hookings (interceptar syscalls): aqui a imaginação é o limite, criar um MagicUID (usuário acima do root) pode ser uma boa ideia, virtualização com ptrace (ou simplesmente desabilitá-la), etc
- PaX/GrSec: nesse caso seria para Linux, para as medidas de segurança que falei acima (NX/XN, SSP, PIE, RELRO, ASLR, etc)
- Um bom IPTables/ipfw configurado (com Port Knocking e etc)
- E aqui começaria a ir mais para alto nível (P2P, browser, qual userland utilizar, compilador, libc, init system, etc)

E a seguir, uma lista das criptografias dos mensageiros:
- AES-IGE, AtE + DH/RSA: MTPRoto (Telegram)
- AES-OTR(CTR), EtA + DRA/Curve25519: Axolotl (Signal)
- AES-GCM + DH/P521: Wickr
- XSalsa20, Poly1305-AES + DH/Curve25519: Threema

De longe, o melhor sendo o Axolotl do Signal (com a única desvantagem de expor seu número). Também já fiz [críticas ao Telegram](#os-problemas-do-telegram).

Fontes:
- [GitHub | Overview of CLIP OS 4](https://github.com/CLIPOS-Archive/clipos4_doc)
- [ClipOS' Docs | Kernel](https://docs.clip-os.org/clipos/kernel.html)
- [GitHub | libecc](https://githubmemory.com/repo/Fretory/libecc)