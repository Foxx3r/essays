# Por que não usar o Windows?
O Ruindows não se preocupa muito com a segurança, só dar uma olhada na implementação do ASLR (Address Space Layout Randomization) que é literalmente uma pseudo-randomização. Ele randomiza somente o que quer e não randomiza o que não quer. Por que? Ninguém sabe, é assim e você não pode fazer nada sobre isso. DEP (Data Execution Prevention)? Muito mal implementado. O NX/XN do GNU/Linux e o W^X do BSD são muito superiores.

O ASLR continua do mesmo jeito desde a primeira vez que implementaram. Ainda existem endereços (que são os mesmo desde o Ruindows 7) que não são randomizados. E não é preciso pesquisar muito para ver que basicamente toda implementação do Ruindows é mal feita. Veja uma comparação da GrSecurity do NX/XN e o DEP e verá qual está se preocupando em proteger seus dados.

ASLR é algo básico quando se trata de segurança de um sistema. Sem randomização um simples ret2libc (return-to-libc, a técnica usada nos anos 2000) já é o suficiente para overflows. ffd00000 não é randomizado, você não precisa aplicar nenhuma técnica avançada para explorar alguma falha [overflow do Windows](http://pt.scribd.com/document/326411245/Bypassing-ASLR-in-Windows-10), só utilizar esse endereço.

Hoje em dia, a stack em sistemas operacionais modernos não é executável diretamente, tem o Canary/ProPolice antes do EBP, ASLR randomizando os endereços e por aí vai. E você nem pode processá-los se der merda. Um keylogger foi descoberto em um hardware da HP, posso processá-los? Todas as funções e APIs "obscuras/secretas" do Ruindows que estão documentadas na WikiLeaks, posso processar também? Os arquivos do Snowden? Essas libs "obscuras" aí do Ruindows denunciadas pela WikiLeaks, posso processar por causa disso? Isso aí é claramente uma backdoor e tem até mesma uma lib para manipular.

![](images/windows_lib_wikileaks_1.png)

![](images/windows_lib_wikileaks_2.png)

Stack overflow significa sobrescrever o retorno (registradores RIP/EIP) para fazer o programa tomar outro rumo, ou seja, exploit.
```c
char buf[5]; // tamanho 5, então se passar algum argumento maior que 5 char então temos um overflow
strcpy(buf, argv[1]);
printf("%s\n", buf);
```

Execute esse programa como:
```sh
./code "AAAAAAAAAAAAAAA"
```

E verá que gerou um segmentation fault, isso é por causa que sobrescrevemos o retorno com "A".

Em um sistema da década de 90, você poderia passar 5 "A" como argumento e quaisquer instruções que você quiser depois disso que seria executado pelo sistema, essas instruções eram chamadas de "shellcode", essa é a origem do termo. Foi aí que surgiu algo chamado NX/XN (Never eXecute) conhecido também como DEP (Data Execution Prevention) no Windows e W^X (Write XOR eXecute) nos BSDs que tornava a stack não-executável, ou seja você não conseguiria executar mais nada. Nos anos 2000 surgiu a técnica return-to-libc ou ret2libc, que ao invés de executar instruções diretamente na stack, começaram a apontar endereços de memória como o do /bin/bash ou de alguma função libc, ou seja, com um overflow em algum programa você receberia uma shell. Para evitar ret2libc, surgiu o ASLR que randomizava esses endereços de memória, é aqui que a segurança do Windows parou, já que ele não faz isso.

Já temos Canary/SSP, ret2plt (return-to-plt), GOT overwrite e GOT dereference, stack pivoting, ROP (return-oriented programming), RELRO, PIE entre outros mecanismos que foram se desenvolvendo, mas o Windows não liga para segurança.