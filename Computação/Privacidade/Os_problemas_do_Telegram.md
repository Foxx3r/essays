# Os problemas do Telegram
O Telegram utiliza AES no modo IGE (que é um modo muito pouco utilizado/quase que desconhecido, e a comunidade crypto costuma desconfiar/ter um “pé atrás” diante do desconhecido).

![](images/MTProto.png)

Assim como o modo CBC (Cipher Block Chaining), o IGE faz o uso de IV (Initialization Vector) e pode ter problemas caso o mesmo IV seja utilizado mais de uma vez (assim como no CBC, mas no caso do Telegram isso não acontece) e é um modo maleável (e por causa disso, é vulnerável a CCA, então precisa adotar um AE - Authenticated Encryption). O Telegram no caso escolheu utilizar MtE/AtE (MAC then/and Encrypt) e é criticado pela comunidade por isso.

![](images/IGE_working.png)

Há um [PDF](http://web.archive.org/web/20210707202649/https://cseweb.ucsd.edu/~mihir/papers/oem.pdf) explicando três tipos de AE, vou fazer um pequeno resumo, primeiro vou definir o que significa cada símbolo:
- M = message
- Ke = key encrypt
- ε = encrypt function
- c' = ciphertext
- τ = tag-verification (mac)
- T = mac function
- Km = key mac
- || = append
- c = resultado

Agora vamos ver o que é cada AE (Authenticated Encryption):

EtA/EtM (Encrypt then Auth/Mac, utilizado no IPSec): criptografa a mensagem e depois adiciona o MAC (Message Authentication Code) do ciphertext. Algoritmo:
```c
c' <- ε (Ke, M)
τ' <- T (Km, c')
c <- c' || t'
return c
```

E&A/E&M (Encrypt and Auth/Mac, utilizado no SSH): criptografa a mensagem/plaintext e depois adiciona o MAC da mensagem/plaintext. Algoritmo:
```c
c' <- ε (Ke, M)
τ <- T (Km, M)
c <- c' || t
return c
```

AtE/MtE (Auth/Mac then/and Encrypt, utilizado no SSL/TLS): adiciona o MAC ao plaintext e depois criptografa tudo. Algoritmo:
```c
τ <- T (Km, M)
c <- ε (Ke, M || τ)
return c
```

Sobre contra quais ataques cada um oferece proteção (de forma geral, claro que depende de como for implementado), temos:
- EtA/EtM: CPA, CCA
- E&A/E&M: CPA
- AtE/MtE: Nenhum

Ambos o SSL/TLS e o SSH (que não utilizam EtA/EtM), mesmo com modificações sofreram com vulnerabilidades envolvendo CCA (Chosen Ciphertext Attacks). O EtA/EtM também oferece uma certa proteção contra DoS (Denial of Service), pois você pode simplesmente verificar o MAC e discartar os pacotes não-autentificados (IPSec), ou seja, você descarta pacotes forjados mais rapidamente. Não oferece nenhuma proteção (generalizando) contra CCA (que é a razão principal de se utilizar AE em modos de operação maleáveis como CBC ou IGE).

No caso, o Telegram acabou respondendo às criticas sobre o MtE/AtE (MAC then Encrypt) em sua FAQ, mas não ofereceu nenhuma razão do porque não utilizar EtM/EtA (encrypt then MAC).
>**P: Por que vocês não adotam uma abordagem padrão encrypt then MAC?**<br/>
>Usando encrypt-then-MAC, por exemplo, envolvendo GCM (modo de contador de Galois), permitiria à parte receptora detectar textos cifrados não autorizados ou modificados, eliminando assim a necessidade de descriptografá-los em caso de adulteração.
>
>No MTProto, os clientes e o servidor autenticam as mensagens garantindo que SHA-256 (auth_key_fragment + plaintext + padding) = msg_key e que o texto simples sempre contenha o comprimento da mensagem, server salt, session_id e outros dados não conhecidos por um invasor potencial antes de aceitar qualquer mensagem. Essas verificações de segurança realizadas no cliente antes de qualquer mensagem ser aceita garantem que mensagens inválidas ou adulteradas sejam sempre descartadas com segurança (e silenciosamente).

Basicamente essas são as duas polêmicas envolvendo a MTProto do Telegram:
- AE (Authenticated Encryption): escolheram utilizar AtE (MAC then encrypt) ao invés de EtA (Encrypt then MAC);
- Modo de operação IGE que não é muito auditado/conhecido, por isso a desconfiança por alguns.

[Contest](http://cryptofails.com/post/70546720222/telegrams-cryptanalysis-contest) do Telegram:
>O problema deve estar claro agora: o concurso do Telegram não dá ao adversário poder suficiente. O adversário não obtém textos simples conhecidos, não pode escolher textos simples, não pode escolher textos cifrados, não pode modificar o tráfego de rede ou qualquer coisa como abordamos nas seções anteriores. O concurso mal se encaixa no modelo conhecido de ataque de texto simples (KPA).
>
>Se ninguém ganhar o concurso, isso não significa que o Telegram é seguro. Isso significa que o Telegram pode estar seguro dentro das restrições do concurso. No entanto, existem sistemas extremamente fracos que podem sobreviver a um concurso do tipo Telegram, então se ninguém ganhar o concurso, isso não nos dará mais confiança na segurança do Telegram.

Fontes:
- [On the CCA (in)security of MTProto](https://eprint.iacr.org/2015/1177.pdf)
- [Security analysis of end-to-end encryption in Telegram](https://www.semanticscholar.org/paper/Security-analysis-of-end-to-end-encryption-in-Lee-Choi/93fe3a5e70d64964e775ea77dcfaee218b8e62e1)
- [The Cryptographic Doom Principle](https://moxie.org/2011/12/13/the-cryptographic-doom-principle.html)