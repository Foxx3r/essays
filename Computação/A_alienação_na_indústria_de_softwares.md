# A alienação na indústria de softwares
Para ser desenvolvedor nas fábricas de software, precisa ser alienado, porque é um trabalho maçante e idiota, fazer CRUDs estúpidos e atender as demandas estúpidas dos clientes. Para fazer esse trabalho sem sofrer muito, é preciso estar alienado, e os liberais são o motor da economia.

Justamente porque são alienados, identidades tão frágeis, que se apegam no "clima de equipe", nos "motivacionais", no "basta você querer", pois se eles não se tornassem "positivos", seriam obrigados à confrontar a realidade nua e crua, e só sobraria o suicídio.

Não é que eles só tenham a opção de serem idiotas ou se matarem, mas é que eles não têm a força moral e a competência cognitiva para se darem conta da alternativa realmente valiosa e desejável, eles estão presos em um ciclo acrítico de punições e recompensas, e de constante reestruturação de identidades fragmentadas.

É por isso também que nós estamos vendo gente de pele clara se dizer "negra", porque eles precisam de uma constante renovação de totens para formar identidades parciais nas quais eles projetam o conforto de uma personalidade saudável, a qual eles não têm.

O raciocínio deles está quebrado na continuidade temporal, de tanto que eles ficam se auto-sugestionando para acreditarem no que são forçados à fazer, então eles não conseguem refletir o que estão fazendo, é por isso também que há idiotas que conseguem falar "sou negro" tendo a pele clara, com a maior cara de pau. Eles simplesmente estão acostumados à fazer o quadrado passar pelo círculo, pois eles tiveram de fazer isso a vida toda para serem aceitos nas bolhas nas quais estão se prendendo.

Desse raciocínio fraturado e absurdo vem todo tipo de baboseiras que eu não tenho mais paciência de analisar.

Agora que expus o problema - da forma dura como tive que ser, afinal, a realidade não é e não está sendo bonita -, é fácil ver como este problema pode ser resolvido: consciência de classe.