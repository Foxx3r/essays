# Gentoo: o sistema operacional de Deus
O Gentoo é uma distro Linux aonde você compila tudo, e assim é mais otimizado para o seu hardware. Nas outras distros, os binários são feitos genéricos e isso faz com que sejam mais lentos, ou então eles são feitos para sua arquitetura mas não para o seu processador.

No Gentoo você tem um make.conf onde você coloca todas as flags de compilação dos pacotes. Você pode colocar a opção -j4 no GCC e ele vai compilar em 4 threads, por exemplo. E com todo esse poder de customização, você pode otimizar seus pacotes para compilar rápido, para serem performáticos ou então para serem seguros, expondo configurações dos tipos mais complexos, para todo nível de necessidade.

O Gentoo é uma distro aonde você consegue fazer tudo. Por exemplo, no Arch e no Debian/Ubuntu você não consegue trocar de IS ou de FS sem quebrar o sistema. No Gentoo você pode.

Um grande diferencial do gentoo é as USEFLAGS. Se eu tenho uma USEFLAG global do KDE por exemplo, então todos os pacotes serão baixados com features adicionais para integrar no KDE.

No Gentoo você também pode criar custom kernel, que é um kernel seu modificado e configurado. Você pode, por exemplo, desabilitar o ASLR, que é um recurso em todas as distros que randomiza a stack e isso faz com que haja uma perda de performance, e no gentoo você pode mudar isso. Você também pode tirar os blobs da Intel, os patches pesados da Intel, pode desabilitar o SSP, pode desabilitar o canary, pode desabilitar o perf, qualquer coisa que você imaginar.

No Gentoo, a gente se preocupa bastante com minimalismo (KISS) por causa da segurança, sem perder a performance (porém sendo impossível não sacrificar a performance de vez em quando). Até existe um Gentoo próprio para segurança, o Gentoo hardened.