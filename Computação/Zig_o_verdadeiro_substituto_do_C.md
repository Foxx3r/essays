# Zig: o verdadeiro substituto do C
A seguir, argumento porque acho que Zig pode ser o substituto para C que Rust nunca foi - e talvez (eufemismo) nunca tentou ser.
- Tem um sistema de import melhor que o include do C
- Todo o top-level de Zig é lazy
- Zig tem genéricos
- A maioria dos UBs de C são compile error em Zig
- Zig tem um sistema de build (e também ainda vai ter um gerenciador de pacotes)
- Cross compilation com Zig é mais fácil
- Zig tem irrefutable patterns, e optionable types
- Compilador self hosting
- Gerador de documentação built-in
- Usa um modelo de concorrência inspirado no Go
- Você pode usar Zig como um compilador C
- Sintaxe simples
- Muitas vezes mais rápido que C
- Tempo de compilação rápido
- Interoperabilidade com C
- Você pode trocar a stdlib