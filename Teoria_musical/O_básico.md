# O básico
O ponto mais básico para a gente começar a entender teoria musical são as notas, e aposto que você já as conhece! São elas:
- Dó (C)
- Ré (D)
- Mi (E)
- Fá (F)
- Sol (G)
- Lá (A)
- Si (B)

Essas são as notações universais das notas, chamadas de cifras, que tem vantagens e desvantagens, mas não falaremos disso agora. Bom, o que distingue uma nota, fisicamente, de outra? Basicamente é a sua frequência! A frequência é sempre medida em Hertz, ou seja, o número de oscilações por segundo (por exemplo, se uma bola bate numa parede e em outra e assim infinitamente, 7 vezes por segundo, dizemos que a frequência da bola é 7 Hertz), ou abreviando, Hz, e quanto maior a frequência, mais aguda é a nota. Um som é alto quando ele é agudo, e baixo quando é grave. Porém, note: altura não é volume! Volume altera intensidade. Por exemplo, C tem 261,6 Hz, D tem 293,6 Hz, E tem 329,6 Hz, F tem 349,2 Hz, G tem 392 Hz, A tem 440 Hz e B tem 493,9 Hz (o ouvido humano pode ouvir entre 20 Hz e 20.000 Hz, totalizando 10 oitavas aproximadamente, além disso temos o infrassom e ultrassom). O gráfico da frequência do som é chamado de espectro sonoro (que varia de instrumento para instrumento, assim como as frequências variam ligeiramente em cada instrumento, oitava e sistema de afinação), e eles variam de instrumento para instrumento, e nunca são bonitinhos e regulares como os gráficos senoidais, pois são combinados a partir de várias frequências, dependem da forma de produção do som, da geometria do emissor, da umidade, da temperatura, acústica, sincronia, intensidade e duração do som, etc. Já o timbre é a capacidade subjetiva de distinguir sons.

Posto isso, vamos sair da realidade física e matemática por enquanto e investigar a abstração musical. A distância entre as notas é chamada de intervalo, que é medida por tons. Por exemplo, entre C e D há 1 tom de distância, e entre D e E há também um tom de distância, ou o que equivale a 2 semi-tons (caracterizado pela notação em cifra de #, que é chamado de sustenido). Assim, D## = E. No entanto, também podemos descrever meio-tom abaixo, que é chamado de bemol, e caracterizado pela notação de cifra b. Logo, D# = Eb, e esse fenômeno é chamado de enarmonia. No entanto, a distância entre E e F é de meio-tom. Mas por que isso ocorre? Porque não há sustenido de notas terminadas com "i", ou seja, temos 12 notas (no ocidente) no total: Dó, Dó#, Ré, Ré#, Mi, Fá, Fá#, Sol, Sol#, Lá, Lá# e Si. Entre C e D temos um C#, e esse C# é chamado de nota de passagem.

Sabe as teclas pretas do teclado? Elas são chamadas de acidentes (veremos porquê em breve), e emitem os sons sustenidos. No entanto, o teclado tem vários Dó, Ré, Mi, etc. Quando partimos para o próximo Dó, dizemos que subimos uma oitava (dobrando ou dividindo a frequência do som quando subimos ou descemos uma oitava). A notação C4 quer dizer Dó na 4° oitava.

Por fim, temos os elementos de uma música (em categorias não tão bem-definidas):
- Melodia (sequência de notas tocadas ou cantadas, com modulação (mudança de tonalidade), intervalos, fraseados (organização de notas), contornos, notas, uníssono, arpejo e escalas)
- Harmonia (sobreposição de notas que serve de base para a melodia, com fraseados, dissonâncias (tensão entre elementos musicais), resolução de dissonâncias, progressões de acordes, textura (combinação de sons), tonalidades e campo harmônico)
- Ritmo (organização dos eventos sonoros e silêncios, com durações, pausas, pulsação (frequência das batidas), síncope e acentos)
- Forma (estrutura lógica da música, com seção (partes da música), verso (parte onde melodia e letra mudam), refrão (repetição memorável a cada verso), introdução (seção antes do primeiro verso ou refrão), ponte (ligação de versos ou refrão), solo, interlúdio (seção instrumental que separa duas seções vocálicas), ad-lip (improvisação instrumental ou vocálica), atemporal (seção sem cadéncia rítmica ou métrica), fade-out (conclusão com cessar gradual da música), instrumentação (combinação de instrumentos), groove (interação entre ritmo e harmonia), dinâmica (variação da intensidade), estrofe, finalização (mudança na dinâmica, letra ou progressão harmônica) e cadência (final de uma seção ou música))
- Letra (idêia da música, com temas, perspectivas, cenários, enredos, personagens, figuras de linguagem, rimas, ênfases e estilos literários)
- Vocais (com vocalistas principais, vocalista de apoio, vocalista instrumental, vocalista uníssono, efeitos (falsete, autotune, vibrato, drive, equalização, etc), coros, respiração, afinação, emoção, timbre e articulação (staccato, legato, portato, pizzicato, marcato, tenuto, etc))

Partituras são mais eficientes do que cifras porque, dentre outras coisas, elas também ditam o ritmo da música. Mas como elas são complexas, irão ficar para o futuro.

Podemos também marcar o intervalo de tempo de uma música com a notação de compasso, por exemplo: | C | C | D | E |, dizemos que C durou 2 compassos no arpejo. Um arpejo é quando você toca várias notas uma por uma. Note: guitarristas chamam de arpejo o movimento de dedilhar a guitarra, então não confunda, são coisas totalmente diferentes.

Porém, esse dedilhar nos leva ao próximo nível da teoria musical: escalas. Escalas são a sequência (seguindo alguma regra) de notas. Por exemplo, a escala natural maior tem um padrão tom-tom-semitom-tom-tom-tom-semitom. A sequência de notas que você viu no começo é a escala natural maior de Dó (Cmaj ou CM)! Agora você sabe porque começa com Dó e não com outra, e também porque os bemol/sustenidos são chamados de acidentes (pois eles não estão na escala de Dó maior). Mas por que a escala é chamada de natural? Existem diversas escalas, como a melódica, harmônica, cromática, cigana, grega, árabe, hexafônica,  etc mas a natural é a mais comum e simples (com usos traçam desde Pitágoras), por isso é chamada de natural.

Vamos observar de novo a sequência Dó, Ré, Mi, Fá, Sol, Lá, Si. Repare agora na escala maior: entre Dó e Ré há um tom, entre Ré e Mi há um tom, entre Mi e Fá há meio-tom, entre Fá e Sol há um tom, entre Sol e Lá há um tom, entre Lá e Si na um tom e entre Si e Dó (numa oitava acima) há meio-tom. Agora vamos fazer o mesmo, mas com a escala maior de Fá: Fá, Sol, Lá, Lá#, Dó, Ré, Mi, Fá. O primeiro termo de uma escala é chamado de tônica, enquanto os demais são graus (2° grau, 3° grau, etc), denotados por números romanos: I, II, III, IV, V, VI e VII.

A escala maior natural muitas vezes é chamada simplesmente de escala maior, escala primitiva ou escala diatônica maior (diatônica significa "movimentar-se pela tônica"). Agora vamos partir para a escala menor natural: tom-semitom-tom-tom-semitom-tom-tom. É mais comum nessa escala escrevermos bemol ao invés de sustenido, por exemplo, ao invés de escrever F#, escreveríamos Gb. Vamos escrever a escala menor de Dó: Dó, Ré, Mib, Fá, Sol, Láb, Sib, Dó (uma oitava acima).

A escala cromática nada mais é do que a sequência semitom-semitom-semitom-semitom. Se fizermos uma sequência de D, D#, E e E#, temos uma escala cromática. A aproximação cromática é a técnica de se mover entre as notas por meio de semitons.

O trítono ê uma escala de 3 tons ou 6 semitons.

A escala relativa menor de uma escala maior é a escala menor do 6° grau dessa escala. Por exemplo, a escala relativa menor de Dó maior é a escala menor de Lá (Lá, Si, Dó, Ré, Mi, Fá, Sol), pois Lá está no 6° grau da escala de Dó maior. Observe que a escala relativa menor de Dó maior (ou seja, a escala menor de Lá) tem os mesmos elementos da escala maior de Dó, por isso ela é chamada de relativa.

Agora, iremos falar de acordes! Nada mais é que a junção de várias notas, tocando ao mesmo tempo, com base em uma escala. Essa é a conexão do "dedilhado" com o tópico em questão. Alguns dizem que um acorde surge quando combinamos 2 notas, outros dizem que são 3 (existem apenas algumas exceções de junções de 2 notas que criam sons harmoniosos). Sem entrar na questão, vamos trabalhar aqui com tríades (3 notas no acorde), que são os mais comuns; embora exista também tétrades (4 notas no acorde), pêntades (5 notas no acorde), acordes de sétima e nona e outras extensões de acordes base (por exemplo, Cm7, acorde de Dó menor com a sétima).

Temos, então, o acorde natural, formado por (T + 3° + 5°) da escala (tônica, terceiro e quinto grau da escala). Por exemplo, se pegarmos o acorde natural maior da escala maior de Dó, o acorde seria uma combinação de Dó, Mi e Sol. Quando o terceiro grau (no caso, Mi) é mantido inalterado, chamamos o acorde de acorde natural maior (em cifra, C). Quando o terceiro grau tiver um bemol (no exemplo, um Mib, que é o mesmo que Ré#), o acorde é chamado de acorde menor (em cifra, Cb ou Cm). Se o quinto grau tiver um sustenido (no exemplo, um Mi#, que é o mesmo que Fá), o acorde é chamado de acorde aumentado (em cifra, C#). Se o terceiro e quinto grau tiverem um bemol (no exemplo, Mib + Solb, que é o mesmo que Ré# + Fá#), então o acorde é chamado de acorde diminuto (em cifra, Cbb). Se não houver um 3° grau, é chamado de acorde suspenso (em cifra, Csus). O acorde dominante (C7: Dó dominante) é composto a partir do 5° grau de uma escala em uma tríade com a sétima, formando um tétrade, e o acorde dominante secundário é um dominante temporário, enquanto o acorde subdominante é formado a partir do 4° grau e a subdominante secundária é uma subdominante temporária, e esses são exemplos de funções harmônicas (papel de uma nota em uma progressão harmônica), além da tônica. A extensão de acorde é realizada quando adicionamos a sétima, nona, décima primeira, décima terceira, etc (porém, também é possível construir a partir de números pares), com uma notação como A7(b9), significando acorde dominante de Lá com 9 bemol. Um acorde disfarçado é um acorde similar ao acorde original. As notas alvo são notas da progressão harmônica ou do solo que se pretende destacar, as colocando no começo ou fim. Tons homônimos são tons com a mesma tônica mas com modos (maior e menor) diferentes, enquanto a diferença entre acordes homônimos se dá somente na terça e no modo. Tons vizinhos são tons que possuem afinidade, podendo realizar a modulação. Tons vizinhos diretos são tonalidades que possuem apenas 1 nota de diferença, tons vizinhos indiretos possuem 2 notas de diferença e tons vizinhos próximos são tons próximos em uma escala.

A gente também pode nomear os intervalos com base na sua distância da tônica:
- 1 semitom = segunda menor
- 1 tom = segunda maior
- 1 tom e meio = terça menor
- 2 tons = terça maior
- 2 tons e meio = quarta diminuta
- 3 tons = quarta justa
- 3 tons e meio = quinta diminuta
- 4 tons = quinta justa
- 4 tons e meio = sexta menor
- 5 tons = sexta maior
- 5 tons e meio = sétima menor
- 6 tons = sétima maior
- 6 tons e meio = oitava (daí o nome)

Por exemplo, qual a distância entre Dó e Mi? Uma terça menor. Qual a distância entre Dó e Sol? Uma quinta justa.

Qual a importância destas nomenclaturas? Elas fazem todo o sentido para definir campos harmônicos (também chamado de tonalidade)! Campos harmônicos nada mais são do que um conjunto de acordes. O 1°, 4° e 5° grau da tonalidade maior é sempre maior. O 2°, 3° e 6° grau da tonalidade maior é sempre menor. O 7° grau é diminuto. Se eu toco uma música com os acordes C, F e G, elas pertencem ao campo harmônico C (que é C Dm Em F G Am B⁰). As nomenclaturas de intervalo são interessantes apenas se você quer calcular na mão cada campo harmônico (e assim descobrir o porquê de cada campo harmônico ter essa regra), o qual não abordarei aqui, pois é apenas um detalhe; no entanto, essa nomenclatura aparece frequentemente por aí, então é bom saber.

O círculo das quartas e quintas é um diagrama da relação entre tonalidades e seus acordes.

Se uma nota ou acorde tocado na música está presente no campo harmônico, ele é chamado de tonal. Caso contrário, é chamado de atonal. Lembra da nota de passagem? Geralmente ela é atonal, no entanto, há casos em que ela é tonal.

Geralmente, dado um acorde (tríade ou tétrade), a nota mais grave (chamada de "baixo") é a tônica. Daí podemos fazer a inversão (denotada por /):
- Baixo na terça = 1° inversão
- Baixo na quinta = 2° inversão
- Baixo na sétima = 3° inversão

Cadência interrompida é uma conclusão inesperada, terminando com um acorde fora da progressão harmônica, através da resolução deceptiva (desvio da progressão harmônica). Já a cadência de engano dá a ilusão de que a música ainda não terminou, pois ela conclui com um acorde da progressão harmônica.
