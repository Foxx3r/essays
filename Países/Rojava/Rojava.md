# Rojava
Rojava não é necessariamente anarquista, eles usam um sistema chamado "confederalismo democrático" de acordo com eles, que é uma mistura de ideologias. O patrocínio americano do SDF e a criação de um estado curdo sírio serve às políticas americanas estratégicas por meio do controle de recursos essenciais, um estado sírio enfraquecido e um estágio do qual contém o Irã. A liderança curda síria explorou essa aliança e o caos da guerra para federalizar unilateralmente (sem referendo nacional). Os dois atores precisam um do outro, mas não devemos confundir qual ator detém o poder. A balcanização se alinha com os interesses curdos, mas a ocupação projeta o domínio americano e enriquece sua classe dominante.

Nesse contexto, você pode ver o Hasakah como não uma aberração ou irrelevância. O SDF lutou contra a SAA pelo controle de um portal importante durante o auge de sua luta contra o ISIS. Relatos de limpeza étnica curda remontam a 2015. Assim, os curdos não estavam apenas se livrando do ISIS, eles estavam em busca de território, recursos e mudanças demográficas. A "guerra civil" produziu condições favoráveis ​​para a secessão e os curdos agiram em busca de seus próprios interesses. Como os EUA apoiaram o SDF com poder aéreo, artilharia e pessoal, não haveria revolução sem o patrocínio americano. Em outra ação pouco notada em 2018, os EUA atiraram contra a SAA em Deir Ezzor, enquanto o SAR tentava recuperar seus recursos de petróleo legítimos. 100 mortos da SAA. Essas ações são vistas isoladamente apenas por causa da mistificação da ocupação americana.

Rojava não está organizada com base na luta de classes. É incorreto dizer que seu estado avançará a luta de classes de alguma forma mais do que a Síria. Um ideal igualitário e a busca pelos direitos de gênero não é uma luta de classes em si. A burguesia curda abandonou Rojava para as cidades ocidentais. O restante é um movimento das classes média e proletária. O componente intermediário tem interesse em manter o Estado como agente do capital global. A questão da luta de classes é subsumida na luta pela identidade curda radical. Inclusive, Abdullah Öcallan que é o fundador do PKK, uma das organizações mais influentes em Rojava, uma vez disse:
>O comunismo fez do governo tudo, mas do ser humano nada. Os Estados Unidos representam desenvolvimento.

A Síria é em si mesma um estado progressista, pluralista e secular, um estado historicamente pós-colonial que protegeu admiravelmente os direitos das minorias. É claro que no Ocidente nunca ouvimos falar dos sucessos da Síria. A Síria é inimiga e implacavelmente demonizada, enquanto a cobertura de Rojava como a "melhor esperança" no Oriente Médio é barulhenta e incessante. Uma campanha de propagandização bem-sucedida alimentada e explorada pela liderança política curda e por nossa classe dominante em busca de seus interesses no Oriente Médio.

Não posso dizer se os curdos tinham opções melhores do que a aliança, mas o resultado é claro. O estado curdo foi conquistado à custa da integridade territorial da Síria e nasceu de uma exploração oportunista do caos de guerra. Rojava, através da ocupação dos EUA agora controla 30% dos campos de grãos e 95% dos campos de petróleo. Os Estados Unidos intermediaram as vendas de petróleo que renderam à Rojava expressivos US$10 milhões por mês. É do interesse dos EUA que um estado curdo forneça para seu povo, enquanto a Síria for privada de recursos e lucros essenciais pelos quais poderia se recuperar. Como ainda é política oficial que "Assad deve ir", alia-se uma economia débil incapaz de se recuperar. O povo sírio enfrenta longas filas de combustível e pão e os lucros de Rojava. Esses campos pertencem ao SAR, apreendidos durante a guerra e controlados por ocupantes e clientes americanos.

Para desmistificar Rojava, temos que entender a questão curda. Samir Amin argumentou que os curdos são uma nação contestável. A linguagem é dialeticamente distinta por região. As classes burguesas adotam as línguas do Estado hospedeiro. Os curdos persas falam farsi. Os curdos atuaram como agentes turcos no genocídio assírio e dobraram seu território na apreensão dessas terras. Assírios transferidos para o norte da Síria após o genocídio, vivem em contato próximo com seus agressores históricos. Eles são particularmente amargos e desconfiados do etnonacionalismo expansionista curdo. Nesta guerra, milícias curdas apreenderam casas e propriedades desocupadas, interferiram no currículo escolar e estão implicadas na tentativa de assassinato de um líder político siríaco.

Na Síria, o nacionalismo curdo é um nacionalismo retrógrado. Os curdos representam 7% a 10% da população. Os curdos são uma minoria étnica na Síria, mas nada impede de eles conviverem harmoniosamentecom outras etnias no país, como acontece com os Uighurs na China, tanto é que nem todo curdo é defensor do etno-estado que Rojava pretende construir, assim como existem judeus que são contra o Estado genocida de Israel. Mais importante, o nacionalismo curdo sírio não é uma luta anti-imperialista anticolonial. Ele falha o princípio mais básico do ML ainda, estamos tão confusos com esta situação complexa que vacilamos em relação a Rojava e vacilamos em nosso apoio à Síria, que teve sua soberania barbaramente violada. A Síria é a agredida, não o agressor, não nos esqueçamos disso. A situação atual é uma ocupação americana, provavelmente indefinida, de 30% da Síria. Rojava é um instrumento dessa ocupação. Restam cerca de 4.000 soldados, dez bases e PMC não contabilizados e pessoal de apoio. Dada a primazia do FP para a contenção iraniana, os planejadores militares americanos falaram de um 'Sunnistão' abrangendo o território curdo do Iraque e da Síria. É por meio desses territórios que a América pode conter o Irã, bem como lançar esforços desestabilizadores. É incrivelmente ingênuo acreditar que Rojava exercerá controle sobre as bases americanas, pessoal ou ações anti-Irã. Quanto mais nos distraímos com a discussão do sucesso ou fracasso da revolução em seus próprios méritos, mais mistificamos a agressão e ocupação imperialistas. 

Fontes:
- [Middle East Forum | Abdullah Öcalan: "We Are Fighting Turks Everywhere"](https://www.meforum.org/399/abdullah-ocalan-we-are-fighting-turks-everywhere)
- [Assyrian Confederation Of Europe | Assyrians Under Kurdish Rule: The Situation in Northeastern Syria](https://fanack.com/wp-pdf-reader.php?pdf_src=/wp-content/uploads/ace201701.pdf)
- [Mint Press News | U.S. Coalition Cleansing Raqqa Of Arabs To Expand Kurdish “Autonomous Region”](https://www.mintpressnews.com/u-s-coalition-cleansing-raqqa-of-arabs-to-expand-kurdish-autonomous-region/229054/)
- [Syria Comment | Romancing Rojava: Rhetoric vs. Reality](https://www.joshualandis.com/blog/romancing-rojava-rhetoric-vs-reality/)
- [Voltaire Network | The Kurds: Washington’s Weapon Of Mass Destabilization In The Middle East](https://www.voltairenet.org/article197437.html)
- [Alkarama | SYRIA: 15-YEAR-OLD GIRL ABDUCTED AND FORCIBLY RECRUITED BY THE KURDISH PEOPLE'S PROTECTION UNITS IN DECEMBER 2014 STILL MISSING](https://www.alkarama.org/en/articles/syria-15-year-old-girl-abducted-and-forcibly-recruited-kurdish-peoples-protection-units)
- [Monthly Review | How the U.S. occupied the 30% of Syria containing most of its oil, water and gas](https://mronline.org/2018/04/18/how-the-u-s-occupied-the-30-of-syria-containing-most-of-its-oil-water-and-gas/)
- [Middle East Monitor | Israel businessman working with army has his eye on Syria oil](https://www.middleeastmonitor.com/20190717-israel-businessman-working-with-army-has-his-eye-on-syria-oil/)
- [Gowans Blog | The (Largely Unrecognized) US Occupation of Syria](https://gowans.blog/2018/03/11/the-largely-unrecognized-us-occupation-of-syria/)
- [The Grayzone | All About Syria](https://thegrayzone.com/tag/kurds/)
- [France 24 | In Syria's breadbasket, Kurds and regime battle for wheat](https://www.france24.com/en/20190611-syrias-breadbasket-kurds-regime-battle-wheat)
