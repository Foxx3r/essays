# Direitos das mulheres na Alemanha Oriental
A RDA teve um histórico notavelmente forte na proteção dos direitos das mulheres, muito mais forte do que o Ocidente capitalista. De acordo com a Divisão de Pesquisa Federal dos EUA:
>O histórico da Alemanha Oriental na área dos direitos das mulheres tem sido bom. As mulheres estão bem representadas no mercado de trabalho, compreendendo cerca de metade da população economicamente ativa. Em 1984, aproximadamente 80% das mulheres em idade produtiva (entre dezoito e sessenta) estavam empregadas. O estado encorajou as mulheres a procurar trabalho e carreiras e forneceu ajuda às mães que trabalham na forma de centros generosos de benefícios de maternidade.

O acesso das mulheres à educação era muito forte na RDA, novamente muito mais forte do que no Ocidente capitalista:
>O estado também tem feito um esforço concentrado para oferecer oportunidades educacionais para mulheres. O número de mulheres com formação universitária ou técnica tem aumentado ao longo dos anos. Dos alunos matriculados em universidades e faculdades em 1985, cerca de 50% eram mulheres.

O controle da natalidade estava amplamente disponível e gratuito, e o aborto estava disponível mediante solicitação da mulher. Relatórios da Divisão de Pesquisa Federal dos EUA:
>Uma lei liberal de aborto, promulgada em 1972 em meio a protestos de círculos religiosos, permite o aborto a pedido da mãe. Em meados da década de 1980, informações sobre métodos anticoncepcionais estavam disponíveis ao público e as mulheres podiam obter pílulas anticoncepcionais gratuitamente.

Além disso, o estado buscou prestar assistência às mães que trabalham por meio de um sistema de creche altamente desenvolvido:
>Uma elaborada rede de creches cuida da criança enquanto a mãe está trabalhando. Em 1984, havia 6.605 creches durante todo o ano, com capacidade para 296.653 crianças. Essas creches cuidavam de 63% das crianças elegíveis.

No geral, a situação das mulheres na Alemanha Oriental era muito melhor do que na Alemanha Ocidental, e o histórico de direitos das mulheres da RDA era bastante impressionante.
