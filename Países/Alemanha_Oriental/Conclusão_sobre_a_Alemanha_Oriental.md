# Conclusão sobre a Alemanha Oriental
A República Democrática Alemã não era uma sociedade perfeita e não é sensato fingir que sim; no entanto, proporcionou um alto padrão de vida para seu povo, juntamente com uma forte segurança econômica e social. Garantia de emprego, moradia, saúde e educação, além de subsídios para as necessidades básicas; fortes proteções para os direitos das mulheres e crianças; atividades culturais de baixo custo e amplamente disponíveis, como teatros e concertos; esses são benefícios dos quais muitos milhões de pessoas passaram a sentir muita falta nos anos desde a queda da RDA.

Talvez o melhor resumo desse tópico complexo seja dado por Bruni de la Motte, na conclusão de seu livro Stasi Hell or Workers' Paradise:
>A experiência de socialismo da RDA está em marcante contraste com o desmantelamento do Estado de bem-estar e a concomitante privatização desenfreada de todos os aspectos da vida que agora ocorrem na Europa Ocidental, da cultura à saúde e outros serviços essenciais, bem como à negação do valores e a individualização extrema da vida. Vivemos em uma sociedade atomizada, rapidamente se desintegrando, com pouco etos social e sem objetivos de longo prazo. Muitos hoje, especialmente os jovens, vivem sem esperança ou senso de um futuro seguro. O socialismo ainda pode oferecer um antídoto e uma alternativa. E a experiência de países socialistas como a RDA pode fornecer indicações para um caminho a seguir e ajudar a renovar a esperança.

Em nossa era de capitalismo tardio, mudança climática e fascismo ressurgente, essa mensagem é mais relevante do que nunca.

Fontes:
- [Der Spiegel | Majority of East Germans Defend the GDR](https://www.spiegel.de/international/germany/homesick-for-a-dictatorship-majority-of-eastern-germans-feel-life-better-under-communism-a-634122.html)
- [European Review of Economic History | The Roots of Economic Failure: What Explains East Germany's Falling Behind Between 1945 and 1950?](https://www.deepdyve.com/lp/ou-press/the-roots-of-economic-failure-what-explains-east-germany-s-falling-A6wSaF2DRj?key=OUP)
- [US Federal Research Division | East Germany: A Country Study](https://archive.org/details/eastgermanycount00bura_0)
- [German Historical Institute | The East German Economy: 1945-2010](https://www.cambridge.org/core/books/the-east-german-economy-19452010/C2414E858CB605F6EE9D5AB0A839B24C)
- [Historical Social Research | National Accounts, GDR, 1950-1989](https://www.ssoar.info/ssoar/bitstream/handle/document/28587/ssoar-hsrsupp-2009-no_21-heske-volkswirtschaftliche_gesamtrechnung_ddr_1950-1989.pdf)
- [University of Bremen | Economic Growth in East and West Germany](http://www.memo.uni-bremen.de/docs/m3309.pdf)
- [Health Care Financing Review | Reform of Health Care in Germany](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4193657/)
- [The Guardian | East Germans Lost Much in 1989](https://www.theguardian.com/commentisfree/2009/nov/08/1989-berlin-wall)
- [The Guardian | German Reunification 25 Years On](https://www.theguardian.com/world/2015/oct/02/german-reunification-25-years-on-how-different-are-east-and-west-really)
- [Artery Publications | Stasi Hell or Workers' Paradise? Socialism in the GDR - What Can We Learn From It?](https://archive.org/details/StasiHellOrWorkersParadise)
