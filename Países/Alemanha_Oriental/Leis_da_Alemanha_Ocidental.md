# Leis da Alemanha Ocidental
A seguir, leis bizarras da "democrática" Alemanha Ocidental:
- Até 1958, um homem podia cancelar o contrato de sua esposa com seu empregador - sem a intervenção dela.
- Até 1977, uma mulher precisava da permissão do marido para trabalhar.
- Até 1957, alguns estados federais tinham leis que determinavam que as professoras deveriam ser demitidas ao se casar.
- Até 1958, os maridos legalmente tinham a palavra final em qualquer coisa em seu casamento/família, não importando se se tratava de sua esposa ou filhos. Além disso, as mulheres deveriam ter permissão de seus maridos/pais para obterem a carteira de habilitação.
- Até 1962, as mulheres casadas não podiam abrir uma conta bancária sem a permissão do marido.
- Somente em 1969 as mulheres casadas foram consideradas "geschäftsfähig", que é capaz de fazer negócios em seu próprio nome e responsabilidade.
- O estupro dentro do casamento foi criminalizado em 1997. Muitos representantes de partidos "conservadores" votaram contra, incluindo pessoas como Erika Steinbach, que fingem estar preocupadas com o fato de as mulheres serem vítimas de imigrantes.
