# A Líbia
A Líbia foi um país extremamente próspero na África, até ser destruída por forças da OTAN. Hoje, a mesma restaurou até a [escravidão](https://time.com/longform/african-slave-trade/). Vamos análisar as conquistas deste enorme país hoje:
- Grandes impostos e taxas de juros proibidas por lei
- Moradia era considerada um direito humano OBRIGATÓRIO
- Mortalidade infantil em 1969: 146 mil. Mortalidade infantil em 2008: 16,6 mil. Tinha a menor mortalidade infantil de toda a África
- Seu Banco Central era 100% estatal
- Cada líbia que dava a luz recebia 5 mil dólares do governo
- Recém casados recebiam 50 mil dólares do governo para comprar um apartamento
- Bancos estatais concediam empréstimos sem juros para seus cidadãos
- Líbios sem emprego e com formação profissional recebiam salário médio do Estado até conseguirem uma vaga
- Gaddafi nacionalizou o petróleo líbio e a parte de toda a venda do hidrocarboneto era creditada na conta dos cidadãos
- Saúde e educação 100% gratuitas e de qualidade
- 40 pães custavam 0,15 centavos
- Famílias rurais recebiam 10 hectares de terra, 1 trator, habitação, gados, sementes e irrigação
- Construiu o maior e mais caro projeto de irrigação da história, o projeto GMMR (Great Man-Made River) de 33 bilhões de dólares. O GMMR oferece água potável para 70% da população líbia
- Água e eletricidade totalmente gratuitos
- Intercâmbio pago pelo Estado
- O Estado pagava 50% do valor do seu carro, 65% se fosse militar
- O PIB líbio crescia a taxas chinesas: 10,6% em 2010
- Medicamentos pagos pelo Estado
- PIB 1990: US$28,9 bilhões. PIB 2008: US$87,14 bilhões
- Antes de Gaddafi, apenas 25% dos líbios eram alfabetizados. Saltou para 87% durante seu governo
- Gasolina custava 0.14 centavos o dinar líbio
- Maior expectativa de vida da África: 77 anos
- Maior IDH da África
- Classificado como um país desenvolvido em 2004

Existe um documentário que entrevista as pessoas que se envolveram na Primavera Árabe contra o Gaddafi e a maioria diz que se arrepende profundamente, e que se soubessem o que viria depois, teriam lutado ao lado dele.

Hoje em dia, a Líbia tem até mesmo [escravidão](https://noticias.uol.com.br/internacional/ultimas-noticias/2017/11/25/africanos-sao-torturados-e-vendidos-na-libia-o.htm) em massa.

