# Pandemia
Há um [artigo](https://www.tandfonline.com/doi/full/10.1080/25751654.2021.1906592) retratando as grandes medidas precoces da Coreia do Norte frente à pandemia. O dr. Li Yi [comenta](https://youtube.com/watch?v=Ig8zjMxzBZ4) sobre o caso de sucesso da Coreia do Norte:
>A Coreia do Norte é a número 1 no combate a pandemia (...) Esse sistema pode fazer isso, o nosso é bom, mas não tão bom quanto o norte-coreano.

Lembreme-nos também dos manifestantes financiados pelos EUA para jogarem balões com propaganda anti-RPDC e [contaminados de Covid-19](http://english.hani.co.kr/arti/english_edition/e_northkorea/949419.html) na Coreia do Norte.
