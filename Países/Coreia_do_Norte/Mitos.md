# Mitos
Há muito o que percorrer quando se trata de nos desprogramar das narrativas da mídia liberal/ocidental. Em que ponto reconhecemos que estamos sendo alimentados por propaganda? Quer seja um governo repressivo em comparação com o Ocidente ou não (não é, pelo que sei), devemos ser capazes de discernir fatos de falsidades sensacionais. Mas não podemos. Porque a mídia divulga artigos de sucesso de forma desonesta. É quase impossível discernir o fato da ficção hoje em dia.

Lista de pessoas na Coreia do Norte que morreram e "voltaram a vida" na Coreia do Norte:
- Hwang Pyong-so
- Choi Ryong-hae
- Ma Won-chun
- Ri Sol-Ju
- [Hyon Song-Wol - 2013, 2014](http://english.chosun.com/site/data/html_dir/2013/08/29/2013082901412.html). [2](https://www.theguardian.com/world/2014/may/17/north-korean-singer-rumoured-executed-appears-tv)
- A banda Moranbong inteira
- A orquestra Unhasu inteira
- [Kim Kyong Hui - 2015, 2017](https://edition.cnn.com/2015/05/11/asia/north-korea-kim-aunt-poisoned/index.html). [2](http://m.yna.co.kr/mob2/en/contents_en.jsp?cid=AEN20170831011000315&site=0300000000&mobile)
- [Ri Yong Gil - fev 2016, mai 2016](https://www.independent.co.uk/news/world/asia/north-korea-kim-jong-un-executes-chief-of-military-on-corruption-charges-a6864571.html). [2](https://www.rt.com/news/342612-north-korea-general-congress/)
- Kim Jong-Un
- Kim Il-Sung
- [Jang Song Taek](https://www.independent.co.uk/news/world/kim-jong-uns-executed-uncle-jang-song-thaek-stripped-naked-fed-to-120-dogs-as-officials-watched-9037109.html). [2](https://www.theguardian.com/world/2014/jan/06/story-kim-jong-un-uncle-fed-dogs-made-up) (apesar dele ter sido baleado, circulou diversos mitos de morte alternativa pelo Ocidente)
- [Ri Su Yong - 2013](https://www.lavanguardia.com/internacional/20131211/54396050491/corea-del-norte-fusila-al-extesorero-de-kim-jong-il.html)

Se você quiser saber como realmente é morar na RPDC, veja [esta série de 34 vídeos](https://www.youtube.com/playlist?list=PLegd4KP36a0Y775Xl_HI_tvDKB6qoxPrx) feita por estudantes.

O fato é que Kim Jong-Un, seu pai e seu avô não ocupam e não ocuparam os mais altos cargos de poder no governo. O chefe de estado de fato na RPDC é atualmente Choe Ryong Hae, que foi nomeado Presidente do Presidium da Assembleia Popular Suprema da Coreia do Norte, em 11 de abril de 2019, substituindo Kim Yong Nam, que ocupava o cargo desde que foi criado em 1998. A posição atual de Kim Jong-Un é a presidência da comissão de assuntos do estado, tornando-o o líder supremo do país de acordo com a constituição, já que ele ocupou anteriormente o cargo de Presidente da Comissão de Defesa Nacional, que foi alterado em 2016 para refletir melhor a situação em questão com uma melhor organização. Kim Il-Sung ocupou os cargos de primeiro-ministro e presidente do Presidium por um longo tempo, embora tenha se aposentado deste último em 1983, sendo sucedido por Yang Hyong-Sop; isso não é surpreendente, já que Kim Il-Sung era um líder incrivelmente competente e respeitado, especialmente entre seu próprio povo. O fato é que as posições influentes tornaram-se mais descentralizadas a cada geração, à medida que o poder do Estado se torna mais distribuído entre o proletariado e de acordo com a sociedade, de uma forma bastante natural que pode, esperançosamente, levar a um declínio completo do estado no futuro, como outros aspectos, também, declinar, assumindo uma derrota dos poderes capitalistas simultaneamente com a existência da RPDC.

Supostamente, Kim Il-Sung sugeriu que Kim Jong-Il o sucedesse, porque Jong-Il foi instilado com uma ideologia revolucionária por toda a sua vida. Diz-se que o resto do partido e do povo concordaram com isso - por meio de suas discussões usuais, pode-se supor - e assim, Kim Jong-Il sucedeu seu pai em alguns cargos, enquanto outros foram removidos, divididos e redistribuídos, ou transferido. É por isso que não há mais um Presidente da Coreia do Norte: O cargo não era mais necessário, e poderes equivalentes deveriam ser divididos em múltiplas funções.

A decisão de quem é eleito não é feita com a cédula. A votação é feita por meio de discussões abertas antes da votação, comparáveis ​​ao projeto soviético, que também podia ser visto na Líbia em algum grau (antes de ser destruída pelos EUA e colaboracionistas imperialistas). Depois de alcançado um consenso comum ao longo destes períodos de discussão, o voto é lançado como uma confirmação desta seleção, daí a falta de votos negativos para os candidatos; o centralismo democrático requer unidade, afinal.

Recentemente, o assunto sobre o caso Otto Warmbier voltou à tona, após a notícia de que [um juiz ordenou que a RPDC pagasse à família Warmbier 500 milhões de dólares](https://www.washingtonpost.com/education/2018/12/24/judge-orders-north-korea-pay-more-than-million-damages-otto-warmbiers-death/) por homicídio culposo. A RPDC não vai pagar nada ao Warmbier. O dinheiro viria de [um fundo especial que os EUA geram de países que designam como patrocinadores do terror](https://www.nytimes.com/2018/04/26/us/politics/otto-warmbier-lawsuit-north-korea.html).

Otto fez uma viagem para a RPDC [com outras 10 pessoas](https://www.nytimes.com/2016/01/23/world/asia/north-korea-american-arrested.html), enquanto se hospedava no famoso [Yanggakdo International Hotel](https://en.wikipedia.org/wiki/Yanggakdo_International_Hotel), Otto tentou roubar um pôster que [invocava o povo da RPDC a "se armarem com um socialismo forte."](https://www.independent.co.uk/news/world/asia/north-korea-video-apparently-shows-us-student-otto-warmbier-taking-propaganda-banner-a6938091.html) Claro, isso não foi tudo o que ele fez, [ele também invadiu zonas não-civis](https://www.independent.co.uk/news/world/asia/north-korea-video-apparently-shows-us-student-otto-warmbier-taking-propaganda-banner-a6938091.html), particularmente o 5º andar, fora dos limites para todos, exceto para tarefas domésticas. Obviamente, por causa de sua nacionalidade, de suas travessuras obscuras e da natureza do conflito entre os EUA e a RPDC, a RPDC reagiu como deveria. Agora, de acordo com o que nos foi ensinado na mídia capitalista, deveria haver guardas armados arrastando um Otto chorando para os campos de trabalho.

De acordo com um cara que testemunhou a prisão, ["Dois guardas simplesmente se aproximaram e simplesmente deram um tapinha no ombro de Otto e o levaram embora"](https://www.washingtonpost.com/gdpr-consent/?destination=%2fnews%2fjosh-rogin%2fwp%2f2017%2f06%2f15%2fotto-warmbiers-north-korea-roommate-speaks-out%3f&utm_term=.a76cfd7ea107). Todos os outros deixaram o país com segurança e solidez. Durante a preparação para a reportagem dos eventos, a mídia ocidental fez um foco contínuo em um slogan aleatório que a empresa de turismo havia usado; "Esta é a viagem que seus pais não querem que você faça!" Usado em fontes como o [Telegraph](https://www.telegraph.co.uk/news/2017/06/15/otto-warmbiers-father-accuses-chinese-tour-company-luring-young/), [PBS](https://www.youtube.com/watch?v=pp5qQVoPsOE), [WAPO](https://www.washingtonpost.com/news/grade-point/wp/2016/01/22/what-we-know-about-student-travel-in-north-korea-and-young-pioneer-tours/) e [The Blaze](https://www.theblaze.com/video/gq-chronicles-the-unbelievable-true-story-of-otto-warmbiers-rescue-the-details-are-shocking). Esse foco interessante em uma frase tão alarmante é uma indicação óbvia de como a mídia queria retratar a Coreia do Norte, usando a descrição óbvia de captura de uma entidade não alinhada como uma avaliação sistemática da situação.

Em uma [conferência de imprensa](https://www.youtube.com/watch?v=eiVLUPLcILU), Warmbier leu uma declaração preparada, admitindo o que tinha feito e pedindo perdão à RPDC. De acordo com várias fontes, a declaração foi coagida e forçada a Warmbier, como parecia ser lida, e continha detalhes duvidosos, como o envolvimento da Igreja Metodista da Amizade em Wyoming e a sociedade Z da Universidade de Maryland. Mas principalmente porque outras pessoas que foram presas na RPDC fizeram confissões em vídeo e depois se retrataram depois de deixar o país. WaPo fala sobre [alguns deles](https://www.washingtonpost.com/gdpr-consent/?destination=%2fnews%2fworldviews%2fwp%2f2016%2f02%2f29%2fnorth-koreas-recipe-for-bargaining-detained-westerner-script-tv-cameras%2f%3fnoredirect%3don%26utm_term%3d.909403e7d761&noredirect=on&utm_term=.b686952e1173). O problema com isso são as histórias apresentadas no artigo. Isso inclui John Short, no qual o artigo não dá a mínima para ser forçado, já que "investigações exaustivas" não indicam coerção.

Hyeon Soo Lim, cuja evidência [é apresentada por um homem que não estava lá](https://pbs.twimg.com/media/DvQpOxRXgAECeXX.jpg).

Outro prisioneiro americano que os norte-coreanos aparentemente forçaram a ler uma declaração de confissão foi [Merril Newman](https://www.theguardian.com/world/2013/nov/30/merrill-newman-north-korea-american-confessed). O artigo do WaPo mostra que ele estava sendo forçado a se manifestar e como estava tentando transmitir ao mundo que estava sendo forçado.

O interessante sobre o Sr. Newman é sua história. Newman disse que serviu durante a Guerra da Coréia como conselheiro militar da "unidade Kuwol do 6º Regimento Partidário da Coreia do Norte" e pediu aos guias turísticos do governo que o ajudassem a contatar membros sobreviventes da Associação de Camaradas em Armas Partisan Kuwol... Que é/era um grupo anticomunista ativo na RPDC. Uma ilustração completa desses detalhes interessantes pode ser lida [aqui](https://www.mercurynews.com/2013/11/30/new-details-emerge-in-baffling-case-of-palo-altos-merrill-newman-north-korean-prisoner-of-war/).

Na verdade, esse artigo é muito interessante e cheio de recursos para decifrar o indivíduo que Merrill Newman é:
>Newman ostensivamente aceita a responsabilidade de ajudar um grupo guerrilheiro chamado Kuwol Partisan Regiment - que estava sob o comando da 8240ª Unidade do Exército dos Estados Unidos - atacar e matar soldados norte-coreanos enquanto a guerra civil grassava por toda a península. Mas ele não menciona o nome do grupo.
>
>“Como eu matei tantos civis e soldados do KPA (Exército do Povo Coreano) e destruí objetos estratégicos na RPDC (República Democrática do Povo da Coreia) durante a Guerra da Coréia”, disse Newman no vídeo, lendo em voz alta uma declaração escrita à mão: “Eu cometi atos ofensivos indeléveis contra o governo da RPDC e o povo coreano.”

Um artigo da [Reuters](https://www.reuters.com/article/korea-north-partisans/unforgotten-fighter-of-korean-war-u-s-pensioner-a-pow-at-85-idUSL4N0JF03Z20131130) dá uma visão mais aprofundada do Regimento Kuwol. Claro, por causa da fonte e do assunto que está sendo discutido, o Regimento Kuwol é celebrizado.
>O Regimento Kuwol era apenas um dos muitos grupos de guerrilheiros anticomunistas que estavam sob o comando da 8240ª Unidade do Exército dos EUA, apelidados de "Tigres Brancos".
>
>Os Tigres Brancos coordenaram algumas das missões mais ousadas da Guerra da Coréia, incorporando agentes secretos nas profundezas do território inimigo - às vezes por meses - espionando e interrompendo as operações de guerra da Coréia do Norte, de acordo com histórias documentadas do regimento.
>
>A unidade, cuja existência foi classificada até o início de 1990, foi a antecessora das forças especiais dos EUA. Os membros dos Tigres Brancos foram escolhidos a dedo no Exército dos EUA e não foram informados sobre sua missão até chegarem a Seul.
>
>Kim Hyeon, um membro do Regimento Kuwol que manteve contato com Newman e visitou sua família na Califórnia em 2004, estava em um barco no interior do território norte-coreano em uma tarde de verão de 1953, poucas semanas antes de um cessar-fogo ser acordado. “À 1 hora de 15 de julho, os guerrilheiros usaram um barco operacional para chegar a 50 metros da costa norte-coreana sob as instruções do tenente Newman”, diz um livro sobre a unidade editado por Kim.
>
>Kim trocou cartas e e-mails com Newman, e eles se tornaram amigos íntimos. Mas se ele fosse Newman, disse ele, não teria voltado para a Coreia do Norte.
>
>“Aos olhos dos norte-coreanos, ele teria sido literalmente um espião envolvido em algum tipo de atividade de espionagem... Eu não iria lá (se fosse ele)”, disse Kim, agora com 86 anos, à Reuters.
>
>“Nossos membros estavam trabalhando, lutando e se engajando na espionagem ao lado de Newman porque ele era um conselheiro”, disse ele.

Nesse ponto, a questão desta história fica clara e o artigo do Mercury realmente pergunta o seguinte:
>“Esses bastardos já conheciam Newman antes do fim da guerra”, disse à Reuters Kim Chang-sun, que ainda estava na escola em 1953 quando se juntou ao regimento de guerrilha que Newman ajudou a treinar. “Eles obtiveram a lista de todo o nosso regimento.”
>
>As novas informações sobre o histórico de Newman durante a guerra levantam uma grande questão: **por que o avô de Palo Alto faria uma viagem tão arriscada para a Coreia do Norte, presumindo que as autoridades sabiam tudo sobre o passado de Newman?**
>
>Ainda mais intrigante no vídeo foi a alegada admissão de Newman durante esta viagem mais recente de que ele "tinha um plano para encontrar todos os soldados sobreviventes e orar pelas almas dos soldados mortos. Seguindo o itinerário, pedi ao meu guia que me ajudasse a procurar os soldados sobreviventes e suas famílias e descendentes, porque era muito difícil para mim fazer sozinho.”

Em qualquer caso, tudo isso é curioso quando você lê a declaração emitida "em nome da família Warmbier". É interessante que eles falem sobre o "tratamento horrível e tortuoso que recebeu nas mãos dos norte-coreanos". É interessante porque mais tarde eles [recusaram a realização de uma autópsia nele](https://www.smh.com.au/world/otto-warmbiers-family-declines-autopsy-for-us-student-released-by-north-korea-20170621-gwv77z.html).

A realidade é que [os legistas confirmaram que Warmbier não tinha nenhum sinal de tortura sobre ele](https://www.theguardian.com/world/2017/sep/28/otto-warmbier-torture-north-korea-coroner?utm_term=Autofeed&CMP=twt_b-gdnnews#link_time=1506561413). Essa realidade de que Warmbier não foi realmente torturado está em conflito com o que a família Warmbier descreve. A única coisa que ninguém rejeita é que Otto Warmbier entrou na RPDC com seu cérebro totalmente intacto e saiu com uma grande parte de seu cérebro danificado ou morto, devido à falta de oxigênio.

A RPDC disse [que ele tinha botulismo e isso, em coesão com as pílulas para dormir que eles supostamente haviam tomado, é o que o levou a ter uma parada pulmonar](https://www.nbcnews.com/health/health-news/what-killed-otto-warmbier-maybe-infection-or-blood-clot-n774421), o que significa que havia pouco ou nenhum oxigênio indo para seu cérebro, causando a morte do tecido cerebral. Os médicos nos EUA disseram que descartaram o botulismo.

No entanto, outros disseram que [devido ao tempo envolvido, o botulismo não pode ser descartado](https://www.washingtonpost.com/gdpr-consent/?destination=%2fnews%2fgrade-point%2fwp%2f2017%2f06%2f20%2fwhat-happened-to-otto-warmbier-when-the-unthinkable-is-unknowable%2f%3f&utm_term=.7b4c9a15f3e2)

Trump, por sua vez, continuou com o discurso de que Warmbier foi "torturado além da conta".

Acho que é hora de apresentar um exemplo da vida real de como os Estados Unidos tratam os prisioneiros.
>[Sandra Bland](https://anti-imperialism.org/2017/06/26/on-the-death-of-otto-warmbier/) tinha 28 anos quando foi presa por uma infração de trânsito, arrancada de seu carro e levada à força sob custódia, mais tarde encontrada morta em sua cela - um “aparente suicídio”, disseram as autoridades. Para os incautos, este caso pode parecer não relacionado, no entanto, com a morte de Sandra Bland não houve nenhum grande clamor (além de círculos progressistas), e a mídia se curvou para apresentar a narrativa policial de suicídio. A única violação observada foi uma violação não criminal da “política de cortesia” durante sua prisão. Nenhuma acusação foi feita, nenhum policial foi preso. Inconsistências no relatório policial foram reconhecidas quase imediatamente, à medida que a raiva justificada começou a se formar em comunidades negras que já tinham visto tantos assassinatos policiais. No entanto, a mídia hesitou em, a qualquer momento, implicar a polícia em sua morte ou chamá-la de assassinato.

Para concluir, deixarei [os insights](https://www.scmp.com/magazines/post-magazine/long-reads/article/2169308/otto-warmbier-what-happened-north-korean-jail-led) do homem que foi à RPDC para verificar Otto enquanto ele estava no hospital.
>Os norte-coreanos pediram a Flueckiger que assinasse um relatório atestando que Otto fora bem cuidado no hospital. “Eu estaria disposto a falsificar esse relatório se achasse que Otto seria liberado”, disse Flueckiger. “Mas, como se viu,” apesar das instalações mais básicas (a pia do quarto nem funcionava), “ele foi bem cuidado e eu não precisei mentir”.
>
>Otto estava bem nutrido e não tinha escaras, algo que até mesmo hospitais ocidentais lutam para conseguir com pacientes em coma.

Sobre a questão da internet na Coreia do Norte: várias restrições de comunicação estão inclusos nas sanções aplicadas pelo Conselho de Segurança da ONU, está nas resoluções 1718 e 1874 que limita transferências e comunicações entre a Coreia e o mundo.

O líder supremo Kim Jong Un enviou presentes para crianças e idosos em orfanatos e lares de idosos em todo o país na quinta-feira. Os alunos das escolas revolucionárias de Mangyongdae, Kang Pan Sok e Nampho ficaram cheios de profunda emoção e felicidade ao receberem uma variedade de alimentos. O mesmo aconteceu com crianças em orfanatos, orfanatos e escolas primárias e médias para órfãos em todo o país. Os residentes de lares de idosos e de lares para deficientes expressaram seus mais calorosos agradecimentos ao Líder Supremo, que lhes proporcionou lares felizes em lugares pitorescos e constantemente lhes concede favores especiais.

A mídia imperialista está [condenando](https://g1.globo.com/mundo/noticia/2020/06/30/charges-da-esposa-de-kim-jong-un-irritam-coreia-do-norte.ghtml) a Coreia do Norte por se irritar com "charges" que na verdade são montagens da camarada Ri Sol Ju em filmes pornográficos acusando ela de ser a vergonha da Coreia. Já imaginou alguém fazendo uma montagem pornográfica da Hillary Clinton ou Angela Merkel? Será que a manchete seria a mesma? Lembram de quando o Bolsonaro ofendeu a esposa do Macron, presidente da frança, e a imprensa ficou brava defendendo a mulher?

Há mais algumas mentiras:
- [O corte de cabelo](https://www.youtube.com/watch?v=2BO83Ig-E8E)
- [Por que os norte-coreanos reverenciam os Kim? Compreendendo a liderança da Coréia do Norte objetivamente](https://www.visitthedprk.org/north-koreans-revere-kims-understanding-north-korean-leadership-objectively/)
- [Kim Jong Un é um louco? A CIA diz que ele tem racionalidade perfeita](https://www.businessinsider.com/cia-kim-jong-un-rational-actor-north-korea-2017-10)
- [Os desertores da Coreia do Norte para o sul dizem que a grande maioria dos coreanos do norte apóia Kim Jong-Un](https://www.npr.org/sections/thetwo-way/2013/08/30/217186480/defectors-think-most-north-koreans-approve-of-kim-jong-un?t=1562800559287)
- [A RPDC é uma monarquia hereditária? É o contrário de acordo com sua constituição](https://www.kfausa.org/dprk-constitution/)
- [E as pessoas chorando nos funerais de Kim? Razões culturais: Coreia do Sul chorando pela morte de outro presidente](https://www.tumblr.com/privacy/consent?redirect=http%3A%2F%2Ftw-koreanhistory.tumblr.com%2Fpost%2F141712894894%2Fsouth-koreans-mourn-the-death-of-president-roh)
- [Kim Jong-Un tem uma taxa de aprovação de 78%... Na Coreia do Sul](https://time.com/5262898/kim-jong-un-approval-rating/)
- [Kim Jong-Un e RPDC em 2018](https://leftistcritic.wordpress.com/2018/01/20/kim-jong-un-juche-oriented-socialism-and-the-dprk-in-2018/)
- [Desmascarando outros mitos, como o herdeiro de Kim, Kim encontrando unicórnios, pessoas executadas por motivos estúpidos, etc](https://anti-imperialism.org/2014/08/14/western-dprk-propaganda-the-worst-occasionally-hilarious-and-often-racist-lies/)
- [Kim Jong-Un executando sua namorada, enviando drones para a Coreia do Sul, e Kim Jong-Un alimentando cães com pessoas, desmascarado](https://redyouthuk.wordpress.com/2014/08/01/dprk-propaganda-war-a-cavalcade-of-comedy/)
- [Mitos e equívocos sobre a Coreia do Norte, feitos por Debunked, um relato não socialista](https://www.youtube.com/watch?v=OhaHiht50AA&feature=youtu.be)
- [Mentiras de Truman a Trump](https://prolefoods.wordpress.com/2017/12/12/the-hypocrisy-of-western-aggression-against-korea-from-truman-to-trump/)
- [As olimpíadas expuseram todas as mentiras dos EUA na RPDC](https://www.eurasiafuture.com/2018/01/21/olympics-exposed-every-us-lie-north-korea/)
- [Violência sexual doméstica na RPDC? Desmentido!](https://twitter.com/korcounterprop/status/1058043009839710208)
- [Crimes contra a humanidade?](https://www.tandfonline.com/doi/full/10.1080/14672715.2014.863581)
- [Os ocidentais podem usar a internet? Sim](https://www.tandfonline.com/doi/full/10.1080/14672715.2014.863581)
- [Provas de que a maioria das mentiras que ouvimos sobre a RPDC são de fato mentiras](https://rhizzone.net/articles/songbullshit/)
- [O expurgo da Coréia do Norte que não aconteceu](https://blogs.wsj.com/korearealtime/2014/03/07/the-north-korean-purge-that-didnt-happen/?mod=WSJBlog&fref=gc)
- [Wannacry? A RPDC convida os EUA a investigar dentro da RPDC: "Mostre-nos as evidências"](https://www.rt.com/usa/414241-north-korean-wannacry-attack/)
- [Religião na RPDC?](https://stalinsmoustache.org/2015/06/16/religion-in-the-democratic-peoples-republic-of-korea/)
- [Smartphones na RPDC?](https://www.digitaltrends.com/mobile/north-korean-smartphone-jindallae-3/)
- [E quanto ao sistema de castas?](https://www.digitaltrends.com/mobile/north-korean-smartphone-jindallae-3/)
