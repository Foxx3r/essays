# Direitos civis
A RPDC é muitas vezes ridicularizada pelos reacionários e liberais de 'esquerda' que a comparam ao ‘socialismo’ no ‘nacional-socialismo’. No entanto, a RPDC tem uma estrutura democrática avançada e complicada, em todos os níveis, e seus cidadãos têm acesso a um nível de democracia não visto nas chamadas nações "livres" do Ocidente. Investigaremos, brevemente, como funciona seu sistema governamental e de gestão.

Os cidadãos da RPDC gozam de uma ampla gama de direitos, e muitos direitos são relevantes para os estabelecimentos democráticos. Citarei sua constituição para demonstrar estes direitos:

**Artigo 4**
>A soberania da RPDC reside nos trabalhadores, camponeses, intelectuais trabalhadores e todos os demais trabalhadores. Os trabalhadores exercem o poder por meio de seus órgãos representativos - a Assembleia Popular Suprema e as assembleias locais em todos os níveis.

**Artigo 5**
>Todos os órgãos do Estado na RPDC são formados e funcionam com base no princípio do centralismo democrático.

**Artigo 6**
>Os órgãos do poder do Estado em todos os níveis, da Assembleia do Povo do condado ao SPA, são eleitos com base no princípio do sufrágio universal, igual e direto por voto secreto.

**Artigo 7**
>Os deputados aos órgãos do poder do Estado em todos os níveis têm laços estreitos com seus constituintes e são responsáveis ​​por seu trabalho.
>
>Os eleitores podem destituir os deputados que elegeram, se estes não forem de confiança.

**Artigo 64**
>O Estado garante efetivamente os direitos e as liberdades democráticas genuínas, bem como o bem-estar material e cultural dos seus cidadãos.
>
>Na RPDC, os direitos e a liberdade dos cidadãos serão ampliados com a consolidação e o desenvolvimento do sistema social.

**Artigo 65**
>Os cidadãos gozam de direitos iguais em todas as esferas do Estado e das atividades públicas.

**Artigo 67**
>Aos cidadãos é garantida a liberdade de expressão, de imprensa, de reunião, manifestação e associação.
>
>O Estado deve garantir condições para a livre atuação dos partidos políticos democráticos e das organizações sociais.

**Artigo 73**
>Os cidadãos têm direito à educação. Este direito é garantido por um sistema educacional avançado e pelas medidas educacionais promulgadas pelo Estado em benefício da população.
>
>Como pode ser visto, os cidadãos da Coreia do Norte têm sufrágio universal por voto secreto e todos os cidadãos com mais de 17 anos podem concorrer a cargos públicos, independentemente da situação econômica ou lealdade política. Mencionamos o direito à educação, porque isso também é importante para a democracia. A verdadeira democracia só pode ocorrer entre um povo informado, porque se o povo for enganado ou ignorante, não poderá fazer escolhas que melhor representem a si mesmo e a seus desejos.
>
>Portanto, o direito à educação garantido constitucionalmente pela RPDC (inclusive em nível universitário) é um direito democrático importante, que falta na maioria das "democracias" capitalistas que exigem muito dinheiro para pagar uma educação universitária ou que colocam tão pouco financiamento em seu público sistema escolar que não se pode esperar que funcione adequadamente, deixando assim as crianças mais pobres incapazes de acessar a educação no mesmo padrão que as crianças mais ricas com educação privada.

Existem muitos órgãos do poder estatal na RPDC, todos eleitos democraticamente.
