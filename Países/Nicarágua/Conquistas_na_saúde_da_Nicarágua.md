# Conquistas na saúde da Nicarágua
Quando os sandinistas chegaram ao poder, eles herdaram uma nação terrivelmente doente, que havia sido devastada pela ditadura de direita. De acordo com um [artigo](https://journals.sagepub.com/doi/abs/10.1177/089692058901600105) na revista Critical Sociology:
>Quando os sandinistas chegaram ao poder em 1979, enfrentaram uma série de graves problemas de saúde legados pelo antigo regime de Somoza.

Apesar disso, os sandinistas conseguiram obter muitos "ganhos impressionantes na área da saúde", como é demonstrado por um exame das evidências estatísticas. De acordo com um [estudo](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1651882/pdf/amjph00633-0082.pdf) do American Journal of Public Health:
>Antes da revolução nicaraguense de 1979, o acesso aos serviços de saúde era amplamente limitado aos setores abastados da população urbana e à minoria de trabalhadores com cobertura de seguridade social. Tentativas repetidas de reforma pela medicina organizada foram ineficazes. Desde a revolução, ocorreu uma grande expansão nos serviços de saúde.

O acesso aos cuidados de saúde expandiu-se maciçamente e foi desenvolvido um enfoque nos cuidados preventivos. De acordo com o relatório da Oxfam acima mencionado:
>Enquanto se estima que em 1979 pouco mais de um quarto da população poderia obter serviços médicos, em 1982 cerca de 70% dos nicaraguenses tinham acesso regular aos cuidados de saúde. No processo, houve uma mudança radical de cuidados primordialmente curativos e urbanos para uma minoria privilegiada para uma ênfase na prevenção, o que é particularmente notável na área de cuidados materno-infantis.

Essas políticas levaram a melhorias acentuadas nas condições de saúde da população. De acordo com o [estudo](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1651882/pdf/amjph00633-0082.pdf) citado:
>A evolução dos serviços de saúde, atividades de prevenção e educação pode estar relacionada a melhorias rápidas no estado de saúde da população desde 1979. Estima-se que, entre 1978 e 1983, a mortalidade infantil diminuiu de 121 para 80,2 por 1.000 nascidos vivos, expectativa de vida ao nascer passou de 52 para 59 anos. O número de casos notificados de malária diminuiu em 50$, os casos de poliomielite não foram notificados durante dois anos, não foram notificados casos de sarampo no primeiro semestre de 1984 e a maioria das outras doenças evitáveis ​​por imunização foram consideravelmente reduzidas.

O sistema de seguridade social também foi amplamente expandido:
>O sistema de seguridade social está se expandindo rapidamente. Desde 1979, a percentagem da população ativa coberta pela segurança social duplicou, de 16% para 32%. Talvez mais importante, a maioria dos grupos recentemente cobertos trabalha nos setores agrícolas anteriormente negligenciados em partes remotas do país. A cobertura do INSS oferece seguro de aposentadoria e indenização trabalhista, entre outros benefícios não médicos.

Essas são conquistas importantes, que representam uma melhoria substancial na qualidade de vida de centenas de milhares (senão milhões) de pessoas.
