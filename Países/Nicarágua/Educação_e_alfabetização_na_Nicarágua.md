# Educação e alfabetização na Nicarágua
De acordo com um relatório da UNESCO, a campanha de alfabetização sandinista foi um grande sucesso:
>Os resultados da campanha foram algo para se orgulhar. A Nicarágua deu uma contribuição substancial para a experiência mundial na busca de soluções para erradicar o analfabetismo.

A taxa de alfabetização melhorou dramaticamente, de aproximadamente 50% a menos de 13%:
>Em cinco meses, 95.582 brigadistas conseguiram ensinar 406.056 nicaraguenses a ler e escrever em espanhol de forma que a taxa de analfabetismo de 50,35% pudesse ser reduzida em 37,39 pontos percentuais para 12,96%. Na região mais industrializada do Pacífico a taxa de analfabetismo foi reduzida de 28,06 para 7,8%, na região montanhosa central de 66,74% para 20,21% e na região menos desenvolvida do Atlântico o salto foi maior com 78,07% para 25,59%.

A campanha foi um grande benefício para o povo marginalizado da Nicarágua:
>Trouxe evidências tangíveis para os grupos mais marginalizados de nicaraguenses de que a sociedade iria incluí-los e o movimento revolucionário mudaria suas vidas para melhor. A campanha foi concebida como parte de um processo de transformação social que visa a redistribuição de poder e riqueza. A campanha ajudou as pessoas a desenvolver habilidades básicas, conhecimentos e atitudes que conduzam a essa transformação.

Esta é uma das expansões mais substanciais da alfabetização que a América Latina já viu e deve ser reconhecida como um modelo de educação revolucionária.
