# Direitos das mulheres na Iugoslávia
A Iugoslávia do pré-guerra tinha uma visão fortemente reacionária dos direitos das mulheres, que mudou rapidamente no período socialista. De acordo com o Comitê de Helsinque:
>A emancipação das mulheres no Reino da Iugoslávia foi o resultado de esforços individuais, enquanto na Iugoslávia socialista foi o resultado de uma política organizada. Embora existisse um movimento feminista no Reino da Iugoslávia, ele permaneceu à margem da influência social, enquanto o status da mulher era mais bem expresso no Código Civil, em que uma mulher casada não tinha capacidade legal. Essa anacrônica disposição legal foi abolida já em 1946, com o término da vigência do Código Civil.

Os comunistas promoveram ativamente a emancipação das mulheres:
>Compreensivelmente, as mulheres obtiveram o direito de voto, as relações conjugais foram liberalizadas, o ativismo político das mulheres foi promovido através da Frente Feminina Antifascista e outras organizações de massa, e as mulheres assumiram cada vez mais funções sociais e políticas, enquanto as soluções jurídicas em todas as esferas da vida tentou garantir a igualdade de gênero. Um aumento considerável no número de divórcios também pode ser considerado uma expressão da emancipação da mulher.

O acesso educacional das mulheres melhorou dramaticamente:
>Nos cursos de alfabetização realizados durante o período de 1948–1950, até 70% dos participantes eram mulheres, embora a alfabetização e a escolaridade das meninas encontrassem resistência em ambientes conservadores, principalmente por motivos religiosos e patriarcais. De 1921 a 1981, a porcentagem de mulheres analfabetas diminuiu de 60% para 14,7%.

As mulheres foram integradas à economia, da qual anteriormente haviam sido amplamente excluídas:
>Depois de 1945, a proporção da população masculina na população economicamente ativa estava diminuindo, enquanto a taxa da população economicamente ativa na população feminina era relativamente estável e variava de 30,7% a 35,1%. Isso significa que o número absoluto da população feminina economicamente ativa (e, portanto, a parcela da população economicamente ativa) estava aumentando na proporção do aumento da parcela da população feminina na população total. No entanto, este não foi o caso da população masculina. Este é um testemunho muito confiável da emancipação das mulheres em comparação com o período pré-guerra, que ficou especialmente evidente depois de 1961.

Os comunistas, portanto, fizeram uma série de melhorias enormes no status dos direitos das mulheres na Iugoslávia.
