# Invasão do Tibete
O Tibete é uma região a oeste da China que consiste em quase todas as montanhas com pontas brancas que formam a cadeia mais alta do mundo. Situado nas regiões mais altas dessa faixa, Lhasa, sua capital.

Por uma parte importante de sua história, o Tibete esteve sob domínio chinês, passando de imperador a imperador e testemunhando vários níveis de autonomia, até a revolução de Xinhai em 1912. Antes disso, ele experimentou uma pequena [rebelião](https://en.wikipedia.org/wiki/1905_Tibetan_Rebellion) em 1905, depois que missionários cristãos entraram no Tibete em busca de novos convertidos. Esta rebelião foi rapidamente esmagada pelo governante da Dinastia Qing.

Declarou sua independência em 1913, citando vastas diferenças étnicas e culturais. Também incorporou, sem aviso ou testemunha, a província de Xikang ao seu território, desencadeando a guerra sino-tibetana. O oficial do Kuomintang, Tang Kesan, imediatamente procurou tentar encontrar um [fim para os combates](https://books.google.ie/books?id=ak3SQTVS7acC&pg=PA150&dq=tang+kesan&hl=en&ei=VH2uTNivA8P48AbSs8DqBA&sa=X&oi=book_result&ct=result&redir_esc=y#v=onepage&q=tang%20kesan&f=false). Outros funcionários do Kuomintang quebraram imediatamente o cessar-fogo, por medo de motivações políticas dos rivais. E eles rapidamente esmagaram as forças tibetanas.

Nos anos seguintes, os tibetanos [atacaram repetidamente](https://books.google.ie/books?id=Nm5wAAAAMAAJ&q=ma+pu-fang+egypt&dq=ma+pu-fang+egypt&redir_esc=y) as forças do Kuomintang, mas foram derrotados várias vezes. Em 1932, o Tibete tomou a decisão de expandir a guerra para Qinghai contra o líder muçulmano Ma Bufang, razões pelas quais muitos historiadores especularam. Minha opinião pessoal é que eles acreditaram que isso desencadearia a guerra entre budistas e muçulmanos, que sinalizaria a chegada do ser do céu que venceria todos os descrentes. Os tibetanos se expandiram para Yushu e Qinghai e agora superavam em número as forças locais, totalizando cerca de 3.000. Ataques tibetanos repetidos foram repelidos pelas forças locais de Qinghai - embora eles estivessem em menor número, conforme mencionado - já que os tibetanos estavam mal preparados para a guerra e sofreram baixas mais pesadas do que o exército Qinghai.

As forças tibetanas [estupraram e assassinaram muitas freiras, moradores locais e prisioneiros](https://escholarship.org/content/qt1qf0664z/qt1qf0664z.pdf) que puderam encontrar, bem como embarcaram em uma campanha destrutiva de saques e incêndios em aldeias inteiras. Em troca, as forças de Qinghai executaram prisioneiros tibetanos, incendiaram mosteiros e fizeram os civis pagá-los por ajudarem os tibetanos. Um novo reagrupamento dos tibetanos falhou e eles perderam o controle de Xikang e foram empurrados para o outro lado do rio Jinsha. Lá, vários generais tibetanos se renderam, o Dalai Lama na época [os rebaixou](https://books.google.ie/books?id=Q7MKAAAAYAAJ&dq=Dalai+Lama+telegraphed+the+Government+of+India+asking+for+diplomatic+assistance&q=Dalai+Lama+telegraphed+the+Government+of+India+asking+for+help&redir_esc=y). Perder tanto território em tão pouco tempo causou pânico nos tibetanos. Eles imediatamente [pediram ajuda aos britânicos](https://www.jstor.org/stable/24442792), na forma de fundos e armas que os britânicos deram. Os indianos também deram alguns armamentos.

Em 1933, os tibetanos sofreram tantas derrotas históricas que [negociaram uma rendição](https://books.google.ie/books?id=YD0sAQAAIAAJ&dq=ma+bufang+liu+wenhui&q=ma+bufang&redir_esc=y). Após a vitória comunista na guerra civil chinesa, o Partido Comunista da China assinou um acordo denominado acordo de dezessete pontos com os delegados do Dalai Lama, que neste ponto é o atual Dalai Lama que você conhece bem, o que facilitou Medos territoriais chineses e deu autonomia ao Tibete.

O Dalai Lama enviou uma [carta](https://books.google.ie/books?id=WWjMMS7dHc0C&pg=PT197&lpg=PT197&dq=%22The+Tibet+Local+Government+as+well+as+the+ecclesiastic+and+secular+people+unanimously+support+this+agreement,+and+under+the+leadership+of+Chairman+Mao+and+the+Central+People%27s+Government,+will+actively+support+the+People%27s+Liberation+Army+in+Tibet+to+consolidate+national+defence,+drive+out+imperialist+influences+from+Tibet+and+safeguard+the+unification+of+the+territory+and+the+sovereignty+of+the+Motherland.%22&source=bl&ots=3dgKXsZv21&sig=P_Lu9SsN8ZdWg02mtFnPV37JDmw&hl=en&sa=X&ved=2ahUKEwjzm8PB6-XdAhXKJsAKHX07AJsQ6AEwA3oECAcQAQ) concordando com o acordo:
>O Governo Local do Tibete, bem como o povo eclesiástico e secular apoiam unanimemente este acordo, e sob a liderança do Presidente Mao e do Governo Popular Central, apoiarão ativamente o Exército de Libertação Popular no Tibete para consolidar a defesa nacional, expulsar as influências imperialistas de Tibete e salvaguardar a unificação do território e a soberania da Pátria.

O Panchen Lama também concordou.

O governo tibetano no exílio se recusa a reconhecer essa assinatura e alguns de seus membros mais antigos, que eram delegados, nunca assinaram o acordo. Mas a assembleia nacional tibetana pediu ao governo que aceitasse o acordo e [afirmou](http://www.tibet.net/en/index.php?id=183&rmenuid=11) que enviaria sua própria confirmação por rádio mais tarde. Hoje, o governo tibetano no exílio afirma que as autoridades chinesas presentes na reunião forçaram os tibetanos a assinar e usaram selos tibetanos falsificados sempre que os tibetanos se recusaram a assinar.

No entanto, Melvyn Goldstein, que entrevistou pelo menos dois negociadores e o único intérprete (o cunhado do Dalai Lama) do lado tibetano, afirma:
>Os chineses fizeram novos selos para os tibetanos, mas estes eram apenas selos pessoais com o nome de cada delegado gravado neles. Fora isso, não havia selos governamentais forjados. Em sua autobiografia, o Dalai Lama afirma que os delegados tibetanos alegaram que foram forçados "sob coação" a assinar o acordo. Seu sentimento de coação deriva da ameaça geral chinesa de usar a força militar novamente no Tibete Central se um acordo não fosse concluído. No entanto, de acordo com o direito internacional, isso não invalida um acordo. Desde que não haja violência física contra os signatários, o acordo é válido. No entanto, o acordo exige acordo total. O Dalai Lama realmente tinha motivos para rejeitá-lo.

Um negociador tibetano lembrou que de fato existem casos em que os delegados tibetanos, com a autorização do Dalai Lama, foram livres para sugerir uma alteração. Posteriormente, devido ao seu exílio auto-imposto, o Dalai Lama rejeitou o acordo.

Como sempre, os Estados Unidos, um ardente oponente do comunismo, enfiou a cabeça na situação. Em junho de 1951, de acordo com o memorando divulgado do Departamento de Estado, o irmão mais velho do Dalai Lama, Thubten Jigme Norbu, conhecido como Taktse Rinpoche, [se encontrou](https://books.google.ie/books?id=C_W0AwAAQBAJ&pg=PT227&lpg=PT227&dq=taktse+rinpoche+evan+wilson+robert+linn&source=bl&ots=oUMgWMsBw8&sig=Bn4GyZjSKzo9R7tIhrFnRtNkNrU&hl=en&sa=X&ved=2ahUKEwiT7dGF7-XdAhUrCMAKHQrqBNEQ6AEwAHoECAIQAQ#v=onepage&q=taktse%20rinpoche%20evan%20wilson%20robert%20linn&f=false) com o Cônsul Geral dos Estados Unidos Evan M. Wilson, seu adido Robert H. Linn, dois vice-cônsules, bem como George Patterson (referido como um “missionário”, o que dificilmente foi o motivo da presença deste famoso explorador neste encontro). O assunto da reunião foi “organização da resistência no Tibete e fornecimento de assistência militar e financeira” ao jovem rei de 16 anos.

A análise de documentos recentemente divulgados sobre a fuga do Dalai Lama do Tibete para a Índia revela até que ponto o líder tibetano foi usado pela inteligência americana como ferramenta de propaganda contra o comunismo chinês. Em troca, o líder tibetano pediu ajuda militar para apoiar um movimento de resistência armada contra os chineses. Um que usaria os mosteiros e templos tibetanos como “casas seguras” e parte de uma rede de inteligência e apoio clandestino.

Em 1952, [foi dito](https://books.google.ie/books?id=LQJZBQAAQBAJ&pg=PA246&lpg=PA246&dq=%E2%80%9CWe+believe+that+if+you+should+return+to+China,+your+life+will+be+in+jeopardy%E2%80%A6.they+will+murder+you+the+moment+your+usefulness+to+them+is+over%E2%80%A6if+you+Leave+Tibet+and+if+you+organise+resistance+to+the+Chinese+communists,+we+are+prepared+to+send+you+light+arms+through+India.&source=bl&ots=b50WubNGe3&sig=4eA7CFywcvKNOF7-0SCT1qyk6ao&hl=en&sa=X&ved=2ahUKEwitl42a8uXdAhUFOsAKHTfUCh4Q6AEwAHoECAkQAQ) ao Dalai Lama:
>Acreditamos que se você voltar para a China, sua vida estará em perigo, eles irão matá-lo no momento em que sua utilidade para eles acabar, se você deixar o Tibete e organizar resistência aos comunistas chineses, estamos preparados para enviar armas leves através da Índia.

As operações visavam fortalecer uma série de grupos isolados de resistência tibetana, o que acabou levando à [criação de uma força paramilitar](https://history.state.gov/historicaldocuments/frus1964-68v30/d337) na fronteira com o Nepal com aproximadamente 2.000 homens.

A CIA [não fazia ideia](https://www.cia.gov/readingroom/docs/CIA-RDP82-00457R006300270010-6.pdf) do quanto de tropas havia na invasão do Tibete.

![](images/CIA_tibet_troops.jpg)

Na verdade, eles [exageraram sobre a invasão](https://www.cia.gov/readingroom/docs/CIA-RDP82-00457R008300510002-6.pdf) de propósito.

![](images/CIA_exagerated_Tibet_invasion.jpg)

Relembrando de uma notícia honesta do NYT.

![](images/peiping_bids_tibet_confiscate_riches.jpg)

E o mesmo afirmando que nem tudo era sagrado nos mosteiros tibetanos.

![](images/tibet_smoulders_under_reds_rule_1.jpg)

![](images/tibet_smoulders_under_reds_rule_2.jpg)

A CIA realizou várias operações secretas, incluindo:
- [St. Circus](http://inthesetimes.com/article/656/tibets_gamble), onde rebeldes tibetanos foram treinados em Saipan e no Colorado
- [St. Barnum](http://www.liberation.fr/tribune/1998/08/13/livre-du-viet-nam-a-cuba-l-epopee-clandestine-des-pilotes-de-la-cia-les-ailes-de-l-amerique-frederic_245411), pelo qual os EUA transportaram soldados, equipamento de armas e agentes da CIA para o Tibete
- [St. Bailey](https://www.goodreads.com/book/show/508371.CIA_s_Secret_War_in_Tibet), que envolveu uma campanha secreta de propaganda contra a China

Os grupos que eles treinaram incluíram o [Chushi Gangdruk](https://en.wikipedia.org/wiki/Chushi_Gangdruk), que foi treinado junto com outros em Camp Hale. A CIA também usou tropas tibetanas para lutar pela Índia contra Bangladesh.

Em 1955, o Departamento de Estado dos EUA [relatou](https://case.edu/affil/tibet/tibetanSociety/documents/usstuff.PDF) que usaria algo chamado comitê de alívio do Dilúvio no Tibete, “para golpe de propaganda contra os comunistas chineses e para fortalecer os grupos de resistência tibetana”.

Um [memorando](https://history.state.gov/historicaldocuments/frus1964-68v30/d337) do departamento de estado de 9 de janeiro de 1964 com a linha declarada "revisão das operações tibetanas" foi aprovado onde afirma:
>A atividade tibetana da CIA consiste em ação política, propaganda e atividade paramilitar. O objetivo do programa nesta fase é manter vivo o conceito político de um Tibete autônomo dentro do Tibete e entre as nações estrangeiras, principalmente a Índia, e construir uma capacidade de resistência contra possíveis desenvolvimentos políticos dentro da China comunista. A CIA está apoiando o estabelecimento de Casas do Tibete em cidades secretas, Genebra, Nova York. As casas do Tibete têm como objetivo servir como representação não oficial do Dalai Lama e manter o conceito de uma identidade política separada. A casa do Tibete em Nova York trabalhará em estreita colaboração com apoiadores tibetanos na ONU, particularmente as delegações malaia, irlandesa e tailandesa.

Um [memorando](https://wikileaks.org/plusd/cables/10NEWDELHI290_a.html) da embaixada dos EUA em Nova Deli em 11 de fevereiro de 2010 afirma:
>Aproximadamente 6.000 tibetanos servem agora e mais de 30.000 tibetanos foram treinados, no estabelecimento 22, uma força de fronteira tibetana-nepalesa dentro do exército indiano que supostamente surgiu em 1962 após um levante tibetano fracassado na China. A filiação ao estabelecimento 22 era obrigatória para os alunos tibetanos que se graduassem nas escolas de crianças da aldeia tibetana (TCV) até o final dos anos 1980. Eles lutaram na Operação Meghdoot durante a luta indo-paquistanesa de 1999 em Kargil.

Em outras palavras, a CIA recrutou tibetanos à força para guerras que nada tinham a ver com o Tibete. Em 1973, quando os EUA saudaram ansiosamente a China sob Deng, eles [cancelaram](http://www.nybooks.com/blogs/nyrblog/2013/apr/09/cias-cancelled-war-tibet/) o treinamento paramilitar e encerraram os pagamentos mensais do Dalai Lama.

No livro, "Autobiografia de um monge tibetano", se você ler com atenção, verá que é sempre da classe rica (e isso implica em automaticamente ter escravos). Palden Gyatso fala sobre ter Iaques e manteiga, que eram itens de luxo no antigo Tibete.

![](images/Palden_Gyatso.png)

Grande coisa é que seus livros são revigorantemente honestos... se você souber como lê-los corretamente. Ele engana, mas confirma, essencialmente, o que a China estava dizendo é verdade.

![](images/Palden_Gyatso_truth.jpg)

Os "inquilinos" não têm que realizar trabalhos forçados... mas... ele mais ou menos admite...

![](images/Palden_Gyatso_slave.jpg)

![](images/Dalai_Lama_slaves.jpg)

<em>Nesta foto, obviamente, a esquerda e a direita são peles de crianças, e o do meio pertence a um escravo adulto</em>

![](images/Dalai_Lama_slaves_2.png)

Sabe o que é isso? Peles de adultos e crianças retiradas para as cerimônias budistas de Dalai Lama. Os proprietários de escravos mataram seus escravos e descascaram suas peles. Os proprietários de escravos estão dispostos a fornecer peles humanas ao Dalai, porque acreditam que serão abençoados pelo Dalai de acordo com o budismo tibetano.

![](images/Dalai_Lama_hunger_games.jpg)

Por anos (1950 a 1970), Dalai Lama estava na folha de pagamento da CIA, uma agência que perpetrou assassinatos contra rebeldes, trabalhadores, camponeses, estudantes e outros em países ao redor do mundo. Seu irmão mais velho assumiu um papel ativo em um grupo de frente da CIA. Outro irmão estabeleceu uma operação de Inteligência com a CIA. Que incluiu uma unidade de guerrilha treinada pela CIA cujos recrutas voltaram de paraquedas ao Tibete para fomentar a insurgência. Dalai Lama não era pacifista. Ele apoiou a intervenção militar da OTAN dos EUA no Afeganistão, também o bombardeio de 78 dias da Iugoslávia e a destruição daquele país.

Mao Zedong deu um prazo para o Dalai se desfazer dos escravos e etc, mas Dalai não obedeceu e teve o Tibete invadido. Não foi a China que ateou fogo nos monges, foram eles mesmos, para recuperar o poder e luxo que os monges dispunham no passado.

As autoridades coloniais britânicas do Raj britânico declararam ter uma taxa de escravos de 95-97% entre sua população, punições corporais como quebrar crânios até que os olhos saiam, mãos cortadas e pés removidos eram bastante comuns e as mulheres não podiam usar nada na parte inferior da metade, então seria fácil para os líderes religiosos estuprá-las quando quisessem, o que era tão ruim que a China teve que gastar tudo de suas reservas de moeda estrangeira em antibióticos para tratar DSTs presentes na população feminina.

Esse ponto foi frequentemente descartado como propaganda chinesa. Interessante ver a CIA [admitir](https://www.cia.gov/readingroom/docs/CIA-RDP81-01036R000100030021-5.pdf) isso.

![](images/CIA_tibet_admitting_1.png)

![](images/CIA_tibet_admitting_2.png)

O ENL deu assistência médica gratuita aos habitantes do Tibete e assistência veterinária gratuita aos animais... de acordo com a CIA (1953).

![](images/CIA_on_china-tibet_care.png)

![](images/CIA_on_china-tibet_care_2.png)

![](images/CIA_on_china-tibet_care_3.png)

![](images/CIA_on_china-tibet_care_4.png)

De acordo com o NYT:

![](images/reds_pay_for_rebuilding_monasteries_in_Tibet.jpg)

![](images/reds_pay_for_rebuilding_monasteries_in_Tibet_2.jpg)

Fontes:
- [Wikipedia | CIA Tibetan program](https://en.wikipedia.org/wiki/CIA_Tibetan_program)
- [Liberation School | China, Tibet and U.S.-sponsored counterrevolution](https://liberationschool.org/08-04-01-china-tibet-ussponsored-coun-html/)
- [The New York Review | Tibet: The CIA’s Cancelled War](https://www.nybooks.com/daily/2013/04/09/cias-cancelled-war-tibet/)
