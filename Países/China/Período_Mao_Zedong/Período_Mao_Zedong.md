# Período Mao Zedong
A maioria das pessoas (incluindo muitos que se dizem socialistas) têm uma impressão muito enganosa de Mao Zedong. Eles tendem a confiar nos mitos e ficções burgueses como suas únicas fontes de informação sobre ele e, portanto, carecem de uma compreensão adequada de suas imensas realizações (que são ignoradas), bem como de suas falhas (que são exageradas e descaracterizadas).

Como a ideologia de Mao continua a ser a força motriz por trás do setor mais ativo e revolucionário do movimento comunista internacional (como demonstrado pelos naxalitas na Índia, o NPA nas Filipinas e muitos outros), é importante que tenhamos um entendimento correto das políticas maoístas e dos imensos ganhos que elas trouxeram para o povo chinês.
