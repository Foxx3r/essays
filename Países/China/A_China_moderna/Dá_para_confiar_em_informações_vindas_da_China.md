# Dá para confiar nas informações vindas da China?
Algumas pessoas, tomadas pelo preconceito, ainda insistem em perguntar: "dá para acreditar nas informações que vêm da China?". Eu sempre respondo com outras perguntas: "dá para acreditar nas informações que vêm dos EUA?"; "dá para acreditar nas informações que vêm da Europa?"; "dá para acreditar nas informações que vêm do Brasil?"
 
Há um equívoco monumental, ideológico, em associar capitalismo à verdade e comunismo à mentira. A brasileirada periférica no mundo - que durante uma certa "década" sentiu o gosto de ser protagonista - permanece escravizada em concepções "ocidentalizantes". 
 
Quem mais mentiu na história da humanidade atende pelo nome de Estados Unidos da América. São o único país cujo governo pôde se dar ao luxo de lançar duas bombas atômicas em cidades japonesas, assassinar mais de 100 mil civis e ainda se dizerem "heróis". Europeus assassinaram milhões de pessoas nas Américas entre os séculos 16 e 18. Escravizaram milhões de negros africanos. Eram outros tempos? Resposta composta: "sim, e você se contenta com essa explicação?". Outra resposta domesticada possível é: a humanidade "é assim mesmo". Chineses mataram, japoneses mataram... Mas esses, a gente sabe (que mataram). 
 
O mundo do sentido das palavras e da produção do discurso não é para amadores. É ele que vem nos esmagando há séculos. Tudo isso só para dizer que, daqui do alto da minha angústia existencial regada à anos de estudos da linguagem humana, eu tenho como óbvio que os enunciados provenientes da China e do governo chinês são, de longe, os mais confiáveis desta cena tétrica de suicídio pandêmico no mundo ocidental.

A China avisou o mundo com todas as informações de que dispunha sobre a gravidade da epidemia que começava e Wuhan já em janeiro de 2020. Essa mania "necrossuicida" de ocidentais "inteligentes" duvidarem de "comunistas atrasados" nos trouxe essa catástrofe global.

Querem saber que discurso mais se relaciona com a verdade e os fatos do mundo? Tentem olhar para a China - que controlou e controla de maneira ultra-competente a propagação do vírus. Ali, as informações são responsivas, correspondem à responsabilidade do poder público e aos parâmetros minimamente éticos que afiançam o sentido das palavras e a sobrevivência da espécie. Os enunciados produzidos "na China" não parecem estar submetidos exclusivamente à lógica neoliberal do "lucro acima de tudo" nem às alucinações publicitárias de quinta categoria que subscrevem o "texto jornalístico" de um jornal como "o Globo", por exemplo.

Chineses têm problemas, mas nós temos muito mais. Antes que alguém diga que "a mão de obra chinesa" é "barata", torno a devolver: "a brasileira é cara?". A estadunidense é ampla e democrática? (são 50 milhões de pobres nos EUA, com viés de alta).

Detalhe: a China acabou de anunciar a erradicação da pobreza com parâmetros estatísticos mais rigorosos do que aqueles postulados pelo Banco Mundial. 
 
E aí, cara pálida? Pensar que o Brasil poderia ter sido um exemplo para o mundo no combate à pandemia se tivéssemos um governo sério e democrático neste momento. Poderia até salvar a "lavoura" da credibilidade mínima do capitalismo - afinal o Brasil democrático não era "comunista". Para os que insistem em desacreditar das informações oficiais do governo chinês, meus pêsames. A tendência é que vocês peguem "COVID brasileira" pelo alto grau de desinformação e preguiça que permeia suas vidas. 
 
Aliás, esse é o ponto: a COVID é muito mais um problema de informação do que sanitário propriamente. A China entendeu isso. O resto do mundo, não.
