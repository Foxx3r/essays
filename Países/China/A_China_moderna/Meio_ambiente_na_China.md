# Meio ambiente na China
A China, ao contrário da crença popular, faz esforços enormes para cuidar do seu meio-ambiente. Por exemplo, [a China realocou 60.000 soldados](https://www.independent.co.uk/news/world/asia/china-tree-plant-soldiers-reassign-climate-change-global-warming-deforestation-a8208836.html) para plantar árvores em uma tentativa de combater a poluição, [está plantando uma floresta do tamanho da Irlanda](https://thespaces.com/chinas-planting-forest-size-ireland/), plantou o [equivalente a 56 anos de desmatamento amazônico](https://www.ecycle.com.br/programa-reflorestamento-florestal-china/) em apenas 4 anos (comparável ao tamanho do estado brasileiro de Alagoas) e reflorestou o [deserto Maowusu](https://www.euronews.com/green/2020/05/17/sand-dunes-turned-into-oasis-in-china). [2](https://www.intelligentliving.co/green-forest-chinas-maowusu-desert).

![](images/china_environment_1.jpg)

<em>Antes</em>

![](images/china_environment_2.jpg)

<em>Depois</em>

![](images/china_environment_3.jpg)

<em>Antes</em>

![](images/china_environment_4.jpg)

<em>Depois</em>

[China reduz em 32% taxa de poluentes no ar e pode elevar expectativa de vida da população](https://noticias.uol.com.br/meio-ambiente/ultimas-noticias/redacao/2018/03/13/china-reduz-em-32-taxa-de-poluentes-no-ar-e-pode-elevar-expectativa-de-vida-da-populacao.htm). [As emissões da China podem atingir o pico 10 anos antes da promessa climática de Paris](https://www.carbonbrief.org/chinas-emissions-could-peak-10-years-earlier-than-paris-climate-pledge).
>Com base em dados coletados em 200 receptores espalhados por toda China, o estudo da Universidade de Chicago calcula que a taxa de partículas finas no ar, muito prejudiciais à saúde, reduziu-se em 32% entre 2013 e 2017. Se essa tendência se mantiver, a expectativa de vida média dos chineses pode aumentar até 2,4 anos, segundo o estudo. As partículas finas - também conhecidas como PM2,5 por seu diâmetro máximo - são determinantes tanto nas doenças cardiovasculares e respiratórias, quanto no câncer.
>
>"Não existem exemplos de um país que tenha conseguido uma redução tão rápida da poluição atmosférica. É extraordinário", disse à AFP Michael Greenstone, que dirigiu o estudo do Instituto de Política Energética da Universidade de Chicago.
>
>Em comparação, os Estados Unidos precisaram de mais de dez anos para uma melhora comparável, após aprovarem uma lei nesse sentido em 1970.

[14º Plano Quinquenal da China é importante para resposta global às mudanças climáticas](http://portuguese.people.com.cn/n3/2020/1029/c309809-9774474.html).

[China exige que carros elétricos representem 30% das compras](https://exame.com/negocios/china-exige-que-carros-eletricos-representem-30-das-compras/).

Todo programa de transição energética necessariamente passa por impactos ambientais. Não existe energia 100% limpa. Por isso, ao mesmo tempo em que a China é quem mais investe em energia alternativa, e também está entre os países que mais emitem CO2.

A capacidade de energia renovável da China é maior do que a dos EUA, Reino Unido e UE combinados. A China reduziu o uso de carvão em 19% desde 2005. 99% dos ônibus elétricos estão na China.
