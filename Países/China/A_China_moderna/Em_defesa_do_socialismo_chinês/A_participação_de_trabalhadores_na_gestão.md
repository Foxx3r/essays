# A participação dos trabalhadores na gestão
Para muitos, "a China é socialista" ainda é uma posição muito controversa. Uma razão para isso é um mal-entendido geral de como a economia chinesa realmente funciona, desde o planejamento de alto nível até a gestão empresarial.

Você pode se perguntar, então, como as coisas funcionam ao contrário. Como os trabalhadores comuns têm sua contribuição na gestão da empresa e como essa contribuição se relaciona com a tomada de decisão central. A resposta é o sistema de congressos dos trabalhadores.

Então, qual é esse sistema? Simplificando, é um corpo representativo dos trabalhadores em cada empresa que delibera e vota nas decisões gerenciais. Aqui está o que a lei sobre empresas estatais tem a dizer sobre isso.
>CAPÍTULO V<br/>
>Funcionários e trabalhadores & funcionários e Congresso dos trabalhadores
>
>Artigo 49.<br/>
>O pessoal e os trabalhadores da empresa têm o direito de participar na sua gestão democrática; o direito de formular opiniões e sugestões sobre sua produção e atuação; direito de usufruir, nos termos da lei, de proteção trabalhista, seguro trabalhista, descanso e férias; e o direito de denunciar a verdadeira situação aos órgãos do Estado e de fazer críticas e denúncias contra os quadros dirigentes da empresa.
>
>As funcionárias e as mulheres têm direito a proteção especial e seguro trabalhista de acordo com as disposições do Conselho de Estado.
>
>Artigo 50<br/>
>O pessoal e os trabalhadores devem encarar seu trabalho como os senhores do país que são, observar a disciplina, as regras e os regulamentos trabalhistas e cumprir suas tarefas na produção e no trabalho.
>
>Artigo 51.<br/>
>O congresso do pessoal e dos trabalhadores deve ser a forma básica para o exercício da gestão democrática na empresa e o órgão para o exercício do poder de gestão democrática por parte do pessoal e dos trabalhadores.
>
>O órgão de trabalho do congresso de funcionários e dos trabalhadores é a comissão sindical da empresa. A comissão sindical da empresa será responsável pelo trabalho quotidiano do pessoal e do congresso dos trabalhadores.

E aqui estão os privilégios dos congressos. Entre eles estão o poder de endosso sobre os reajustes salariais, supervisão da liderança e eleição do diretor da fábrica (com aprovação do governo).
>Artigo 52.<br/>
>O congresso de funcionários e trabalhadores exercerá as seguintes funções e atribuições:
>
>1) ouvir e deliberar sobre os relatórios do diretor da fábrica sobre a política de operação, planos de longo prazo e anuais, programas para construção de capital e grandes transformações técnicas, planos para o treinamento da equipe e trabalhadores, programas para a distribuição e uso dos fundos retidos e programas para sistemas de contrato e arrendamento de responsabilidade gerencial, e para apresentar opiniões e sugestões
>2) examinar e endossar ou rejeitar os programas da empresa para reajuste salarial, programas de distribuição de bônus, medidas de proteção ao trabalho, medidas para prêmios e penalidades e outras regras e regulamentos importantes
>3) deliberar e decidir sobre os programas para o uso do fundo de bem-estar do pessoal e dos trabalhadores, programas para a alocação de pessoal e alojamento dos trabalhadores e outras questões importantes relativas ao bem-estar e benefícios dos funcionários e trabalhadores
>4) avaliar e supervisionar os quadros administrativos dirigentes nos vários níveis da empresa e apresentar sugestões para recompensá-los ou puni-los e para a sua nomeação ou destituição
>5) eleger, por decisão do departamento competente do governo, o diretor da fábrica e reportar a tal departamento para aprovação
>
>Artigo 53.<br/>
>A gestão democrática será praticada em oficinas, através das assembleias gerais do pessoal e dos trabalhadores, dos grupos de funcionários e representantes dos trabalhadores ou de outras formas; os trabalhadores devem participar diretamente na gestão democrática de turnos e grupos.
>
>Artigo 54.<br/>
>O congresso de pessoal e dos trabalhadores deve apoiar o diretor da fábrica no exercício de suas funções e atribuições de acordo com a lei e deve educar o pessoal e os trabalhadores para o cumprimento das obrigações previstas nesta lei.

Os congressos eleitos diretamente se reúnem pelo menos duas vezes por ano. Nesse ínterim, um comitê sindical lida com o trabalho do dia-a-dia.
>Um congresso de trabalhadores é composto por representantes de trabalhadores eleitos diretamente por equipes, grupos ou seções de oficina. São operários, pessoal técnico, pessoal administrativo, quadros dirigentes e outros membros de uma empresa, e estão agrupados em delegações. O congresso dos trabalhadores se reúne pelo menos uma vez em meio ano, e reuniões provisórias podem ser convocadas para tratar de questões importantes que surgiram. O congresso tem cinco categorias de funções e poderes:
>1) Ouvir e revisar relatórios sobre a política operacional de negócios da empresa, plano de longo prazo, plano anual, plano de construção de capital, plano de grande transformação tecnológica, plano de treinamento de trabalhadores, plano para a distribuição e utilização dos fundos retidos e plano para a implementação do sistema de responsabilidade contratual ou arrendamento na operação empresarial. O congresso oferece seus pontos de vista e sugestões sobre tais relatórios. O congresso também examina qualquer plano importante apresentado pelo diretor da empresa em relação ao seu desenvolvimento e faz uma resolução sobre ele;<br/>
>2) Examinar e decidir aprovar ou negar planos para ajustes de salários, distribuição de bônus, medidas de proteção de segurança no trabalho, prêmios e punições e outras regras e regulamentos importantes. O congresso examina os principais planos apresentados pelo diretor que dizem respeito diretamente aos interesses dos trabalhadores e adota resoluções para aprovar ou rejeitar os planos. Qualquer plano negado pelo congresso deve ser revisto com base nas opiniões do congresso e ser novamente submetido ao congresso para deliberação. Quando um plano é aprovado, ele é divulgado e implementado;<br/>
>3) Examinar os planos para o uso de fundos de bem-estar para os trabalhadores, distribuição de moradia e outras questões importantes relativas ao bem-estar dos trabalhadores;<br/>
>4) Avaliar e supervisionar quadros dirigentes em todos os níveis da empresa e propor prêmios, penalidades, promoções, rebaixamentos e remoções relevantes;<br/>
>5) Nomear ou eleger o diretor da empresa. De acordo com a Lei das Empresas, o diretor de uma empresa é nomeado pelo departamento governamental competente ou eleito pelo congresso dos trabalhadores com o resultado da eleição relatado ao departamento governamental competente para aprovação. Para que o congresso operário possa eleger o diretor, quadros competentes e com apoio popular podem assumir cargos de direção, e os trabalhadores também exercem seus direitos de senhores da empresa.
>
>Quando o congresso dos trabalhadores não está reunido, a comissão sindical da empresa, como órgão de trabalho do congresso, conduz o trabalho quotidiano do congresso, de modo a assegurar a implementação regular da gestão democrática. De acordo com os estatutos pertinentes, uma comissão sindical exerce as seguintes funções e atribuições:
>1) Organizar a eleição dos representantes dos trabalhadores;<br/>
>2) Propor a agenda do congresso dos trabalhadores, e preparar e organizar as reuniões do congresso;<br/>
>3) Presidir as reuniões conjuntas dos chefes das delegações (grupos) e dos grupos especiais (comissões especiais);<br/>
>4) Organizar grupos especiais (comissões especiais) para realizar investigações, apresentar propostas ao Congresso dos Trabalhadores, fiscalizar e supervisionar a implementação das resoluções aprovadas pelo Congresso e mobilizar os trabalhadores na implementação das resoluções;<br/>
>5) Educar os trabalhadores na gestão democrática e organizar os representantes dos trabalhadores para estudar as políticas e conhecimentos de gestão relevantes de modo a elevar sua qualidade;<br/>
>6) Aceitar e tratar reclamações e sugestões dos representantes dos trabalhadores e salvaguardar os seus legítimos direitos e interesses;<br/>
>7) Realizar outros trabalhos relativos à gestão democrática da empresa.

Este sistema não é novo. Aqui está um artigo da Revisão de Pequim de 1965 discutindo isso.
>A conferência do pessoal e dos representantes dos trabalhadores é um meio importante de ampliar a democracia e fazer com que as massas de funcionários e trabalhadores participem da gestão e supervisionem o trabalho da administração. O camarada Teng Hsiao-ping disse: “A conferência de funcionários e representantes dos trabalhadores sob a liderança do comitê do Partido Comunista é um bom meio de ampliar a democracia nas empresas, de recrutar trabalhadores e funcionários para participar da gestão e de superar a burocracia. É um método eficaz de lidar corretamente com as contradições entre as pessoas.” A conferência ajuda a integrar a liderança centralizada com a colocação em jogo da iniciativa das massas de funcionários e trabalhadores, assim, simultaneamente, fortalecendo a liderança centralizada de cima para baixo e fornecendo supervisão pelas massas de baixo para cima. Isso resulta na melhoria contínua do trabalho administrativo e na garantia do cumprimento geral dos planos estaduais.
>
>Em empresas industriais estatais, a conferência de funcionários e representantes dos trabalhadores é uma forma importante por meio da qual os funcionários e os trabalhadores participam da gestão. A conferência pode ouvir e discutir o relatório do diretor sobre o trabalho da empresa, examinar e discutir os planos produtivos, financeiros, tecnológicos e salariais, bem como as principais medidas para realizá-los, verificar regularmente a implementação desses planos e apresentar propostas. Pode examinar e discutir o uso de bônus da empresa, bem-estar, assistência médica, proteção ao trabalho e fundos sindicais, bem como outros fundos alocados para a subsistência e bem-estar dos funcionários e trabalhadores. Na condição de que as diretrizes e ordens emitidas por autoridades superiores não sejam violadas, a conferência pode adotar resoluções sobre o dispêndio dos fundos acima e cobrar dos departamentos administrativos ou outros departamentos interessados ​​a sua execução. Pode criticar qualquer um dos dirigentes da empresa e, quando necessário, propor às autoridades administrativas superiores que punam ou demitam dirigentes que negligenciem gravemente as suas funções e se portem mal. Em caso de desacordo com as decisões das autoridades administrativas superiores, a conferência pode apresentar as suas próprias propostas, mas se as autoridades superiores insistirem nas decisões originais após o devido estudo, deve realizá-las em conformidade.
>
>É por isso que a conferência de funcionários e representantes dos trabalhadores é um meio importante de desenvolver a democracia e fazer com que as massas de funcionários e trabalhadores participem da gestão em toda a fábrica. Através desta conferência, os princípios e políticas do Partido podem ser melhor implementados entre as massas, as relações entre os interesses do Estado e os dos funcionários e trabalhadores da empresa em questão podem ser tratadas corretamente, as entre a administração, por um lado e a organização sindical e as massas do pessoal e dos trabalhadores, por outro lado, podem ser corretamente ajustadas; e, ao mesmo tempo, a consciência socialista do pessoal e dos trabalhadores e seu senso de responsabilidade como senhores em sua própria casa podem ser aumentados, a supervisão das massas sobre o trabalho administrativo reforçada e a melhoria da gestão promovida.

E aqui está uma afirmação pós-reforma do sistema. Isto é de um plenário ampliado do Comitê Central em 1980.
>Quinto, congressos ou conferências de representantes de trabalhadores e funcionários administrativos serão introduzidos em todas as empresas e instituições. Isso foi decidido há muito tempo. A questão agora é como popularizar e aperfeiçoar o sistema. Esses congressos ou conferências têm o direito de discutir e tomar decisões sobre questões importantes de interesse de suas respectivas unidades, de propor às organizações superiores a destituição de administradores incompetentes e de introduzir - gradativamente e dentro dos limites apropriados - a prática de eleger seus líderes.

Algumas perguntas surgem depois de ler sobre os fundamentos do sistema. Primeiro, quão prevalentes são os congressos? Uma pesquisa do setor estima que 80% das empresas os possuem - incluindo algumas empresas privadas.
>Tabela 1<br/>
>SWRC na indústria de manufatura, por tipo de propriedade<br/>
>| Propriedade | Configuração (%) | Em processo (%) | Não-configurado ainda (%) | Não se sabe (%) | Número de casos |
>|---         |---               |---              |---                        |---              |---              |
>| Estado | 92 | 1 | 2 | 5 | 1,037 |
>| Coletiva | 71 | 6 | 5 | 18 | 554 |
>| Privado | 42 | 4 | 25 | 29 | 48 |
>| Operação conjunta | 94 | – | 3 | 3 | 70 |
>| Ações | 93 | 2 | 1 | 4 | 187 |
>| Investimento externo | 63 | 1 | 13 | 23 | 152O |
>| Ultramarino | 25 | 3 | 53 | 19 | 89 |
>| Total | 80 | 3 | 6 | 10 | 2,137 |
>
>[...]<br/>
>Tabela 2<br/>
>Distribuição de Trabalhadores na Indústria Transformadora: Comparação entre Amostra e Estatísticas Nacionais
>| Propriedade |Frequência (Pesquisa ACFTU) | Porcentagem (Pesquisa ACFTU) | População (Estatísticas nacionais) | Porcentagem (Estatísticas nacionais) |
>|---         |---                         |---                           |---                                 |---                     |
>| Estado | 1,049 | 48.2 | 30,110,000 | 54.4 |
>| Coletiva | 567 | 26.0 | 12,440,000 | 22.5 |
>| Privado | 50 | 2.3 | 4,506,000 | 8.1 |
>| Operações conjuntas | 70 | 3.2 | – | – |
>| Ações | 188 | 8.6 | – | – |
>| Investimento externo | 161 | 7.4 | – | – |
>| Ultramarinos | 93 | 4.3 | – | – |
>| Grupo total | 512 | 23.5 | 8,267,100 | 14.9 |
>| Total | 2,178 | 100 | 55,323,100 | 100 |
>
>Fontes: China Statistics Yearbook, 1998<br/>
>[...]<br/>
>Deve-se ter em mente que apenas onde há um sindicato local de trabalho é provável que exista um SWRC. Portanto, a densidade de SWRCs na amostra também é super-representada (ver Tabela 1). No entanto, a densidade por tipos de propriedade representa amplamente a tendência geral, com alta densidade em empresas estatais (92%) e baixa em empresas privadas (42%) e empresas estrangeiras financiadas pela China (25%).

Essa porcentagem aproximada é verdadeira hoje.
>É claro que o efeito líquido das leis e decisões que fortalecem os poderes dos SWRCs uma vez que existem, mas que não exigem que existam, pode ter sido o de endurecer a resistência da administração à sua formação. Mas, nos últimos anos, as SWRCs fizeram avanços significativos no setor não-estatal. As províncias de Xangai e Zhejiang, entre outras jurisdições, emitiram regulamentos exigindo que todas as empresas públicas e privadas estabeleçam SWRCs. Nesse ínterim, os oficiais do partido e do sindicato em empresas privadas e estrangeiras têm criado SWRCs (pelo menos no papel). Em março de 2013, cerca de 81 por cento das organizações, públicas e privadas, que tinham um capítulo sindical - ou seja, mais de 4 milhões de empresas - haviam estabelecido um congresso de trabalhadores

Outra questão é se os congressos são ferramentas eficazes de democracia no local de trabalho. Afinal, teoria e prática nem sempre se alinham. A resposta é complicada. A década de 1980 foi considerada uma "época de ouro" para os congressos. Na década de 1990, as coisas estavam... piores.
>Durante o curso da década de 1990, menos empresas estatais da China continuaram a convocar sessões regulares do conselho, à medida que o poder dos gerentes se expandia e mais empresas estatais caíam no vermelho, reduziam os benefícios dos funcionários e reduziam sua força de trabalho. Mas os conselhos continuaram a existir, mesmo que inativamente, em 92% das empresas estatais pesquisadas em 1997, e em algumas partes da China eles permaneceram ativos. Em 1998, na província de Liaoning, mais de 2.300 gerentes de empresas estatais foram demitidos ou rebaixados depois de não obterem os 60 por cento dos votos necessários do conselho de suas empresas. Em algumas empresas onde a gestão é corrupta, os conselhos, nos últimos anos, resolveram o assunto por conta própria, convocando-se para entrar em sessão na tentativa de impedir a venda de ativos da empresa, de bloquear a privatização da empresa ou mesmo de demitir os administradores.<br/>
>[...]<br/>
>Vários representantes ativos aproveitaram a oportunidade para brigar e tentaram convencer os demais deputados a não votarem a favor. A gerência obteve a ajuda do governo local para persuadir, intimidar e coagir os membros do SWRC e, em seguida, a polícia foi enviada para impedi-los de votar. A administração conseguiu o que queria, mas os ativistas buscaram vigorosamente uma compensação maior, acusando a administração de corrupção.

O estudo revela uma mistura de coisas durante esse período. Embora o sistema tenha diminuído, ele não morreu. Muitos congressos firmes (referidos como SWRCs nestes documentos) permaneceram eficazes.
![](https://pbs.twimg.com/media/DnoL1rjUwAAUxs9.jpg)
![](https://pbs.twimg.com/media/DnoL2ttUwAAicj7.jpg)
![](https://pbs.twimg.com/media/DnoL3aYUUAERgEL.jpg)

Muitos gerentes seriam chutados naquela época, se não tivessem a aprovação de seus congressos.
>Houve exceções. Anita Chan e seus colegas relatam que os SWRCs continuaram a influenciar o destino dos diretores e gerentes de fábricas até o final da década de 1990. Em uma província, “mais de 2.300 gerentes [SOE]... foram demitidos ou rebaixados após não obterem os 60 por cento dos votos necessários” de seus SWRCs. Algumas SOEs mantiveram uma cultura mais paternalista, e os sindicatos puderam usar os SWRCs e seus poderes para melhorar a saúde e a segurança no local de trabalho ou para distribuir de forma justa novos benefícios habitacionais. Portanto, embora os SWRCs fossem frequentemente impotentes, os sindicatos ativos às vezes podiam colocar seus poderes formais em uso em nome dos trabalhadores, pelo menos quando a gestão não era antagônica.

Portanto, nesta época mais sombria, ainda havia pontos positivos a serem obtidos. Mas as coisas começaram a mudar para melhor em meados dos anos 2000. As revisões da lei fortaleceram ainda mais os congressos.
![](https://pbs.twimg.com/media/DnoL5pbUYAAO1DV.jpg)
![](https://pbs.twimg.com/media/DnoL6SMVAAAOtvu.jpg)

A Lei de Contratos de 2007, que marcou um retorno parcial ao sistema "tigela de arroz de ferro" (mais sobre isso em outro tópico), fortaleceu mais os congressos.
![](https://pbs.twimg.com/media/DnoL8BGUwAABwYF.jpg)
![](https://pbs.twimg.com/media/DnoL8-eUUAEL1Xo.jpg)

A expansão do sistema de congressos foi encorajada e protegida nas Disposições sobre a Gestão Democrática de Empresas de 2012, elaboradas pela Federação de Sindicatos de Toda a China (ACFTU).

As disposições recomendam fortemente o estabelecimento de congressos de trabalhadores em empresas de todo o país, tanto públicas quanto privadas. Eles também exigem contagens mínimas de membros e percentagens máximas de membros da administração: 20%.
>Artigo 3º: O Congresso dos Empregados (ou Assembleia dos Empregados, as mesmas infra) é o órgão pelo qual os empregados exercem os seus direitos à gestão democrática e à forma básica da gestão democrática das empresas.
>
>As empresas devem, de acordo com os princípios da legalidade, ordem, abertura e imparcialidade, estabelecer um sistema de gestão democrática com o congresso dos funcionários como forma básica, abrir os negócios da empresa e promover uma gestão democrática. As empresas corporativas (doravante denominadas "empresas") devem estabelecer um sistema de diretores e supervisores de funcionários de acordo com a lei.
>
>As empresas devem garantir e respeitar os direitos dos funcionários à informação, participação, expressão e supervisão e outros direitos democráticos de acordo com a lei, e apoiar a participação dos funcionários na gestão da empresa.<br/>
>[...]<br/>
>Seção 1: Sistema de organização e poderes dos congressos de funcionários
>
>Artigo 8º: As empresas podem determinar a convocação de congresso de empregados ou de assembleia de empregados em função do número de empregados. Quando uma empresa convoca o congresso dos empregados, o número de representantes dos empregados deve ser determinado com base em pelo menos 5% do número de todos os empregados e não deve ser inferior a 30. Quando o número de representantes dos empregados é maior de 100, o excesso pode ser determinado pela empresa em consulta com o sindicato.
>
>Artigo 9: Os representantes do congresso dos empregados devem ser operários, técnicos, gerentes e dirigentes da empresa e demais empregados. O número de gerentes de nível médio ou superior e de líderes de uma empresa geralmente não deve exceder 20% do número total de representantes dos empregados. As empresas com empregadas femininas e empregados despachados para trabalho devem ter uma proporção apropriada de representantes de empregados femininos e empregados enviados para trabalho.

Eles também definem as responsabilidades do congresso. Isso é diferente para empresas privadas e estatais; os congressos da SOE têm ainda mais poder.
>Artigo 13: O congresso de funcionários exercerá as seguintes funções:
>
>1. Ouvir os relatórios feitos pelo principal responsável pela empresa sobre os planos de desenvolvimento da empresa, produção anual e operação e gestão de negócios, reforma da empresa e formulação de estatutos importantes, conclusão e cumprimento dos contratos de uso de mão de obra da empresa os contratos e contratos coletivos, as condições de segurança no trabalho do empreendimento, bem como o pagamento pelo empreendimento dos prêmios da previdência social e da caixa de previdência habitacional, e apresentar pareceres e sugestões; deliberar sobre os estatutos ou planos sobre matérias importantes formuladas, alteradas ou decididas pela empresa em relação à remuneração do trabalho, horas de trabalho, descanso e férias, segurança e saúde do trabalho, seguro previdenciário, treinamento de empregados, disciplina do trabalho, gestão de cotas de trabalho e aqueles que envolvam diretamente os interesses vitais dos trabalhadores, e apresentar opiniões e sugestões;
>2. Deliberar e adotar minutas de contratos coletivos, planos para o uso de fundos de previdência dos empregados coletados de acordo com as disposições pertinentes do estado, planos para ajustar a proporção de pagamento e tempo para a caixa de previdência e prêmios de seguro social, trabalhadores modelo recomendados e outros assuntos importantes;
>3. Eleger ou destituir os diretores ou supervisores dos empregados, eleger os representantes dos empregados para as reuniões de credores e comitês de credores de empresas em processo de falência nos termos da lei, e recomendar ou eleger os gerentes de negócios da empresa mediante autorização;
>4. Examinar e supervisionar a implementação pela empresa das leis e regulamentos trabalhistas e dos estatutos trabalhistas, deliberar sobre os dirigentes da empresa de forma democrática e propor sugestões de prêmios e punições;
>5. Outras funções conforme prescrito por leis e regulamentos.
>
>Artigo 14: O congresso de funcionários de uma empresa estatal ou de uma holding estatal deve desempenhar as seguintes funções, além de desempenhar as funções prescritas no Artigo 13:
>1. Ouvir e deliberar os relatórios feitos pelo principal responsável pela operação e gestão do negócio sobre o investimento da empresa, grandes reconstruções técnicas, orçamentos financeiros e contas finais e o uso das despesas de entretenimento empresarial da empresa, bem como os planos para a avaliação de títulos técnicos profissionais, a utilização de fundos de acumulação pela empresa e reestruturação, e apresentar opiniões e sugestões;
>2. Deliberar e adotar planos de demissão e reassentamento de empregados nos planos de implementação de fusão, cisão, reestruturação, dissolução ou falência do empreendimento;
>3. Outras funções prescritas por leis, regulamentos administrativos e regulamentos administrativos.

Essas disposições não têm força de lei, mas olhe para as autoridades emissoras: CCDI do PCC (o principal órgão anticorrupção), o Departamento de Organização do PCC (órgão do partido que determina as nomeações e promoções - muito poderoso!) e SASAC, o órgão governante das SOEs.
- Comissão Central do PCCh para Inspeção Disciplinar
- Departamento de Organização do Comitê Central do Partido Comunista da China
- Comissão de Administração e Supervisão de Ativos Estatais do Conselho de Estado
- Ministério de Supervisão
- Federação de Sindicatos de Toda a China
- Federação de Indústria e Comércio de toda a China

Portanto, embora não seja uma lei em si, as disposições têm um grande peso político por trás delas. Há muito mais interesse em melhorar este sistema fora da ACFTU do que a maioria das pessoas gostaria de admitir.

Encontramos ainda mais expressão oficial disso em documentos-chave de 2013, logo após o 18º Congresso do Partido. As recomendações do Comitê Central do PCC incluem a promoção de congressos de trabalhadores.
>Dar pleno jogo à democracia ao nível da comunidade. Abriremos canais mais amplos para a democracia e melhoraremos o sistema de eleição em nível comunitário, discussão política, divulgação de informações, relatórios de deveres e prestação de contas. Exercitaremos a democracia consultiva no nível da comunidade de várias formas e torná-la institucional. Vamos estabelecer e melhorar a supervisão de residentes urbanos e aldeões, encorajando-os a realizar autogestão, autosserviço, autoeducação e autogestão no exercício da governança da comunidade urbana e rural, na gestão de assuntos públicos em nível comunitário e na gestão pública programas de serviço. Melhoraremos o sistema de gestão democrática nas empresas e instituições públicas com o Congresso dos trabalhadores e dos empregados como sua forma básica, fortaleceremos a construção do mecanismo democrático nas organizações sociais e garantiremos os direitos democráticos dos empregados na participação na gestão e supervisão.

O que os próprios trabalhadores pensam do sistema? A avaliação dos trabalhadores sobre o desempenho do congresso é, em geral, positiva.
>Para descobrir se um SWRC tem um efeito sobre os resultados positivos, nos concentramos em como os trabalhadores avaliaram o desempenho do SWRC em suas empresas. Selecionamos sete perguntas que pediam aos trabalhadores para avaliar diretamente as funções atribuídas do SWRC (ver Tabela 5). Os entrevistados foram solicitados a escolher em uma escala de cinco pontos, conforme mostrado na Tabela 5. Todas as avaliações, exceto uma, são notavelmente consistentes. Todas as respostas das perguntas 2 a 7 pontuaram em torno de 8% a 9% para muito bom e variaram entre 22% e 27% para bom, ligeiramente abaixo de 50% para mais ou menos e menos de 10% para muito ruim. As respostas da pergunta 1 são anômalas, com pontuação de 14% para muito bom e 32% para bom, com pontuação muito mais alta do que as outras seis perguntas. Pode-se dizer que as avaliações gerais foram bastante positivas.

![](https://pbs.twimg.com/media/DnoMG8oUYAAGQZG.jpg)

Você pode pensar que essas porcentagens positivas são baixas. Mas nem todo trabalhador está entusiasmado com a participação no sistema - lembre-se de que não é obrigatório estar envolvido.
>Os céticos podem querer nos desafiar sobre a porcentagem de respostas positivas na amostra da ACFTU - que apenas 1/3 das respostas são avaliações positivas, o que não é suficiente para mudar a imagem do sistema SWRC sendo em grande parte uma fachada e o sindicato do local de trabalho sendo nada mais do que um braço de gestão. Mas gostaríamos de chamar a atenção para o fato de que mesmo entre as democracias capitalistas, onde a representação dos trabalhadores no local de trabalho é institucionalizada e legalizada, e onde a gestão pode ser processada por descumprimento, como na Holanda, a participação dos trabalhadores não é garantida. Os acadêmicos que estudam os conselhos de trabalho europeus descobriram que eles variam desde o controle da administração até aqueles onde existe uma representação genuína; de paternalista a representante; de “conselhos de gestão” a “conselhos de guerra de classes”. Da mesma forma, os estudiosos que estudam o sistema japonês de consultoria de gestão de mão de obra descobriram que a atitude dos trabalhadores em relação ao sistema é função do tamanho da fábrica.<br/>
>[...]<br/>
>Há uma tendência entre os críticos de ter expectativas irrealistas em relação ao SWRC. É instrutivo tomar emprestado um argumento feito por Michael Burawoy e János Lukács em seu esforço para dissipar o mito de que o socialismo de estado é necessariamente menos eficiente do que o capitalismo avançado. Eles apontaram que os economistas neoclássicos “compararam uma realidade empírica das sociedades soviéticas com um tipo ideal de concepção do capitalismo”, com a presunção de que a realidade capitalista necessariamente corresponde ao ideal, enquanto as sociedades socialistas estatais inevitavelmente ficam aquém do ideal. Portanto, se mesmo sob sistemas democráticos, os conselhos de trabalhadores e sindicatos não recebem avaliações altas da maioria dos trabalhadores e empregados, não podemos esperar que a maioria dos SWRCs na China opere perto do ideal.

E quando pressionados sobre os detalhes, os trabalhadores pesquisados que expressaram dúvidas sobre a eficácia dos congressos admitiram os sucessos.
>Mais um caso foi descoberto por Zhu Xiaoyang em uma empresa coletiva em Kunming, após ele ter realizado longas entrevistas com alguns dos representantes do SWRC. Um dos representantes disse que o SWRC “na verdade não tem funcionado” e de fato está piorando cada vez mais. Mas quando questionado sobre uma série de questões muito específicas relacionadas ao procedimento de tomada de decisão sobre questões políticas importantes, como a relocação do local da fábrica, a instalação de equipamentos e instalações em grande escala ou enormes transações financeiras, ele disse que o gerente da fábrica e o secretário do Partido levaria essas questões para serem discutidas no SWRC. Se as propostas fossem aprovadas, elas seriam implementadas. Se não for aprovado, “eles ainda não ousariam prosseguir com isso”. Ele também deu dois exemplos concretos em que em dois congressos o SWRC nos últimos cinco anos vetou com sucesso a proposta do gerente da fábrica de comprar algumas instalações em grande escala e a fabricação de novos produtos. Isso nos alertou para o fato de que a imagem negativa do SWRC nem sempre condiz com a realidade, e precisamos ser muito cuidadosos na forma como enquadramos nossas questões ao conduzir o trabalho de campo.

Mais alguns resultados da pesquisa. A satisfação com as questões-chave dos direitos dos trabalhadores é maior em empresas que estabeleceram congressos. Lembre-se de que esses números são anteriores à Lei do Contrato de Trabalho em 2007 e às melhorias gerais nos direitos dos trabalhadores a partir de meados dos anos 2000.
![](https://pbs.twimg.com/media/DnoNn7hU8AAfXMA.jpg)

Essas melhorias significam um maior grau de envolvimento na tomada de decisões por meio dos congressos.
>A afirmação de Andrea de que o declínio da "gestão democrática" decorre da perda de segurança no emprego pelos trabalhadores pode atingir um ponto forte entre os juristas americanos, inclusive eu, que argumentou que a liberdade de demissão injustificada é um predicado necessário para a capacidade dos trabalhadores de exercer quaisquer direitos que eles gostam no papel. Se for assim, então o destino dos SWRCs pode depender parcialmente de se a Lei do Contrato de Trabalho pode cumprir um de seus objetivos mais ambiciosos de conferir maior segurança no emprego aos trabalhadores chineses. No papel, a China chegou: lembre-se que a Organização para Cooperação e Desenvolvimento Econômico (OCDE) classificou a legislação chinesa como a mais protetora do mundo.

Você pode estar pensando que isso soa muito semelhante aos conselhos de empresa europeus. É um ponto justo, que os próprios estudiosos trouxeram.

![](https://pbs.twimg.com/media/DnoO5BuVsAAOr2s.jpg)
![](https://pbs.twimg.com/media/DnoO5xxU4AMekrv.jpg)

Existem algumas diferenças importantes, no entanto. O antigo sistema é mais resistente do que muitos pensam; a cultura da unidade de trabalho permanece em muitas empresas.

![](https://pbs.twimg.com/media/DnoPih2V4AEsWeH.jpg)
![](https://pbs.twimg.com/media/DnoPjSUUUAIwRnb.jpg)

A outra e mais importante diferença é o próprio partido-estado. Nos conselhos de empresa alemães, por exemplo, a governança (limitada) dos trabalhadores começa e termina na empresa. Mas, ao se integrar aos comitês do PCC, os congressos têm um vínculo direto com o aparato estatal.
>A história da “socialização” começaria com o estabelecimento de capítulos do PCCh dentro de grandes empresas privadas. Isso facilitou o impulso da ACFTU para "organizar" capítulos sindicais e, por sua vez, a imprensa recente para estabelecer SWRCs nessas empresas. O resultado líquido foi reproduzir nas empresas maiores da economia de mercado as formas (embora longe da substância completa) da autoridade do Partido e da representação dos trabalhadores que haviam sido estabelecidas dentro da economia planejada, incluindo os SWRCs. Conforme já discutido, isso pode promover vários objetivos interconectados do regime - manutenção da estabilidade de curto e longo prazo por meio de canais oficiais mais eficazes para resolução de conflitos e aplicação de direitos dentro da empresa.

Essa dupla revivificação dos comitês do partido e dos congressos operários não pode ser mera coincidência. Isso significa uma reversão da mercantilização que caracterizou os anos 1990.
>Portanto, a maioria dos elementos do regime de legislação trabalhista unificada sugere uma convergência em direção ao capitalismo regulatório moderno. Mas o renascimento dos SWRCs e sua extensão ao setor não-estatal podem sinalizar algo novo na evolução da governança do local de trabalho na China - não uma rejeição da regulamentação capitalismo, mas uma variante dele que pode parecer mais familiar para os europeus do que para os americanos. Se os SWRCs devem ser algo mais do que instituições de papel, eles podem sugerir que a China pretende não apenas regular as empresas por meio de mecanismos administrativos tradicionais, mas também mudar seus modos internos de governança em uma direção pró-trabalhador. Em suma, depois de inicialmente “corporatizar” as SOEs, ou reestruturá-las para imitar as empresas capitalistas do Ocidente, a China (ou alguns de seus líderes) pode agora estar procurando “socializar” essas empresas capitalistas em um grau modesto - para reestruturá-las para dar aos trabalhadores um papel mais importante na governança corporativa.

Essas são mudanças inegáveis de volta a um modelo de governança que prioriza a participação dos trabalhadores e a gestão democrática - precisamente em linha com o que chamamos de socialismo.
>No final, os movimentos provisórios da China para reviver os SWRCs provavelmente refletem um consenso sobreposto e uma mistura de motivos e objetivos que são ideológicos e pragmáticos, e tanto de interesse público quanto de interesse próprio. Mas parte da mistura pode ser um desejo genuíno de capacitar os trabalhadores por meio dos SWRCs e seus mecanismos de apoio do Partido-Estado enquanto a China se move em direção à próxima fase do "socialismo com características chinesas". Essa aspiração é frequentemente proclamada e frequentemente contradita pelas realidades locais; mas tem seguidores dentro do PCCh. As recentes medidas de "gestão democrática" podem representar uma vitória ou uma concessão para aqueles no Partido que buscam restaurar o que consideram um lado salutar do legado de Mao - os compromissos igualitários e pró-trabalhadores que foram deixados de lado durante o primeiras décadas de liberalização econômica. Seja real ou simbólico, o renascimento dos SWRCs pode ser parte de um esforço para aumentar a legitimidade popular do regime, honrando uma ideologia pró-trabalhador que muitos cidadãos e alguns membros do Partido compartilham.
>
>Mesmo que alguém esteja convencido, no entanto, de que a ideologia oficial da China nada mais é do que um fino verniz para uma determinação inabalável de manter o poder, ainda importa como e por que o regime busca manter o poder. Se a liderança da China está tentando perpetuar seu domínio do poder político, em parte vivendo melhor sua ideologia pró-trabalhador e melhorando as vidas e aumentando as vozes dos trabalhadores descontentes, então tanto os empregadores quanto os trabalhadores do mundo, e aqueles que se preocupam com seu destino, terão que olhar novamente para a China e talvez repensar o futuro que a China está moldando.

Fontes:
- [Research Gate | SWRC in the Manufacturing Industry, by Ownership Type](https://www.researchgate.net/figure/SWRC-in-the-Manufacturing-Industry-by-Ownership-Type_tbl1_265966556)
- [CPC Encyclopedia | On the reform of the system of Party and state leadership](http://cpcchina.chinadaily.com.cn/fastfacts/2010-10/18/content_11425374.htm)
- [Marxists | How China’s Socialist State-Owned Industrial Enterprises Are Managed](https://www.marxists.org/subject/china/peking-review/1965/PR1965-09k.htm)
- [Ministry of Commerce - People's Republic of China | Law on Industrial Enterprises Owned by the Whole People](http://english.mofcom.gov.cn/article/lawsdata/chineselaw/200303/20030300072563.shtml)
- [People | Workers' Congresses and Self-governance of Enterprises](http://en.people.cn/92824/92845/92869/6439952.html)
- [zlibrary | Will Workers Have a Voice in China's 'Socialist Market Economy?' The Curious Revival of the Workers Congress System](https://ur.booksc.org/book/75681510/8020c0)
- [JSTOR | The Internal Politics of an Urban Chinese Work Community: A Case Study of Employee Influence on Decision-Making at a State-Owned Factory](https://www.jstor.org/stable/4127882?seq=1)
- [Bell School | The Evolution of China's Industrial System, the Japanese-German Model, and China's Workers' Congress](http://psc.bellschool.anu.edu.au/sites/default/files/IPS/PSC/CCC/publications/papers/anita_chan_evolution_of_china_industrial_system_japanese_german_model.pdf)
- [Dokumen.pub | A New Deal for China’s Workers?](https://dokumen.pub/a-new-deal-for-chinas-workers-2016017881-9780674971394.html)
- [Law Info China | Provisions on the Democratic Management of Enterprises](http://lawinfochina.com/display.aspx?lib=law&id=9318&EncodingName=big5)
- [China.org.cn | Decision of the Central Committee of the Communist Party of China on Some Major Issues Concerning Comprehensively Deepening the Reform](http://www.china.org.cn/china/third_plenary_session/2014-01/16/content_31212602_8.htm)
- [Research Gate | State Corporatism and Business Associations in China: A Comparison with Earlier Emerging Economies of East Asia](https://www.researchgate.net/publication/296472469_Jonathan_Unger_and_Anita_Chan_State_Corporatism_and_Business_Associations_in_China_A_Comparison_with_Earlier_Emerging_Economies_of_East_Asia_International_Journal_of_Emerging_Markets_Vol_10_No_2_April)
- [Scribd | Will Workers Have a Voice in China’s](https://pt.scribd.com/document/494233219/Will-Workers-Have-a-Voice-in-China-s)
