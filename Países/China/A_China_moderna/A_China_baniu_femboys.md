# A China baniu femboys?
A China baniu o Niangpao que, embora possa significar "maricas" (quando usado de forma depreciativa), geralmente se traduz como "estética falsa e afeminada" - o que significa reconstrução facial para parecer as estrelas do K-pop, que é o que foi proibido devido à repressão à "idol culture".

A falsa mídia de notícias americana está tentando promover isso como um ataque aos atores transgêneros; quando na realidade este é um ataque à cultura pop sul-coreana. A cirurgia de 双眼皮 ("pálpebra dupla") se tornou uma grande mania na China depois de ser normalizada no sul ocupado da Coreia. A China está tentando se afastar da normalização das modificações estéticas e permanentes do corpo, como a cirurgia plástica, porque são influências estrangeiras derivadas da raiz da Supremacia Branca. O sul da Coreia não tem dúvidas sobre isso. É por isso que os jovens de 16 anos rotineiramente recebem a cirurgia de “pálpebra dupla” para parecer mais ocidental, geralmente como um [presente de aniversário de 16 anos](https://www.theatlantic.com/international/archive/2013/06/south-korean-high-schoolers-get-plastic-surgery-for-graduation/277255/). Esta não é a cultura que a China deseja promulgar. E os transgêneros chineses têm mais direitos do que no "Ocidente progressista".

Alterar-se para se ajustar às “normas de beleza” criadas pelo capitalismo é uma coisa comum e aceita no Ocidente. E em estados fantoches centrados no oeste, como o sul ocupado da Coréia. Os países socialistas não desejam replicar essa nojenta automutilação corporativa.

Um lembrete de que, se é seguro ser uma pessoa transgênero na China, provavelmente também não há problema em femboys. Os americanos precisam se acalmar.

O governo/mídia chinês nunca deveria ter usado a palavra "Niangpao/娘炮". Um termo que é depreciativo, mas não anti-LGBT. Os internautas resistiram antes ao uso do termo online. A China perseguiu a cultura insípida das celebridades, que inclui os 'ídolos' e as "normas de beleza" criadas pelo capitalismo.

As reformas do estado da indústria do entretenimento incluem:
 - Sem emprego de criminosos
 - Limitação de pagamentos altíssimos para estrelas
 - Proibição de programas que criem ídolos ou empreguem filhos de estrelas
 - Criação de listas para encorajar os fãs a comprar itens ou pagar taxas de membros para apoiar ídolos
 - Punir a evasão fiscal da indústria
 - Repressão às plataformas de mídia da internet em busca de clickbait
 - Proibição da participação de menores em reality shows
 - Sem programas de competição de ídolos
 - Os programas não devem promover uma representação incorreta da beleza
 - Proibição de exibir riqueza

Não há absolutamente nenhum ataque à cultura LGBT ou às pessoas com essas reformas. Há apenas um ataque aos padrões corporais dentro da indústria - algo que os ocidentais deveriam entender perfeitamente. Em vez disso, isso está sendo inventado pela mídia corporativa para tentar usar os LGBTs como uma arma.

Um bom teste dos novos regulamentos que a China divulgou é se você verá Hua Chenyu e Li Yugang ainda na TV. Por um lado, ambos estão longe de serem menores (Hua tem 31 anos) e ambos são realmente talentosos - muito longe da cultura de "carne fresca" (小嫩肉) sexualizada e fetichizada que é popular na China.
