#;A narrativa da taxa de mortalidade
>A taxa de mortalidade do coronavirus é de 1% apenas.

Existem dois problemas com esta questão.
1. Ele negligencia a lei dos grandes números.
2. Assume que uma das duas coisas aconteça: você morre ou está 100% bem.
3. Os EUA têm uma população de 328.200.000. Se 1% da população morrer, serão 3.282.000 pessoas mortas.

Três milhões de pessoas mortas afetariam maciçamente a economia, aconteça o que acontecer. Isso mais do que dobra o número de mortes anuais de uma só vez. A segunda parte é que as pessoas continuam falando sobre mortes. Mortes, mortes, mortes. Apenas 1% morre! Apenas 1%! 1 é um número pequeno! Não é grande coisa, certo? E as pessoas que sobrevivem? Para cada pessoa que morre:
- Mais 19 requerem hospitalização.
- 18 deles terão danos permanentes ao coração pelo resto de suas vidas.
- 10 terá dano pulmonar permanente.
- 3 terão derrames.
- 2 terão dano neurológico que leva a fraqueza crônica e perda de coordenação.
- 2 terão dano neurológico que leva à perda da função cognitiva.

Então agora, de repente, "mas é apenas 1% fatal!" torna-se:
- 3.282.000 pessoas mortas.
- 62.358.000 hospitalizados.
- 59.076.000 pessoas com danos cardíacos permanentes.
- 32.820.000 pessoas com danos pulmonares permanentes.
- 9.846.000 pessoas com AVC.
- 6.564.000 pessoas com fraqueza muscular.
- 6.564.000 pessoas com perda de função cognitiva.

Se os hospitais ficarem lotados, alguns dos acidentes vasculares cerebrais também se tornam fatais. E outros incidentes não-COVID sérios, mas não fatais com hospitalização se tornam fatais. Há pessoas com infarto do miocárdio com elevação de ST (uma forma particularmente mortal de ataque cardíaco) e ataque isquêmico transitório (também chamado de mini-AVC) que não procuram ajuda porque têm medo de ir ao hospital e contrair COVID.

Isso é o que as pessoas que insistem em dizer sobre "apenas 1% de mortos, qual é o problema?" não entendem. A escolha não é “arruinar a economia para economizar 1%”, não há uma dicotomia entre se proteger do vírus e sobreviver. Se reabrirmos a economia, ela será destruída de qualquer maneira. A economia dos EUA não pode sobreviver a todos pegando COVID-19.

Você é bom de lógica? Então vamos lá: o Brasil é o país que menos faz testes por milhão. O Brasil é o país com mais novos casos por dia no mundo. Se o país testa pouco e ainda assim tem muito caso, a lógica diz que somos o grande epicentro da doença, certo? Vamos adiante. Se o Brasil é o epicentro da doença, por que diabos estamos reabrindo o comércio e retomando a vida ao normal? Bom, tem uma explicação lógica para o que está acontecendo, e é uma lógica perversa, baseada na ideia de que a sua vida não vale nada. Está preparado? Pesquisas mostram que no Brasil a COVID-19 é uma doença que mata pobre. Basicamente morreram mais pessoas pobres de 20-40 anos do que de classe média alta de 70-90 anos. Então esqueça a frase "a doença só mata velho". 65% dos mortos ganham menos de R$3.000,00 mês

Sabe qual o percentual dos mortos dentro da faixa de renda compatível com o salário de governadores, presidente, Ministro, empresários, etc? 2%. O que significa que a parte rica da população está decidindo sobre a reabertura das coisas, ciente que quem vai morrer é a parte pobre. Então eles falam em "salvar a economia". Acorda. Salvar a economia para eles é salvar o lucro deles, sem se preocupar que você vai morrer por isso, porque você é mão de obra barata e fácil de repor. Você morre hoje, e amanhã tem outro "salvando a economia" no seu lugar. Então você me diz: "mas eu preciso trabalhar". Concordo. Ninguém está dizendo para você ficar em casa passando fome e necessidade. Mas é muito importante que você entenda que salvar a economia de verdade seria o Governo subsidiar de forma decente seu isolamento social. Como na Argentina, por exemplo. Viu que lá morreu bem menos gente? É que lá o governo matou no peito e prendeu a bola. Chamou a responsabilidade e as pessoas ficaram em casa sem precisar se preocupar em pagar sequer aluguel. O Estado fez seu papel. Por aqui, existe a política do Guedes de Estado mínimo. Política essa que veta liberar 50 bilhões para você e eu ficarmos seguros em casa, e nos coloca para morrer em busão lotado pra "salvar a economia" e o lucro do patrão, enquanto o Governo libera 1 trilhão para banco privado.

Então se você é bom de lógica, responda: tem lógica arriscar a vida, sabendo que está na faixa de 65% dos mortos pela COVID, para salvar o lucro do patrão, sabendo que o Governo prefere dar dinheiro para banco que lucra bilhões do que dar dinheiro para você ficar seguro em casa? Discussões sobre o shopping reabrir ou o futebol voltar são totalmente secundárias agora. Precisamos mesmo é brigar para o auxílio emergencial (coronavoucher) durar pelo menos até o fim do ano. Para ter subsídio para aluguel, luz, água e gás. Para as pessoas ficarem em casa em segurança e sem fome. Isso não quebra o país. Incentivo social movimenta a economia da base para cima. O que quebra um país do tamanho do Brasil é a desigualdade, e é essa desigualdade que está sendo fomentada por Bolsonaro e Guedes, e potencializada pelo coronavirus. Não é, portanto, hora da gente falar em austeridade. É hora da gente debater distribuição de renda, taxação de grandes fortunas e herança, controle da evasão tributária das grandes empresas.

Você precisa tomar consciência de que está na faixa que morre mais, se quiser viver. Não dá mais para se achar elite. Para se achar um aristocrata porque tem carro financiado. Se essa postura de ficar do lado do patrão para se sentir elite antes era mera burrice, agora é pular na bala: é uma sentença de morte. É um novo dia. Basicamente estão falando em retomar aulas parcialmente nesse momento! Loucura! Estamos no pico da pandemia. Não um pico que bate no teto e já começa a descer, mas um pico duradouro, que ainda vai causar mortes demais. O que João Dória faz? Começa a discutir reabertura das escolas! As crianças são vetores de contágio. Vai ser um massacre!

Não só isso, Dória vetou projeto de reintegração de posse e despejos em plena pandemia em São Paulo. Bolsonaro também vetou um projeto que suspendia despejos durante a pandemia.

Por causa de toda essa irresponsabilidade, o Brasil se torna o [principal destino](https://www.metropoles.com/colunas/guilherme-amado/brasil-se-torna-principal-destino-em-posts-de-viagens-para-nao-vacinados?utm_source=dlvr.it&utm_medium=twitter) em posts de viagens para não vacinados. O [projeto genocida evangélico](https://telegra.ph/Pastores-est%C3%A3o-orientando-ind%C3%ADgenas-a-n%C3%A3o-se-vacinarem-relatam-lideran%C3%A7as-01-30) está a todo vapor.
