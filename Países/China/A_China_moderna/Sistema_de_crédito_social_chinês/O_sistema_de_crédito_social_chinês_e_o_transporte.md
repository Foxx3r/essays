# O sistema de crédito social chinês e o transporte
Esse contexto é útil, mas menos relevante quando se trata de reportagens sobre o sistema na imprensa ocidental. Você viu as manchetes: "Black Mirror é real", "Deus nos ajude a todos", etc. Então, quais são as consequências reais de quebrar as regras deste sistema? Você pode ver por si mesmo: Esta é a primeira lista, relacionada a investimentos e finanças. Esse conjunto de punições visa claramente os inadimplentes de alta renda para limitar sua atividade no mercado.
>II, Fortalecimento da punição conjunta.
>
>(1) Limites de envolvimento em indústrias ou projetos específicos
>
>>1. Limites de constituição de sociedades financeiras. Tornar as informações de inadimplência uma consideração para a devida diligência durante o exame e aprovação para o estabelecimento de entidades bancárias ou financeiras ou suas filiais, bem como na compra de ações ou aquisição de entidades bancárias e financeiras, e é uma referência para a devida diligência no exame e aprovação da constituição de sociedades de valores mobiliários, sociedades de gestão financeira e sociedades de futuros, bem como o registo de pessoal gestor em fundos de investimento privados. Impedir que os inadimplentes do Julgamento criem empresas de garantia financeira e seguradoras.
>>2. Limites de emissão de títulos. Rever estritamente a emissão de títulos por inadimplentes no mercado interbancário, limitar a emissão pública de títulos corporativos por inadimplentes.
>>3. Limites no número de investidores qualificados. Faça das circunstâncias de inadimplência uma base de consideração cuidadosa no exame, aprovação e gerenciamento de investidores institucionais estrangeiros qualificados e investidores institucionais domésticos.
>>4. Limites dos incentivos patrimoniais. Auxiliar na suspensão dos planos de incentivo de ações quando o delinquente delinquente sujeito à execução for uma empresa estatal nacional listada; quando a pessoa inadimplente sujeita à execução for uma empresa pública nacional listada, ajudar a encerrar a elegibilidade para desfrutar de incentivos de capital.
>>5. Limitar a emissão de ações e as transferências de listagens. Faça das informações relativas aos inadimplentes em Julgamento uma consideração na emissão de ações e exame e verificação da cotação das ações e transferência aberta no sistema nacional de transferência de ações de pequenas e médias empresas.
>>6. Limites ao estabelecimento de organizações sociais. Fazer uma consideração das informações de inadimplência revisão e aprovação do início de organizações sociais, limitar o estabelecimento de organizações sociais por inadimplentes de julgamento.

Outros incluem assumir cargos de direção em empresas estatais, tornar-se funcionário público e ingressar no PCC. Deixar de pagar dívidas ou fraudar clientes provavelmente já seria uma questão de verificação de histórico de rotina para essas posições, mas tanto faz.

Aqui está o grande problema, o castigo que está ganhando todas as manchetes: passagens de trem e de avião. Mas o que a política realmente diz?
>(7) Restrições ao consumo conspícuo e consumo relacionado.
>>1. Restrições ao uso de trens e aeronaves. Restringir as pessoas sujeitas à aplicação para quebra de confiança e o representante legal, principais responsáveis, pessoas que realmente controlam, bem como pessoas diretamente responsáveis ​​que influenciam o desempenho da dívida em pessoas sujeitas à aplicação de quebra de confiança por andarem com travessas macias em trens, todos assentos em conjuntos de trens da classe G e todos os assentos de primeira classe em outros conjuntos de trens, aeronaves civis e outros consumíveis que não sejam necessários para a vida ou para o trabalho.
>>2. Restrições a visitas a hotéis e restaurantes. Restringir pessoas sujeitas à execução para quebra de confiança e o representante legal, principais responsáveis, pessoas que realmente controlam, bem como pessoas diretamente responsáveis ​​que influenciam o desempenho da dívida em pessoas sujeitas à execução para quebra de confiança de permanecerem em classificação com estrelas e superior pensões e hotéis, restaurantes nacionais de primeiro nível e superiores e outros locais de hospitalidade de consumo conspícuo; restringir o consumo em boates, campos de golfe e outros locais de consumo conspícuos.
>>3. Restrições em viagens de consumo conspícuo. Restringir pessoas sujeitas à execução para quebra de confiança e o representante legal, principais responsáveis, pessoas que realmente controlam, bem como pessoas diretamente responsáveis ​​que influenciam o desempenho da dívida em pessoas sujeitas à execução para quebra de confiança por participar de viagens em grupo estrangeiras organizadas por agências de viagens, bem como usufruir de outros serviços prestados por agências de viagens relacionados com viagens ao estrangeiro; implementar controle restritivo sobre o consumo dentro de áreas de férias ou empresas de viagens que obtiveram avaliação de ranking de turismo.
>>4. Restrições para filhos e filhas que frequentem escolas com taxas altas. Restringir pessoas sujeitas à execução para quebra de confiança e o representante legal, principais responsáveis, pessoas que realmente controlam, bem como pessoas diretamente responsáveis ​​que influenciam o desempenho da dívida em pessoas sujeitas à execução para quebra de confiança de usar seus ativos para pagar por seus filhos ou filhas se matriculem ou frequentem escolas particulares de alto custo.

Em outras palavras, se você está na lista de "crédito ruim", não pode comprar passagens de avião de primeira classe ou de trem de luxo. Você também não pode se envolver em viagens de "consumo conspícuo" ou mandar seus filhos para escolas particulares caras. Uau, exatamente como aquela série da Netflix!

Os comunicados mais recentes sobre esta política foram emitidos recentemente e oferecem um pouco mais de detalhes. Primeiro, trens:
>I, Escopo das restrições
>
>(1) Pessoas responsáveis ​​por atos que influenciam gravemente a segurança operacional ferroviária e a segurança da produção, que são multados por órgãos de segurança pública ou determinados como tal pela estação ferroviária ou unidades ferroviárias:
>>1. aqueles que perturbam a ordem operacional das estações ferroviárias e chuvas, e colocam em risco a segurança ferroviária, resultando em grave influência social prejudicial;
>>2. aqueles que fumam em várias unidades de trem ou fumam em áreas para não fumantes em outros trens;
>>3. os investigados e processados ​​por escalpelamento de bilhetes de trem ou produção e comercialização de bilhetes falsificados;
>>4. aqueles que usam ilegalmente documentos de identidade preferenciais (especiais) ou usam documentos de identidade preferenciais (especiais) falsificados ou inválidos para comprar ingressos;
>>5. portadores de bilhetes de trem falsificados, vencidos ou inválidos, ou que usam ilegalmente bilhetes suplementares informados como perdidos nos trens;
>>6. aqueles que embarcam em trens sem passagens ou excedem a distância da passagem e se recusam a comprar uma passagem suplementar;
>>7. aqueles que deveriam estar sujeitos a punições administrativas com base nas leis e regulamentos pertinentes.
>
>As pessoas responsáveis ​​pela conduta acima mencionada estão impedidas de andar nos trens.<br/>
>[...]<br/>
>(2) Pessoas relevantes responsáveis ​​por conduta gravemente ilegal em outras áreas
>>1. as partes em casos importantes de infrações fiscais que têm a capacidade de implementar, mas não implementam, a sentença;
>>2. pessoas responsáveis ​​por conduta gravemente indigna de confiança na área de gestão e uso financeiro, que se envolveram em fraude e engano, fizeram pedidos ou reivindicações fraudulentas, obtiveram ativos por meio de fraude, retiveram ou desviaram fundos, ou estão em atraso no vencimento de dívidas emitidas por estrangeiros organizações financeiras ou governos estrangeiros;
>>3. responsáveis ​​por conduta gravemente indigna de confiança na área da previdência social, nas seguintes circunstâncias: empregar unidades de trabalho que não participam da previdência social nos termos da regulamentação e recusam-se a retificar; empregar unidades de trabalho que não relatem os números de pagamento da previdência social com precisão e se recusem a retificar o assunto; aqueles que deveriam pagar taxas de previdência social e têm capacidade para fazê-lo, mas se recusam a pagá-las; aqueles que escondem, desviam, apreendem ou desviam fundos da previdência social ou os investem em operações que violem regulamentos; aqueles que obtêm benefícios previdenciários de forma fraudulenta, por meio de fraude, falsificação de materiais de prova ou outros meios; organismos de serviço de segurança social que violam acordos de serviço ou regulamentos relevantes; aqueles que se recusam a auxiliar os departamentos de administração de ciências sociais no exame e verificação de acidentes e problemas;
>>4. aqueles que não pagaram multas ou receita confiscada por atos ilícitos relativos a valores mobiliários ou futuros dentro do prazo; sujeitos responsáveis ​​em empresas de capital aberto que não tenham cumprido os seus compromissos públicos dentro do prazo;
>>5. aqueles sujeitos a medidas de restrição de consumo adotadas de acordo com a lei por um tribunal popular de acordo com os regulamentos, ou foram incluídos na lista de nomes de pessoas não confiáveis ​​sujeitas à execução;
>>6. quando os departamentos relevantes acreditarem que as pessoas responsáveis ​​por outras condutas gravemente não confiáveis ​​devem ser impedidas de ocupar assentos de alto nível nos trens, e o departamento relevante se juntar a este documento, isso deve ser esclarecido por meio de uma revisão deste documento.
>
>As pessoas responsáveis ​​pela conduta acima mencionada estão proibidas de usar assentos de nível alto em trens, incluindo travessas macias, todos os assentos em trens de unidades múltiplas com a letra G e assentos de primeira classe ou classe alta em outros trens de unidades múltiplas.

Em suma, o primeiro conjunto de violações é para regras específicas relacionadas a viagens de trem, como emissão de passagens fraudulentas ou escalpelamento. Esta é uma proibição universal de compra de ingressos. A segunda lista é mais ampla, que contém as punições de "alta classe" que vimos anteriormente.

A lista mais ampla inclui coisas que você pode esperar, como fraude ou engano financeiro, mas também inclui não pagar a previdência social ou empregar entidades que não o fazem. Quão terrível que eles sejam punidos com um assento em um trem-bala da classe C ou D em vez da classe G!

**Uma observação**: os trens-bala das classes C e D são um pouco menos rápidos do que a classe G. Eles também custam menos, porque não têm dois níveis de assentos luxuosos. Ninguém que eu tenha visto relatando isso se preocupou em apontar a distinção.

Mas digamos que alguém viole as regras da primeira categoria e queira sair da lista negra. Como eles fariam isso? Aqui está como.
>IV, Mecanismos de remoção
>
>Pessoas gravemente indignas de confiança específicas serão proibidas de usar trens por um determinado período. Depois que um assunto relevante é removido da lista de nomes de pessoas impedidas de andar em trens, eles não estão mais sujeitos a medidas restritivas de andar em trens, o método de remoção de concreto é o seguinte:
>
>>1) Quando uma pessoa é responsável por uma conduta que influencia gravemente a segurança operacional ferroviária e a segurança de produção, conforme listado nas Cláusulas 1-3 e 7, todas as empresas de operação ferroviária irão impedi-la de comprar bilhetes por um período de 180 dias, a ser calculado a partir da conclusão data do período de publicação em que não haja objeção válida, após os 180 dias, eles são retirados automaticamente, e as empresas de operação ferroviária lhes restituirão a venda de passagens.
>>
>>2) Quando uma pessoa é responsável por uma conduta que influencia gravemente a segurança operacional da ferrovia e a segurança da produção, conforme listado nas Cláusulas 4-6, todas as empresas de transporte ferroviário irão impedi-la de comprar bilhetes. Quando a pessoa responsável reembolsa a tarifa devida do bilhete (a ser calculada a partir do dia seguinte ao upgrade do bilhete), as empresas de operações ferroviárias restauram as vendas de bilhetes; quando a pessoa responsável age em violação das Cláusulas 4 a 6 por três vezes dentro de um ano após alterar a primeira tarifa do bilhete devida, e a pessoa responsável devolve a tarifa devida no prazo de 90 dias (incluindo 90 dias), a empresa de operações ferroviárias restaura a venda de passagens para eles, onde eles não pagam a passagem devida, as empresas de operações ferroviárias não restaurarão a venda de passagens.
>>
>>3) A lista de nomes de pessoas impedidas de viajar em assentos de nível alto em trens produzidos em outras áreas é válida pelo período de um ano, a ser contado a partir do dia final do período de publicação, após o término do ano, são removido automaticamente; dentro do período de validade, se os seus deveres estatutários tiverem sido concluídos ou cumpridos, o departamento relevante deve notificar a General Railway Company para removê-los da lista de nomes no prazo de 7 dias úteis.

Resumindo, se você é cambista ou fumante, ou está sujeito a punições criminais, você está impedido de viajar de trem por 180 dias. Na verdade, é duvidoso que uma pessoa pega fumando uma vez seja incluída na lista. Imagino que infratores reincidentes serão os únicos punidos dessa forma.

Se você usar documentos falsos para comprar uma passagem, ou viajar sem pagar, sua punição é: Pagar a passagem. Absolutamente orwelliano.

Os aviões são um pouco diferentes. As punições são mais rígidas, mas as ofensas também o são. Dê uma olhada:
>I, Escopo das restrições
>
>(1) Passageiros que pratiquem os seguintes atos em aeroportos ou aeronaves, que estejam sujeitos a punições administrativas impostas por órgãos de segurança pública ou responsabilidade criminal:
>>1. fabricar e disseminar deliberadamente informações falsas sobre terrorismo envolvendo a aviação civil ou segurança de defesa aérea;
>>2. falsificar, alterar ou usar ilegalmente o documento de identidade de embarque ou cartão de embarque de outra pessoa;
>>3. bloquear, ocupar à força ou atacar balcões de check-in, passagens de controle de segurança, portões de embarque (passagens);
>>4. transportar ou expedir pessoalmente mercadorias perigosas, mercadorias proibidas ou mercadorias controladas determinadas pelas leis e regulamentos nacionais; aqueles que intencionalmente ocultem mercadorias categorizadas como proibidas para a aviação civil ou transporte controlado em sua bagagem de mão ou consignada;
>>5. embarcar ou atacar aeronaves à força, forçar a entrada ou atacar as cabines, pistas ou pistas de pouso da aeronave;
>>6. obstruir ou incitar outras pessoas a obstruir as tripulações de voo, guardas de segurança, pessoal de check-in e outro pessoal da aviação civil no desempenho de suas funções e realizar ou ameaçar realizar ataques pessoais;
>>7. ocupar assentos ou bagageiros à força, lutar ou brigar, provocar brigas ou perturbar a ordem da cabine de passageiros, entre outros, por intencionalmente danificar, roubar ou abrir sem autorização aeronaves civis ou instalações ou equipamentos de aviação;
>>8. usar uma chama aberta, fumar ou usar equipamento eletrônico em violação dos regulamentos a bordo de aeronaves civis e não dar ouvidos a dissuasões;
>>9. roubar propriedade de outras pessoas a bordo de aeronaves.

Nesta lista, temos o embarque forçado, lutar em um avião, acender um incêndio, praticar terrorismo de verdade. Essas são coisas objetivamente perigosas.

A lista de outras infrações da política de viagens de trem é a mesma para as viagens aéreas: fraude financeira, fraude previdenciária, não pagamento de multas relativas a valores mobiliários. Crimes de colarinho branco, em outras palavras.

Portanto, as punições para viagens de avião são as seguintes: Para violações da primeira lista específica, proibição de um ano. Lembre-se, essa é a lista de "roubo ou terrorismo". Para a segunda lista geral do "colarinho branco", a punição é a mesma, salvo o cumprimento dos deveres estatutários.
>IV, Mecanismos de remoção
>
>Pessoas específicas gravemente não confiáveis ​​serão apropriadamente impedidas de viajar em aeronaves civis por um determinado período. Depois que um assunto relevante é removido da lista de exclusão aérea, eles não estão mais sujeitos a medidas restritivas de trens, o método de remoção de concreto é o seguinte:
>
>>1) No caso de pessoa responsável por conduta que influencie gravemente a segurança do voo da aviação civil ou a segurança da produção, o prazo de validade é de um ano, a contar da data final do período de publicação, após um ano são retirados automaticamente.
>>
>>2) A lista de exclusão aérea produzida nas demais áreas é válida pelo período de um ano, a ser contado a partir do dia final do período de publicação, após o término do ano são retiradas automaticamente; se os seus deveres estatutários tiverem sido concluídos ou cumpridos dentro do período de validade, o departamento relevante deve notificar a General Railway Company para removê-los da lista de nomes no prazo de 7 dias úteis.
>
>Quando suspeitos de crimes escoltados ou criminosos condenados precisam voar, a exclusão temporária será concedida após o departamento de escolta registrar um requerimento junto à Administração de Aviação Civil

Por que a lista do "colarinho branco" está sujeita a punições mais rígidas para viagens aéreas do que viagens de trem? A resposta deveria ser óbvia: é mais fácil fugir do país em aviões.

O escopo das infrações desta política indica que ela é destinada a indivíduos ricos, como um incentivo para que paguem suas dívidas e multas.

Alguns meios de comunicação adotaram alguns sistemas locais e estabeleceram uma ligação furtiva com a política nacional. Por exemplo, Xangai testou sua própria versão do sistema, que inclui ofensas repetidas para coisas como travessias em trânsito. A província de Zhejiang fez isso com a triagem de lixo.

Os exemplos acima não foram realmente implementados em nível nacional, porém, e não está claro se algum dia serão. Tenho as minhas dúvidas, pois o prazo para que o sistema de crédito seja implementado a nível nacional é 2020 e o tempo está a correr.

Se o governo central pensasse que valia a pena incluir esses esquemas locais, já teríamos visto algo sobre isso agora, e não vimos. A ligação entre o jaywalking e o crédito nacional é, para dizer o mínimo, duvidosa.

No momento, essas infrações municipais têm uma multa mínima (5 yuans). É isso. Mas se você relatar isso e relatar o sistema de crédito social na mesma história, estará implicando em um relacionamento que realmente não existe. É especulativo e é uma reportagem irresponsável.

Fontes:
- [China Copyright and Media | Opinions concerning Accelerating the Construction of Credit Supervision, Warning and Punishment Mechanisms for Persons Subject to Enforcement for Trust-Breaking](https://chinacopyrightandmedia.wordpress.com/2016/09/25/opinions-concerning-accelerating-the-construction-of-credit-supervision-warning-and-punishment-mechanisms-for-persons-subject-to-enforcement-for-trust-breaking/)
- [China Copyright and Media | Opinions concerning Appropriately Limiting Specific Gravely Untrustworthy Persons from Riding Trains for a Certain Period, and Promoting the Construction of the Social Credit System](https://chinacopyrightandmedia.wordpress.com/2018/03/17/opinions-concerning-appropriately-limiting-specific-gravely-untrustworthy-persons-from-riding-trains-for-a-certain-period-and-promoting-the-construction-of-the-social-credit-system/)
- [China Copyright and Media | Opinions concerning Appropriately Limiting Specific Gravely Untrustworthy Persons from Traveling on Civil Aircraft for a Certain Period, and Promoting the Construction of the Social Credit System](https://chinacopyrightandmedia.wordpress.com/2018/03/18/opinions-concerning-appropriately-limiting-specific-gravely-untrustworthy-persons-from-traveling-on-civil-aircraft-for-a-certain-period-and-promoting-the-construction-of-the-social-credit-system/)
