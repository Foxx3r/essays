# Nostalgia pós-comunista
Apesar das questões da República Popular da Hungria, muitas pessoas na Hungria desde então anseiam pelo retorno do sistema socialista. Uma pesquisa conduzida pelo Pew Research Center encontrou o seguinte:
>Notáveis ​​72% dos húngaros dizem que a maioria das pessoas em seu país está realmente pior economicamente hoje do que sob o comunismo. Apenas 8% dizem que a maioria das pessoas na Hungria está em melhor situação e 16% dizem que as coisas estão quase iguais.

Mesmo as fontes de notícias reacionárias foram incapazes de ignorar a opinião favorável que muitos húngaros têm do antigo sistema socialista. O Daily Mail (um dos jornais de direita mais linha-dura da Grã-Bretanha) publicou um artigo de uma mulher da Hungria, que fez algumas declarações interessantes:
>Quando as pessoas me perguntam como foi crescer atrás da Cortina de Ferro na Hungria nas décadas de 1970 e 1980, a maioria espera ouvir histórias de polícia secreta, filas de pão e outras manifestações desagradáveis ​​da vida em um Estado de partido único.
>
>Eles ficam invariavelmente desapontados quando eu explico que a realidade era bem diferente, e a Hungria comunista, longe de ser o inferno na terra, era na verdade um lugar divertido para se viver.
>
>Os comunistas proporcionaram a todos emprego garantido, boa educação e saúde gratuita. O crime violento era praticamente inexistente.

O autor observa que a vida cultural foi expandida para incluir todos os húngaros, não apenas as classes altas:
>A cultura era considerada extremamente importante pelo governo. Os comunistas não queriam restringir as coisas boas da vida às classes alta e média - o melhor da música, literatura e dança eram para todos desfrutarem.
>
>Isso significava que subsídios generosos eram dados a instituições, incluindo orquestras, óperas, teatros e cinemas. Os preços dos ingressos eram subsidiados pelo Estado, tornando acessíveis as visitas à ópera e ao teatro.
>
>'Casas culturais' foram abertas em cada cidade e vila, para que as pessoas da classe trabalhadora provinciana, como meus pais, pudessem ter fácil acesso às artes cênicas e aos melhores artistas.

Ela observa que a publicidade e a cultura consumista eram praticamente inexistentes na Hungria:
>Embora vivêssemos bem sob o "comunismo goulash" e sempre houvesse comida suficiente para comermos, não fomos bombardeados com propaganda de produtos de que não precisávamos.

Ela lamenta que as perspectivas da classe trabalhadora comum do Bloco Oriental sejam normalmente ignoradas pelo Ocidente:
>Quando o comunismo na Hungria acabou em 1989, não fiquei apenas surpresa, mas triste, como muitos outros. Sim, havia gente marchando contra o governo, mas a maioria das pessoas comuns - eu e minha família incluída - não participou dos protestos.
>
>Nossa voz - a voz daqueles cujas vidas foram melhoradas pelo comunismo - raramente é ouvida quando se trata de discussões sobre como era a vida atrás da Cortina de Ferro.
>
>Em vez disso, os relatos que ouvimos no Ocidente são quase sempre da perspectiva de emigrados ricos ou dissidentes anticomunistas com um machado para moer.

Finalmente, ela observa que as perdas da era pós-comunista excederam em muito qualquer ganho potencial:
>As pessoas não têm mais segurança no emprego. A pobreza e o crime estão aumentando. A classe trabalhadora não pode mais se dar ao luxo de ir à ópera ou ao teatro. Como na Grã-Bretanha, a TV se emburreceu a um grau preocupante - ironicamente, nunca tivemos o Big Brother sob o comunismo, mas o temos hoje.
>
>O mais triste de tudo é que o espírito de camaradagem de que gostávamos quase desapareceu. Nas últimas duas décadas, podemos ter ganhado shoppings, 'democracia' multipartidária, telefones celulares e internet. Mas perdemos muito mais.

Ao realmente ouvir os relatos de pessoas que viveram sob o socialismo, bem como examinar as pesquisas, podemos ver o que a classe trabalhadora realmente sente sobre o socialismo, bem como sobre o capitalismo implacável que o sucedeu.
