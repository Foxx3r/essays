# Saúde e bem-estar social
Os cuidados de saúde na Hungria pré-comunista eram de baixa qualidade e a população sofria de problemas de saúde e baixa expectativa de vida. No entanto, após o estabelecimento da República Popular da Hungria, as condições de saúde começaram a melhorar significativamente. Relatórios da Divisão de Pesquisa Federal dos EUA:
>Depois que o governo comunista assumiu o poder na Hungria, dedicou muita atenção ao atendimento das necessidades específicas de saúde e segurança social da população. Em comparação com os padrões anteriores à guerra, o cidadão médio recebeu cuidados de saúde e assistência social muito melhores como resultado da política do governo.

As notas da Enciclopédia Britânica:
>Após a Segunda Guerra Mundial, os cuidados de saúde melhoraram dramaticamente sob o socialismo estatal, com aumentos significativos no número de médicos e leitos hospitalares na Hungria. Na década de 1970, o atendimento médico gratuito era garantido a todos os cidadãos.

O sistema de previdência social também melhorou significativamente, oferecendo cobertura à grande maioria dos trabalhadores da Hungria. A Divisão de Pesquisa Federal dos EUA observa:
>No final da década de 1980, o sistema previdenciário do país cobria cerca de 85% da população em idade de aposentadoria. Trabalhadores do sexo masculino podiam se qualificar para pensões aos 60 anos, e as trabalhadoras aos 55.

Esses avanços são confirmados pela Enciclopédia Britânica:
>Uma ampla gama de serviços sociais foi fornecida pelo governo comunista, incluindo pensão alimentícia, extensa licença-maternidade e um sistema de aposentadoria para o qual os homens se tornaram elegíveis aos 60 anos e as mulheres aos 55.

No entanto, essas melhorias no bem-estar social tiveram uma desvantagem estranha. À medida que as pessoas começaram a viver mais e o acesso ao bem-estar social foi expandido, o sistema foi colocado sob mais pressão. A Divisão de Pesquisa Federal dos EUA declara:
>O número de aposentados havia aumentado rapidamente desde o final da Segunda Guerra Mundial, à medida que as pessoas viviam mais e a cobertura previdenciária se expandia para incluir segmentos adicionais da população.

Além disso, vários males sociais, como o alcoolismo, começaram a representar um problema para a saúde pública:
>Em meados da década de 1980, as autoridades também discutiam a crescente incidência de abuso de substâncias. A incidência de alcoolismo havia aumentado durante a geração anterior, e uma alta porcentagem de vítimas de suicídio eram alcoólatras. Em 1986, o consumo de álcool por pessoa por ano era de 11,7 litros; o consumo de bebidas fortes (4,8 litros por pessoa) era o segundo maior do mundo.

Para agravar essas questões, o governo húngaro gastou uma porcentagem preocupantemente baixa do PIB em saúde:
>Analistas ocidentais estimam que a Hungria gasta apenas 3,3% de seu produto interno bruto especificamente em serviços de saúde (os 6% listados na maioria dos dados estatísticos na verdade incluem alguns serviços sociais). Essa porcentagem foi a mais baixa de qualquer país do Leste Europeu, exceto a Romênia (em comparação, os Estados Unidos gastaram 11% do PIB em saúde).

Apesar destes problemas, o sistema socialista na Hungria proporcionou objetivamente benefícios claros às pessoas em termos de saúde e bem-estar social. Existem várias lições importantes que podemos aprender com a experiência húngara:

1. É essencial que os recursos adequados sejam dedicados à saúde e ao bem-estar social. A austeridade não beneficia ninguém nessas questões, porque o declínio resultante na qualidade e disponibilidade levará à instabilidade e inquietação, para não mencionar uma redução nos padrões de vida. A ditadura do proletariado deve colocar as necessidades e interesses do proletariado como prioridade máxima; caso contrário, qual é o ponto?
2. Também é essencial que um governo socialista tome medidas para resolver problemas sociais como o alcoolismo e o vício em drogas, que contribuíram maciçamente para o declínio da saúde em todo o Bloco de Leste nas décadas de 1970 e 1980. A importância dessas questões pode ser vista comparando o Bloco de Leste com outras nações socialistas, como Cuba, que não tem problemas tão generalizados de abuso de substâncias e que tem visto melhorias constantes e estáveis ​​nos resultados de saúde por décadas (mesmo durante o período especial "após a queda da URSS).
3. Apesar desses fatores, o sistema socialista na Hungria conseguiu melhorar muito a saúde da população, bem como o acesso dos trabalhadores ao bem-estar social. Essas melhorias não devem ser ignoradas.
