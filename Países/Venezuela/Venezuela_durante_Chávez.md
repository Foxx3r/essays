# Venezuela durante Chávez
As reformas empreendidas por Chávez ajudaram objetivamente os pobres, embora suas reformas dependessem de uma boa economia (que a Venezuela tinha na época). A porcentagem de pessoas na pobreza caiu muito. Caiu de 54% em 2003 para 26% em 2008. A pobreza extrema caiu ainda mais (um declínio de 72% para cerca de 7%). Além disso, a desigualdade caiu de forma extremamente acentuada.

![](https://imgur.com/a/RSwKPZE)

Em termos de saúde, a mortalidade infantil diminuiu 1/3 (21,4 a 14,2 óbitos/1.000 nascimentos), assim como a mortalidade infantil (26,5 a 17).

![](https://imgur.com/a/JBYJDeU)

A segurança alimentar também aumentou com Chávez, com a ingestão calórica média aumentando de 91% dos valores recomendados para 101,6% em cerca de 9 anos. As mortes por desnutrição caíram mais de 50%.

Em 1998, 80% dos venezuelanos tinham acesso à água potável e 62% ao saneamento. Em 2007, esses números eram 92% e 82%, respectivamente. Isso é um aumento de 4 milhões e 5 milhões, respectivamente.

![](https://imgur.com/a/8AFARcD)

Resumindo, entre 1999 e 2014 (mandato Chávez), o PIB venezuelano aumentou em 500%, a renda per capita aumentou em 2% ao ano, a FAO, organização da ONU que cuida sobre assuntos relacionados à nutrição e à fome, atestou a Venezuela como exemplo mundial de combate à fome, a pobreza foi reduzida em 49%, a pobreza extrema foi reduzida em 63%, até 2014, a Venezuela era o país com o menor coeficiente de Gini da América do Sul, a mortalidade infantil foi reduzida, a expectativa média de vida nacional aumentou e o IDH cresceu.
