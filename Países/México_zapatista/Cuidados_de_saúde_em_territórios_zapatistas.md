# Cuidados de saúde em territórios zapatistas
Antes do levante inicial, as pessoas no atual território do EZLN tinham muito pouco acesso a cuidados de saúde e viviam em condições de saúde realmente péssimas. De acordo com a National University of Ireland, Cork:
>Este descaso com a provisão de serviços de saúde adequados em Chiapas tornou-se um dos principais problemas que levaram o movimento zapatista a vocalizar suas preocupações. Essa visão rapidamente reuniu a comunidade zapatista para iniciar a construção de um sistema de saúde centrado em suas necessidades individuais, costumes e cultura. O sistema de saúde autônomo nasceu e o conceito rapidamente se tornou popular entre todas as comunidades zapatistas.

Desde a revolta do EZLN de 1994, os zapatistas fizeram grandes avanços na área da saúde nos territórios sob seu controle:
>É claro que os esforços investidos pelos zapatistas no estabelecimento de um sistema de saúde totalmente independente usando recursos limitados foram recompensados ​​pelos benefícios de saúde significativos alcançados em centenas de comunidades zapatistas. Os zapatistas testemunharam melhorias na saúde das mulheres e das crianças. Eles também viram os benefícios para a saúde decorrentes da melhoria da higiene como resultado do papel do promotor de saúde e de seu papel na educação da comunidade. A assistência médica autônoma despertou um senso de propósito nos corações e mentes dos habitantes da comunidade, enquanto eles lidam com confiança com os problemas de saúde de suas aldeias. Em essência, a saúde autônoma sem dúvida trouxe benefícios de saúde duradouros para o mundo dos zapatistas.

A Organização Mundial da Saúde declara:
>Existem atualmente cerca de 200 casas de saúde comunitárias e 25 clínicas regionais autônomas, algumas das quais já em funcionamento há 10 anos, e uma clínica odontológica... Se tivermos em mente que quase todas as instalações médicas foram construídas nos locais onde não existia antes, é fácil avaliar a importância de alcançar este objetivo graças ao esforço da comunidade.

A saúde das mulheres melhorou dramaticamente nos territórios zapatistas:
>A saúde autônoma está tendo um efeito positivo na saúde das mulheres e das crianças. Em regiões onde antes havia taxas significativamente altas de mortalidade durante o parto, agora houve um período de oito anos ou mais em que nenhuma morte materna foi registrada. Com a maior disponibilidade de parteiras locais e o conhecimento médico básico fornecido pelo promotor de saúde, as mulheres zapatistas agora estão dando à luz com segurança em suas próprias comunidades, com um risco de morte drasticamente reduzido. Além disso, os exames de câncer e de saúde sexual são realizados com maior frequência, melhorando ainda mais a qualidade de vida das mulheres.

A saúde das crianças também melhorou muito. A National University of Ireland, Cork declara:
>Assim como acontece com as mulheres, a melhora na saúde das crianças também merece destaque. Os zapatistas agora priorizam ativamente a vacinação das crianças, garantindo que o maior número possível de crianças receba a imunização necessária. Os promotores de saúde também são treinados para detectar os sintomas associados à sepse e icterícia em bebês recém-nascidos e, por meio da mídia visual, organizações estrangeiras estão ensinando os promotores de saúde a identificar e tratar essas doenças.

As comunidades zapatistas têm taxas de vacinação mais altas do que as comunidades pró-governo em Chiapas:
>Com a maior disponibilidade de assistência médica autônoma, 84% das comunidades zapatistas recebem vacinas importantes contra doenças como a malária. Nas comunidades pró-governo esse número é de apenas 75%, o que significa que menos desses habitantes da comunidade têm acesso às vacinas necessárias, apesar das promessas de saúde do Estado.

Além disso, os zapatistas reduziram significativamente os níveis de doenças infecciosas em suas comunidades:
>Uma disparidade maior é aparente entre as comunidades pró-governo e as aldeias zapatistas no que diz respeito ao tratamento da tuberculose. Atualmente, 32% dos habitantes zapatistas sofrem de tuberculose, enquanto em grandes porções das comunidades pró-governo, notáveis ​​84% ​​continuam a ter essa infecção respiratória.

Os zapatistas também melhoraram muito a infraestrutura de higiene e saneamento no território sob seu controle:
>Atualmente, 74% das comunidades zapatistas agora têm acesso a banheiros e isso levou a uma grande melhoria na higiene pessoal. Apenas 54% das comunidades pró-governo podem reivindicar acesso a banheiros em suas casas. Isso fornece evidências claras do impacto positivo que o promotor de saúde tem na educação das comunidades sobre saúde e higiene. Também sugere que as comunidades zapatistas têm apetite por conhecimento e estão dispostas a aprender tudo o que é necessário para proteger sua saúde.

Os zapatistas conseguiram até eliminar virtualmente o consumo de álcool em suas comunidades, o que teve um impacto extremamente positivo na saúde pública:
>Proclamada como uma das maiores conquistas de saúde para o movimento zapatista, a erradicação da fabricação e do consumo de álcool melhorou significativamente a saúde de todos os zapatistas. Esta política de não tolerância está diretamente ligada à redução de muitas doenças e infecções, incluindo “úlcera, cirrose, desnutrición y heridas con machete”.

No geral, os zapatistas fizeram uma série de avanços tremendos na área da saúde em suas comunidades. A Universidade Nacional da Irlanda, em Cork, observa:
>É claro, como demonstram esses números, que a saúde zapatista tem um impacto profundo na qualidade de vida de muitos indígenas que frequentam regularmente os serviços de saúde autônoma. A saúde autônoma zapatista tem se mostrado bem-sucedida, em muitos aspectos, na redução de doenças e promoção da independência da comunidade. Esta abordagem à saúde capacita as comunidades a acessar recursos e conhecimentos que abriram oportunidades sem paralelo para a sobrevivência entre comunidades indígenas remotas e isoladas.

A Organização Mundial da Saúde observa:
>Trata-se de um modelo que tem se mostrado capaz de impactar o que se pode denominar de nível primário de atenção à saúde, atuando dentro de uma estrutura de saúde organizada e como parte de um projeto político. Vista sob essa ótica, conseguiu inserir no cenário uma metodologia que o Serviço Nacional de Saúde não pôde ou não quis desenvolver.

Essas conquistas também conseguiram incutir um senso de dignidade e orgulho nessas comunidades, que antes eram tão mal exploradas. A National University of Ireland, Cork declara:
>Como resultado da saúde autônoma, as comunidades agora controlam seus serviços de saúde e, juntas, decidem sobre a política de saúde local e a nomeação de um promotor de saúde adequado. Portanto, é natural que essas comunidades desenvolvam uma autoconfiança e empoderamento não sentidos antes da introdução generalizada da saúde autônoma.

As conquistas da saúde nos territórios zapatistas são algumas das maiores conquistas da revolução.
