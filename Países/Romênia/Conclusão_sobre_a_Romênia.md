# Conclusão sobre a Romênia
A Romênia, pelo menos na época anterior aos empréstimos excessivos do Fundo Monetário Internacional e às medidas de austeridade, era um país com um governo que fazia o possível para tornar o país autossuficiente e, economicamente falando, competente. O período em que todos os romenos concordam em ser os piores foi aquele com as medidas de austeridade, que viriam por causa das cláusulas leoninas e armadilhas que os países ocidentais colocaram no caminho do país para desestabilizá-lo e torná-lo o mais contra-revolucionário possível.

Ceausescu, embora fosse extremamente ingênuo em matéria de política externa, não era um político mal intencionado, mas um verdadeiro crente do socialismo e um aliado da causa marxista.

Com isso, temos duras lições a aprender com a Romênia. Não podemos jamais confiar em instituições que se dizem "neutras", Bakhtin mesmo destrói o argumento de neutralidade ideológica em suas teses, confirmados pela hipótese Sapir-Whorf/línguistica relativista; como dizia Sankara: "Quem te alimenta, te controla".

Fontes:
- [Transylvania Now | Ceaușescu still most beloved President of Romania](https://transylvanianow.com/ceausescu-still-most-beloved-president-of-romania/)
