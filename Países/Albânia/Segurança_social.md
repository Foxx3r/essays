# Segurança social
O sistema de seguridade social era uma das principais prioridades do governo comunista. Com base no modelo soviético, os comunistas desenvolveram um sistema de bem-estar social do berço ao túmulo em todo o país. O estudo LSE acima mencionado:
>O seguro social foi introduzido pela primeira vez pelo governo comunista albanês em 1947. O esquema inicial de previdência social cobria aproximadamente 75.000 pessoas. O programa de seguro social era administrado por organizações estaduais e cobria assistência médica, indenização por invalidez, aposentadoria por idade, abono de família e descanso e recreação. Várias modificações foram feitas posteriormente no programa básico. A lei de 1953 forneceu um programa muito semelhante ao da União Soviética, ou seja, um sistema clássico de segurança social do berço ao túmulo. Durante vários anos, os sindicatos administraram um grande número de atividades de seguro social. Em 1965, o estado assumiu a administração de todas as fases, exceto as de descanso e recreação.

A licença maternidade e o seguro de invalidez foram fornecidos:
>Se as pessoas perdessem a capacidade de trabalhar total ou parcialmente, recebiam pensões de invalidez. O valor da pensão variava entre 40-85% do salário dependendo da escala de invalidez, causa da invalidez e número de anos de trabalho. As mulheres grávidas tinham licença de oitenta e quatro dias em circunstâncias normais e eram pagas a 95% do seu salário se tivessem trabalhado por mais de cinco anos e 75% se tivessem trabalhado menos de cinco. O período de licença-gravidez foi estendido para seis meses em 1981. As trabalhadoras podiam ficar em casa por períodos limitados para cuidar dos doentes e durante esse período recebiam 60% do seu salário.

Pensões de velhice também foram fornecidas a todos os trabalhadores aposentados:
>As pensões de velhice foram baseadas na idade e anos de trabalho. Os pagamentos foram calculados à taxa de 70% do salário médio mensal do trabalhador. Duas exceções foram os veteranos da Segunda Guerra Mundial e os líderes do Partido que receberam um adicional de 10%. A lei também previa pensões para viúvas e órfãos.

Todos os trabalhadores tiveram licença de trabalho garantida, com remuneração:
>Todos os segurados tinham direito a férias remuneradas. A duração das férias dependia do tipo de trabalho e da duração do vínculo empregatício.

O seguro de cuidados infantis também era um aspecto do sistema de seguridade social:
>Quando crianças menores de sete anos estavam doentes, um dos pais tinha licença de até dez dias durante um período de três meses. Um pagamento único foi feito à família para cada criança nascida. Em caso de morte, uma quantia fixa foi paga à família para despesas de funeral.

Eventualmente, o sistema foi expandido para incluir camponeses em cooperativas agrícolas:
>A partir de 1 de julho de 1972, o sistema de pensões e segurança social foi estendido aos camponeses que trabalhavam em cooperativas agrícolas. O objetivo era diminuir as diferenças entre as áreas urbanas e rurais. Algumas cooperativas agrícolas já haviam introduzido algumas formas de pensões e seguro social que fornecem ajuda para seus membros na velhice e quando eles não podiam trabalhar. O financiamento desse sistema de seguridade social no meio rural veio das contribuições das cooperativas com algum subsídio do Estado.

A provisão de um sistema de bem-estar social do berço ao túmulo é uma enorme conquista e um dos principais avanços feitos pelo governo comunista.
