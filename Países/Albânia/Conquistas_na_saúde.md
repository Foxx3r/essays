# Conquistas na saúde
Devido às condições de saúde extraordinariamente ruins que a população enfrentava antes de 1945, o novo governo comunista rapidamente fez da saúde uma prioridade:
>Conforme explicado anteriormente, quando os comunistas assumiram o poder na Albânia em 1945, o estado de saúde da população e o sistema de saúde albanês estavam em muito mau estado. [...] Para fazer frente a essa situação, o governo, a partir de 1947, introduziu um amplo plano de seguro social e médico. A maioria dos tratamentos médicos (embora não os medicamentos) era fornecida gratuitamente. Foi introduzida legislação para proteger a mãe e o filho e instituiu o regime de pensões, bem como outros regulamentos sobre as condições e controle sanitário e o tratamento de doenças infecciosas.

Os resultados dessas políticas podem ser vistos claramente ao se olhar para as estatísticas de saúde na Albânia. Por exemplo, pegue este gráfico que mostra o aumento na expectativa de vida ao nascer de 1950-1990, retirado do estudo LSE mencionado anteriormente (lembre-se de que a expectativa de vida em 1938 era de aproximadamente 38 anos):

```
1950 - 51.6
1954-55	- 55.0
1960 - 62.0
1964-65	- 64.1
1969 - 66.5
1975-1976 - 67.0
1979 - 68.0
1989 - 70.7
```

A partir dessas estatísticas, fica claro que os comunistas alcançaram um enorme aumento na expectativa de vida, de 38 anos em 1938 para 68 anos em 1979, um aumento de trinta anos em apenas quatro décadas. Continuou a melhorar até 1989, embora de forma mais lenta. Os comunistas também conseguiram eliminar várias doenças infecciosas que assolavam o país, especialmente a malária, que havia sido a maior causa de morte na Albânia antes da guerra:
>Uma série de doenças endêmicas foram controladas, incluindo malária, tuberculose e sífilis. Se olharmos para a transição da mortalidade de 1950 a 1990, é claro que o padrão muda conforme a expectativa de vida aumenta. Assim, as doenças infecciosas e parasitárias (incluindo a tuberculose) diminuem e quase desaparecem nas décadas de setenta e oitenta.

Resumindo a questão do desenvolvimento médico durante a era socialista, O'Donnell escreve que "é inquestionável que grandes avanços foram feitos na área de saúde". Ele continua:
>A expectativa de vida e as taxas de mortalidade infantil foram muito melhoradas. Os serviços de saúde como um todo, em termos de qualidade e, mais importante, em termos de disponibilidade foram melhorados exponencialmente.

Os comunistas albaneses alcançaram uma série de realizações extremamente impressionantes no campo da saúde. Infelizmente, isso também começou a sofrer no final do período comunista, quando a falta de investimento e o isolamento auto-imposto cobraram seu preço, causando a falta de tecnologia médica. O'Donnell escreve:
>Deve-se aplaudir objetivamente as melhorias feitas que foram substanciais e de fato um desenvolvimento positivo. No entanto, por outro lado, deve-se também condenar objetivamente a escassez de equipamentos e suprimentos modernos devido a preocupações ideológicas e isolamento.

Mesmo assim, ele conclui argumentando:
>A assistência médica deve ser considerada um desenvolvimento positivo na história dos anos de Enver Hoxha no poder na Albânia.
