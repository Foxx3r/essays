# Conquistas educacionais
A Polónia pré-comunista viu o analfabetismo generalizado e a falta de educação. De acordo com um artigo de 1935 da revista polonesa New Courier (não deve ser confundido com a New Courier de Varsóvia, um veículo de propaganda nazista fundado em 1939):
>Na Polónia, no povoado de Kobrin, menos de 75% escrevem e lêem nas cidades e apenas 52% no campo. Em Kosowski poviat, 82% em cidades pequenas e 43% em áreas rurais. No povoado de Koszalin, onde não há cidades, há apenas 30% que sabem ler e escrever.
>
>Polesie é de fato uma das áreas da Commonwealth que é econômica e culturalmente negligenciada, mas, deve-se lembrar, não é a mais negligenciada. Infelizmente, os dados dos poviats da Província de Varsóvia, ou seja, de uma posição economicamente bastante elevada e em órbita da influência da capital, mostram que a condição não é muito melhor lá também. No poviat de Płońsk, 73% escrevem e lêem nas cidades. População, 68% no campo, 77% em Sierpc e 68% em Ciechanów 80% (cidades) e 70% (aldeia).
>
>As estatísticas do censo são atuais tanto quanto há três anos. E os números dessa estatística não são apenas perigosos, eles são assustadores.

Após a conquista comunista, o sistema educacional foi drasticamente melhorado. O nível de analfabetismo foi reduzido drasticamente. De acordo com a Enciclopédia Polonesa publicada pela PWN (a principal editora de obras de referência científica e acadêmica na Polônia):
>Já em 1960, o censo mostrava 645.000 analfabetos totais e 270.000 semianalfabetos entre aqueles com mais de 50 anos. Em 1988, a taxa de analfabetismo na Polônia era de 2%.

Embora o PWN coloque as taxas de alfabetização pré-comunista um pouco mais altas do que o New Courier, ainda podemos ver a melhoria drástica na situação educacional feita sob os comunistas, particularmente nas áreas rurais.
