# Conclusão sobre a Polônia
A República Popular da Polônia dificilmente era uma nação perfeita. Teve numerosos conflitos internos, devido à influência social reacionária da igreja, bem como ao fato de que o socialismo polonês não se originou com uma revolução interna, mas sim com a influência soviética externa. Ainda assim, o socialismo na Polônia conseguiu melhorar muito a saúde da população, desenvolver a economia em um ritmo rápido e melhorar muito o sistema educacional.

Em suma, camaradas, devemos aprender algumas lições importantes da República Popular da Polônia:
1. Mesmo nessas condições não ideais, o socialismo é capaz de proporcionar um desenvolvimento econômico superior e qualidade de vida quando comparado ao capitalismo.
2. É essencial que uma revolução se concentre em eliminar os elementos reacionários da cultura nacional, para evitar uma situação semelhante à da Polônia, onde a Igreja Católica controlava amplamente a esfera social. No entanto, devemos também evitar ir longe demais nisso e perseguir a religião e as pessoas religiosas. A revolução não se opõe às crenças pessoais das pessoas; são as instituições religiosas reacionárias (como a Igreja Católica) que devem ser combatidas.
3. Não devemos ser conservadores na Revolução, não adianta tentar manter relações sociais já fragilizadas e arruinadas pela Revolução.

Essas são lições que devem ser aprendidas.
