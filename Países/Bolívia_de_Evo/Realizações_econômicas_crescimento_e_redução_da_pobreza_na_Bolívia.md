# Realizações econômicas, crescimento e redução da pobreza na Bolívia
As políticas econômicas de Evo Morales se concentraram na nacionalização de várias empresas importantes, no aumento dos direitos trabalhistas e do sistema de previdência social e na forte oposição ao FMI e ao Banco Mundial. O The Economist (uma publicação neoliberal) discutiu algumas das políticas de Morales em um [artigo](https://www.economist.com/the-americas/2009/12/10/the-explosive-apex-of-evos-power) sobre o tema:
>Em seu primeiro mandato, o Sr. Morales impôs muitos controles sobre empresas privadas. As telecomunicações e as minas, assim como o gás, foram nacionalizadas. Os preços do gás e de muitos alimentos foram controlados e os produtores de alimentos forçados a vender no mercado local em vez de exportar. Um novo órgão estatal distribuiu alimentos a preços subsidiados.

O artigo também afirma:
>A nacionalização do petróleo e do gás natural por Morales em 2006, junto com os preços mais altos das exportações de gás para o Brasil, deixou seu governo inundado de dinheiro. Ele usou isso para expandir a provisão de bem-estar, incluindo uma pensão não-contributiva por idade e pagamentos às mães, desde que seus filhos estejam na escola e seus bebês sejam levados para exames de saúde. O presidente também distribuiu centenas de tratores gratuitamente.

Essas políticas levaram ao maior crescimento econômico que a Bolívia já viu em décadas, e agora é a economia que mais cresce na América Latina. Isso é demonstrado por um [relatório](https://www.cepr.net/documents/publications/bolivia-2009-12.pdf) do Centro de Pesquisa Econômica e Política, que afirma que o crescimento sob Morales "tem sido maior do que em qualquer momento nos últimos 30 anos, com uma média de 4,9% ao ano desde que o atual governo assumiu em 2006". 

O relatório também comenta algumas das políticas redistributivas do governo Morales:
>O governo deu início a vários programas dirigidos aos bolivianos mais pobres. Isso inclui pagamentos a famílias pobres para aumentar a matrícula escolar; uma expansão das pensões públicas para reviver a pobreza extrema entre os idosos; e, mais recentemente, pagamentos para mães sem seguro para expandir os cuidados pré e pós-natal, para reduzir a mortalidade infantil.

Um [artigo](https://www.theguardian.com/commentisfree/2014/oct/14/evo-morales-reelected-socialism-doesnt-damage-economies-bolivia) do The Guardian, publicado após a eleição boliviana de 2014, analisa a importância do crescimento econômico da Bolívia e seu efeito na qualidade de vida:
>Os benefícios desse crescimento foram sentidos pelo povo boliviano: sob Morales, a pobreza diminuiu 25% e a pobreza extrema diminuiu 43%; os gastos sociais aumentaram em mais de 45%; o salário mínimo real aumentou 87,7%; e, talvez sem surpresa, a Comissão Econômica para a América Latina e o Caribe elogiou a Bolívia por ser “um dos poucos países que reduziu a desigualdade”.

O artigo termina comentando sobre Morales:
>Ele desafiou a sabedoria convencional que diz que as políticas de esquerda prejudicam o crescimento econômico, que a classe trabalhadora não pode administrar economias de sucesso e que a política não pode ser transformadora - e ele fez tudo isso em face de uma enorme pressão política do FMI, da comunidade empresarial internacional e do governo dos EUA. No sucesso de Morales, lições políticas importantes podem ser encontradas - e talvez todos possamos aprendê-las.

Bolívia, com aproximadamente 40% da economia sob algum tipo de propriedade estatal, fechou 2018 com uma das maiores taxas de crescimento do mundo.

De acordo com um [artigo](https://www.telesurenglish.net/news/Bolivia-Closes-2018-Among-The-Highest-Economic-Growth-Rates-20181208-0022.html) da Telesur:
>O modelo econômico seguido pela Bolívia é baseado na Produção Social Comunitária, amparado por uma forte participação do Estado em setores estratégicos, o que vai de encontro às recomendações do FMI, que busca a supressão dos subsídios e a redução dos investimentos públicos. A economia boliviana registrou em média um crescimento de 4,9% no período 2006-2017, onde mais de três milhões de pessoas saíram da pobreza. O PIB registrou um crescimento de 4,2% no ano passado, de acordo com o Relatório de Economia da Bolívia 2017.

A desigualdade econômica (medida pelo [Índice GINI do Banco Mundial](https://data.worldbank.org/indicator/SI.POV.GINI?locations=BO)) também reduziu drasticamente sob Morales (ele foi eleito em 2006).

Segundo sua própria Wikipedia:
>Durante a sua presidência, a pobreza extrema na Bolívia foi reduzida de 36,7% a 16,8% entre 2005 e 2015. O Coeficiente de Gini (utilizado para medir a desigualdade de renda) desceu de 0,60 a 0,47. Aumentado imposto sobre a grande indústria boliviana de hidrocarbonetos, ele usou esses novos lucros para criar políticas sociais de combate ao analfabetismo, pobreza, racismo e sexismo. Um crítico do neoliberalismo, buscou reduzir a dependência monetária da Bolívia do FMI e do Banco Mundial, apostando em uma economia mista. O PIB boliviano cresceu consideravelmente durante o governo Morales.[] Gozou de alta popularidade durante a primeira década que esteve no poder, sendo reeleito em 2009, em 2014 e, de forma controversa, em 2019. Ainda assim, seu governo foi marcado por acusações de corrupção e abuso de poder, com a Bolívia sendo ranqueada baixo nos índices de democracia global. Na política externa, Morales buscou boas relações com outros líderes de esquerda, como Hugo Chávez, Nicolás Maduro, Luís Inácio Lula da Silva e Cristina Kirchner.

Tudo isso deixa claro que as políticas econômicas do governo Morales têm sido extremamente bem-sucedidas no crescimento da economia e na redução drástica da pobreza, ao mesmo tempo que combate o neoliberalismo e o imperialismo.
