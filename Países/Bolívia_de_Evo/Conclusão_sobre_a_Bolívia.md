# Conclusão sobre a Bolívia
Embora Morales possa não ser tão esquerdista quanto gostaríamos, fica claro pelo que dissemos que seu governo (assim como o de Hugo Chávez) alcançou uma série de realizações extremamente impressionantes. Suas políticas reduziram a pobreza e a privatização, combateram vários males sociais e se concentraram fortemente no anti-imperialismo e na solidariedade internacional.

Em 2012, John Crabtree e Ann Chaplin descreveram os resultados de suas entrevistas com o povo boliviano, nas quais pediram sua opinião sobre Morales. Eles afirmaram que:
>Para muitos - talvez a maioria - dos bolivianos, este foi um período em que as pessoas comuns sentiram os benefícios da política de uma forma que não acontecia há décadas, ou nunca.

Durante a presidência de Evo Morales, a pobreza extrema na Bolívia foi reduzida de 36,7% a 16,8% entre 2005 e 2015. O Coeficiente de Gini (utilizado para medir a desigualdade de renda) desceu de 0,60 a 0,47. Aumentou imposto sobre a grande indústria boliviana de hidrocarbonetos, ele usou esses novos lucros para criar políticas sociais de combate ao analfabetismo, pobreza, racismo e sexismo. Um crítico do neoliberalismo, buscou reduzir a dependência monetária da Bolívia do FMI e do Banco Mundial, apostando em uma economia mista. O PIB boliviano cresceu consideravelmente durante o governo Morales.

Isso é algo que todos podemos apreciar.

Obs: Apesar da expropriação e nacionalização dos recursos petrolíferos brasileiros em atividade na Bolívia, o governo boliviano pagou uma indenização à Petrobrás.

Fontes:
- [The Guardian | Evo Morales has proved that socialism doesn’t damage economies](https://www.theguardian.com/commentisfree/2014/oct/14/evo-morales-reelected-socialism-doesnt-damage-economies-bolivia)
- [The Guardian | Bolivia opens 'anti-imperialist' military school to counter US foreign policies](https://www.theguardian.com/world/2016/aug/17/bolivia-anti-imperialist-military-school-evo-morales-us)
- [Center for Economic and Policy Research | Bolivia: The Economy During the Morales Administration](https://cepr.net/report/bolivian-economy-during-morales-administration/)
- [The Economist | The explosive apex of Evo's power](https://www.economist.com/the-americas/2009/12/10/the-explosive-apex-of-evos-power)
- [John Crabtreee & Ann Chaplin | Bolivia: Processes of Change](https://www.amazon.com.br/Bolivia-Processes-English-John-Crabtree-ebook/dp/B00CKPAHE2)
- [Wikipedia | Evo Morales](https://pt.wikipedia.org/wiki/Evo_Morales)
- [Mint Press News | Bolivia’s president declares total independence from World Bank, IMF](https://www.mintpressnews.com/bolivias-president-declares-total-independence-world-bank-imf/230062/)
- [Socialism, not neoliberalism, creates stability: Bolivia’s Evo](https://www.telesurtv.net/english/news/Socialism-Not-Neoliberalism-Creates-Stability-Bolivias-Evo-20170315-0038.html)
