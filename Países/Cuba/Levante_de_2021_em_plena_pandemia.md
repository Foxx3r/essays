# Levante de 2021 em plena pandemia
O que a mídia dos EUA tenta dizer como "levantes de massa contra o socialismo" (que é claramente uma tentativa de astroturfing) em Cuba VS o movimento socialista de massa da vida real em Cuba, sobre o qual a mídia dos EUA contará qualquer mentira para impedi-lo de descobrir.

![](images/cuba_riot_1.png)

![](images/cuba_riot_2.jpg)

Existem cerca de 11 milhões de pessoas em Cuba e mais de 1 milhão delas estão literalmente na marcha retratada na foto à direita. O povo de Cuba apoia EXPRESSAMENTE a revolução socialista e lutará para defendê-la.

As principais queixas dos jovens cubanos insatisfeitos com sua situação:
1. Receber US$30 por mês enquanto as sutilezas importadas, difíceis de conseguir, custam ainda mais em Cuba do que no exterior
2. Sem meios para viajar para o exterior
3. Escassez

Vê um padrão? Todos esses problemas econômicos são indubitavelmente causados por sanções. Essas reclamações são muito reais e sua frustração é compreensível. As sanções foram criadas para fazer isso. Uma vez que segmentos suficientes da juventude estão suficientemente frustrados e desejam mudanças, eles se tornam fáceis para aqueles com motivos ocultos manipulá-los. É assim que funcionam as revoluções coloridas. Eles se aproveitam da raiva existente e os direcionam para os alvos errados. Se você é um jovem cubano furioso que infelizmente aceitou a reação, o que parece mais fácil? Acabar com as sanções que estão em vigor três vezes mais do que você está vivo? Ou derrubar o governo sem pensar nas repercussões?

Lembre-se, a maioria das pessoas que participaram das revoluções coloridas na Europa Oriental ingenuamente pensaram que poderiam manter suas redes de segurança social enquanto melhoravam sua qualidade de vida e ganhavam acesso a bens de consumo estrangeiros mais modernos. Como diz Michael Parenti em Blackshirts and Reds:
>Um metalúrgico da Alemanha Oriental é citado como tendo dito "Não sei se há um futuro para mim e não estou muito esperançoso. O fato é que vivi melhor sob o comunismo" (New York Times, 03/03/91). Uma polonesa idosa, reduzida a uma refeição da Cruz Vermelha por dia: 'Não sou vermelha, mas devo dizer que a vida para os pobres era melhor antes... Agora as coisas são boas para os empresários, mas não para nós, pobres" (New York Times, 17/03/91). Uma mulher da Alemanha Oriental comentou que o movimento das mulheres da Alemanha Ocidental estava apenas começando a lutar "pelo que já tínhamos aqui... Nós tomamos isso como certo por causa do sistema socialista. Agora percebemos o que [perdemos]" (Los Angeles Times, 06/08/91).
>
>Dissidentes anticomunistas que trabalharam duro para derrubar a RDA logo expressaram seu desapontamento com a reunificação alemã. Um notável clérigo luterano comentou: "Nós caímos na tirania do dinheiro. A forma como a riqueza é distribuída nesta sociedade [Alemanha capitalista] é algo que considero muito difícil de aceitar." Outro pastor luterano disse: "Nós, alemães orientais, não tínhamos uma imagem real de como era a vida no Ocidente. Não tínhamos ideia de como ela seria competitiva ... Ganância descarada e poder econômico são as alavancas que movem esta sociedade. Os valores espirituais essenciais para a felicidade humana estão se perdendo ou parecendo triviais. Tudo é comprar, ganhar, vender" (New York Times, 26/05/96).
>
>Maureen Orth perguntou à primeira mulher que conheceu em um mercado se sua vida havia mudado nos últimos dois anos e a mulher começou a chorar. Ela tinha 58 anos, havia trabalhado quarenta anos em uma fábrica de batatas e agora não tinha como pagar a maioria dos alimentos do mercado: "Não é vida, é apenas existência", disse ela (Vanity Fair, 9/94). Orth entrevistou o chefe de um departamento de hospital em Moscou, que disse: "A vida era diferente há dois anos - eu era um ser humano". Agora ele tinha que conduzir as pessoas para obter uma renda extra. E quanto às novas liberdades? "Liberdade para quê?" ele respondeu. "Liberdade para comprar uma revista pornográfica?"

Mas o que se seguiu? Suas empresas estatais foram vendidas aos imperialistas por quase nada e o FMI veio dizer a seus novos governos como gastar seu dinheiro e organizar suas economias, em detrimento das grandes massas. Já vimos esse programa muitas vezes e se você acha que essa repetição é diferente, por favor, estude um pouco de história e pare de acreditar no hype.

Cuba costuma comerciar com outros países mas o embargo impede que as mercadorias cheguem, por exemplo no início da pandemia [o Alibaba doou algumas máscaras, ventiladores e testes rápidos](http://www.xinhuanet.com/english/2020-04/02/c_138940371.htm), mas a companhia aérea (acho que Avianca) tem investidores americanos e por isso as coisas nunca chegaram. O embargo custou à economia cubana [mais de 130 bilhões de dólares](https://www.reuters.com/article/us-cuba-economy-un/u-s-trade-embargo-has-cost-cuba-130-billion-u-n-says-idUSKBN1IA00T). Este número é semelhante às estimativas oficiais do governo cubano. O embargo proíbe o comércio de medicamentos, obrigando Cuba a redirecionar dinheiro para criar seu próprio remédio.

Cuba não tem grandes quedas d'água, petróleo ou qualquer coisa necessária para produzir energia. Não tem muitos recursos naturais, e não pode nem importar remédios de leucemia ou materiais de construção. Nem ouro para comércio secreto. Você já imaginou por que Cuba tem construções antigas com tinta descascando da parede? Porque toda empresa que envia suporte material à Cuba é multada pelos EUA. Cuba também é proibida de comprar remédios que tratam a leucemia.

[PDF](http://web.archive.org/web/20210603094916/https://medicc.org/ns/documents/The_impact_of_the_U.S._Embargo_on_Health_&_Nutrition_in_Cuba.pdf) sobre o impacto dos embargos econômicos dos EUA à Cuba, tudo em detalhes.

Bem, imagine que você não pode exportar ou importar qualquer mercadoria? Isso equivale a uma queda maciça do PIB, bem como ao subdesenvolvimento de alguns setores da economia, pois eles sempre precisarão de importações de outros países. Cuba precisa de importações de petróleo para manter sua economia e o preço do petróleo é em dólares americanos. O embargo significa que os dólares não podem ser obtidos por meio do comércio, então eles atualmente dependem de investimento estrangeiro direto que, por sua vez, requer empresas capitalistas incentivadas pela lucratividade que, por sua vez, envolve uma drenagem parcial da riqueza do país.

Isso também significa que as importações que logicamente deveriam vir de seu vizinho mais próximo estão vindo do outro lado do mundo, de modo que os custos de transporte são exponencialmente mais altos. Finalmente, significa que Cuba não pode usar sua própria indústria nacional como exportação e, em vez disso, tem que negociar com matérias-primas, o que resulta em troca desigual. Tudo isso culminou no embargo que custou centenas de bilhões de dólares em danos. Ninguém (países e empresas) está autorizado a fazer transações em pesos cubanos. Cuba perde milhões todos os anos mudando-os para dólares americanos. Em 2004, o banco suíço UBS foi multado em 100 milhões de dólares pelo Império por aceitar pesos cubanos.

Outro exemplo: se uma empresa fora dos EUA usa niquel cubano, por exemplo, na produção de um carro, essa empresa não pode vender esse carro nos EUA ou para uma empresa americana. Quem vai usar niquel cubano nessas condições?

Também há negacionistas de apoio chinês, mas a China vem [aos poucos defendendo](https://www.diariodocentrodomundo.com.br/essencial/embaixador-da-china-cita-confisco-de-ventiladores-na-pandemia-para-falar-do-boicote-dos-eua-a-cuba/) uma posição mais agressiva contra os EUA. Você acha que a Rússia e a China irão quebrar meia dúzia de protocolos internacionais para defender Cuba? Um país que geopoliticamente não tem valor algum? Você sabe como funciona geopolítica? No micro até vale uma caridade ou outra, macro é puro jogo de interesses e influências.

Não só isso, este caso também serviu para desviar a atenção do assassinato do presidente do Haiti, cujo governo estava prestes a ruir, e cujos [assassinos eram informantes dos EUA](https://www1.folha.uol.com.br/mundo/2021/07/suspeitos-de-assassinar-presidente-do-haiti-foram-informantes-dos-eua-diz-emissora-americana.shtml). Tudo isso, coincidentemente após Biden se tornar presidente e o diretor da CIA visitar a Colômbia e o Brasil (com plano de matar Maduro, segundo espiões venezuelanos relatam), [NÓS AVISAMOS](https://pcb.org.br/portal2/26694/joe-biden-o-senhor-da-guerra/)!

Em toda a Cuba, a classe trabalhadora respondeu ao apelo do presidente cubano Miguel Díaz-Canel, para ir às ruas para se defender e mostrar seu [apoio à revolução](https://communistparty.ie/en/2021/07/the-working-class-of-cuba-defends-their-revolution/).

![](images/people_with_diaz_canel.png)

O apelo foi feito em resposta a uma manifestação realizada em Havana por um grupo de contra-revolucionários, conhecidos por seus vínculos com agências de inteligência estrangeiras e que recebem financiamento dos Estados Unidos. Esses “protestos” receberam ampla cobertura de meios de comunicação apoiados pelo Ocidente. Cuba tem passado por momentos muito difíceis recentemente, com o colapso quase total de sua indústria do turismo resultante da pandemia global COVID-19.

As pessoas em Havana também sofreram vários bloqueios como parte dos esforços do governo para controlar a disseminação do vírus. Tudo isso, somado ao aumento do bloqueio ilegal imposto pelos Estados Unidos a Cuba, tem causado enormes desafios econômicos ao governo.

Por causa do bloqueio ilegal, Cuba não consegue nem importar seringas para dispensar a vacina contra COVID-19 que foi desenvolvida no país (e também houveram casos de saques e sabotagens, há um [artigo](https://countercurrents.org/2021/07/cuba-faces-loot-whose-hands-behind/) muito bom que fala sobre essa questão).

![](images/cuba_people_1.png)

![](images/cuba_people_2.png)

Há 60 anos o exemplo da Revolução Cubana incomoda os Estados Unidos, [afirmou](http://web.archive.org/web/20210715170941/http://en.granma.cu/cuba/2021-07-12/we-defend-the-revolution-above-all-else) o primeiro secretário do Partido Comunista de Cuba e Presidente da República, Miguel Díaz-Canel, durante mensagem especial do Palácio da Revolução explicando ao povo a mais recente provocação orquestrada por pequenos grupos de contra-revolucionários.

A AP está espalhando fake news, [este](https://twitter.com/AFP/status/1414414056065171456) protesto foi feito entre os quarteirões 32 e 37 da 8th Street em Miami. A polícia organizou uma área de protesto para eles. Repare no cara com chapéu do "MAGA" (Make America Great Again - simbolizando a América segregacionista).

["Presidente cubano Díaz-Canel: Revolucionários para as ruas!"](https://www.liberationnews.org/cuban-president-diaz-canel-revolutionaries-to-the-streets/)

Miguel Díaz-Canel [convocou](https://twitter.com/telesurenglish/status/1414369443845640194) os revolucionários a tomar as ruas, centenas encheram o centro de Havana para expulsar grupos que se reuniram para fazer protestos contra a Revolução.

Os cubanos gritam ["Eu sou Fidel!"](https://twitter.com/KawsachunNews/status/1414382277363224578), enquanto milhares inundam as ruas em defesa da revolução.

O povo [se mobiliza](https://twitter.com/BrunoRguezP/status/1414334964670373889) contra a campanha do imperialismo contra Cuba. Solidariedade internacional e cubanos residentes no exterior organizam apoio à Nação.

[Miguel Díaz-Canel](https://twitter.com/KawsachunNews/status/1414336773099032587) em San Antonio de los Baños, Mayabeque:
>Há um grupo de contrarrevolucionários mercenários pagos pelo governo dos Estados Unidos, pagos indiretamente por meio de agências do governo norte-americano para realizar esse tipo de manifestação.

["Novos programas para promover a democracia": EUA destinam 2 mi de dólares para "grupos independentes" em Cuba](https://dialogosdosul.operamundi.uol.com.br/america-latina/70489/novos-programas-para-promover-a-democracia-eua-destinam-2-mi-de-dolares-para-grupos-independentes-em-cuba). Orçamento dos EUA de Joe Biden [destinou 20 milhões de doláres](https://www.govinfo.gov/content/pkg/BILLS-117hr4373rh/html/BILLS-117hr4373rh.htm) este ano para financiamento de atividades golpistas em Cuba.

[Miguel Diaz-Canel](https://twitter.com/PartidoPCC/status/1414339204742586372) pelas ruas de San Antonio de los Baños, Mayabeque, acompanhado por quadros do território reafirmando que "la calle es de los revolucionarios" (a rua é dos revolucionários).

A [cidade de Palma Soriano](https://twitter.com/UJCdeCuba/status/1415330633732263939), reafirmando seu compromisso com a Revolução Cubana e com a maior liderança do país. Mostrando mais uma vez que las calles es de los revolucionarios e que aqui no se rinde nadie.

O povo cubano [toma as ruas](https://twitter.com/teleSURtv/status/1414380187991556098) de Havana para expressar o seu apoio ao Governo de Diaz-Canel em face das recentes provocações e campanhas de difamação.

["Cuba sim, ianques não!"](https://twitter.com/Mision_Verdad/status/1414342945612963841), ouve-se nas manifestações do povo cubano que saiu em defesa da Revolução.

[“Se você quer ter um verdadeiro gesto de apoio a Cuba, se quer se preocupar com o povo, levante o bloqueio e veremos como nos engajaremos”](https://twitter.com/pslweb/status/1414456989220884480) - Miguel Díaz-Canel

A desinformação sobre Cuba é flagrante: [o jornal The Guardian usou uma foto de cubanos se unindo com a bandeira de 26 de julho](https://twitter.com/BenjaminNorton/status/1414591175399514115) (nome do movimento revolucionário fundado por Fidel Castro) e alegou falsamente que são "antigovernamentais manifestantes". O The Guardian [corrigiu](https://theguardian.com/global-development/2021/jul/12/thousands-march-in-cuba-in-rare-mass-protests-amid-economic-crisis) discretamente suas notícias falsas, admitindo que as fotos que publicou de pessoas se manifestando em Cuba eram na verdade pró-governo, não anti-governo, como havia falsamente alegado antes.
>Este artigo foi alterado em 12 de julho de 2021. A legenda original da agência na imagem de pessoas no monumento Máximo Gómez as descreveu incorretamente como manifestantes antigovernamentais. Eles eram, na verdade, apoiadores do governo.

Mais notícias falsas sobre Cuba: [este](https://twitter.com/ElNuevoDia/status/1414588200551583751) meio de comunicação corporativo em Porto Rico está usando uma foto de cubanos se manifestando em apoio à revolução com a bandeira do Movimento 26 de Julho e alegando falsamente que são "antigovernamentais".

Proeminente bandeira de 'linha azul' no comício anti-Cuba/pró-intervenção em Miami.

![](images/blue_flag_miami-cuba.jpg)

[Análise](https://twitter.com/rojas4mayor2021/status/1414681011330461700) muito boa de como funciona o bloqueio imperialista de Cuba.

[68.000 contas falsas no Twitter foram criadas para apoiar o golpe na Bolívia](https://aristeguinoticias.com/2812/mundo/la-cidh-identifico-68-mil-cuentas-en-twitter-creadas-en-apoyo-a-gobierno-de-facto-en-bolivia/). Lembre-se disso se vir mensagens de spam de 'cubanos' com 2 seguidores. [E o Twitter ainda permite](https://sputniknews.com/20210712/bay-of-tweets-twitter-permits-anti-cuba-bot-army-despite-coordinated-inauthentic-behavior-rules-1083371864.html).

O renomado analista espanhol Julián Macías Tovar desmontou a intensa campanha articulada nas redes sociais digitais contra a Revolução Cubana nos últimos dias. Foi lançado do exterior e teve como referência o argentino Agustín Antonelli, um operador político de direita que participou de diversas operações contra os processos de esquerda na América Latina. A operação fez uso intensivo de robôs, algoritmos e contas que acabavam de ser criadas para a ocasião, a fim de criar um coro das mensagens emitidas pelos referentes da campanha manipulativa. O primeiro relato que utilizou a HashTag #SOSCuba relacionado à situação da COVID no país foi aquele localizado na Espanha. Postou mais de mil tweets nos dias 10 e 11 de julho, com uma automação de 5 retuítes por segundo. O pesquisador aponta como um dos referentes da operação o argentino Agustín Antonetti, que integra a direita Fundación Libertad.

Antonetti tem participado ativamente das campanhas de boatos e bots nas redes sociais contra os processos de esquerda na América Latina, entre eles o do boliviano Evo Morales e do mexicano Andrés Manuel López Obrador, conforme revelaram investigações anteriores, com base na sanção que o Facebook aplicou a inúmeras contas para operações políticas nas redes.

Lembro-me de um caso de uma moça cubana no Twitter tirando print de uma conversa com um policial que oferecia dinheiro para ela caso ela se revoltasse contra o governo cubano e mandasse fotos do vandalismo e motim. Infelizmente eu não salvei o tweet.

A seguir, um relato exclusivo de hoje de um camarada cubano:
>Não há notícias de novas provocações nas principais cidades de Cuba. E as forças revolucionárias (civis, policiais e oficiais de segurança do Estado, ainda se mobilizam). Mas a vida volta ao normal: a campanha de vacinação continua (agora com as segundas doses de Abdala), as pessoas vão para o trabalho e as medidas de restrição provocadas pela Covid19 ainda permanecem. Provavelmente a suspensão da Internet móvel e a prisão dos principais dirigentes (bem conhecidos a maioria deles pelo corpo de segurança há anos) (e a maioria deles será julgado nos próximos dias), e a grande repulsa do povo prejudica seus meios de comunicação, primeiro, e desmobilizá-los, segundo.. Sem comunicação com os centros de Miami ou Espanha, eles não podem receber instruções ou mostrar "resultados"... a maioria deles precisa mostrar "resultados" (provocações, ataques) para receber dinheiro. O estado cubano tem evidências suficientes sobre isso, a partir de outras intenções anteriores. Outro elemento, é que existem evidências de que parte dos desordeiros estava bêbada ou usando drogas. E também, para mais, a situação energética do país está estabelecida desde ontem. A partir do exterior, outros apelos a novas provocações são feitos, e continuam fazendo, mas não há resultados visíveis disso. A maioria dos desordeiros eram jovens sem estudos ou trabalho, ou qualquer intenção de fazer as coisas e pessoas que acreditam no "sonho americano" e querem emigrar, mas sem chance para isso. Também existe a probabilidade de que queiram reorganizar e aguardar a normalização dos serviços de Internet (a Internet móvel está disponível apenas à noite, mesmo no facebook) para forçar novas ações, após análise dos resultados dos primeiros “motins”. Mas, o tempo está passando e as forças revolucionárias trabalham para prender todos os líderes e neutralizar outras ações. A Revolução cubana não está desarmada e ainda não está usando todos os seus recursos. Então, agora é a hora de mentiras na internet, notícias falsas, etc, etc, tentando manter Cuba nos "tópicos de tendência" e empurrar o governo dos EUA ou da União Européia para "a ação". (a direita do parlamento europeu ataca o acordo entre Cuba e a UE).

De outro camarada de Cuba:
>A situação está totalmente sob controle, depois dos acontecimentos de ontem em alguns lugares de algumas cidades. Os elementos de contra-revolução, apoiados pelas agências governamentais dos Estados Unidos, e os ultras de Miami, tentaram usar alguma desconformidade de alguns cidadãos, especialmente com os recentes problemas com o fornecimento de energia elétrica, e a longa resistência ao impacto do Covid19 e o impacto do sanções de Trump (apoiado pela Administração Biden) para criar distúrbios, atacar a polícia e perturbar em algumas cidades. Mas o povo revolucionário os confrontou. Eles manipularam imagens da Argentina ou do Egito e "colocaram" essas "fotos" em Cuba.

Ele também mandou 2 artigos:
- [Minuto a Minuto: La Revolución se defiende en las calles](http://cubadebate.cu/noticias/2021/07/11/minuto-a-minuto-la-revolucion-se-defiende-en-las-calles/)
- [¿Buenos Aires o Alejandría son La Habana? La manipulación descarada de la realidad en Cuba](http://cubadebate.cu/noticias/2021/07/12/la-manipulacion-descarada-de-la-realidad-en-cuba/)]

Recomendo vocês a verem sobre a [anatomia de uma revolução colorida](https://www.reddit.com/r/InformedTankie/comments/ok53h5/luna_oi_a_guide_to_color_revolution/), feita pela camarada Luna Oi.
