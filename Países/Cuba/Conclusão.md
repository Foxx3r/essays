# Conclusão
Cuba é uma nação com muitos problemas; a economia desacelerou desde a queda da União Soviética (perder seu único grande parceiro comercial tende a prejudicar a economia de uma nação), e a pressão internacional dos EUA continua a colocar Cuba sob pressão. No entanto, as enormes conquistas da revolução não podem ser negligenciadas; Cuba forneceu padrões educacionais e de saúde de primeiro mundo com um orçamento de terceiro mundo, bem como nutrição e infraestrutura acima da média, ao mesmo tempo em que enfrentava a força imperialista mais poderosa do mundo, a apenas 90 milhas de sua costa.

Luta-se para encontrar uma declaração adequada para resumir as conquistas da revolução cubana. Talvez este, do ex-secretário-geral da ONU Kofi Annan (de 11 de abril de 2000):
>As conquistas de Cuba no desenvolvimento social são impressionantes, dado o tamanho de seu produto interno bruto per capita. Como o índice de desenvolvimento humano das Nações Unidas deixa claro ano após ano, Cuba deveria ser invejada por muitas outras nações, aparentemente muito mais ricas. [Cuba] demonstra o quanto as nações podem fazer com os recursos de que dispõem se se concentrarem nas prioridades certas - saúde, educação e alfabetização.

Talvez a melhor declaração seja dada por Aviva Chomsky em seu livro [A History of the Cuban Revolution](https://edisciplinas.usp.br/pluginfile.php/4447044/mod_resource/content/1/A-History-of-the-Cuban-Revolution.pdf):
>A revolução foi extremamente audaciosa, experimental e diversa. Ela evoluiu em circunstâncias muitas vezes adversas. Criou uma igualdade socioeconômica sem precedentes e mostrou ao mundo que é realmente possível para um país pobre do Terceiro Mundo alimentar, educar e fornecer cuidados de saúde para sua população. Se quisermos imaginar um mundo melhor para todos nós, Não consigo pensar em lugar melhor para começar do que estudar a Revolução Cubana.

Isso é algo que todos podemos apreciar.

Fontes:
- [Cornell University | Cuba's Food-Rationing System and Alternatives](https://www.coralgablescavaliers.org/ourpages/users/099346/IB%20History/Americas/Cuba/Cuba_s%20Food%20Rationing.pdf)
- [West Indian Medical Journal | Cuba: Healthcare and the Revolution](https://caribbean.scielo.org/scielo.php?pid=S0043-31442013000300015&script=sci_arttext&tlng=pt)
- [American Journal of Public Health | The Curious Case of Cuba](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3464859/)
- [The Guardian | Cuba: A Development Model That Proved the Doubters Wrong](https://www.theguardian.com/global-development/poverty-matters/2011/aug/05/cuban-development-model)
- [Oxfam America | Cuba: Social Policy at the Crossroads](https://web.archive.org/web/20090303221405/http://www.oxfamamerica.org/newsandpublications/publications/research_reports/art3670.html/OA-Cuba_Social_Policy_at_Crossroads-en.pdf)
- [UNdata | Country Profiles: Cuba](http://data.un.org/en/iso/cu.html)
- [2019 Global Hunger Index | Cuba](https://www.globalhungerindex.org/cuba.html)
- [Our World in Data | Deaths by Malnutrition in the USA and Cuba](https://ourworldindata.org/grapher/malnutrition-death-rates?tab=chart&country=CUB+USA)
- [Food and Agricultural Organization (United Nations) | Report on Nutrition in Cuba](http://www.fao.org/ag/agn/nutrition/cub_en.stm)
- [USDA | Cuba Food and Agricultural Situation](https://web.archive.org/web/20131105150934/http://www.fas.usda.gov/itp/cuba/CubaSituation0308.pdf)
- [World Food Program USA (United Nations) | Cuba Has "Largely Eliminated Hunger and Poverty"](https://www1.wfp.org/countries/cuba)
- [The Guardian | Why We Should Applaud Cuba's Progress Towards the Millennium Development Goals](https://www.theguardian.com/global-development/poverty-matters/2010/sep/30/millennium-development-goals-cuba)
- [Ecological Economics | The Sustainable Development Index: Measuring the Ecological Efficiency of Human Development in the Anthropocene](https://www.sciencedirect.com/science/article/pii/S0921800919303386)
- [Forbes | Every Country in the World is Developing, According to the New Sustainable Development Index](https://www.forbes.com/sites/christinero/2019/12/01/every-country-is-developing-according-to-the-new-sustainable-development-index/#4ce6dd7618bc)
- [Telesur | As World Burns, Cuba Number One for Sustainable Development](https://www.telesurenglish.net/news/As-World-Burns-Cuba-Number-1-for-Sustainable-Development-WWF-20161027-0018.html)
- [Global Footprint Network | Only Eight Countries Meet Two Key Conditions for Sustainable Development, as United Nations Adopts Sustainable Development Goals](https://www.footprintnetwork.org/2015/09/23/eight-countries-meet-two-key-conditions-sustainable-development-united-nations-adopts-sustainable-development-goals/)
- [World Wildlife Fund | The Other Cuban Revolution](https://wwf.panda.org/wwf_news/?1944/the-other-cuban-revolution)
- [National Association of Social Workers | Social Services in Cuba](http://www.ecocubanetwork.net/wp-content/uploads/NASW%20CUBA%20REPORT.pdf)
- [World Bank | Life Expectancy in the USA and Cuba](https://data.worldbank.org/indicator/SP.DYN.LE00.IN?locations=CU-US)
- [World Bank | Infant Mortality in the USA and Cuba](https://data.worldbank.org/indicator/SP.DYN.IMRT.IN?locations=CU-US)
- [Journal of Developing Societies | Cuban Public Healthcare: A Model of Success for Developing Nations](https://journals.sagepub.com/doi/abs/10.1177/0169796X19826731)
- [World Health Organization (United Nations) | Cuba First Country in the World to Eliminate Mother-to-Child HIV Transmissions](https://www.who.int/mediacentre/news/releases/2015/mtct-hiv-cuba/en/)
- [The Washington Post | USA to Test Cuban Lung-Cancer Vaccine](https://www.washingtonpost.com/news/to-your-health/wp/2016/10/27/in-a-first-u-s-trial-to-test-cuban-lung-cancer-vaccine/?utm_term=.9a6205587548)
- [International Journal of Epidemiology | Health in Cuba](https://academic.oup.com/ije/article/35/4/817/686547)
- [The Guardian | Healthcare in Cuba](https://www.theguardian.com/news/gallery/2007/jul/17/internationalnews)
- [CIA World Factbook | Cuba](https://www.cia.gov/the-world-factbook/countries/cuba/)
- [CIA World Factbook | Country Comparison: Educational Expenditures by Nation](https://www.cia.gov/the-world-factbook/field/education-expenditures/country-comparison)
- [World Bank | Great Teachers: How to Raise Student Learning in Latin America and the Caribbean](https://www.worldbank.org/content/dam/Worldbank/document/LAC/Great_Teachers-How_to_Raise_Student_Learning-Barbara-Bruns-Advance%20Edition.pdf)
- [HuffPost | World Bank: Cuba Has the Best Education System in Latin America and the Caribbean](https://www.huffpost.com/entry/world-bank-cuba-has-the-b_b_5925864)
- [Environmental Defense Fund | The Cuban Electric Grid](https://www.edf.org/sites/default/files/cuban-electric-grid.pdf)
- [UNdata | Country Profiles: Cuba](http://data.un.org/en/iso/cu.html)
- [New Republic | Polls Show Cubans Have Higher Political Satisfaction Than Americans](https://newrepublic.com/article/121502/cuban-poll-shows-more-political-satisfaction-americans)
- [Reuters | Cubans Overwhelmingly Ratify New Socialist Constitution](https://www.reuters.com/article/us-cuba-constitution-referendum-idUSKCN1QE22Y)
- [The Independent | My Family Live in Cuba - The People May Be Poor, But Fidel Castro's Legacy Will Live On](https://www.independent.co.uk/voices/fidel-castro-death-cuba-embargo-peasants-poor-urban-areas-modernisation-a7441036.html)
- [Oxford University Press | Cubans in Exile: A Demographic Analysis](https://www.jstor.org/stable/799194?seq=1#metadata_info_tab_contents)
- ["A History of the Cuban Revolution" by Aviva Chomsky](https://edisciplinas.usp.br/pluginfile.php/4447044/mod_resource/content/1/A-History-of-the-Cuban-Revolution.pdf)
- [Medium | Cuba — Gestão governamental orientada para a inovação: Contexto e caracterização do modelo.](https://medium.com/@bolivariandoapatriagrande/cuba-gestão-governamental-orientada-para-a-inovação-contexto-e-caracterização-do-modelo-564f9db8bc14)
- [Cuba Si | No puede concebirse una Cuba soberana que no sea antimperialista](http://web.archive.org/web/20210128131426/https://cubasi.cu/es/noticia/no-puede-concebirse-una-cuba-soberana-que-no-sea-antimperialista)
