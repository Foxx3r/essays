#;Críticas legítimas a Che Guevara
Che era um grande homem, mas não era perfeito. Por um lado, é inegável que ele foi cúmplice das políticas homofóbicas do governo revolucionário; se ele desempenhou um papel direto neles é desconhecido, mas ele certamente não fez nenhuma oposição visível.

São questões legítimas sobre as quais Guevara pode e deve ser criticado. Como marxistas, é essencial não cairmos na armadilha fatal da adoração ao herói; mesmo os maiores revolucionários continuam sendo humanos falíveis.

Che Guevara foi um dos grandes revolucionários do século 20; isso está além de qualquer dúvida. Suas ações durante e após a Revolução Cubana ajudaram a trazer uma vida melhor para milhões, enquanto sua imagem e influência foram beneficiadas por muitos mais. As tentativas de caluniá-lo como racista e assassino são vergonhosas e desonestas.

Apesar disso, ele não era perfeito; ele não fez nada para combater a homofobia em Cuba. Dito isto, ele continua sendo um dos maiores de todos os revolucionários proletários e uma inspiração para todos os que buscam um mundo melhor.

>Vale milhões de vezes mais a vida de um único ser humano do que todas as propriedades do homem mais rico da terra.<br/>
>– <cite>Che Guevara</cite>
