# Fidel era o homem mais rico do mundo?
Há alegações de que Fidel Castro tinha uma ilha particular e um aquário de golfinhos.

A ilha não era de Castro. Sempre foi do Estado cubano. Desde 1997 a Forbes e outras mídias mentem sobre as propriedades de Castro, atribuindo todas as terras e empresas do Estado cubano ao líder. Ninguém foi capaz de provar nada.

De fato, o nome de Fidel Castro apareceu na relação das pessoas mais ricas do mundo por quase 10 anos, entre 1997 e 2006, em um ranking da revista norte-americana Forbes. De acordo com a publicação, a fortuna do líder cubano seria, inclusive, maior do que a da rainha britânica Elizabeth. Fidel então, em 2006, desafiou a publicação: “Desafio que provem. E, se o provarem (...), renuncio ao cargo e às funções que estou desempenhando”. Ele fez o mesmo desafio ao então presidente George W. Bush, para que usasse as empresas e inteligência norte-americanas para provar que ele tinha US$1 que fosse.

A publicação não respondeu, o governo dos Estados Unidos tampouco e o nome do cubano foi retirado da lista nos anos subsequentes.

Mas de onde vêm esses números? A revista contava como sendo de Fidel as companhias de propriedade do Estado cubano. Em matéria publicada pela Folha de S.Paulo em 1997, a repórter Carleen Hawn explicou o “método” utilizado na contagem: “Tivemos de fazer uma estimativa", justificou. “Não temos conhecimento exato do que Fidel Castro tem ou deixa de ter. Não sabemos, por exemplo, se ele tem conta na Suíça ou algo do gênero. Estimamos que 10% da economia cubana passa pelas mãos dele, uma estimativa até muito conservadora". A própria repórter, à época, fez uma ressalva: o [número não significa que esta seja a fortuna pessoal](http://www1.folha.uol.com.br/fsp/mundo/ft150711.htm) do presidente de Cuba, mas a que poderia, eventualmente, vir a ser.
>Mas não é errado Fidel Castro ter rolex enquanto o povo passava fome?

Não, por um motivo simples: ele combateu a fome em Cuba, e tirou o país do mapa da fome. Ter rolex não o impediu de zerar a fome, o analfabetismo, os sem teto, reduzir a prostituição de Cuba. Não é como se ele estivesse tendo esses luxos enquanto o povo padecia. Não é como um Bruno Covas, que aumenta o seu próprio salário enquanto tira direito à gratuidade do ônibus de idosos de 60 anos. Qualquer um que critique Cuba sem entender a história do país, está errado.
