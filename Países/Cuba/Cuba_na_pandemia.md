# Cuba na pandemia
Há um [artigo](https://revistaopera.com.br/2020/05/28/o-sucesso-de-cuba-com-a-covid-19-explicado-de-dentro) explicando o enorme sucesso de Cuba e sua grande responsabilidade em lidar com a pandemia, apesar das dificuldades:
>Em Cuba, por conta do bloqueio, contamos com menos recursos ainda. A situação piorou, mas temos treinamento e estamos preparados para trabalhar sem recursos. É a população, por exemplo, que tem feito as grossas máscaras de tecido com três camadas.

De fato, Cuba foi [o primeiro país a vacinar os menores de 2 anos](http://english.hani.co.kr/arti/english_edition/e_northkorea/949419.html).
