# Beauvoir e a inveja
Vamos começar, trago aqui um parágrafo (página 167) onde Ana Campagnolo afirma, de maneira descarada e difundida em profunda ignorância e mal-caratismo, que Simone de Beauvoir:
1. Afirma que mulheres estão em situação tão infeliz que causava inveja da opressão de Outros (judeus/negros/proletários).
2. Que lamenta que, diferente de negros, judeus e proletários, a mulher não pode extinguir o homem.
>Para se aproveitar da abominável situação dos escravos, intentando colocar maridos e amos na mesma alçada, Simone de Beauvoir também chegou a afirmar que a condição das mulheres em relação aos homens era tão infeliz que causava inveja da condição dos negros escravizados, proletários e judeus. Os negros, segundo Beauvoir, ao menos, poderiam desejar uma humanidade toda negra, ou livre de brancos; já a mulher, como se queixa ela, infelizmente nunca poderia eliminar o homem. 

Ana, envergonhada por ter consciência de seu trabalho calunioso e desonesto, esconde as frases e afirmações originais de Beauvoir sobre essa situação.

Beauvoir começa a fazer paralelos sobre a mulher logo no início, onde afirma:
>as ciências biológicas e sociais não acreditam mais na existência de entidades imutavelmente fixadas, que definiriam determinados caracteres como os da mulher, do judeu ou do negro; consideram o caráter como uma reação secundária a uma situação. Para ela a opressão se expressa nos elogios às virtudes do bom negro, de alma inconsciente, infantil e alegre, do negro resignado, como na louvação da mulher realmente mulher, isto é, frívola, pueril, irresponsável, submetida ao homem.

Depois, continua
>Os judeus são "outros" para o anti-semita, os negros para os racistas norte-americanos, os indígenas para os colonos, os proletários para as classes dos proprietários... Os proletários dizem "nós". Os negros também. Apresentando-se como sujeitos, eles transformam em "outros" os burgueses, os brancos. As mulheres — salvo em certos congressos que permanecem manifestações abstratas — não dizem "nós". Os homens dizem "as mulheres" e elas usam essas palavras para se designarem a si mesmas: mas não se põem autenticamente como sujeito.

Como podemos ver até então, nada de Beauvoir menosprezando as opressões de outros Outros, mas, fazendo um paralelo entre essas relações e apontar que a mulher diferente desses Outros não se põe como "nós", transformando em Outro o homem, como fazem outros grupos oprimidos (sem em nenhum momento citar inveja ou semelhante).

Assim, Beauvoir aponta que:
>os proletários fizeram a revolução na Rússia, os negros no Haiti, os indo-chineses bateram-se na Indo-China: a ação das mulheres nunca passou de uma simbólica... A mulher é escrava de sua própria situação: não tem passado, não tem história, nem religião própria. Um negro fanático pode desejar uma humanidade inteiramente negra, destruindo o resto com uma explosão atômica. Mas a mulher mesmo em sonho não pode exterminar os homens. O laço que a une a seus opressores não é comparável a nenhum outro. A divisão dos sexos é, com efeito, um dado biológico e não um momento da história humana.

Beauvoir, diferente do apontado pela Campagnolo, não põe o amor e marido na mesma alçada, mas traça paralelos entre os dois usando a dialética mestre-escravo. Beauvoir, diferente do que é dito por Ana Campagnolo, em momento algum de sua obra lamenta com infelicidade que mulheres não possam se ver livres de homens, diga-se de passagem.

Na página 163, Ana Campagnolo, contextualizando ela, começa a usar sexismo biológico para tentar justificar a opressão sofrida por mulheres, até aí isso já cai bem mal. Logo em seguida, ela começa a citar uma frase da Simone de Beauvior descontextualizada, a frase completa seria essa:
>A mulher é mais fraca do que o homem; ela possui menos força muscular, menos glóbulos vermelhos, menor capacidade respiratória; corre menos depressa, ergue pesos menos pesados, não há quase nenhum esporte em que possa competir com ele; não pode enfrentar o macho na luta. A essa fraqueza acrescentam-se a instabilidade, a falta de controle e a fragilidade de que falamos: são fatos. Seu domínio sobre o mundo é portanto mais estrito; ela tem menos firmeza e menos perseverança em projetos que é também menos capaz de executar. Isso significa que sua vida individual é menos rica que a do homem. Em verdade, esses fatos não poderiam ser negados, mas não têm sentido em si.

Desde que aceitamos uma perspectiva humana, definindo o corpo a partir da existência, a biologia torna-se uma ciência abstrata; no momento em que o dado fisiológico (inferioridade muscular) assume uma significação, esta surge desde logo como dependente de todo um contexto; a "fraqueza" só se revela como tal à luz dos fins que o homem se propõe, dos instrumentos de que dispõe, das leis que se impõe.

Simone, de fato, já refutou o argumento de que existe uma “inferioridade natural” da mulher, em O Segundo Sexo.
