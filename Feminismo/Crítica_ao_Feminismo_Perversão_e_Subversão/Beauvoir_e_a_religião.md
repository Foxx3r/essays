# Beauvoir e a religião
Chegou o hora de falar sobre as inúmeras citações religiosas da Ana! Na página 16, Ana começa a citar sua crença como forma de argumentação. Ana também não mostra quase nenhuma crítica a Wollstonecraft, pelo o contrário, ela faz um grande elogio a ela sobre a tradição familiar, monogamia e casamento. Não há quase nenhuma postura crítica da autora em relação a Mary, mostrando uma postura moralista e retrógrada, esses trechos são só exemplos das citações religiosas da Ana, mas pode apostar que tem muito mais.
>Por sua vez, os homens, pela virtude do sacrifício, tinham de abnegar suas vocações espiritual e familiar justamente para manter livres e vivas aquelas de suas respectivas ajudadoras.<br/>
>[...]<br/>
>Nenhuma teóloga feminista, por exemplo, faz questão de notar o óbvio: o homem foi feito do barro e a mulher do homem. Ela não foi feita da lama, mas da carne. O que isso significa? No mínimo, que ela carrega uma origem mais refinada, organizada e - por que não? - superior.<br/>
>[...]<br/>
>A freira piedosa reza o rosário para o fiel descrente.<br/>
>[...]<br/>
>As vocações espiritual e familiar da mulher foram sobrepujadas por uma necessidade irrefreável de exercer a profissional, de modo que a mulher, que quando perguntada sobre seu estado dizia ser mãe, tia, avó ou esposa, seguindo-se sempre a declaração de seu credo - cristã, católica, protestante, espírita ou ortodoxa -, hoje declara ser médica, diretora, atriz ou professora, como se sua profissão fosse exercida mesmo enquanto reza ou troca a fralda do seu filho. <br/>
>[...]
>Outra importante constatação de Mary acerca da proteção exacerbada sobre as mulheres diz respeito ao casamento monogâmico. Ela escreveu sobre o "alto respeito que presto ao matrimônio como o fundamento de quase todas as virtudes sociais", defendendo que somente o casamento monogâmico pode assegurar proteção a mulher e seus filhos, sendo a variação poligâmica uma "degradação física [...] que destrói toda a virtude doméstica". Estava tão consciente do caráter protetor do matrimônio que afirmou: "Quando um homem seduz uma mulher, deveria ser obrigado por lei a manter a mulher e seus filhos, a menos que o adultério, um divórcio natural, revogasse a lei". Fica transparente aqui a tradição judaico-cristã do casamento monogâmico.<br/>
>[...]<br/>
>E pontuo: não há melhor termo para a mulher do que a expressão bíblica ajudadora.

Ana Campagnolo, como de costume, se apropria de trechos e reflexões de autoras feministas para montar espantalhos do que ela realmente diz, ela aponta que Simone deturpa a imagem de Virgem Maria, mas ao contrário, ela fala da imagem que ela é representada à sociedade como um modelo padrão a ser seguido e quem estiver de fora dele seria uma mulher vil, abjeta.
>Para Simone, o corpo é um fardo, livrar-se dele é começar a se libertar. Com a maternidade não é diferente. Estamos acostumados a pensar na gestação como fonte de vida e luz; mas, para ela, ao contrário, aceitar uma gravidez é aceitar ser "escravizada como mãe". Ao odiar a maternidade e a feminilidade, ela odeia, imediatamente, o símbolo máximo dessas virtudes, a Virgem Maria:
>
>A virgindade de Maria tem principalmente um valor negativo [...] pela primeira vez na história da humanidade, a mãe ajoelha-se diante do filho; reconhece livremente a própria inferioridade.

Simone diz: no coração da Idade Média, ergue-se a imagem mais acabada da mulher propícia aos homens:
>a figura da Virgem Maria cerca-se de glória. É a imagem invertida de Eva, a pecadora; esmaga a serpente sob o pé; é a mediadora da salvação como Eva o foi da danação. É como Mãe que a mulher é temível; é na maternidade que é preciso transfigurá-la e escravizá-la. A virgindade de Maria tem principalmente um valor negativo. Não é carnal aquela por quem a carne foi resgatada; não foi tocada nem possuída.<br/>
>[...]<br/>
>Maria também não conheceu a mácula que a sexualidade implica. Aparentada à Minerva guerreira, ela é torre de marfim, cidadela, torreão inexpugnável. As sacerdotisas antigas, como a maioria das santas cristãs, eram igualmente virgens. A mulher votada ao bem deve sê-lo no esplendor de suas forças intatas; cumpre que ela conserve em sua integridade indomada o princípio de sua feminilidade. Se se recusa a Maria o caráter de esposa é para lhe exaltar mais puramente a Mulher-Mãe. Mas é somente aceitando o papel subordinado que lhe é designado que será glorificada.<br/>
>[...]<br/>
>“Eu sou a serva do Senhor.” Pela primeira vez na história da humanidade, a mãe ajoelha-se diante do filho; reconhece livremente a própria inferioridade. É a suprema vitória masculina que se consuma no culto de Maria: é a reabilitação da mulher pela realização de sua derrota. Ichtar, Astarté e Cibele eram cruéis, caprichosas, luxuriosas; eram poderosas, fonte da morte como da vida, engendrando os homens, transformavam-nos em escravos.

O mais engraçado na colocação da Ana é como ela tira completamente de contexto o que Beauvoir queria dizer:
>A virgindade de Maria tem principalmente um valor negativo. Não é carnal aquela por quem a carne foi regastada; não foi tocada nem possuída.

Aqui ela não ataca Maria ou a virgindade por si só, como a Campagnolo faz parecer. No mesmo parágrafo, Beauvoir explica:
>A Grande Mãe asiática, não se lhe reconhecia tampouco um esposo: ela engendrara o mundo e sobre ele reinava solitariamente; podia ser lúbrica por capricho, mas nela a grandeza da Mãe não era diminuída pelas servidões impostas à esposa. Maria também não conheceu a mácula que a sexualidade implica.

Ou seja, se se recusa a Maria o caráter de esposa é para lhe exaltar mais puramente a Mulher-Mãe. Mas é somente aceitando o papel subordinado que lhe é designado que será glorificada.
