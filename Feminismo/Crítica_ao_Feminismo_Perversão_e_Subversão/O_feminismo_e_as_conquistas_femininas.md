# O feminismo e as conquistas femininas
Como todos sabemos, a Campagnolo adora afirmar que o feminismo não trouxe nada para as mulheres.

Na página 75 ela fala sobre a primeira onda feminista. Curiosamente logo abaixo ela cita John Stuart Mill:
>A reivindicação das mulheres em serem uniformemente educadas como os homens, nos mesmos ramos de conhecimentos, está crescendo intensamente

Na página seguinte ela diz:
>Nessa época, os homens deixaram definitivamente de ser o provedor ou chefe de família: urna pesquisa indicou que.apenas 8% ainda sustentava a casa sozinho.

Assim podemos entender que ou é muita coincidência os homens deixarem de ser "provedor ou chefe de família" na mesma época da primeira onda feminista, ou o pensamento dos homens resolveu mudar do nada em 1790? Enfim, contradições.
