# C6 e o golpe no INSS
O banco C6 está lavando dólares no Brasil e apoiando golpe no INSS. Eles aprovam empréstimo com assinatura falsa e o C6 empurra parcela discreta na folha de pagamento dos velhinhos, daí os golpistas recebem como se fossem correspondentes bancários comuns e ganham um por fora. Quando o empréstimo acaba, eles dão um mês e fazem outro empréstimo, para pensão ficar no mesmo valor. A C6 também está lavando dinheiro com maquininha de cartão de crédito.

Conheci um cara que vendia maquininha para eles e tomou calote da empresa, ele me explicou o que eles fazem, teve denúncia, mas sabe quem abriu o banco C6? Ex-sócio do Paulo Guedes. Os caras estão lavando muito dinheiro. O BTG Pactual, banco que o Guedes era sócio, criou um papel na bolsa que é um investimento estruturado. Uma das empresas no fundo de investimento por trás desse papel, é o C6 Bank. Ou seja, criaram um banco com laranja para lavar a grana. A Tim está dando um cartão de crédito para quem fizer conta pós-paga com eles até o final de Dezembro.

Não sei se a Tim está envolvida nos esquema. O cartão de crédito do PicPay é do Banco Original, felizmente, e acho que o Banco Original não se mete nesses esquemas.

Luís Nassif até mesmo foi censurado por divulgar esta informação.
