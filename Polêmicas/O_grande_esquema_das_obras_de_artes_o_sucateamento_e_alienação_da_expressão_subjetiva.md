# O grande esquema das obras de artes: o sucateamento e alienação da expressão subjetiva
O esquema das obras de arte ficou popular ultimamente.

O cara paga um artista para fazer qualquer merda, como por exemplo prender uma banana com fita adesiva na parede, daí o rico chama um "avaliador" de arte que tenha muitos contatos, e o avaliador diz que aquela obra vale 150 milhões. Então eles organizam uns eventos para elogiar a obra, e acham algum curador de museu que tenha interesse em uma nova exibição.

A banana presa com fita adesiva na parede (em referência à obra de Maurizio Cattelan que foi avaliada em 120 mil dólares) foi avaliada por 150 milhões, o ricasso doa a obra para o museu por 100 milhões, e abate os 100 milhões do imposto de renda. O museu faz um rio de dinheiro com um monte de gente xarope que vai lá ver a banana presa com fita adesiva na parede, e paga um "cafézinho" para o doador, como "agradecimento". É por isso que tem pia em museu, mictório em museu. Obras que só tem rabisco em museu.

Você olha aquilo e pensa, "essa merda eu faço com sono e cagando, ainda por cima", e realmente, é um monte de merda, porém, as pessoas certas chamaram aquilo de obra de arte, e ganharam um monte de dinheiro, e realmente é fácil de ser detectado, mas existe uma indústria por trás disso. Se você ameaçar o lucro dos caras, você morre. Quem tentou espalhar isso que eu falei aqui, se fudeu totalmente.
