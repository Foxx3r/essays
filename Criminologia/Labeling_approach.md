# Labeling approach
A grande mudança no seio da criminologia se dá com a criação do labeling approach (também conhecida como teoria do etiquetamento social, reação social e interacionismo), desenvolvida nos anos 60 na comunidade norte-americana.

O início desta teoria vem com a dramatização do mal, por meio da detenção, prisão e julgamento do detento pela primeira vez, muitas vezes na adolescência. A dramatização do mal detalha em 3 etapas como um criminoso vira um criminoso para a sociedade:
1. O indivíduo realiza atividades criadas a partir de um "distúrbio social"
2. Há uma mudança gradual da definição do indivíduo como mau, de modo que todos os seus atos passam a ser vistos com suspeita
3. Na fase 3, começa a teoria do rotulamento: do ponto de vista do indivíduo, ocorre uma mudança em seu autoconceito

![](images/labeling-approach.png)

Aquele que é escolhido e rotulado agora reconhece que a definição dele como um indivíduo é diferente das outras pessoas em sua comunidade.

Isso desempenha um papel maior em tornar o criminoso do que talvez qualquer outra experiência. O indivíduo vive em seu próprio mundo e se associa a outros como ele devido a essas mudanças. Portanto, uma vez que a rotulagem ocorre, o jovem começa a se envolver no mesmo comportamento que foi colocado ao seu redor. Um exemplo é um jovem que começou a fumar maconha para experimentar, apenas, mas ele passa a ser visto pela sociedade como um "maconheiro" e realmente "se torna" um. Esse processo, denominado rotulação, atribui certas características ao indivíduo, que será por elas expulso da sociedade honrada e recebido pela delinquencial, já que entre os outros delinquentes encontrará abrigo, reconhecimento, aceitação e prestígio.

Tannenbaum demonstra que a dramatização do mal vai separar os jovens do grupo fazendo que os mesmos assumam um papel construtor na criminalidade por meio da rotulação e identificação. Um breve comentário sobre criminologia clássica: autores criminólogos clássicos como Jeremy Bentham (pai da criminologia), Lombroso (pai do modelo positivista criminológico) e Clifford Shaw reconhecem que, de alguma forma, prisões contribuem para um aumento na criminalidade. Shaw em "The Jack-roller" faz uma análise compreensiva daqueles que "mesmo tendo cometido crimes pouco relevantes, são transformados em criminosos profissionais pela reação social das instituições correcionais para crianças e adolescentes".
>As relações sociais não surgem como determinada de uma vez por todas, mas como abertas e dependendo de constante aprovação em comum.<br/>
>– <cite>Herbert Blumer</cite>

Em sua obra Outsider, Howard Becker entende o "Outsider" como sendo uma pessoa que não é aceita como membro de uma sociedade, grupo ou clube, sendo tratada como diferente, não confiável para viver sob as regras de determinada sociedade. Becker analisa que os usuários de maconha sofrem o processo de estigmatização (serem vistos como algo "ruim" pela sociedade), então se cria uma identidade dos usuários de droga. Assim, o comportamento desviante é originado na própria sociedade. Uma visão materialista da sociedade, onde ela tem um fim nela mesma, e não depende de algo abstrato ou de caráter metafísico. Todo grupo social tem uma regra de desviação, assim, criando os seus próprios "outsiders". [2](https://meusitejuridico.editorajuspodivm.com.br/2017/07/04/o-que-se-entende-por-criminalizacao-primaria-e-secundaria/).

>O labeling desloca o problema criminológico do plano da ação para o da reação (from bad actors to powerful reactors), fazendo com que a verdadeira característica comum dos delinquentes seja a resposta das audiências de controle<br/>
>– <cite>Sérgio Salomão Shecaira</cite>

A solução, para Shecaira, seria a "política dos 4 Ds": Descriminalização, Diversão, Devido processo legal e Desinstitucionalização.

>Como atesta o funcionamento da Lei de Delinquência Juvenil, depende da sociedade oficial, em certo grau, carimbar algumas violações à referida lei como crimes ou apenas trangressões. A diferença de nomenclatura, longe de ser indiferente, decide o destino de milhares de homens, e a tonalidade moral da sociedade. A lei em si pode não apenas punir o crime, mas improvisá-lo, e a lei dos juristas é bastante apta para trabalhar nesta direção.<br/>
>– <cite>Karl Marx</cite>, População, crime e pauperismo.
