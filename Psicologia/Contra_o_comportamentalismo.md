# Contra o comportamentalismo
O comportamentalismo é a tendência reacionária da psicologia que rejeita a consciência como um possível assunto para a ciência e, em vez disso, visa desenvolver a ciência do controle do comportamento das pessoas. O verdadeiro fundador do comportamentalismo foi Ivan Pavlov, mas o comportamentalismo de Pavlov era de um caráter diferente do comportamentalismo que foi desenvolvido nos EUA por Skinner e outros.

O construtivismo, por outro lado, está preocupado com a forma como o sujeito usa a consciência para controlar seu próprio comportamento. Para o construtivismo, é possível tirar conclusões sobre a consciência a partir da colaboração do psicólogo com o sujeito, ao contrário da norma científica burguesa da ciência experimental, que exige o isolamento do sujeito experimental de qualquer forma de colaboração.

Originado por Watson da Universidade de Chicago, em 1913, o comportamentalismo surgiu como uma tendência na psicologia burguesa moderna, baseada filosoficamente no pragmatismo e no positivismo. O comportamentalismo continua a tendência mecanicista da psicologia, reduzindo os fenômenos psíquicos às reações do organismo. A tendência identifica consciência e comportamento cuja unidade principal considera ser a correlação estímulo-reação. O conhecimento, de acordo com o comportamentalismo, é inteiramente uma questão de reações condicionadas dos organismos.

Na década de 1930, a teoria de Watson foi substituída por uma série de teorias neocomportamentais, das quais os principais expoentes foram Hull, olman, Guthrie e Skinner. Os neo-comportamentalistas (exceto Tolman) emprestaram a terminologia de Pavlov e a classificação das formas de comportamento e substituíram o operacionalismo e o positivismo lógico pelos fundamentos materialistas de sua teoria. Skinner também abordou o processo de educação a partir de posições neocomportamentais e desenvolveu a teoria da instrução linear programada, que foi criticada por psicólogos soviéticos como Leontiev, Galperin e outros.

O behaviorismo de Skinner é muito mecanicista e não leva em conta os fenômenos cognitivos de forma alguma, como se o comportamento fosse apenas aparência.
>Toda a ciência seria supérflua se a forma fenomênica e a essência das coisas coincidissem diretamente.<br/>
>– <cite>Karl Marx</cite> (O Capital, t. III, Berlim, 1953, p. 870 - ed. alemã)

Enquanto fazem uso de técnicas de reflexo condicionado, os comportamentalistas ignoram o papel do córtex cerebral no comportamento. O comportamentalismo contemporâneo também modificou a fórmula estímulo-reação ao inserir o que se chama de "variáveis ​​intermediárias" (habilidade, potencial de excitação e inibição, necessidade, etc). Isso não muda, no entanto, a natureza mecanicista e idealista de sua essência.

O comportamentalismo também foi criticado por Pavlov em seu artigo, intitulado "A Physiologist's Answer to Psychologists" de 1932.
