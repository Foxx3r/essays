# Psicologia evolucionista
![](images/evolutionary_psychology.jpg)

**Universalidade das emoções básicas**

Raiva, alegria, surpresa, nojo, tristeza, medo (e suas leituras sociais através das expressões faciais) mostram semelhanças fundamentais onde quer que você vá (Ekman & Friesen, 1986), o que indica um forte componente filogenético.

**Herança tribal**

A mente humana se volta facilmente para a lógica tribalista, independentemente da orientação política (Haidt, 2012). É por isso que polarização e sectarismo surgem tão facilmente, enquanto que o racionalismo político precisa ser trabalhado e policiado frequentemente.

**Psicologia da religião**

Embora as religiões variem bastante em todo o mundo, a maioria inclui um conjunto básico de normas projetadas para a dependência e a gratificação, e todas elas cultivam uma abordagem orientada aos outros (Wilson, 2002).

**Redes sociais**

Quando se trata da experiência social, a mente humana responde como se ainda vivêssemos em pequenos clãs nômades com média de 150 pessoas (Geher et al., 2019). A rede social virtual reproduz a vida offline nesse aspecto: você provavelmente não interage com mais do que 150 pessoas pelo Facebook.

**Política**

A política mundial é em parte caótica pois a mente humana evoluiu para lidar com situações políticas de pequena escala em vez de situações políticas de grande escala (Geher et al., 2015).

**Estética**

Enquanto os detalhes da arte variam muito entre grupos e culturas humanas, várias formas de arte existem universalmente em todo o mundo. Isso sugere que a experiência estética está profundamente enraizada em nossa história evolutiva (Miller, 2000).

**Medicina**

Doenças como diabetes andam de mãos dadas com a prevalência em larga escala de alimentos processados. O conceito de incompatibilidade evolutiva nos ajuda a entender o porquê (Giphart & Van Vugt, 2018), já que nossas preferências alimentares foram programadas pela evolução para priorizar alimentos com alto teor calórico em detrimento de alimentos mais leves. Além disso, muitos sintomas físicos desagradáveis, como náusea encontrada em mulheres grávidas ou febre como resposta a uma infecção bacteriana, podem ser melhor entendidos como adaptações evolutivas anacrônicas (Nesse & Williams, 1994).

**Universalidade da violência masculina**

Machos de todos os outros primatas dimórficos tendem a ser mais violentos do que as fêmeas - pelo menos em média. Na nossa espécie, o padrão se repete: homens são muito mais violentos que mulheres em todas as culturas e períodos históricos conhecidos (Archer, 2009; Daly & Wilson, 1985). Essas diferenças têm implicações profundas para a criminologia como ciência preditiva e experimental (Ward & Durrant, 2015), além de surgirem cedo durante a infância (Nivette et al. 2014).

**Moralidade**

As emoções morais, como culpa, indignação e perdão, estão intimamente ligadas ao conceito de altruísmo recíproco ou à tendência dos seres humanos em desenvolver relacionamentos duradouros com os outros baseado na ajuda e confiança mútuas, que é um princípio básico no campo da psicologia evolucionista (Trivers, 1971; veja também Curry et al., 2016).

**Laços familiares**

Podemos entender o valor extraordinário que as pessoas atribuem a seus filhos com base simplesmente na ideia evolucionária de que a prole é o meio para transmissão dos genes dos parentais (Dawkins, 1976). Mesmo em sociedades sem núcleos familiares (pais, tios, primos, sobrinhos etc) bem definidos, existe um enviesamento natural de organização entre parentes de sangue. Isso também explica, pelo menos em parte, a razão do nepotismo aparecer tão facilmente em tantas culturas humanas e registros históricos diferentes (Betzig).
