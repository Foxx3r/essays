# Os problemas da incipiente psicologia
Os empiristas, como John Locke, estudavam a natureza humana, e diziam que as ideias se produziam através de estímulos ambientais. O maior problema para eles era descrever como ideias complexas podem se formar através de estímulos simples.

Kant acreditava que a ideia de espaço e tempo e conceitos de quantidade, qualidade e relação se originavam da mente humana e não podiam ser decompostos em elementos mais simples.

A filosofia deles se originava em Descartes, que afirma que o estudo do ser humano deve se reduzir ao corpo físico. No entanto, o escopo dessa discussão mudou com a publicação de 3 grandes livros (dos quais nenhum deles se considera e tampouco são considerados psicólogos atualmente):
1. A origem das espécies, de Charles Darwin, que afirmava a semelhança entre humanos e animais; levou a discussão para uma separação do homem e do animal.
2. A psicofísica, de Gustav Fechner, que propunha uma quantificação entre processos ambientais e seus impactos na mente humana.
3. Reflexos do cérebro, de Sechenov, que afirmava que o córtex pré-frontal funcionava por processos de inibição e excitação; ligava os estudos da filosofia aos estudos fisiológicos dos animais; foi uma dedicatória a Charles Darwin

Esses livros foram importantes para a psicologia, uma ciência jovem, se perguntasse: quais as diferenças entre o comportamento humano e o comportamento animal? Entre eventos ambientais e eventos mentais? Entre processos fisiológicos e psicológicos?

Wundt, criador da escola psicológica experimental, analisava a relação da mente humana com os estímulos ambientais e estudou os vários estados de consciência a partir de seus conteúdos: elementos simples. Wundt excluiu elementos como "consciência de si mesmo" e "percepção de relações" por os considerarem subprodutos de uma introspecção falha. Wundt propôs que, portanto, elementos complexos da mente (processos psicológicos superiores) não podiam ser estudados pela psicologia experimental, que só poderiam ser explicados através de estudos históricos dos elementos da cultura.

A segunda escola, discordava de Wundt da necessidade de analisar a mente a partir de seus elementos mais básicos, que demonstrou que muitos fenômenos intelectuais (ex: estudos de Kohler com macacos antropoides) e fenômenos perceptuais (ex: estudos de Wertheimer sobre o movimento aparente de luzes intermitentes) não poderiam ser explicados por elementos simples. Essa escola se chamava Gestalt.
