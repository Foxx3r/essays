# Lacan, topologia, psicanálise
Qual a diferença de usarmos a linguagem natural, números ou representações ideográficas para nos referirmos a uma mesma coisa? Deixo esta reflexão a cargo do leitor.

Lacan tenta apreender o Real, o Imaginário e o Simbólico por artifícios topológicos. A topologia é uma área matemática privilegiada – tem mais a ver com aspectos qualitativos do que quantitativos, mais com o papel do que o cálculo inscrito nele. O gênio de Lacan nos permite responder a pergunta "Qual o papel da topologia na psicanálise?" de duas maneiras: uma freudiana e uma lacaniana.

Freud acreditava na divisão entre 2 mundos reais: o externo e o interno (psíquico), e somente o interno tinha alguma chance de ser cognoscível. Dois problemas irão abalar e fazer a estremecer a teoria do aparelho psíquico em Freud: mesmo que possamos conhecer o real interno, é preciso haver um dispositivo externo que, porém, depende das condições do real interno. É como o problema do oráculo de Turing: é indecidível se eu posso confiar nele (e consequentemente, se eu tivesse uma prova de que eu posso confiar neste oráculo, seria equivalente a saber todo o conteúdo do real interno, assim sem necessitar do dispositivo externo). Porém, ao contrário do que esse paradoxo formulava, havia sim uma maneira de apreender esse real interno, sem paradoxos, e isso era feito por meio da experiência psicanalítica, que apreende o desejo do paciente. Como 2 mundos tão fundamentalmente diferentes estariam ligados, de uma forma que poderiam se comunicar um com o outro?

Freud, então, sem muito explicitar, no final de sua vida compreende o interno como tendo uma extensão no espaço e o espaço como tendo uma extensão no aparelho psíquico; isso resolvia o paradoxo. Porém, não foi essa a linha dominante da psicanálise que correu, cujos trabalhos posteriores se baseavam mais no que era propagado por Anna Freud. No entanto, surge Lacan, o herói desbravador de mundos topológicos e faz um retorno a Freud, mas um Freud maduro.

E qual o papel da topologia neste sentido? Basicamente é a melhor área da matemática para estudar continuidades – não há 2 reais, mas apenas um, unívoco. Diante do real do sexo do sujeito há os meios relativos aos significantes (sintomas) e os meios relativos aos objetos (fantasias).

**Toro: a demanda e o desejo**

Assim como o toro, essa díade é marcada pela repetição: precisamos repetir 2 voltas para que voltemos ao ponto de partida. A primeira volta é a demanda, que é direcionada ao Outro, e que inversamente retorna ao sujeito como demanda também. Só quando essa demanda se fecha, que podemos seguir para a segunda volta: a volta do desejo.

**Fita de Möbius: o sujeito dividido e seu dizer significante**
 
Como podemos aceitar que somos sujeitos e objetos, significados e significantes, ativos e passivos? O ser não é uma fita de Möbius: sua conexão com ela se dá no fato de que, ao realizar o corte da fita, ela já não é mais uma fita de Möbius. É assim com o ato de dizer: o sujeito representa, é representado e então desaparece. O sujeito aparece no corte da fita de Möbius.
 
**Garrafa de Klein: o significante e os outros**
 
Só há inconsciente onde há sintoma. Essa relação é expressa na garrafa de Klein, onde sob todos os ângulos podemos ver o círculo de reviramento da garrafa. Por exemplo, uma equação física, por mais repleta de formalismo matemático que seja, referencia algum processo real (mesmo que o custo deste formalismo seja uma certa arbitrariedade e distanciamento da realidade).

**Cross-cap: o sujeito e o objeto**
 
Como entender que o sujeito possa incluir em si e ao mesmo tempo se incluir num objeto que, no entanto, lhe é radicalmente exterior e heterogêneo? Como a fantasia, claramente uma imagem do psíquico, possa ser muitas vezes indistinguível da realidade? Se uma formiga caminha pelo cross-cap e passa pelas linhas de falsa intersecção, e faz isso 2 vezes, de modo que ela volte ao ponto de partida, ela passou do interno para o externo e do externo para o interno sem cruzar nenhum limite. Se pegarmos o trajeto desta formiga e realizarmos um corte no cross-cap, teremos uma fita de Möbius e um disco. Eis os 3 elementos da fantasia: o sujeito, o corte e o objeto.
