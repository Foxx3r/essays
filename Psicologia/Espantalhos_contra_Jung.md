# Espantalhos contra Jung
Um dos espantalhos é que Jung introduziu misticismo na filosofia com a ideia de inconsciente coletivo e por isso ele é anticiência. Na verdade, psicanálise que era pseudocientífica. Jung formulou a ideia dos arquétipos do inconsciente coletivo após observar que diversos de seus pacientes mesmo tendo representações mentais diferentes, tinham sonhos e arquétipos semelhantes.

Também tem o mito de que ele ignora o inconsciente pessoal. Jung jamais afirma isso, o que acontece de fato é que a psicologia analítica se preocupa mais em lidar com o desconhecido inconsciente coletivo, mas Jung os considera trabalhando junto em uma relação dialética na mente.

Outro espantalho é que Jung ignora a libido sexual. Não, Jung nunca fez isso, ele apenas descobre que há algo muito além da libido sexual de Freud, bem como algo muito além do inconsciente pessoal, o que o leva a começar "A jornada do herói" rompendo com a psicanálise.
