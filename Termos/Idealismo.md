# Idealismo
>A grande questão básica de toda filosofia, especialmente da filosofia mais recente, é aquela sobre a relação entre pensar e ser. As respostas que os filósofos deram a esta questão os dividiram em dois grandes campos. Aqueles que afirmaram o primado do espírito sobre a natureza constituíam o campo do idealismo. Os outros, que consideravam a natureza como primordial, pertencem às várias escolas do materialismo.<br/>
>– <cite>Friedrich Engels</cite>

Idealismo é um processo de pensamento (ex: racionalismo) de como o mundo material adere às idéias. Os idealistas seguem um certo conceito ideal (ex: fé) e entendem tudo desde sua adesão a esse conceito. O idealismo está em contraste com o materialismo, um processo de pensamento (ex. materialismo dialético) de como o mundo material cria ideias. Essas idéias assim criadas não são concretas e fixas, mas estão constantemente mudando e sendo remodeladas pelas diferenças e mudanças no mundo material.

Idealismo também pode ser entendido como a prática de compreender abstrações por meio de outras abstrações; onde uma abstração é algo que não tem necessariamente base nem relação com a realidade, mas só existe em relação a outras abstrações. A principal preocupação do idealista é criar conceitos que explicam adequadamente (e mudam de ponto de vista) o mundo como o conhecemos. O idealismo pode rejeitar a existência do mundo externo por completo (o mundo além do pensamento, além da sensação) ou afirmar que, embora possa existir um mundo além da sensação, ele é irreconhecível. Essas tendências são conhecidas como idealismo subjetivo. Por outro lado, o idealismo pode aceitar a objetividade da natureza, mas considerar o material como a expressão de forças ideais, como a vontade de Deus, a idéia absoluta, etc, cuja natureza é acessível diretamente na mente. Essas tendências são conhecidas como idealismo objetivo.

Para um exemplo de idealismo, o que se segue são as crenças de três filósofos idealistas proeminentes a respeito do que é verdade. Enquanto a verdade é um abstrato, ou ideal da realidade; os idealistas entendem essas abstrações equiparando-as a outras abstrações:

Descartes:
>Verdades são as coisas certas.

Husserl:
>Verdade é dúvida.

Hegel:
>O elemento em que se encontra a verdade é a noção.

Se você acha que a consciência precede a existência do ser, ou seja, que a interpretação do sujeito sobre o objeto vem antes do objeto em si, isso é idealismo.

O materialista, por outro lado, entende abstrações equiparando-as à realidade.

Marx:
>A verdade é conhecida pela prática. Já tivemos mais de uma ocasião para nos familiarizar com o método [idealista]. Consiste em dissecar cada grupo de objetos de conhecimento até o que se afirma serem seus elementos mais simples, aplicando a esses elementos igualmente simples e o que se afirma serem axiomas evidentes, e então continuar a operar com a ajuda dos resultados assim obtidos. Mesmo um problema na esfera da vida social "deve ser decidido axiomaticamente, de acordo com formas básicas simples e particulares, exatamente como se estivéssemos lidando com as formas simples básicas da matemática" {D. Ph. 224}.

E assim, a aplicação do método matemático à história, à moral e ao direito é nos dar também nesses campos a certeza matemática da verdade dos resultados obtidos, para caracterizá-los como verdades genuínas e imutáveis. Isso é apenas dar uma nova guinada ao antigo método ideológico favorito, também conhecido como método a priori, que consiste em averiguar as propriedades de um objeto, por dedução lógica a partir do conceito do objeto, ao invés do próprio objeto. Primeiro, o conceito de objeto é fabricado a partir do objeto; então o cuspe é girado e o objeto é medido por seu reflexo, o conceito. O objeto é então adaptar-se ao conceito, não o conceito ao objeto. Com Herr Dühring, os elementos mais simples, as abstrações finais que ele pode alcançar, prestam serviço ao conceito, que não altera as coisas; esses elementos mais simples são, na melhor das hipóteses, de natureza puramente conceitual. A filosofia da realidade, portanto, prova aqui novamente ser pura ideologia, a dedução da realidade não de si mesma, mas de um conceito.

E quando tal ideólogo constrói a moral e o direito a partir do conceito, ou dos chamados elementos mais simples da "sociedade", em vez das relações sociais reais das pessoas ao seu redor, que material está então disponível para essa construção? Material claramente de dois tipos: primeiro, o escasso resíduo de conteúdo real que pode sobreviver nas abstrações das quais ele parte e, em segundo lugar, o conteúdo que nosso ideólogo mais uma vez introduz de sua própria consciência. E o que ele encontra em sua consciência? Em sua maioria, noções morais e jurídicas que sejam uma expressão mais ou menos precisa (positiva ou negativa, corroborativa ou antagônica) das relações sociais e políticas em que vive; talvez também ideias retiradas da literatura sobre o assunto; e, como última possibilidade, algumas idiossincrasias pessoais. Nosso ideólogo pode virar e torcer como quiser, mas a realidade histórica que ele lançou à porta entra novamente pela janela, e enquanto ele pensa que está formulando uma doutrina de moral e lei para todos os tempos e para todos os mundos, ele está, na verdade, apenas criando uma imagem das tendências conservadoras ou revolucionárias de sua época - uma imagem que é distorcida porque foi arrancada de sua base real e, como um reflexo em um espelho côncavo, está de cabeça para baixo.

Dialética não é só contradições, mas sobretudo síntese: a famosa "unidade de contrários". Se o sujeito só enxerga oposições estanques e absolutas, sem conexões e transições entre os contrários, desconfie: ele pode se dizer "dialético", mas está mais afeito às antinomias kantianas. Como dizia Trotsky:
>Dar golpes juntos, marchar separados.
