# Existencialismo
O existencialismo é uma corrente diversa de filósofos, que compartilham uma distinção entre as categorias de Ser (ser) e Existir (aí), sustentando que o Ser não pode ser entendido através do pensamento racional e da percepção, mas apenas através da existência pessoal. O existencialismo tem suas raízes na reação do século 19 contra o racionalismo “impessoal” do Iluminismo, Hegelianismo e Positivismo, especialmente Nietzsche e Kierkegaard. Também inclui a Fenomenologia de Edmund Husserl. Seus fundadores são Martin Heidegger, Karl Jaspers, Simone de Beauvoir e Jean-Paul Sartre.
>O modo de produção não é um regime jurídico - mas uma forma de relações sociais e estruturas sociais.

Leituras:
- The Ethics of Ambiguity
- Existentialism is a Humanism
