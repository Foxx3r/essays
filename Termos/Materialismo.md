#;Materialismo
Essas tendências filosóficas que enfatizam o mundo material (o mundo fora da consciência) como fundamento e determinante do pensamento, especialmente em relação à questão da origem do conhecimento. Compare com idealismo. Para o materialismo, os pensamentos são “reflexos” da matéria, fora da Mente, que existia antes e independentemente do pensamento. De acordo com Marx em Teses sobre Feuerbach:
>O principal defeito de todo materialismo até agora existente - o de Feuerbach incluído - é que a coisa, realidade, sensualidade, é concebida apenas na forma de objeto ou de contemplação, mas não como atividade humana sensual, prática, não subjetivamente.

Se quisermos nos envolver seriamente com a questão do materialismo dialético, devemos tomar como ponto de partida o ditado de que "a substância é material". A substância, de qualquer maneira que a concebemos - matéria, idéia, espírito - deve precisamente perder seu domínio substancial, deve passar para o outro, perder-se como substância, seja o que for que consideremos substância; deve ser abandonado como o primeiro princípio. E se houver uma única palavra hegeliana que se possa tomar como diretriz para esse processo, essa palavra seria Sichanderswerden, traduzida de forma bastante apropriada como "tornar-se outro consigo mesmo".

Leituras adicionais:
- Família sagrada (Marx)
- A ideologia alemã (Marx)
- As três raízes e as três partes componentes do marxismo (Lenin)
- Second Attitude of Thought to Objectivity (Hegel)
