# Pseudoconservadorismo
Adorno fala que "os pseudoconservadores chegam a deturpar os preceitos do conservadorismo em nome de anseios antidemocráticos". A estrutura psicológica deles seria composta por convencionalismo e submissão autoritária na esfera do eu e por violência, impulsos anárquicos e destrutividade caótica na esfera inconsciente.

O pseudoconservador, em nome da preservação dos valores e instituições tradicionais e da defesa deles contra perigos no geral fictícios (ex: o fantasma do comunismo), consciente ou inconscientemente visa a sua abolição, há um forte impulso anárquico no fundo.

Para eles a democracia está muito longe do povo e deve ser substituída por um sistema indefinido de braço forte, este discurso serve como um disfarce democrático para desejos antidemocráticos.

Eles defendem uma pseudorevolução. Existe uma vaga ideia de mudança violenta, sem qualquer referência concreta ao objetivo das pessoas envolvidas. Soa um rompimento súbito e violento, mas parece mais uma medida administrativa. É uma ideia rancorosa, rebelde e passiva. Há uma identificação com os grupos sociais mais elevados como um meio de ser admitido ao grupo dominante já que a ascensão econômica se torna cada vez mais difícil.

Em resumo: é um "vamos mudar tudo isso que está aí" mas sem mudar nada. Bolsonaro é um dos proponentes deste fenômeno.
