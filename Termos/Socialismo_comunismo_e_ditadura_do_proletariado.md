#;Socialismo, comunismo e ditadura do proletariado
Socialismo é a fase de transição para o comunismo. Vai envolver industrialização, reforma agrária, criação de conselhos populares, obtenção de poder político pela classe trabalhadora, etc. O comunismo seria a abundância da produção, então o espertalhão querendo tirar proveito já não existe mais. Você pode até pensar isso do socialismo, a transição, mas isso é falso também. O socialismo impede esse tipo de gente de ganhar poder, pois há uma impossibilidade material nisso.

Socialismo é o sistema que visa socialização dos bens de produção. É considerado pelos marxistas, um meio de transição para o comunismo. Comunismo é o sistema político sem qualquer tipo de desigualdade hierárquica, onde todos são da classe trabalhadora e trabalham por livre associação.

A ditadura do proletariado é claramente posta como um momento de transição para o comunismo.

Entre a sociedade capitalista e a sociedade comunista - continua Marx - situa-se o período de transformação revolucionária da primeira para a segunda. A esse período corresponde um outro, de transição política, em que o Estado não pode ser outra coisa senão a ditadura revolucionária do proletariado.

Mas o socialismo já é comunismo, contudo em sua fase inicial. O de que se trata aqui é de uma sociedade comunista, não tal como se desenvolveu na base que lhe é própria, mas, ao contrário, tal como acaba de sair da sociedade capitalista; por conseguinte, de uma sociedade que, sob todos os pontos de vista, econômico, moral e intelectual, traz ainda os estigmas da antiga sociedade de cujos flancos sai. É essa sociedade comunista que acaba de sair dos flancos do capitalismo, e que ainda traz todos os estigmas da velha sociedade, o que constitui para Marx a "primeira" fase, a fase inferior do comunismo.

Assim, na primeira fase da sociedade comunista, corretamente chamada socialismo, o "direito burguês" é apenas parcialmente abolido, na medida em que a revolução econômica foi realizada, isto é, apenas no que respeita aos meios de produção. O "direito burguês" atribui aos indivíduos a propriedade privada daqueles. O socialismo faz deles propriedade comum. É nisso, e somente nisso, que o "direito burguês" é abolido.

Contudo o Estado ainda tem um papel mesmo nessa fase inicial do comunismo, porque ainda existem características do capitalismo a serem extintas. Mas, o Estado ainda não sucumbiu de todo, pois que ainda resta salvaguardar o "direito burguês" que consagra a desigualdade de fato. Para que o Estado definhe completamente, é necessário o advento do comunismo completo.

A própria organização do capítulo deixa claro que existe a transição para o comunismo (ditadura do proletariado - quando Marx diz que a ditadura do proletariado ainda não é socialista, é porque ela ainda é capitalista de Estado) e duas fases do comunismo.
