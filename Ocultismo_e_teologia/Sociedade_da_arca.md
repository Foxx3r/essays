# Sociedade da arca
A sociedade da arca foi uma sociedade secreta que foi criada há 5 mil anos para prevenir que o conhecimento humano fosse perdido, daí eles se separaram para que o conhecimento ficasse disperso. Muitas sociedades secretas como a arca, as abelhas, a rosacruz etc perderam muito conhecimento subjetivo porque as mesmas estavam concentradas na Europa e sofreram demais com a Primeira Guerra Mundial. Eles foram burros ao ficar concentrados na Europa e morreram quase tudo na Primeira Guerra Mundial.

O resto morreu na Segunda Guerra Mundial.

Eu sei de tentativas para tentar reativar a sociedade das arcas, mas não teve muita força. Por outro lado eu também sei de sociedades secretas que tiveram sucesso em se reinventar, como a Shorinji Kempo, que se reinventou em artes marciais. Kung-fu foi coisa de sociedade secreta monástica por mais de mil anos na China e Índia. O templo shaolin que era fechado, hoje tem visitação do público. Há 200 anos atrás, você só entrava em certos templos se fosse monge, se fosse alguém muito importante, ou se estivesse morto. Não sei se isso aconteceu com o kempo ou o shotokan.
