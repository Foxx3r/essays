# Por que a educação no socialismo é melhor
Por que o sistema educacional é melhor no socialismo e por que o sistema educacional capitalista é uma piada?

A educação no capitalismo é um instrumento de discriminação social, um fator de marginalização. A sociedade é essencialmente marcada pela divisão de classes, sendo a marginalidade inerente à própria estrutura da sociedade, logo, a educação é inteiramente dependente da estrutura social, servindo como fator de marginalização. Assim, a função da escola consiste somente na reprodução da sociedade em que está inserida. Um sistema de manutenção da ordem, assim como o aparelho policial, e mais recentemente no capitalismo, a indústria cultural.

No capitalismo, a educação - como todas as esferas da vida - é estruturada de forma a favorecer a finalidade central de nossa sociedade: gerar lucro para a burguesia. Nosso sistema de ensino é profundamente marcado por preocupações voltadas ao mercado de trabalho, buscando lapidar o indivíduo para torná-lo cada vez mais apto à gerar a riqueza que virá a ser tomada de suas mãos. Assim, toda a dimensão cultural, criativa e lúdica do processo de aprendizado é negligenciada, visto que não se mostra útil para o enriquecimento da classe dominante. Para nós, trabalhadores, a educação deveria ser o instrumento principal de compreensão do mundo, permitindo apreender, questionar e degustar a realidade que nos cerca.

Não apenas a lógica da educação é corrompida na sociedade liberal, mas também as formas de acesso a ela. Enquanto os comunistas defendem a educação enquanto um direito fundamental que deve ser universalizado, os liberais atuam em favor dos oligopólios privados da educação, sucateando e boicotando todo o aparato público da educação. Os conflitos entre setor público e privado no que diz respeito a educação nada mais são do que um desdobramento da luta de classes, onde para se manterem relevantes, os barões da educação fazem do ensino um mercado e da educação uma mercadoria.

Nós, comunistas, defendemos a construção da escola e da Universidade popular, onde o ensino seja universal, gratuito e busque a superação dos desvios individualistas em pro? de um mundo sustentado na coletividade, pela emancipação humana para além dos limites do capital.

A educação não é uma mercadoria! Se você paga, não deveria.

Primeiramente, o que é uma mercadoria? É um objeto ou relação, produto do trabalho humano, que satisfaz dadas necessidades humanas e possui um valor de troca. Exemplo: PFF2 tem como o valor de uso te proteger do coronavírus, mas tem como valor de troca $3,90.

No capitalismo, o valor de troca subverte o valor de uso. O sentido da produção está nos lucros e não nas necessidades humanas.
>O capital não tem a menor consideração pela saúde ou duração da vida do trabalhador, a não ser quando a sociedade o força a respeitá-la.<br/>
>– <cite>Karl Marx</cite>

Através da intensa luta de classes, os trabalhadores conquistam o acesso a direitos que só são viabilizados com os impostos (no caso de uma economia fiduciária, veja a secção sobre [MMT](#mmt)).

O Brasil é um dos poucos países do mundo com um sistema de imposto regressivo (proporcionalmente, cobra muito do trabalhador, das camadas médias e pouco de grandes empresários). Somos 1 dos 3 únicos países no mundo (Brasil, Estônia e Letônia) que não taxam dividendos. Grandes empresários lucram milhões com a compra de ações de empresas e não pagam nada de imposto nesse processo. 50% da arrecadação de impostos no país é sobre o consumo, que para famílias de classe média/baixa significa a maior parte de sua renda familiar. Isso é artificial. Nós poderíamos formar muito mais médicos e professores do que formamos hoje, e isso me lembra uma coisa triste: na Federação Russa de hoje, existem mais prostitutas do que engenheiros e professores somados. É um país destruído, assim como o Brasil, que é o país mais violento do mundo - se mata mais do que em países em estado contínuo de guerra. Precisamos de um sistema de impostos que cobre mais dos ricos e menos dos pobres! Beneficiando assim o conjunto da sociedade em bem-estar e direitos.

No capitalismo, a lógica da mercadoria tende a se expandir para cada instância da vida social. Assim, a educação seria meramente um ativo a se investir e lucrar. Para os comunistas, a educação é um complexo constitutivo do ser social. Precisa ser universal, gratuita, voltada para as necessidades dos trabalhadores e buscar a emancipação humana para além dos limites do capital.

Os pré-Althusserianos consideram que toda sociedade constitui-se como um sistema de relações de força material entre grupos ou classes. Sobre a base da força material surge um sistema de relações de força simbólica cujo objetivo é reforçar as relações de força materiais. Assim, a dominação econômica acaba por determinar a dominação cultural. A ação pedagógica seria assim entendida como uma imposição arbitrária da cultura dos dominantes para os dominados, que se materializaria pelo trabalho pedagógico. Já tivemos revolução científica, tecnológica-industrial e pedagógica. Se pegássemos um cirurgião ou um piloto de 2 séculos atrás para o mundo atual, eles não saberiam o que fazer. Mas se fizéssemos o mesmo com um professor para o ensino público, ele não veria dificuldades. 

Althusser entende a escola como o instrumento mais acabado de reprodução das relações sociais da sociedade capitalista. Nesse contexto os marginalizados são considerados os próprios trabalhadores. Ao invés de instrumento de equalização social, a escola, constituída pela própria burguesia tem a função de garantir o funcionamento do sistema burguês de produção.

Para os pós-Althusserianos, a escola estaria dividida em duas grandes redes: a burguesa e a proletária, explicada pela divisão da sociedade em duas classes opostas. Nessa perspectiva o papel da escola não é mais o de reforçar a marginalidade, mas sim, impedir o desenvolvimento da ideologia dos trabalhadores e a consequente luta revolucionária. Ou seja, a escola assume-se como duplo fator de marginalização convertendo os trabalhadores em marginais, colocando à lado do movimento proletário todos aqueles que ingressam nas escolas.

Se você viveu na Europa Oriental/Rússia/outros países soviéticos, e estava vivo há mais de 30-35 anos, você conseguiu ver a qualidade do sistema educacional declinando, os padrões cada vez mais baixos enquanto os estudantes lutavam cada vez mais para se formar. Te digo isso depois de obter o bacharelado em Engenharia, conversando com a maioria dos professores sobre o passado e o presente e trabalhando por dois anos em educação. Mas por que o padrão baixou tanto?

Para entender isso, primeiro precisamos entender as perdas:
1. Embora os salários não fossem iguais, a desigualdade era muito baixa (a relação entre o salário máximo e o mínimo era de cerca de 3 para 1). Em contraste com o sistema capitalista, os trabalhadores com os trabalhos mais perigosos ou difíceis tinham salários maiores do que as pessoas em posições de liderança (engenheiros, por exemplo)/funcionários do partido, etc. Essa baixa desigualdade financeira levou as pessoas a seguirem aquilo em que são boas (o que elas gostavam) a uma taxa muito mais alta do que no capitalismo.
2. A correlação entre o número de empregos em uma área específica e o número de vagas disponíveis para estudantes nessa área. O que quero dizer com isso? Digamos que o país precisasse de cerca de 1000 engenheiros químicos por ano - haveria cerca de 1000 vagas nas universidades para estudar engenharia química. Isso significava que, uma vez formado, você tinha um valor e um objetivo na sociedade. Com base em seu conjunto de habilidades e no "mercado" de trabalho, eles lhe perguntariam, uma vez formado, onde você quer trabalhar. Então, eles lhe dariam um apartamento/casa naquela cidade/localidade/aldeia.
3. O ensino era sempre gratuito.

Como essas coisas, que eram muito importantes, foram perdidas, hoje em dia, a graduação em 90% das universidades não garante que você realmente conseguirá um emprego na área que estudou e os estudantes sabem disso e não estudam muito.

O que a sociedade admirava eram pessoas instruídas, mas hoje em dia as pessoas preferem ser ricas e sem instrução porque sentem como é ruim ser um escravo assalariado.

O fato da pergunta: "Em que sou realmente bom?" se transformar em "O que compensa no mercado de trabalho de hoje?" é, em minha opinião, o maior problema. Isso faz com que as pessoas não sigam seu talento e por consequência as pessoas mais miseráveis fazem a maioria dos trabalhos. Além disso, a qualidade dos professores educados no socialismo é surpreendente. A Europa Oriental + ex-URSS ainda tem alguns remanescentes dessa época.

Basta conferir esse vídeo, por exemplo: https://www.youtube.com/watch?v=qw_M7kV3GLY. Até 1991, os países socialistas tinham ganhado 115 medalhas nas olimpíadas, enquanto os países capitalistas ganharam apenas 24. Não havia chance para os capitalistas.
