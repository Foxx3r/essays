# O argumento de que os socialistas criaram o termo capitalismo
Muitos liberais e anarcocapitalistas clamam que o capitalismo é natural ou que o mesmo sempre existiu, e ignoram o termo usado pelos socialistas porque designaria um período pequeno da história econômica, sem levar em conta outros períodos, tornando assim a palavra "capitalismo" para se referir ao processo econômico que surgiu no pós-revolução industrial, como uma categoria de análise falha.

Mas a verdade é que o capitalismo é definido literalmente como o domínio político dos detentores de capital. Por isso o nome. Quem criou esse termo foram socialistas, para apontar o viés favorável ao capital nesse sistema. Depois que os liberais apropriaram o termo e tentaram dar uma carga positiva.
