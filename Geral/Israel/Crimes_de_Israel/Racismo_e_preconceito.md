# Racismo e preconceito
800.000 enlutados comparecem ao cortejo fúnebre do influente Rabino Ovadia Yosef (o maior da história de Israel). (2013) A seguir estão seus comentários polêmicos feitos quando ele estava vivo:
>“6.000.000 de vítimas do Holocausto eram reencarnações das almas dos pecadores, pessoas que transgrediram e fizeram todo tipo de coisas que não deveriam ser feitas. Eles foram reencarnados a fim de expiar.”
>
>“Houve um tsunami (furacão Katrina) e desastres naturais terríveis, porque não há estudo suficiente da Torá. Pessoas negras residem lá [Nova Orleans]. Os negros vão estudar a Torá? [Deus disse] vamos trazer um tsunami e afogá-los. “Centenas de milhares permaneceram desabrigados. Dezenas de milhares foram mortos. Tudo isso porque eles não têm Deus.”
>
>“Os goyim (gentios) nasceram apenas para nos servir. Sem isso, eles não têm lugar no mundo - apenas para servir ao povo de Israel.”
>
>“Em Israel, a morte não tem domínio sobre eles. Com os gentios, será como qualquer pessoa - eles precisam morrer, mas [Deus] lhes dará longevidade. Por quê? Imagine que o burro de alguém morresse, eles perderiam seu dinheiro. Este é seu servo. É por isso que ele tem uma vida longa, para trabalhar bem para este judeu.”
>
>“Por que os gentios são necessários? Eles trabalharão, eles ararão, eles colherão. Vamos sentar como um efendi e comer. É por isso que os gentios foram criados”.
>
>"O conhecimento de uma mulher está apenas na costura. As mulheres devem encontrar outros empregos e fazer hamin, mas não lidar com assuntos da Torá."
>
>“Como você pode fazer as pazes com uma cobra?” (Referindo-se aos árabes)
>
>“Esses malfeitores, os árabes - diz no Gemara [Talmud] que Deus lamenta ter criado aqueles filhos de Ismael.”
>
>“Eles são estúpidos. A religião deles é tão feia quanto eles.” (Referindo-se aos muçulmanos)
>
>"É proibido ser misericordioso com eles. Você deve enviar mísseis a eles e aniquilá-los. Eles são maus e condenáveis." (Referindo-se aos palestinos)

**Fontes:** [Times of Israel](https://www.timesofisrael.com/jerusalem-closes-down-for-rabbi-ovadia-yosefs-funeral/), [Times of Israel](https://www.timesofisrael.com/5-of-ovadia-yosefs-most-controversial-quotations/), [Wiki](https://en.wikipedia.org/wiki/Ovadia_Yosef)

Shlomo Benizri (então ministro do Trabalho e Assuntos Sociais) referindo-se ao povo chinês (2001):
>Eu simplesmente não entendo por que um restaurante precisa de um olho puxado para me servir a minha refeição.

**Fontes:** [Labournet](http://www.labournet.net/world/0112/israel6.html), [JewAge](https://www.jewage.org/wiki/he/Article:Shlomo_Benizri_-_Biography)

Defendendo a pureza racial em Israel, trabalhadores chineses em Israel assinam contrato de proibição de sexo (2003).

**Fonte:** [The Guardian](https://www.theguardian.com/world/2003/dec/24/israel1)

Enquete: Casamento com homens árabes é visto como "Traição Nacional" (2007).

**Fonte:** [YNet](https://www.ynetnews.com/articles/0,7340,L-3381978,00.html)

Equipe de jovens conselheiros e psicólogos estabelecida por uma autoridade local em Israel criada para patrulhar e identificar jovens mulheres judias que estão namorando homens árabes e "resgatá-las" (2009).

**Fontes:** [National news](https://www.thenationalnews.com/world/mena/israeli-drive-to-prevent-jewish-girls-dating-arabs-1.490477), [JPost](https://jpost.com/Jewish-World/Protecting-Jewish-girls-from-Arabs)

Palestino que alegou ser judeu após um encontro sexual consensual com uma mulher israelense preso por 'Estupro por Decepção' (2010).

**Fonte:** [ABCnews](https://abcnews.go.com/International/palestinian-claimed-jew-jailed-rape-deception/story?id=11224513)

Túmulos de muçulmanos e cristãos profanados em Israel com pichações racistas (2011).

**Fonte:** [Reuters](https://www.reuters.com/article/us-palestinians-israel-graves/muslim-christian-graves-desecrated-in-israeli-city-idUSTRE79725I20111008)

Os livros escolares de Israel retratam os palestinos/árabes como terroristas, refugiados e fazendeiros primitivos (2011).

**Fonte:** [The Guardian](https://theguardian.com/world/2011/aug/07/israeli-school-racism-claim)

Tzipi Hotovely (ex-político israelense, atual embaixador no Reino Unido) em 2011:
>Devemos confrontar o fato de que o país não valoriza a educação, que é a única maneira de evitar que as mulheres judias forjem laços de vida com não-judeus. A luta de assimilação chega às manchetes por meio de histórias de mulheres judias que se casam com muçulmanos, mas é importante lembrar que esse fenômeno é muito mais amplo. Mais de 92.000 famílias que vivem em Israel são mestiças, precisamos de programas para ensinar as meninas no ensino médio sobre a identidade judaica.

**Fontes:** [Ynet](https://www.ynetnews.com/articles/0,7340,L-4025788,00.html), [JPost](https://www.jpost.com/diplomacy-and-politics/mks-told-more-education-is-needed-to-combat-intermarriage)

Os israelenses atacam migrantes africanos e saqueiam lojas que atendem refugiados e solicitantes de refúgio durante o protesto. Indicando um clima cada vez mais volátil em Israel sobre o que chama de "infiltrados". Miri Regev, membro do parlamento israelense, disse à multidão:
>Os sudaneses são um câncer em nosso corpo.

O protesto seguiu-se a uma reclamação no domingo do primeiro-ministro Netanyahu, de que "infiltrados ilegais estavam inundando o país e ameaçando a segurança e a identidade do Estado judeu" (2012).

**Fontes:** [The Guardian](https://theguardian.com/world/2012/may/24/israelis-attack-african-migrants-protest), [Haaretz](https://www.haaretz.com/demonstrators-in-south-tel-aviv-attack-african-migrants-1.5162222)

Colono israelense tentando convencer os agricultores palestinos de que ele é o legítimo proprietário de suas terras (2012):
>Quando o Messias vier, todos vocês serão nossos escravos, se forem dignos, se se comportarem bem.

**Fonte:** [YouTube](https://www.youtube.com/watch?v=tEgKT0cIOZk)

Hierarquia da espécie humana de acordo com Eli Ben-Dahan, ex-vice-ministro da defesa de Israel: Goyims (não judeus) colocados abaixo dos judeus homossexuais (2013).

**Fonte:** [Haaretz](https://www.haaretz.com/.premium-the-human-hierarchy-1.5305946)

>Eu matei muitos árabes na minha vida, não há problema com isso. (2013)<br/>
>– <cite>Naftali Bennett</cite> (Ministro de Comércio de Israel)

**Fonte:** [JPost](https://www.jpost.com/Diplomacy-and-Politics/Bennett-under-fire-for-comments-about-killing-Arabs-321467)

Judeus etíopes injetados com anticoncepcionais sem o seu consentimento (2013).

**Fonte:** [Haaretz](https://www.haaretz.com/israel-news/.premium-ethiopians-fooled-into-birth-control-1.5226424), [Independent](https://www.independent.co.uk/news/world/middle-east/israel-gave-birth-control-to-ethiopian-jews-without-their-consent-8468800.html?amp), [The Guardian](https://amp.theguardian.com/world/2013/feb/28/ethiopian-women-given-contraceptives-israel)

Eli Ben-Dahan, ex-vice-ministro da defesa de Israel (2015):
>Judeus homossexuais são superiores aos gentios (não judeus), um judeu sempre tem uma alma mais elevada do que um gentio, mesmo que seja homossexual. Para mim, eles (palestinos) são como animais, não são humanos.

**Fonte:** [Times of Israel](https://www.timesofisrael.com/new-deputy-defense-minister-called-palestinians-animals/)

Beitar Jerusalem, a fanbase de futebol mais racista do mundo (2015).

**Fonte:** [YouTube](https://youtu.be/GJOV_cN-JP8)

O ministro das Relações Exteriores de Israel diz que árabes desleais devem ser decapitados com um machado (2015).

**Fonte:** [Washington Post](https://www.washingtonpost.com/news/worldviews/wp/2015/03/10/israeli-foreign-minister-says-disloyal-arabs-should-be-beheaded/?noredirect=on&utm_term=.9a0459aeeefe)

Policial israelense ataca soldado africano das FDI (2015).

**Fonte:** [YouTube](https://youtu.be/QAi2fIZNqyQ)

Soldado druso das FDI espancado em boate por jovens israelenses por falar árabe (2015).

**Fonte:** [Times of Israel](https://www.timesofisrael.com/druze-idf-soldier-beaten-in-nightclub-for-speaking-arabic/)

O grupo ultranacionalista de direita israelense Lehava, que tem membros em cargos importantes no gabinete, pede a proibição do Natal. O líder também chamou os cristãos de 'vampiros sugadores de sangue' (2015).

**Fonte:** [i24News](https://youtu.be/8Gkfg2rFp2Y), [Haaretz](https://www.haaretz.com/israel-news/.premium-jewish-extremist-group-wants-to-ban-christmas-in-israel-1.5380284), [Salon](https://www.salon.com/2015/12/24/christmas_has_no_place_in_the_holy_land_far_right_israeli_leader_wants_to_expel_christians_and_ban_the_holiday/)

Pesquisa: Quase metade dos judeus israelenses quer expulsar os árabes (2016).

**Fonte:** [Reuters](https://reuters.com/article/idUSKCN0WA1HI), [The Straits Times](https://www.straitstimes.com/world/middle-east/about-half-of-israeli-jews-want-to-expel-arabs-survey)

Os israelenses falam abertamente sobre os palestinos para Abby Martin, inclui muito racismo (2017).

**Fonte:** [YouTube](https://youtu.be/lFoxL3sOAio)

Adolescentes israelenses espancaram motorista de ônibus após se certificarem de que ele era árabe, tribunal israelense considera isso como 'violência na estrada' em vez de 'crime de ódio' (2018).

**Fonte:** [Haaretz](https://www.haaretz.com/israel-news/bus-driver-says-two-israeli-teens-beat-him-after-making-sure-he-was-arab-1.6745180), [Times of Israel](https://www.timesofisrael.com/beating-of-arab-bus-driver-ruled-road-rage-not-a-hate-crime/)

A Lei do "Estado-nação" dá aos judeus direitos exclusivos sobre os árabes que vivem em Israel (2018).

**Fonte:** [The Guardian](https://www.theguardian.com/world/2018/jul/19/one-more-racist-law-reactions-as-israel-axes-arabic-as-official-language)

O ex-presidente do Partido Trabalhista de Israel, Isaac Herzog, diz que as relações entre judeus e não judeus são "uma verdadeira praga" (2018).

**Fonte:** [Twitter](https://twitter.com/davidsheen/status/1012611303574253568?s=20)

O Rabino Chefe, Yitzhak Yosef, chama os negros de "macacos" em seu sermão semanal (2018).

**Fonte:** [Times of Israel](https://www.timesofisrael.com/chief-rabbi-compares-african-americans-to-monkeys/)

O filho de Netanyahu diz que prefere que todos os muçulmanos deixem Israel (2018).

**Fonte:** [Haaretz](https://www.haaretz.com/israel-news/netanyahu-s-son-says-he-d-prefer-if-all-the-muslims-leave-the-land-of-israel-1.6747732)

Desclassificado: Israel garantiu que os árabes não pudessem retornar às suas aldeias (2019).

**Fonte:** [Haaretz](https://www.haaretz.com/.premium-israel-lifted-military-rule-over-arabs-in-1966-only-after-ensuring-they-couldn-t-ret-1.7297983)

Um político israelense tuitou um vídeo dele atirando em um parlamentar árabe como uma piada (2019).

**Fonte:** [Twitter](https://twitter.com/oren_haz/status/1107964732554792960?s=20)

O partido de Netanyahu contrata secretamente 1.200 pessoas para filmar ilegalmente a votação dos árabes (2019).

**Fonte:** [NBCNews](https://www.nbcnews.com/politics/elections/netanyahu-s-party-admits-it-hired-1-200-people-secretly-n992381)

Netanyahu: "Os políticos árabes (em Israel) querem destruir todos nós" (2019).

**Fontes:** [New York Times](https://www.nytimes.com/2019/09/12/world/middleeast/facebook-netanyahu-bot.html), [972Mag](https://972mag.com/arabs-bibi-ethnic-violence/144502/), [Haaretz](https://www.haaretz.com/israel-news/elections/.premium-netanyahu-at-emergency-party-rally-arab-joint-list-wants-to-destroy-the-country-1.8136337)

Arcebispo de Jerusalém Theodosios (Hanna) de Sebastia: “Os cristãos de todo o mundo devem rejeitar a ocupação da Palestina por Israel”. 2 semanas depois, ele foi hospitalizado após inalar um gás venenoso depois que botijões de gás foram atirados contra sua igreja em Jerusalém (2019).

**Fontes:** [Youtube](https://www.youtube.com/watch?v=FoRr7kfNdYg&feature=youtu.be), [Egyptian Streets](https://egyptianstreets.com/2019/12/21/palestines-archbishop-hanna-poisoned-by-israeli-gas-canister-following-calls-for-christians-to-defend-palestine/), [Al-Quds TV](http://web.archive.org/web/20200705172410/https://twitter.com/QdsTvSat/status/1207406318690209793?s=19)

Rafi Peretz, Ministro da Educação de Israel: "Casamento misto entre judeus e não judeus nos Estados Unidos é como um segundo holocausto" (2019).

**Fontes:** [Haaretz](https://www.haaretz.com/israel-news/israel-s-education-minister-says-intermarriage-is-like-a-second-holocaust-1.7486330), [Axios](https://axios.com/rafi-peretz-second-holocaust-intermarriage-jews-us-359a9bc6-ae75-46cb-8844-32da55c086d8.html?__twitter_impression=true&fbclid=IwAR1HlkZifDed4t3Oz3yQ6h_RW-pYXvktmGXG6zh_AfFaj1wl7VClrxVxRKI)

Centenas de ultranacionalistas israelenses guardados pela polícia de choque entram na mesquita de Al-Aqsa, intimidando os fiéis muçulmanos (2019).

**Fonte:** [Al Jazeera](https://www.aljazeera.com/news/2019/06/israeli-forces-settlers-enter-al-aqsa-mosque-compound-190602065712978.html)

O grupo de ultra fãs 'La-Familia' do time de futebol israelense Beitar Jerusalem postou no Facebook que eles não têm problemas com a nova contratação de Ali Mohamed, já que ele é cristão, mas exigem que ele mude de nome (2019).

**Fontes:** [Times of Israel](https://www.timesofisrael.com/far-right-beitar-jerusalem-fans-incensed-by-addition-of-player-named-mohamed/), [JPost](https://m.jpost.com/Israel-News/Sports/La-Familia-We-are-happy-that-Mohamed-is-coming-592141)

Israel quer estar "na África", mas manter os africanos fora de Israel (2019).

**Fonte:** [TRTWorld](https://www.trtworld.com/mea/israel-wants-to-be-in-africa-but-to-keep-africans-out-of-israel-23196)

A Suprema Corte de Israel decide para o Grupo de Colonizadores Judeus na batalha pelas terras e ativos da Igreja no Bairro Cristão de Jerusalém (2019).

**Fontes:** [Haaretz](https://www.haaretz.com/israel-news/.premium-christian-leaders-blast-decision-to-sell-jerusalem-properties-to-settler-group-1.7366340), [Haaretz](https://www.haaretz.com/israel-news/new-details-emerge-on-greek-orthodox-churchs-fire-sale-in-israel-1.5460636), [Haaretz](https://www.haaretz.com/israel-news/.premium-settler-group-strengthens-hold-on-jerusalem-s-christian-quarter-after-court-victory-1.7354250)

Israel para a Polônia: “Não mande judeus doentes ou deficientes para Israel” (2019).

**Fonte:** [Haaretz](https://www.haaretz.com/1.5017879?fbclid=IwAR2-6qbRY2QswHLfaWOzRyLsv12vJ2WqlABKxlJleo-mWtitQdl4N4Zb2ss)

"Nós não dirigimos para árabes": motorista de uma empresa de táxis com sede em Jerusalém se recusou a dirigir para dois irmãos depois de ouvi-los falar em árabe (2019).

**Fonte:** [Haaretz](https://www.haaretz.com/israel-news/.premium-we-don-t-drive-arabs-taxi-company-to-compensate-arab-clients-it-didn-t-serve-1.7776916)

"Israel é o estado-nação dos judeus apenas" Netanyahu responde à estrela de TV dizendo que os árabes são cidadãos iguais (2019).

**Fonte:** [Haaretz](https://www.haaretz.com/israel-news/.premium-israel-belongs-to-jews-alone-netanyahu-responds-to-tv-star-on-arab-equality-1.7003348)

Mesquita em Jerusalém incendiada por colonos israelenses, vandalizada com graffiti racista (2020).

**Fontes:** [JPost](https://www.jpost.com/Breaking-News/Hate-crime-suspected-as-Jerusalem-mosque-goes-up-in-flames-615217), [Mid East Eye](https://www.middleeasteye.net/news/east-jerusalem-mosque-burned-israeli-settlers-price-tag-attack)

>Sob nenhuma condição, permitiremos o estabelecimento de um estado palestino ou o reconhecimento de tal estado, nem permitiremos que um centímetro de terra seja entregue aos árabes. (2020)<br/>
>– <cite>Naftali Bennett</cite>, ministro da Defesa de Israel

**Fontes:** [Haaretz](https://www.haaretz.com/israel-news/elections/.premium-as-netanyahu-heads-for-washington-bennett-says-israel-won-t-allow-palestinian-state-1.8446791), [Times of Israel](https://www.timesofisrael.com/liveblog-january-26-2020/)

O assessor sênior de Netanyahu em uma gravação que vazou: "O ódio é o que une nosso campo" (2020).

**Fonte:** [Times of Israel](https://www.timesofisrael.com/netanyahu-aide-in-leaked-recording-hate-is-what-unites-our-camp/)

Explosão no Líbano: O ex-membro israelense do Knesset, Moshe Feiglin em sua página do Facebook, saudou a explosão devastadora em Beirute como um presente de Deus a tempo para o festival judaico, Tu B’Av. Em uma entrevista à rádio local, ele disse que esperava que Israel fosse o responsável pela explosão, e que ele teve permissão para se “alegrar” por ter acontecido em Beirute e não em Tel Aviv (2020).

**Fontes:** [Ynet News](https://www.ynetnews.com/article/S1zxJEdbv), [MEM](https://www.middleeastmonitor.com/20200805-ex-israel-mk-declares-lebanon-blast-as-gift-from-god/)

A Igreja de Jerusalém Getsêmani (que consagra uma seção de cristãos que acreditam que Jesus orou antes de sua crucificação) sofre danos após uma tentativa de incêndio criminoso por um colonizador israelense (2020).

**Fontes:** [Reuters](https://www.reuters.com/article/israel-palestinians-church-fire/jerusalem-church-suffers-damage-in-arson-near-garden-of-gethsemane-idUSKBN28E2PV), [Mid East Eye](https://www.middleeasteye.net/news/israeli-arrested-attempted-arson-jerusalem-church-all-nations), [Times of Israel](https://www.timesofisrael.com/jewish-man-detained-after-suspected-arson-attempt-at-east-jerusalem-holy-site/)

Legislador israelense: "Taxa de natalidade beduína em Israel é 'bomba' que precisa ser neutralizada" (2020).

**Fontes:** [Haaretz](https://www.haaretz.com/israel-news/.premium-far-right-israeli-lawmaker-says-bedouin-birthrate-is-bomb-that-needs-defusing-1.8914313), [Times of Israel](https://www.timesofisrael.com/liveblog_entry/smotrich-says-bedouin-birthrate-a-bomb-that-must-be-defused/)

Mulher israelense para motorista de táxi: "Não gosto de pegar táxi com árabe" (2020).

**Fontes:** [Instagram](https://www.instagram.com/p/CEZHCOJJ3PG/?igshid=1ufouvuvdfd8u), [Gfycat](https://gfycat.com/piercingyawningblacknorwegianelkhound)

O rabino ultraortodoxo israelense Daniel Asor, conhecido por suas campanhas contra o proselitismo cristão em Israel, diz que a vacina Covid-19 pode tornar as pessoas gays (2021).

**Fontes:** [Independent](https://www.independent.co.uk/news/world/middle-east/covid-vaccine-rabbi-gay-b1788543.html), [Yahoo](https://uk.news.yahoo.com/crackpot-rabbi-thinks-covid-19-102757453.html)

Manifestantes ultra-ortodoxos entram em confronto com a polícia israelense por causa das regras de Covid, atacam o motorista do ônibus e atearam fogo em seu ônibus porque pensaram que ele era árabe (2021).

**Fontes:** [Haaretz](https://www.haaretz.com/israel-news/.premium-ultra-orthodox-protesters-clash-with-police-over-covid-rules-attack-bus-driver-1.9478480), [Bhol.il](https://www.bhol.co.il/news/1177604)

Estado informa Tribunal Superior: Judeus de Uganda não são elegíveis para imigrar para Israel, apesar de terem a maioria de suas conversões supervisionadas por rabinos afiliados ao movimento conservador. (De acordo com a Lei de Retorno de Israel, qualquer pessoa que foi convertida ao judaísmo por uma comunidade judaica reconhecida é elegível para imigrar para Israel) (2021)

**Fonte:** [Haaretz](https://outline.com/wvSDN7)

“Vocês são todos árabes e palestinos, não israelenses”, assediam os colonos israelenses armados e evitam a família palestina portadora de identidade israelense por fazerem um piquenique em uma área pública. Mais tarde, as FDI chegam e dizem à família para ir embora (2021).

**Fontes:** [Facebook](https://www.facebook.com/story.php?story_fbid=10159157206483330&id=769028329), [Instagram](https://www.instagram.com/tv/CLJ8U70pb3S/?igshid=1bzcd37s7g922)

Pesquisa da Universidade Hebraica: grande porcentagem da juventude israelense odeia os árabes e apoiaria a revogação de sua cidadania.

**Fontes:** [Times of Israel](https://www.timesofisrael.com/poll-shows-large-swaths-of-israeli-youth-hate-arabs-back-revoking-citizenship/), [MEM](https://www.middleeastmonitor.com/20210222-poll-most-young-israelis-hate-palestinian-citizens-of-israel/)

Grande grupo de israelenses gritando "Morte aos árabes" e "Estamos queimando árabes esta noite" em Jerusalém. O grupo posteriormente atacou palestinos, ferindo muitos (2021).

**Fontes:** [Haaretz](https://www.haaretz.com/israel-news/.premium-we-re-burning-arabs-today-jewish-supremacists-gear-up-for-jerusalem-march-1.9737755), [Instagram](https://www.instagram.com/p/CN4yIF0p9nM/?igshid=1mkxni52maz1l), [Twitter](https://twitter.com/nirhasson/status/1384237738518409226?s=09)

Os israelenses lincham violentamente o homem inconsciente que eles acreditavam ser árabe. Acontece que ele era judeu (2021).

**Fonte:** [Yahoo](https://au.news.yahoo.com/lynched-shocking-video-emerges-during-violent-conflict-222141824.html)

Ouça o que essas crianças de uma escola particular ultra-ortodoxa estão dizendo sobre árabes e palestinos:
- [Reddit](https://www.reddit.com/r/Palestine/comments/nefa6z/listen_to_what_the_children_are_saying_in_an/)
