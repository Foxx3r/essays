# Condenações da ONU contra Israel
9 de novembro: [Resolução 31/6-E](http://www.un.org/documents/ga/res/31/ares31.htm): Condenação da colaboração de Israel e da África do Sul.

13 de dezembro: [Resolução 32/91](https://www.jewishvirtuallibrary.org/jsource/UN/unga32_91.html): Relatório do Comitê Especial para Investigar as Práticas Israelenses que Afetam os Direitos Humanos da População dos Territórios Ocupados.  Exorta Israel a respeitar as Convenções de Genebra

15 de dezembro: [Resolução 32/111](https://www.jewishvirtuallibrary.org/jsource/UN/unga32_111.html): Necessidades de saúde das crianças refugiadas palestinas.

23 de novembro: [Resolução 39/14](http://research.un.org/en/docs/ga/quick/regular/39): Reitera a exigência de que Israel retire sua ameaça de atacar instalações nucleares de nações vizinhas.

[Resolução 465](https://en.wikipedia.org/wiki/United_Nations_Security_Council_Resolution_465): "... 'lamenta' os assentamentos de Israel e pede a todos os estados membros que não ajudem o programa de assentamentos de Israel".

[Resolução 452](https://en.wikipedia.org/wiki/United_Nations_Security_Council_Resolution_452): "... 'apela' a Israel para que cesse a construção de assentamentos em territórios ocupados".

[Resolução 467](https://en.wikipedia.org/wiki/United_Nations_Security_Council_Resolution_467): "... 'lamenta veementemente' a intervenção militar de Israel no Líbano".

[Resolução 468](https://en.wikipedia.org/wiki/United_Nations_Security_Council_Resolution_468): "... 'apela' a Israel para rescindir as expulsões ilegais de dois prefeitos palestinos e um juiz e facilitar seu retorno".

[Resolução 592](https://en.wikipedia.org/wiki/United_Nations_Security_Council_Resolution_592): "... 'lamenta veementemente' o assassinato de estudantes palestinos na [Universidade de Birzeit](https://en.wikipedia.org/wiki/Birzeit_University) por tropas israelenses"

[Resolução 2334](https://en.wikipedia.org/wiki/United_Nations_Security_Council_Resolution_2334) (23 de dezembro de 2016) pedia o fim da construção de assentamentos israelenses

Resolução da Assembleia Geral das Nações Unidas A/RES/33/24 de 29 de novembro de 1978:
>Reafirma a legitimidade da luta dos povos pela independência, integridade territorial, unidade nacional e libertação da dominação colonial e estrangeira e ocupação estrangeira por todos os meios disponíveis, particularmente a luta armada.
