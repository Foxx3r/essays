# Pedofilia e chantagem sexual
Jewish Community Watch: "Israel se tornando um refúgio seguro para pedófilos em todo o mundo" (2016).

**Fonte:** [Independent](http://web.archive.org/web/20210513135532/https://www.independent.co.uk/news/world/israel-safe-haven-paedophiles-jerusalem-sex-abuse-jewish-community-watch-a7445246.html).

De acordo com [Ari Ben-Menashe](http://web.archive.org/web/20210513135532/https://nationalpost.com/news/canada/the-unbelievable-life-of-ari-ben-menashe) (Ex-Oficial de Inteligência Militar e Conselheiro de Inteligência Estrangeira de Israel), Jeffrey Epstein e Ghislaine Maxwell trabalharam com a Inteligência israelense para administrar uma Empresa de Chantagem Sexual com o objetivo de prender indivíduos e políticos poderosos nos Estados Unidos e no exterior. O ex-primeiro-ministro israelense, Yitzhak Shamir, também elogiou o pai de Ghislaine Maxwell durante seu funeral, afirmando: “Ele fez mais por Israel do que pode ser dito hoje.” (2019).

**Fontes:** [7 News](http://web.archive.org/web/20210513135532/https://7news.com.au/the-morning-show/jeffrey-epstein-was-a-mossad-spy-says-investigative-journalist-dylan-howard-c-595812), [Mint Press](http://web.archive.org/web/20210513135532/https://www.mintpressnews.com/ari-ben-menashe-jeffrey-epstein-ghislaine-maxwell-israel-intelligence/262162/), [Telegraph](http://web.archive.org/web/20210513135532/https://www.telegraph.co.uk/news/uknews/1445707/FO-suspected-Maxwell-was-a-Russian-agent-papers-reveal.html), [Narativ](http://web.archive.org/web/20210513135532/https://narativ.org/2019/09/26/blackmailing-america/), [TRT World](http://web.archive.org/web/20210513135532/https://www.trtworld.com/magazine/two-american-billionaires-and-their-shady-deals-with-israeli-intelligence-28819), [Haaretz](http://web.archive.org/web/20210513135532/https://www.haaretz.com/israel-news/ghislaine-maxwell-british-socialite-accused-of-helping-jeffrey-epstein-reportedly-1.8370389).

Jeffrey Epstein também investiu milhões de dólares na Carbyne, uma start-up israelense da qual o ex-primeiro-ministro israelense e atual candidato às eleições Ehud Barak é o acionista controlador (2019).

**Fontes:** [New York Times](http://web.archive.org/web/20210513135532/https://www.nytimes.com/2019/07/16/world/middleeast/epstein-israel-barak-ehud.html), [Haaretz](http://web.archive.org/web/20210513135532/https://www.haaretz.com/israel-news/.premium-barak-says-considering-breaking-off-business-relations-with-jeffrey-epstein-1.7497921), [Haaretz](http://web.archive.org/web/20210513135532/https://www.haaretz.com/israel-news/.premium-revealed-jeffrey-epstein-entered-million-dollar-partnership-with-ehud-barak-in-2015-1.7493648).

As fotos mostram o ex Primeiro-Ministro Ehud Barak entrando na casa de Epstein. Posteriormente, uma mulher saiu dizendo que Epstein a forçou a fazer sexo com o ex Primeiro-Ministro Ehud Barak (2019-2020).

**Fontes:** [JPost](http://web.archive.org/web/20210513135532/https://www.jpost.com/israel-news/released-photos-show-barak-four-women-entering-epstein-house-report-595887), [Daily Mail](http://web.archive.org/web/20210513135532/https://www.dailymail.co.uk/news/article-7250009/Netanyahu-challenger-Ehud-Barak-hides-face-enters-entering-Jeffrey-Epsteins-mansion.html), [Times Of Israel](http://web.archive.org/web/20210513135532/https://www.timesofisrael.com/woman-says-epstein-forced-her-to-have-sex-with-former-pm-barak/), [Ynet News](http://web.archive.org/web/20210513135532/https://www.ynetnews.com/article/Hko9dhlRI).

Em fotos recém-descobertas tiradas na ilha de Epstein, Ghislaine Maxwell vista brincando com o suspeito de estupro fugitivo Jean-Luc Brunel - que deu a Jeffrey Epstein três trigêmeos de 12 anos de idade como presente de aniversário. Nas fotos da fonte, Jean-Luc Brunel está usando um boné do exército israelense. (2020)

**Fontes:** [NZ Herald](http://web.archive.org/web/20210513135532/https://www.nzherald.co.nz/world/12-year-old-french-triplets-allegedly-flown-to-epstein-as-a-birthday-present/S2XRZ4F3KQVVKCGHVCTNDXCXOE/), [New York Post](http://web.archive.org/web/20210513135532/https://nypost.com/2020/08/11/photos-show-maxwell-jean-luc-brunel-on-epsteins-pedophile-island).

Maria Farmer, uma das vítimas de Epstein, deu uma entrevista ao jornalista independente Whitney Webb. Na entrevista, ela fala sobre o anel sexual e como a Supremacia Judaica desempenhou um papel importante. Ela menciona que nenhum dos principais meios de comunicação estava disposto a falar sobre isso. Alan Dershowitz, um advogado pró-Israel, que também estava envolvido na quadrilha sexual de Epstein, classificou-a de anti-semita por sua declaração. Dershowitz é bem conhecido por sua equipe de defesa por Harvey Weinstein em 2018 e pelos julgamentos de impeachment de Donald Trump em 2020 e 2021. Ele foi membro da equipe de defesa legal de Jeffrey Epstein e ajudou a negociar um acordo de não-promotoria em 2006 em nome de Epstein. (2020)

**Fontes:** [Entrevista com Maria Farmer](http://web.archive.org/web/20210513135532/https://youtu.be/DpEJCKvjtyk), [a vítima de Epstein diz que Dershowitz fez sexo com ela 6 vezes quando ela era menor de idade](http://web.archive.org/web/20210513135532/https://youtu.be/_GlK8dr6MP0), [Information Liberation](http://web.archive.org/web/20210513135532/https://www.informationliberation.com/?id=61043), [NYmag](http://web.archive.org/web/20210513135532/https://nymag.com/intelligencer/2019/07/alan-dershowitz-jeffrey-epstein-case.html), [Vox](http://web.archive.org/web/20210513135532/https://www.vox.com/identities/2019/7/30/20746983/alan-dershowitz-jeffrey-epstein-sarah-ransome-giuffre).

A abusadora de crianças Malka Leifer, procurada por 74 acusações de abuso sexual, fugiu da Austrália para Israel. Em outubro de 2019, o Tribunal de Jerusalém a libertou sob fiança, apesar de muitos se oporem a ela. O presidente israelense se recusa a receber as supostas vítimas de Malka Leifer durante sua visita à Austrália. Ela também tem conexões com o ex-ministro da saúde de Israel (2019) (atualização: devido à pressão pública, ela foi extraditada de volta para a Austrália em 2021).

**Fontes:** [The Guardian](http://web.archive.org/web/20210513135532/https://www.theguardian.com/world/2019/oct/02/alleged-child-abuser-malka-leifer-released-on-bail-in-israel), [AP news](http://web.archive.org/web/20210513135532/https://apnews.com/5f479a8102f67e667411e44a41e2510e), [Abc Australia](http://web.archive.org/web/20210513135532/https://www.abc.net.au/news/2020-01-15/further-delays-extradition-accused-child-abuser-malka-leifer/11868406), [abc.au](http://web.archive.org/web/20210513135532/https://www.abc.net.au/radio/programs/am/malka-leifer-supporter-appointed-israeli-health-minister/11833586).

[Como os pedófilos judeus americanos se escondem da justiça em Israel](http://web.archive.org/web/20210513135532/https://www.cbsnews.com/news/how-jewish-american-pedophiles-hide-from-justice-in-israel/#app) (2020).

#### Direito internacional
[Apartheid contra palestinos](http://web.archive.org/web/20210520054352/https://en.wikipedia.org/wiki/Israel_and_the_apartheid_analogy).

O Direito Internacional (Protocolo Adicional I às Convenções de Genebra de 1949, Resolução 37/43 da ONU) determina que os palestinos têm o direito legal de uma luta armada para resistir à ocupação.

**Fontes:** [Protocolo I](http://web.archive.org/web/20210520054352/https://treaties.un.org/doc/publication/unts/volume%201125/volume-1125-i-17512-english.pdf), [Convenções de Genebra (PDF)](http://web.archive.org/web/20210520054352/https://treaties.un.org/doc/publication/unts/volume%201125/volume-1125-i-17512-english.pdf), [Resolução da ONU 37/43 (PDF)](http://web.archive.org/web/20210520054352/https://www.google.com/url?sa=t&source=web&rct=j&url=https://undocs.org/pdf%3Fsymbol%3Den/A/RES/37/43&ved=2ahUKEwi2jrGYp6jmAhXEbSsKHdCyAqUQFjAAegQIAhAB&usg=AOvVaw3XITjNzYRnxin4S4JRtzRt&cshid=1575885696424).

A construção de assentamentos por Israel em territórios ocupados como a Cisjordânia e Jerusalém Oriental são ilegais sob o direito internacional, violando o Artigo 49 da Quarta Convenção de Genebra de 1949, bem como o Estatuto de Roma, art. 8 (2)(b)(viii) que afirma: "A Potência Ocupante não deve deportar ou transferir partes de sua própria população civil para o território que ocupa.".

**Fontes:** [Wiki](http://web.archive.org/web/20210520054352/https://en.m.wikipedia.org/wiki/International_law_and_Israeli_settlements), [ONU](http://web.archive.org/web/20210520054352/https://www.un.org/press/en/2016/sc12657.doc.htm), [Lei Internacional sobre Ocupação](http://web.archive.org/web/20210520054352/https://books.google.com.sg/books?id=lcVOlb0wefwC&pg=PA140&redir_esc=y#v=onepage&q&f=false), [A.J](http://web.archive.org/web/20210520054352/https://interactive.aljazeera.com/aje/palestineremix/phone/settlement.html), [Amnesty](http://web.archive.org/web/20210520054352/https://www.amnesty.org/en/latest/campaigns/2019/01/chapter-3-israeli-settlements-and-international-law/), [Amnesty](http://web.archive.org/web/20210520054352/https://www.amnestyusa.org/lets-be-clear-israels-long-running-settlement-policy-constitutes-a-war-crime/), [B'Tselem](http://web.archive.org/web/20210520054352/https://www.btselem.org/international_law), [YouTube](http://web.archive.org/web/20210520054352/https://youtu.be/Mnf0w9UuV4s).

A destruição de casas por Israel em territórios ocupados, como a Cisjordânia e Jerusalém Oriental são ilegais ao abrigo do Estatuto de Roma, bem como violam o Artigo 53 da Quarta Convenção de Genebra de 1949, que afirma: "Qualquer destruição pela Potência Ocupante de bens imóveis ou pessoais pertencentes individual ou coletivamente a particulares... é proibida, exceto quando tal destruição for tornada absolutamente necessária por operações militares."

**Fonte:** [Quarta Convenção de Genebra (PDF)](http://web.archive.org/web/20210520054352/https://www.google.com/url?sa=t&source=web&rct=j&url=https://www.un.org/en/genocideprevention/documents/atrocity-crimes/Doc.33_GC-IV-EN.pdf&ved=2ahUKEwjfifTV77_mAhXp4HMBHUZwD9sQFjANegQIARAB&usg=AOvVaw1inV-CGU9ExFOyNuIJzAoc).

O Artigo 7, Seção 1 (d) dos Estatutos de Roma declara 'Deportação ou transferência forçada de população' um Crime contra a Humanidade.

**Fonte:** [Estatuto de Roma](http://web.archive.org/web/20210520054352/https://legal.un.org/icc/statute/99_corr/cstatute.htm).

>Nós alugamos casa após casa... não terminamos, vamos para o próximo bairro e depois vamos para mais<br/>
>– <cite>Yonatan Yosef</cite> (porta-voz dos colonos israelenses)

**Fonte:** [Reddit](http://web.archive.org/web/20210520054352/https://www.reddit.com/r/Palestine/comments/mvkadx/israeli_settlers_filmed_stealing_homes_from/).

>Os árabes terão que ir, mas é preciso um momento oportuno para que isso aconteça, como uma guerra.<br/>
>– <cite>Ben Gurion</cite>, Primeiro Ministro de Israel (1937)

**Fonte:** [The Ethnic Cleansing of Palestine](http://web.archive.org/web/20210520054352/https://books.google.com.sg/books?id=Xhq9DwAAQBAJ&pg=PT50&lpg=PT50&dq=The+Arabs+will+have+to+go,+but+one+needs+an+opportune+moment+for+making+it+happen,+such+as+a+war&source=bl&ots=s9S-XcK-Ko&sig=ACfU3U1uKBmf04j3cwPEyKe1y9Gd7FrAfA&hl=en&sa=X&ved=2ahUKEwj97IjV6svwAhXYbn0KHS7BDT8Q6AEwEXoECAwQAg).

>Não há lugar para duas nações nesta terra, e não há outra solução além da expulsão de árabes palestinos para outros países vizinhos. Nem mesmo uma aldeia ou uma família deve permanecer nesta terra.<br/>
>– <cite>Yosef Weitz</cite> (Diretor do Departamento de Terras e Florestas, Fundo Nacional Judaico, 1940)

**Fontes:** Birth of the Palestinian Refugee Problem p. 27, Expulsion of the Palestinians p. 131.

Lista de Leis Discriminatórias contra Palestinos em Israel e Territórios Ocupados:
- [Adalah | Discriminatory Laws in Israel](https://www.adalah.org/en/law/index)
- [AlJazeera | Five ways Israeli law discriminates against Palestinians](https://www.aljazeera.com/news/2018/7/19/five-ways-israeli-law-discriminates-against-palestinians)
- [Relatório PDF HRW](https://www.hrw.org/sites/default/files/reports/iopt1210webwcover_0.pdf)
