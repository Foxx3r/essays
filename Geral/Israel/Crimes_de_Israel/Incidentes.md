# Incidentes
A **Declaração de Balfour** (1917) foi uma carta de apoio britânico a Lord Rothschild para o objetivo sionista de estabelecer um estado judeu na Palestina, que naquela época já era habitado pelos nativos. Foi altamente controverso, pois a declaração foi feita por uma potência europeia a respeito de um território não europeu em total desconsideração tanto da presença quanto dos desejos da maioria nativa no território. Com o início do mandato, os britânicos facilitaram a imigração de judeus europeus para a Palestina.

Os britânicos desarmaram o povo palestino de 1936-1939 durante a 'Grande Revolta Árabe' enquanto fechavam os olhos para a imigração ilegal de judeus europeus e sua aquisição de armas, dificultando enormemente os palestinos na futura guerra árabe-israelense. (Fontes: Righteous Victims: A History of the Sionist-Arab Conflict, 1881–1999 por Benny Morris p. 159, The Hundred Years' War on Palestine by Rashid Khalidi)

Em 1939, os britânicos alteraram sua política, estabelecendo um limite de mais 75.000 imigrantes e encerrando a imigração em 1944, a menos que os palestinos nativos da região consentissem em mais imigração. Os sionistas condenaram a nova política, acusando a Grã-Bretanha de favorecer os árabes. Este ponto foi discutido com a eclosão da 2ª Guerra Mundial e a fundação de Israel em 1948.

**Fontes:** [Britannica](https://www.britannica.com/event/Balfour-Declaration), [Wiki](https://en.wikipedia.org/wiki/Balfour_Declaration), [BBC](https://www.bbc.com/news/world-middle-east-41765892), [AJ](https://www.aljazeera.com/features/2018/11/2/more-than-a-century-on-the-balfour-declaration-explained).

**Assassinato de Jacob Israël de Haan** (1924), um diplomata judeu-holandês, por tentar fazer um acordo de paz com o Emir de Meca - seu plano era um estado palestino em uma federação jordaniana, os sionistas retirariam a declaração de Balfour e qualquer reclamação a um estado em troca de imigração irrestrita. Ele foi assassinado ao sair de uma sinagoga por Avraham Tehomi (que admitiu isso muito mais tarde), por ordem de Yitzhak Ben-Zvi, que mais tarde se tornou o 2º Presidente de Israel.

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Jacob_Isra%C3%ABl_de_Haan), [Haaretz](https://www.haaretz.com/jewish/.premium-this-day-zionism-s-first-political-assassination-1.5288744), [972mag](https://972mag.com/the-first-political-murder-in-jewish-palestine-lessons-of-intolerance/92686/).

**Desastre de Patria** (1940) foi o naufrágio de um SS Patria construído na França pelo Grupo Paramilitar Sionista, Haganah, matando 267 pessoas e ferindo 172. Patria carregava cerca de 1.800 refugiados judeus da Europa que as autoridades britânicas estavam deportando da Palestina para as Ilhas Maurício porque eles não tinham permissão de entrada. As organizações sionistas se opuseram à deportação, então Haganah plantou uma bomba com o objetivo de desativar o navio para impedi-lo de deixar Haifa após a tentativa de Irgun de plantar uma bomba sem sucesso. Eles alegaram ter calculado mal os efeitos da explosão que fez o SS Patria afundar.

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Patria_disaster), [JPost](https://www.jpost.com/blogs/my-nation-lives/this-week-in-israeli-history-operation-moses-raful-eitan-and-the-patria-disaster-435421).

**Atentado a bomba no hotel King David** (1946), onde terroristas sionistas de direita atacaram um QG britânico localizado dentro do hotel, matando 91 e ferindo 46. O líder dos perpetradores, Manachem Begin, tornou-se o primeiro-ministro de Israel em 1977.

**Fontes:** [Wiki](https://en.m.wikipedia.org/wiki/King_David_Hotel_bombing), [Haaretz](https://www.haaretz.com/israel-news/.premium-historian-calls-1946-attack-on-king-david-hotel-terror-1.5414668).

**Massacre de Deir Yassin** (1948), onde 117 aldeões, incluindo mulheres, idosos e crianças, foram massacrados por grupos paramilitares sionistas. Os relatos de primeira mão incluíam o relato de um "jovem" amarrado a uma árvore e incendiado, uma mulher e um velho baleados nas costas e meninas alinhadas contra a parede e disparadas com uma submetralhadora.

Alguns membros dos perpetradores foram posteriormente absorvidos pelas FDI, um (Menachem Begin) tornou-se Primeiro-Ministro de Israel. Mais tarde, ele declarou em seu livro "Sem o que foi feito em Deir Yassin, não teria existido um Estado de Israel."

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Deir_Yassin_massacre), [Haaretz](https://www.haaretz.com/israel-news/MAGAZINE-testimonies-from-the-censored-massacre-at-deir-yassin-1.5494094), [artigo do professor da Universidade de Massachusetts (PDF)](https://www.google.com/url?sa=t&source=web&rct=j&url=https://people.umass.edu/olga/MyArticles/Gershenson_WeAreVictimsofOurPast.pdf&ved=2ahUKEwiG99LG8P3wAhUBWX0KHRNrArcQFjAMegQICBAC&usg=AOvVaw2rjjGYofuM3hh7Ml7U2kVq&cshid=1622806675105), [Documentário 'Born in Deir Yassin'](https://youtu.be/kiLt5awzmyI), [Britannica](https://www.britannica.com/topic/Irgun-Zvai-Leumi).

**O êxodo palestino "Nakba"** (1948) foi a expulsão de 700.000 árabes palestinos de sua terra natal durante a guerra e o estabelecimento de Israel em maio de 1948, incluindo testemunhos de massacres, estupros e saques. Durante e imediatamente após a criação do estado, Israel expropriou aproximadamente 4.244.776 acres de terras palestinas.

**Fontes:** [Haaretz](https://www.haaretz.com/middle-east-news/palestinians/.premium.MAGAZINE-hidden-stories-of-the-nakba-1.6010350), [Haaretz](https://www.haaretz.com/whdcMobileSite/israel-news/.premium.MAGAZINE-how-israel-systematically-hides-evidence-of-1948-expulsion-of-arabs-1.7435103), [Atlantic](https://theatlantic.com/article/560294/), [Vox](https://www.vox.com/2018/11/20/18080030/israel-palestine-nakba), [The Walls Of Jerusalem](https://books.google.com.sg/books?id=D7p9DwAAQBAJ&pg=PA145&lpg=PA145&dq=4244776+acres&source=bl&ots=d-nqWE37SJ&sig=ACfU3U2qcmZrtLXyEFw-mY766JMFS8EIrQ&hl=en&sa=X&ved=2ahUKEwiEkJ7b5LXpAhWLdn0KHSnyDVcQ6AEwAnoECAQQAQ#v=onepage&q=145&f=false).

**Massacre de Abu Shusha** (1948), onde uma vila foi atacada pela Brigada Kiryati durante a Operação Barak. Durante o ataque, foi relatado que um soldado da Haganah tentou estuprar uma prisioneira de 20 anos duas vezes.

O historiador israelense Aryeh Yitzhaki explica os eventos como um massacre citando um testemunho do Kheil Mishmar (unidades de guarda):
>Um soldado da Brigada Kiryati capturou 10 homens e 2 mulheres. Todos foram mortos, exceto uma jovem que foi estuprada e eliminada. Na madrugada de 14 de maio, unidades da brigada Giv'ati atacaram a aldeia de Abu Shusha. Os aldeões em fuga foram mortos à vista. Outros foram mortos nas ruas ou machados até a morte. Alguns foram alinhados contra uma parede e executados. Nenhum homem foi deixado; as mulheres tinham que enterrar os mortos.

Um livro do cientista político israelense Meron Benvenisti mencionou que, em 1995, uma vala comum com 52 esqueletos foi descoberta em Abu Shusha, mas a causa da morte é indeterminada.

Mais recentemente, uma pesquisa conduzida pela Birzeit University, principalmente com base em entrevistas com ex-residentes, sugere que entre 60-70 residentes foram mortos durante o ataque.

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Abu_Shusha), [Livro de Benny Morris pg. 257](https://books.google.com.sg/books?id=uM_kFX6edX8C&pg=PA257&redir_esc=y#v=onepage&q&f=false), [Gutenburg.org](http://self.gutenberg.org/articles/Abu_Shusha_massacre), [Birzeit University Research](http://web.archive.org/web/20031205170448/www.birzeit.edu/ourvoice/history/jan99/destvil.html), [Livro de Meron Benvenisti pg. 248](https://archive.org/details/cityofstonehidde00benvrich/page/248/mode/1up), [Ahram.org](https://english.ahram.org.eg/NewsContent/50/1203/397269/AlAhram-Weekly/World/Anatomy-of-a-massacre.aspx).

**Massacre de Lydda** (1948) liderado por Palmach (considerada a "ponta da lança" durante a guerra e no estabelecimento das FDI) durante a 'Operação Danny', cujo objetivo final durante a Guerra Árabe-Israelense era ganhar o controle da estrada para Jerusalém.

Kenneth Bilby estava na cidade na época. Ele escreveu:
>[A coluna de jipe ​​israelense] correu para Lydda com rifles, Stens e submetralhadoras em chamas. Percorreu as ruas principais, explodindo em tudo que se movia... os cadáveres de homens, mulheres e até mesmo árabes crianças foram espalhadas pelas ruas na sequência desta carga cruelmente brilhante.

O ataque durou 47 minutos, deixando 100-150 árabes palestinos mortos, de acordo com o 89º Batalhão de Moshe Dayan. Yoav Gelber descreve o que se seguiu como provavelmente o massacre mais sangrento da guerra. Moshe Kelman ordenou que as tropas atirassem em qualquer alvo claro, incluindo qualquer pessoa vista nas ruas. Soldados israelenses jogaram granadas nas casas onde suspeitavam que os atiradores estivessem se escondendo. Moradores que fugiram de suas casas em pânico foram baleados. Yeruham Cohen, um oficial de inteligência das FDI, disse que cerca de 250 morreram entre 11h30 e 14h.

Em 2013, depoimento prestado a Zochrot, Yerachmiel Kahanovich, um soldado Palmach presente no local, afirmou que ele próprio, em meio ao bombardeio de uma mesquita, havia disparado um míssil antitanque PIAT com enorme impacto de onda de choque dentro da mesquita para onde os palestinos estavam tomando refúgio em, e examinando-o depois encontrou as paredes espalhadas com os restos mortais de pessoas. Ele também afirmou que qualquer pessoa que se desviou da trilha de voo foi morta a tiros. Este evento também foi confirmado por Binyamin Roski, que também era um soldado Palmach da Brigada Yiftach.

De acordo com Morris, dezenas morreram na mesquita, incluindo homens, mulheres e crianças desarmados; uma testemunha ocular publicou um livro de memórias em 1998 dizendo que ele havia removido 95 corpos de uma das mesquitas.

O Dr. Klaus Dreyer, do Corpo Médico das FDI, reclamou em 15 de julho (3 dias depois) que ainda havia cadáveres dentro e ao redor de Lydda, o que constituía um perigo para a saúde e uma "questão moral e estética".

**Fontes:** [entrevista de Benyamin Roski](https://youtu.be/6qHDlqv1kLM), [entrevista de Yirachmiel Kahanovich](https://zochrot.org/en/testimony/54345), [Ilan Pappé: Salvaging Nakba Documents](https://www.palestine-studies.org/en/node/1650358), [972mag](https://www.972mag.com/despite-efforts-to-erase-it-the-nakbas-memory-is-more-present-than-ever-in-israel/), [Journal](https://www.jstor.org/stable/4327250?seq=1).

**Documento dos arquivos FDI** (1948) que após a tradução afirma:

![](images/IDF_document.jpg)
>Nas aldeias situadas entre Nahraiya e Tarshiha, não há tropas estrangeiras por enquanto. As aldeias estão armadas e prontas para a ação1. Suas ordens são para ocupar, matar os homens2, destruir e incendiar as aldeias de Kabri, Umm al- Faraj e al-Nahr.

Ilan Pappe observa que os moradores não tinham armas adequadas, apenas para fins de caça e pessoas que serviam na polícia; sem as tropas árabes ausentes, eles não tinham chance. Em abril de 1948, a Inteligência de Hagana definiu os homens como qualquer pessoa com mais de 10 anos

**Fontes:** Foto tirada dos Arquivos FDI 1676\51\12, Benny Morris: O Nascimento do Problema dos Refugiados Palestinos, 1947-1949, ISBN 0-521-33028-9. P. 133, Arquivos Hagana, 100\35, datado de 19 de maio de 1948.

**Assassinato de Folke Bernadotte**, (1948) Mediador da ONU para Refugiados Palestinos pelo Grupo Paramilitar Sionista de Israel, Lehi. O assassinato foi aprovado pelo 'centro' de Lehí, um dos quais, após o assassinato, tornou-se o 7º Primeiro Ministro de Israel (Yitzhak Shamir). Ninguém foi condenado pelo assassinato.

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Folke_Bernadotte#Assassination), [Independent](https://www.independent.co.uk/news/world/middle-east/israels-forgotten-hero-the-assassination-of-count-bernadotte-and-the-death-of-peace-934094.html), [Haaretz](https://www.haaretz.com/opinion/the-lessons-of-folke-bernadotte-1.5419395), [WRMEA](https://www.wrmea.org/1995-september/jewish-terrorists-assassinate-u.n.-peacekeeper-count-folke-bernadotte.html).

**O massacre de Al Dawayima** (1948) ocorreu depois que a cidade foi ocupada pelo 89º Batalhão de Comando das FDI. De acordo com o líder da aldeia, as tropas das FDI atiraram indiscriminadamente por mais de uma hora, durante a qual muitos fugiram. Ao voltar no dia com outros moradores, 60 corpos foram encontrados na mesquita, a maioria homens idosos. Numerosos cadáveres de homens, mulheres e crianças jaziam nas ruas. 80 corpos de homens, mulheres e crianças foram encontrados na entrada da caverna El Zagh. De acordo com um soldado das FDI:
>a 1ª [onda] de conquistadores matou cerca de 80 a 100 homens, mulheres e crianças. As crianças mataram quebrando suas cabeças com paus. Não havia uma casa sem mortos.
>
>1 comandante ordenou a seus homens que colocassem 2 mulheres idosas na casa e a explodissem. 1 soldado se gabou de ter estuprado uma mulher e depois atirado nela. 1 mulher, com um bebê nos braços, foi contratada para limpar o pátio onde o soldados comeram. Ela trabalhou um ou dois dias. No final, atiraram nela e em seu bebê.

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Al-Dawayima_massacre), [Mondoweiss](http://mondoweiss.net/2016/02/barbarism-by-an-educated-and-cultured-people-dawayima-massacre-was-worse-than-deir-yassin/), [Haaretz](https://www.haaretz.com/israel-news/.premium-the-poem-that-exposed-atrocities-perpetrated-by-israel-in-48-1.5418995).

**Massacre de Safsaf** (1948) depois que a aldeia da Alta Galiléia se rendeu às FDI, 54-62 moradores tiveram suas mãos amarradas, mortos a tiros e enterradas em um fosso. Também houve 3 relatos de estupro, incluindo o estupro e assassinato de uma garota de 14 anos. Yosef Nachmani (oficial sênior do Haganah e também diretor do Fundo Nacional Judaico) confirmou o incidente em seus diários.

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Safsaf_massacre), [Jewish Currents](https://archive.jewishcurrents.org/october-29-safsaf-massacre-1948/), [Haaretz](https://www.haaretz.com/israel-news/.premium.MAGAZINE-how-israel-systematically-hides-evidence-of-1948-expulsion-of-arabs-1.7435103).

**O Massacre de Hula** (1948) ocorreu em uma vila no Líbano onde, de acordo com um superior das FDI, 35-58 homens com idades entre 15-60 anos que não representaram resistência e se renderam imediatamente foram executados por 2 oficiais das FDI com uma submetralhadora e tiveram a casa explodida em cima deles como uma vingança por um incidente separado com o qual os homens não tiveram nada a ver.

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Hula_massacre), [Haaretz](https://www.haaretz.com/israel-news/.premium-israeli-who-commanded-massacre-of-dozens-of-arab-captives-in-1948-dies-at-93-1.7022943).

Outro documento dos arquivos das FDI.

![](images/IDF_document_2.png)

Contexto: Os aldeões das áreas rurais da Palestina começaram a resistir aos ataques sionistas, inclusive tentando retornar às aldeias de onde haviam sido expulsos. Essas devoluções "não autorizadas" levaram, por sua vez, à realização de mais duas operações de limpeza étnica

Após a tradução:
>Pedidos para a Operação #40
>
>1. Intel: Há um movimento evidente de civis árabes de Gaza em direção ao norte para a Vila Majdal. Os árabes residem em várias aldeias e aí se estabeleceram. Nossas forças: Em Majdal - Forças da Administração Militar, esquadrão da Marinha na delegacia de Jura, 11ª Brigada em Beit Hanoun.
>2. Missão: Expulsão dos aldeões árabes das aldeias mencionadas e negação de seu retorno mais uma vez com a demolição de suas casas.
>3. Método: a) Faça um levantamento das aldeias: Hamama, Jura, Khirbet Khisas, Ni'ilya, Al Jiyya, Barbara, Bayt Jirja, Hiribya, Dayr Sunayd, reúna todos os residentes, carregue-os em veículos e os deporte para Gaza. Eles precisam ser transportados além de nossas linhas para Beit Hanoun. b) Em Majdal, separe os residentes locais e os refugiados e deporte os refugiados conforme mencionado acima. c) Queimar as aldeias e destruir a pedra, as casas. d) Inspecionar os refugiados para verificar se não foram infiltrados pelos batedores inimigos e prendê-los. e) Levantamento das formas de movimentação dos refugiados e colocação de minas nessas rotas.

**Fonte:** Mensagem do Distrito da Planície Costeira para o Batalhão 151, 25 de novembro de 1948, 6308/49/141, arquivos das FDI.

**Testemunho de Zvi Steklov** (ex-oficial das FDI: Batalhão e subcomandante do Nahal)

Após a guerra em 1948, como comandante da área estratégica de Gush Yoav, Zvi levou o Primeiro-Ministro Ben Gurion e o Chefe do Estado-Maior Moshe Dayan em uma excursão. Durante a excursão, ele recebeu ordens de Dayan para saquear uma lua crescente do topo de uma mesquita na vila destruída de Al-Dawayima (cenário de um massacre de guerra em outubro de 1948) e trazê-la para seu escritório na manhã seguinte. Para obedecer a essa ordem, Zvi foi forçado a explodir a mesquita.

**Fonte:** [Entrevista com Zvi Steklov](https://zochrot.org/en/video/54716).

**Estupro coletivo e execução de uma menina beduína capturada pelas FDI em Negev**. De acordo com documentos divulgados, 20 soldados participaram do episódio, incluindo um comandante de pelotão. Todos foram submetidos à corte marcial, mas receberam sentenças de prisão relativamente baixas. (Agosto de 1949)

**Fontes:** [Haaretz](https://www.haaretz.com/1.4746524), [The Guardian](https://www.theguardian.com/world/2003/nov/04/israel1).

**Laços comerciais e militares secretos de Israel com o Apartheid na África do Sul**. Incluindo vendas de armas e ofertas de ogivas nucleares, retirados de documentos sul-africanos desclassificados (finais dos anos 1940-1980)

**Fontes:** Livro completo: "[Unspoken Alliance: Israel's Secret Relationship with Apartheid South Africa](https://books.google.com.sg/books?id=cX0tVB5nu_YC&pg=PA82&source=gbs_selected_pages&cad=3#v=onepage&q&f=false)", [entrevista com o autor](https://youtu.be/UeYgKl1o1eA), [The Guardian](https://theguardian.com/world/2006/feb/07/southafrica.israel), [Haaretz](https://www.haaretz.com/opinion/.premium-why-israel-supported-apartheid-regime-1.5298552), [New York Times](https://www.nytimes.com/1987/01/29/world/israelis-reassess-supplying-arms-to-south-africa.html).

**Os bombardeios de Bagdá** (1950-1951) foram uma série de bombardeios de bandeira falsa contra alvos judeus em Bagdá com a intenção de encorajar os judeus iraquianos a imigrar para Israel. De acordo com Moshe Gat, Meir-Glitzenstein, Samuel Klausner, Rayyan Al-Shawaf e Yehouda Shenhav, há amplo consenso entre os judeus iraquianos de que os sionistas estavam por trás dos atentados. Essa crença foi compartilhada entre o historiador Abbas Shiblak, o judeu iraquiano Naeim Giladi, o agente da CIA Wilbur Crane e a Embaixada Britânica de Bagdá. Quando as 'operações de bandeira falsa do Caso Lavon (1954)' estouraram, Shalom Cohen (político israelense do Knesset em um estágio posterior) observou:
>Este método de operação não foi inventado para o Egito. Foi tentado antes no Iraque.

Em uma publicação de 2006, Yehuda Tager, um agente israelense que operava em Bagdá, confirmou o envolvimento sionista nos atentados.

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/1950%E2%80%931951_Baghdad_bombings), [Journal](https://www.jstor.org/stable/4283249?seq=1), [Haaretz](https://www.haaretz.com/1.4900019).

**Massacre de Qibya** (1953), onde as tropas das FDI sob Ariel Sharon mataram pelo menos 69 aldeões palestinos, dois terços deles eram mulheres e crianças. 45 casas, uma escola e uma mesquita foram destruídas. O ataque começou com uma barragem de morteiros na aldeia. As FDI implantaram cargas explosivas para romper a cerca farpada e estradas minadas para evitar a intervenção das forças jordanianas. Ao mesmo tempo, pelo menos 25 morteiros foram disparados contra Budrus (aldeia vizinha) antes que as tropas das FDI entrassem simultaneamente na aldeia por 3 lados. Ariel Sharon escreveu: "As ordens foram claras, Qibya era para ser um exemplo para todos." Os documentos originais mostraram que Sharon ordenou pessoalmente às suas tropas que atingissem "o máximo de mortes e danos à propriedade", e os relatórios pós-operacionais falam em arrombamento de casas e limpeza com granadas e tiros.

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Qibya_massacre), [972mag](https://www.972mag.com/how-the-media-covers-massacres-lessons-from-1950/), [Cambridge](https://www.cambridge.org/core/journals/modern-american-history/article/edge-of-the-abyss-the-origins-of-the-israel-lobby-19491954/E1690BDB5CA87C66B2B65D12CA1D716A/core-reader), [Haaretz](https://www.haaretz.com/opinion/.premium-israel-prefers-not-to-air-its-dirty-laundry-1.5375076), [Counter Punch](https://www.counterpunch.org/2003/10/14/qibya-and-sharon-50-years-later/).

**Caso Lavon** (1954) uma operação de bandeira falsa fracassada em que judeus egípcios foram recrutados pela Inteligência Militar de Israel para plantar bombas em alvos civis egípcios, americanos e britânicos, seu plano frustrado era culpar a Irmandade Muçulmana pelos ataques.

**Fontes:** [Haaretz](https://www.haaretz.com/israel-news/israel-reveals-controversial-lavon-affair-correspondence-62-years-later-1.5401166), [Times of Israel](https://www.timesofisrael.com/new-revelations-in-lavon-affair-raise-more-questions-then-they-answer), [Wiki](https://en.wikipedia.org/wiki/Lavon_Affair).

**Massacre de Kafr Qasim** (1956), onde a Polícia de Fronteira de Israel matou 49 árabes (19 homens, 6 mulheres, 23 crianças, 1 filho em gestação) voltando para casa do trabalho durante um toque de recolher que desconheciam. Os envolvidos no massacre foram considerados culpados, mas foram perdoados e libertados da prisão em um único ano. O oficial de mais alta patente processado pelo massacre confessou antes de sua morte que o massacre foi planejado para purificar etnicamente os árabes israelenses da região e que seu julgamento foi encenado para proteger as elites políticas e militares israelenses, incluindo o primeiro-ministro Ben Gurion, de assumir a responsabilidade para o massacre.

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Kafr_Qasim_massacre), [Haaretz](https://www.haaretz.com/israel-news/.premium.MAGAZINE-general-s-confession-links-massacre-to-israel-s-secret-plan-to-expel-arabs-1.6550421), [972mag](https://972mag.com/48-human-beings-were-massacred-and-we-have-forgotten-them/81313/), [Medium](https://medium.com/@thepalestineproject/israel-must-acknowledge-the-kafr-qasim-massacre-dc62349b2956).

**Caso Apollo** (1965), um incidente em que 200-600 libras de urânio altamente enriquecido desapareceram de uma Corporação de Materiais e Equipamentos Nucleares em Pittsburgh, Pensilvânia. Altamente suspeito de ter ido para o Programa Nuclear de Israel, especialmente devido à visita de Rafi Eitan, que estava disfarçado de químico, mas depois se revelou um espião israelense e que mais tarde esteve envolvido no incidente de Jonathan Pollard.

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/The_Apollo_Affair), [LATimes](https://www.latimes.com/archives/la-xpm-1986-06-16-mn-11009-story.html), [Mondoweiss](https://mondoweiss.net/2019/03/center-israels-uranium/#_ednref1), [arquivo das descobertas de Roger Mattson](https://nsarchive.gwu.edu/briefing-book/nuclear-vault/2016-11-02/numec-affair-did-highly-enriched-uranium-us-aid-israels-nuclear-weapons-program), um físico que investigou o incidente para a Comissão de Regulamentação Nuclear, [YouTube](https://youtu.be/hUJHA9VhUZE), [relatório do FBI de 1968](https://nsarchive2.gwu.edu/dc.html?doc=3149962-10-Memorandum-from-SAC-WFO-to-Director-FBI), [memorando desclassificado do FBI em PDF](http://irmep.org/ila/numec/03091972_FBI.pdf).

**Incidente de USS Liberty** (1967), onde Israel atacou deliberadamente um navio de carga americano, matando 34 e ferindo 174 sem qualquer reincidência.

Documento da CIA, ainda parcialmente censurado:
>Mas, senhor, é um navio americano.
>
>Deixa pra lá, bata nela!

O diretor da NSA, o tenente-general Marshall Carter, disse mais tarde ao Congresso:
>O ataque "não poderia ser outra coisa senão deliberado".

Benson Buffham, um ex-vice-diretor da NSA, disse em uma entrevista:
>Não acho que você encontrará muitas pessoas na NSA que acreditam que foi acidental.

**Fontes:** [Relatos de sobreviventes do USS Liberty](https://youtu.be/6XFTn70Rdso), [Haaretz](https://www.haaretz.com/us-news/but-sir-its-an-american-ship-never-mind-hit-her-1.5492908), [Chicago Tribune](http://articles.chicagotribune.com/2007-10-02/news/0710010866_1_israeli-pilots-spy-ship-uss-liberty/), [Wiki](https://en.m.wikipedia.org/wiki/USS_Liberty_incident).

**Massacre de Ras Sedr** (1967), onde 49-52 prisioneiros de guerra egípcios foram executados pelas FDI durante a guerra de seis dias. A vala comum foi encontrada em junho de 2000. Um relatório detalhou confissões de oficiais israelenses que testemunharam o ato, incluindo um Brigadeiro-General das FDI que admitiu ter matado 49 prisioneiros de guerra egípcios no deserto do Sinai em entrevistas posteriores após a aposentadoria.

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Ras_Sedr_massacre), [IOL](https://www.iol.co.za/news/africa/executed-egyptain-troops-grave-found-41369), [Al-Araby](https://english.alaraby.co.uk/english/comment/2016/10/24/who-was-responsible-for-israels-1967-massacre), [Reuters](https://www.reuters.com/article/us-egypt-israel-killings-idUSL0464322620070304), [New York Times](https://www.nytimes.com/1995/09/21/world/egypt-says-israelis-killed-pow-s-in-67-war.html).

**Bombardeio na Escola Primária Bahr El-Baqar** (1970), onde a Força Aérea Israelense bombardeou uma escola no Egito, matando 46 crianças e ferindo outras 50. Embora muitos oficiais israelenses considerem isso um erro humano, Moshe Dayan (então ministro da defesa) e Yosef Tekoah (então enviado israelense à ONU) têm defendido constantemente o bombardeio.

**Fontes:** [Military.wikia](https://military.wikia.org/wiki/Bahr_El-Baqar_primary_school_bombing), [Wiki](https://en.wikipedia.org/wiki/Bahr_El-Baqar_primary_school_bombing), [India Print](https://www.theindiaprint.com/international-news-english/the-israeli-army-had-bombed-the-school-of-children-from-the-sky-the-reason-for-some-photos-to-hide-the-mistake-38778), [Ahram](https://english.ahram.org.eg/NewsContent/1/64/199065/Egypt/Politics-/Egypts-Bahr-AlBaqar-Flashbacks-of-an-Israeli-war-c.aspx), [Mondoweiss](https://mondoweiss.net/2018/04/unhappy-history-massacres/), [Jewish Telegraph](https://www.jta.org/1970/04/10/archive/dayan-states-if-israeli-planes-did-hit-school-it-was-inside-military-installation).

**Massacre de Sabra & Shatila** (1982), perpetrado por uma milícia cristã libanesa (falangistas), que estava sob o controle político e militar de Israel. Por 3 dias, eles se envolveram em estupro, assassinato e mutilação de mulheres e crianças com o conhecimento das tropas das FDI que cercavam os campos de refugiados. As forças israelenses dispararam foguetes para o céu noturno para iluminar a escuridão para os falangistas, permitiram que reforços entrassem na área no segundo dia do massacre e forneceram escavadeiras que foram usadas para eliminar os corpos de muitas vítimas. Contagem estimada de mortes: 2000 - 3500. Ariel Sharon foi considerado responsável pelo massacre, mas ainda foi eleito primeiro-ministro de Israel em 2001:

Janet Lee Stevens, uma jornalista americana, escreveu a seu marido, Dr. Franklin, após visitar as aldeias imediatamente após o massacre:
>Eu vi mulheres mortas em suas casas com as saias até a cintura e as pernas abertas; dezenas de jovens baleados após serem alinhados contra a parede de um beco; crianças com a garganta cortada, uma mulher grávida com o estômago aberto, seus olhos ainda bem abertos, seu rosto enegrecido silenciosamente gritando de horror; incontáveis ​​bebês e crianças pequenas que foram esfaqueadas ou rasgadas e que foram jogadas em pilhas de lixo.

**Fontes:** [Vídeo da NSFL](https://youtu.be/kYKxPxsVbt4), [Sciences Po](https://www.sciencespo.fr/mass-violence-war-massacre-resistance/fr/document/sabra-and-chatila.html), [TRT World](https://www.trtworld.com/middle-east/remembering-the-sabra-and-shatila-massacres-29865), [AJ](https://www.aljazeera.com/indepth/opinion/remembering-sabra-shatila-massacre-35-years-170916101333726.html).

**Incidente de Jonathan Pollard** (1984), onde Israel recompensou generosamente um oficial de inteligência americano em troca de informações classificadas dos EUA. De acordo com seu manipulador, Rafi Eitan (espião do Mossad envolvido no "Caso Apollo"), antes da captura de Pollard, ele foi avisado de que foi descoberto e que recebeu um sinal para deixar os EUA, mas em vez disso, Pollard:
>vagou por aí por 3 dias. Ele teve muitas oportunidades de fazer o que eu disse a ele e ele não fez.

Após sua libertação da prisão em 2020, Pollard e sua esposa imigraram para Israel, [ele recebeu a chegada de herói de Netanyahu em sua chegada](https://www.theguardian.com/world/2020/dec/30/former-spy-jonathan-pollard-arrives-in-israel-from-us). Após seu voo em um jato particular de propriedade do mega-doador sionista de Trump, Sheldon Adelson.

2021, Pollard durante uma [entrevista com 'Israel Hayom'](https://www.israelhayom.com/2021/03/25/american-jewry-consider-themselves-more-american-than-they-do-jews/), quando questionado sobre sua dupla lealdade, ele respondeu "Sinto muito, somos judeus, e se formos judeus, sempre teremos dupla lealdade" Ele então sugere que jovens judeus deveriam considerar espionar para Israel.

**Fontes:** [Britannica](https://www.britannica.com/biography/Jonathan-Pollard), [Haaretz](https://www.haaretz.com/ex-handler-pollard-bungled-85-escape-plan-1.5338938), [Wiki](https://en.wikipedia.org/wiki/Jonathan_Pollard), [Jewish News](https://jewishnews.timesofisrael.com/jonathan-pollard-young-jews-should-consider-spying-for-israel/).

**Ex-técnico nuclear israelense que se tornou denunciante foi atraído para a Itália, onde o Mossad o drogou e sequestrou de volta a Israel** para o julgamento que foi realizado secretamente a portas fechadas. Ele passou 18 anos na prisão, incluindo mais de 11 em confinamento solitário. Diz que sofreu "tratamento cruel e bárbaro" nas mãos das autoridades israelenses enquanto estava preso por ser cristão. Após sua libertação em 2004, ele foi submetido a uma ampla gama de restrições muito rígidas em sua fala e movimento (1986).

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Mordechai_Vanunu), [Youtube](https://youtu.be/oVrsxt2mP4s), [New York Times](https://www.nytimes.com/2004/04/21/international/middleeast/21CND-NUCL.html?scp=1&sq=Vanunu%20drugged&st=cse), [The Guardian](https://theguardian.com/world/2018/mar/28/mordechai-vanunu-israel-spying-nuclear-1988), [CNN](http://www.cnn.com/2004/WORLD/meast/04/21/israel.vanunu/), [Wired](https://www.wired.com/2007/10/dayintech-1005/), [Telegraph](https://www.telegraph.co.uk/news/2017/01/23/israeli-nuclear-whistleblower-mordechai-vanunu-convicted-meetings/).

**Operação Trojan** (1986), de acordo com o ex-agente do Mossad em seu livro, o Mossad instigou o bombardeio dos EUA na Líbia plantando um dispositivo de transmissão e se fazendo passar pelo governo líbio, enviando ordens terroristas às suas embaixadas ao redor do mundo. (História na página 113-117) ([Arquivo PDF](https://www.riksavisen.no/uploads/other-side-of-deception-victor-ostrovsky.pdf) para download).

**Caverna do massacre de patriarcas** (1994) colono americano-israelense, Baruch Goldstein abriu fogo na mesquita Ibrahimi, matando 29 (de acordo com as FDI)/48 (de acordo com testemunhas palestinas) e ferindo mais de 125. 100s de israelenses se reúnem para homenagear o assassino quando ele foi enterrado. A conspiração surge quando as pessoas descobrem que os soldados das FDI que deveriam vigiar a mesquita não estavam presentes no dia do ataque.

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Cave_of_the_Patriarchs_massacre), [JPost](https://www.jpost.com/Arab-Israeli-Conflict/The-Goldstein-massacres-shadow-on-Hebron-58423), [New York Times](https://www.nytimes.com/1994/04/01/world/hundreds-of-jews-gather-to-honor-hebron-killer.html), [LA Times](https://www.latimes.com/archives/la-xpm-1994-03-10-mn-32305-story.html).

**Massacre de Qana** (1996) As IDF dispararam projéteis de artilharia contra um complexo da ONU onde 800 civis libaneses estavam se refugiando. 106 civis foram mortos, 116 feridos, 4 forças da ONU em Fiji também ficaram gravemente feridas.

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Qana_massacre), [Al-Araby](https://www.alaraby.co.uk/english/society/2016/9/28/in-lebanon-the-qana-massacre-will-be-peres-legacy), [Independent](https://www.independent.co.uk/voices/shimon-peres-dies-israel-qana-massacre-never-forget-no-peacemaker-robert-fisk-a7334656.html), [Daily Motion](https://www.dailymotion.com/video/x2uiz2l).

**5 israelenses trabalhando para uma empresa israelense "Urban Moving" foram presos** em 11 de setembro depois de serem vistos "documentando" (suas próprias palavras durante uma entrevista israelense) e celebrando o ataque ao WTC. O proprietário da empresa, Dominik Suter, fugiu para Israel após o incidente. Seu nome apareceu na Lista de Suspeitos do FBI de maio de 2002, junto com os sequestradores do 11 de setembro e outros extremistas suspeitos. Israel ainda não o extraditou (2001).

**Fontes:** [9/11 War by Deception](https://youtube.com/watch?feature=youtu.be&t=1541&v=pK6VLFdWJ4I), [YouTube](https://youtu.be/fOiCMMMeXE8), [YouTube](https://youtu.be/yvYcYwNuSYo), [ABC](https://abcnews.go.com/2020/story?id=123885&page=1), [Forward](https://forward.com/news/national/325698/spy-rumors-fly-on-gusts-of-truth/), [New York Times](https://www.nytimes.com/2001/10/08/nyregion/5-young-israelis-caught-in-net-of-suspicion.html), [Haaretz](https://www.haaretz.com/1.5396918).

**A Nova Zelândia impôs sanções diplomáticas a Israel** por causa de um incidente em que 2 israelenses radicados na Austrália, que supostamente trabalhavam para o Mossad, tentaram obter passaportes da Nova Zelândia de forma fraudulenta alegando a identidade de um homem deficiente. Acredita-se que 2 outros tenham sido os 3º e 4º homens envolvidos no escândalo, mas eles conseguiram deixar a Nova Zelândia antes de serem presos. O Primeiro-Ministro neozelandês viu o incidente como "não apenas totalmente inaceitável, mas também uma violação da soberania da NZ e da lei internacional". No ano seguinte, o ministro das Relações Exteriores de Israel se desculpou pelo incidente. (2004)

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/2004_Israel%E2%80%93New_Zealand_passport_scandal), [NZ Herald](https://www.nzherald.co.nz/israeli-spy-case/news/article.cfm?c_id=606&objectid=3596863).

**2º Massacre de Qana** (2006) - ataque aéreo executado pelas IDF em um prédio no Líbano durante a guerra. 28 civis foram mortos, dos quais 16 eram crianças. Após investigação, as IDF concluíram que não havia de fato nenhum foguete lançado de Qana no dia do ataque mortal das IDF.

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Qana_airstrike), [BBC](http://news.bbc.co.uk/2/hi/middle_east/5228554.stm), [Norman Finkelstein](http://normanfinkelstein.com/2006/08/02/comrades-the-party-line-has-changed/).

**Memorandos da CIA**: o Mossad israelense se passou por agentes da CIA para recrutar terroristas para realizar operações de bandeira falsa contra o Irã (2007-2008).

**Fontes:** [Haaretz](https://www.haaretz.com/1.5164851), [Foreign Policy](https://foreignpolicy.com/2012/01/13/false-flag/).

**Operação 'Chumbo Fundido'** (2008-2009): estimativa de 1391 palestinos mortos, 759 civis, 344 crianças, 110 mulheres. 13 israelenses mortos, dos quais 10 são soldados. Israel também admite o uso de 'Fósforo Branco' durante sua ofensiva militar.

**Fontes:** [Haaretz](https://www.haaretz.com/1.5129727), [Haaretz](https://www.haaretz.com/1.5092160), [Amnesty](https://www.amnesty.org/en/documents/MDE15/015/2009/en/), [France 24](https://www.france24.com/en/20090802-israel-acknowledges-use-white-phosphorus-shells-gaza-civilians), [Investigation: F.A](https://forensic-architecture.org/investigation/white-phosphorus-in-urban-environments).

**4 cientistas nucleares iranianos foram assassinados e outro ficou gravemente ferido em uma tentativa de homicídio** entre (2010-2012). Oficiais dos EUA confirmam que o grupo terrorista, MEK, foi financiado, treinado e armado por Israel para matar os cientistas nucleares.

**Fontes:** [NBC](http://www.nbcnews.com/id/46331967/ns/about-press_releases/t/rock-center-brian-williams-web-exclusive-israel-teams-terror-group-kill-irans-nuclear-scientists-us-officials-tell-nbc-news/), [The Guardian](https://www.theguardian.com/commentisfree/2012/jan/16/iran-scientists-state-sponsored-murder), [The Guardian](https://www.theguardian.com/world/2012/jan/11/secret-war-iran-timeline-attacks).

**Em 2013, Israel concedeu à 'Genie Energy' (empresa americana de petróleo e gás) os direitos de perfurar nas Colinas de Golan** (território ocupado pela Síria), apesar da oposição internacional, incluindo especialistas israelenses em direito internacional. Em 2015, Israel alegou uma descoberta massiva de petróleo na área, supostamente acreditando que poderia suprir as necessidades de petróleo do país por muitos anos.

Figuras proeminentes no Conselho Consultivo da Genie Energy:

Jacob Rothschild (banqueiro britânico da proeminente família Rothschild), Dick Cheney (ex-vice-presidente dos EUA), Rupert Murdoch (bilionário americano Media Mogul), James Woolsey (ex-diretor da CIA), Lawrence Summers (ex-chefe do Tesouro dos EUA ), Bill Richardson (ex-secretário de Energia dos EUA), Michael Steinhardt (corretor de Wall Street, investidor e gerente de fundos de hedge).

**Fontes:** [Business Insider](https://www.businessinsider.com/israel-grants-golan-heights-oil-license-2013-2), [TRT World](https://www.trtworld.com/middle-east/trump-s-golan-heights-move-explained-25243/), [Yahoo Finance](https://finance.yahoo.com/news/genie-energy-gne-announces-additions-123000232.html).

**Operação 'Margem Protetora'** (2014) após o sequestro e assassinato de 3 adolescentes israelenses supostamente pelo Hamas (opiniões divididas entre os serviços de segurança de Israel sobre quem realmente estava por trás disso), punição coletiva desproporcional se seguiu ao povo de Gaza (cerca de 2.104 palestinos mortos, incluindo 1.462 civis, dos quais 495 eram crianças e 253 mulheres. 17.200 casas destruídas ou gravemente danificadas. Mais de 10.000 feridos).

![](images/operation_protective_edge.jpg)

**Fontes:** [NYmag](https://nymag.com/intelligencer/2014/07/hamas-didnt-kidnap-the-israeli-teens-after-all.html), [BBC](https://www.bbc.com/news/world-middle-east-28439404), [B'Tselem](https://www.btselem.org/press_releases/20160720_fatalities_in_gaza_conflict_2014), [Amnesty](https://www.amnesty.org.uk/gaza-operation-protective-edge).

**Ataque Duma Arson** (2015) Colonos israelenses bombardearam uma casa palestina, matando um bebê de 18 meses e seus pais, que morreram de queimadura de terceiro grau semanas depois. Seu filho de 4 anos, que sofreu queimaduras de 2º e 3º graus em mais de 60% do corpo, torna-se o único sobrevivente do ataque. Graffiti deixado pelos incendiários dizia "Vingança, etiqueta de preço, Viva o Messias". Meses depois do incidente, os radicais israelenses [celebram o casamento apunhalando a foto de um bebê palestino de 18 meses que foi morto no ataque incendiário de Duma](https://www.haaretz.com/israel-news/video-hilltop-youth-stab-photo-of-dawabsheh-baby-at-wedding-1.5381297).

Em 2020, Yair Netanyahu [retuitou o recurso legal de crowdfunding para terrorista israelense](https://www.timesofisrael.com/after-sentencing-netanyahus-son-tweets-in-support-of-duma-killer/) condenado pelo ataque incendiário de Duma. Duas dúzias de rabinos israelenses seniores também [emitiram uma declaração apoiando o terrorista](https://www.middleeastmonitor.com/20200918-israel-rabbis-issue-statement-supporting-israeli-terrorist-convicted-of-murdering-dawabsheh-family/).

**Fontes:** [Wiki](https://en.wikipedia.org/wiki/Duma_arson_attack), [The Guardian](https://theguardian.com/world/2015/jul/31/death-18-month-old-in-arson-attack-heightens-tensions-west-bank-israel).
