# Contra Bakunin
Bakunin era elitista na estratégia revolucionária, não acreditava em uma organização revolucionária que compusesse e organizasse o povo, não acreditava em programa de formação para o proletariado médio, acreditava simplesmente numa organização que incitasse o povo à revolta. Cito exemplos:
>Ensinar o povo? Isso seria estúpido… nós não devemos ensinar o povo, mas incitá-lo a revoltar-se. (Paul Avrich, The Russian Anarchists (Oakland: AK Press, 2005))

>Tudo que uma sociedade secreta bem organizada pode fazer é, primeiro, ajudar no nascimento da revolução pela difusão entre as massas de ideias correspondentes aos seus instintos e organizar, não o exército da revolução – o exército deve ser sempre o povo. (No Gods, No Masters)

Ele não tinha em mente uma ideia de organização horizontal que colocasse o povo na participação do processo revolucionário, mas numa sociedade secreta que incitasse o povo à revolta, e considerava o povo como mero exército desse processo revolucionário. (Em off não relacionado a essa crítica, mas ele também era bem antissemita, assim como Proudhon).
