# Como países capitalistas realmente se desenvolveram
Como todos sabemos, a Europa é imperialista e isso já foi tratado em secções anteriores.

Na Suécia, o Estado é dono até de 50% das ações de todas as empresas. O estado comanda a Bolsa de Valores.

Na Noruega, 30% de empregados públicos, mas ela está em 23° no ranking da HF, mesmo sendo o país com maior proporção de propriedade coletiva/estatal na OCDE. Além de poluir países ao redor do mundo e não pagar por esses crimes. E também explora petróleo de muitos países, incluindo o Brasil, quase de graça.

Os países nórdicos são os países com [a maior carga tributária do mundo](https://www.aeaweb.org/articles?id=10.1257/jep.28.4.77).

Singapura está longe de ser um país libertário; contém uma dívida pública de 130% em relação ao PIB, segundo maior fundo de investimento estatal do mundo, lá tudo é regulado, o mesmo partido está há anos no governo, têm as leis mais bizarras do mundo (que se assemelham à imagem que a mídia ocidental tenta passar sobre a Coreia do Norte), além de ter sido financiada para ser uma colônia de férias da coroa britânica (a maior parte do lucro de Singapura vem do capital financeiro, isto é, de riqueza não produzida por ela).

A Áustria era a economia não-comunista mais estatizada do mundo de 1948 até 1990 (até 1990 a Áustria tinha 50% da economia na mão do Estado), período na qual manteve boas relações com o Bloco Soviético e o Ocidente, sendo uma ponte entre os 2. E nesse período, teve o segundo melhor desempenho econômico dentre todos os países da OCDE. Energia e rede ferroviária, por exemplo, seguem na mão do Estado, ou como sócio majoritário ou com controle total.

Alemanha é o país central da UE e ainda hoje se beneficia de créditos coloniais (como no Congo, onde grande parte do seu chocolate vem de crianças trabalhando de graça para a Suíça e Alemanha) e extrai matéria prima da África para vender de volta produtos industrializados.

A Suíça é um paraíso fiscal, não sofreu com as guerras, recebe crédito colonial e só deu direito às mulheres em 1973.

Finlândia ainda explora petróleo em outros países, e se hoje o Brasil perdeu sua soberania sobre seu petróleo, é em parte pela ação do capital finlandês e norueguês.

Estônia até hoje tem taxa de empregabilidade muito mais baixa do que quando era parte da URSS, e 50% da população recebe benefícios de seguro social e pensões todos os meses. Usar a Estônia como modelo de liberalismo seria um grande equívoco. Não há como um país, que pertencia à segunda nação mais desenvolvida (URSS), não ter vantagens advindas de sua formação histórica (setor industrial, infra-estrutura). Com a crise da Rússia em 98, as economias dos Países Bálticos foram arrastadas e entraram e profunda crise também. Foi em 1999 que a Estônia percebeu a importância de se estabelecer uma economia complexa e industrializada para diminuir a instabilidade econômica e promover o desenvolvimento sustentável no longo prazo. Foi no segundo governo de Mart Laar (e não no primeiro) que a Estônia adotou uma política pragmática de suporte ao desenvolvimento tecnológico de sua economia, sobretudo inovações, passando a procurar aumentar o nível de sua complexidade produtiva. Dois exemplos de sucesso são o Skype e o Transferwise.

A Letônia, ao adotar o (neo)liberalismo como motor de desenvolvimento, desindustrializou-se, perdeu "complexidade econômica", e vê a renda per capita de seus cidadãos diminuírem. A sua economia está assentada em finanças, transporte e setor imobiliário [Veja o livro 'Guerra dos lugares: a colonização da terra e da moradia na era das finanças', da Raquel Rolnik, para uma crítica a esse modelo como forma de desenvolvimento]. A Letônia, quando república socialista, era e o país mais desenvolvido entre os bálticos, graças ao seu setor industrial e à complexidade de sua economia, e se tornou a menos desenvolvida após a terapia de choque. A Letônia socialista foi a república responsável por desenvolver parte dos setores complexos da URSS, como TVs, rádios e itens da indústria militar. Note que o conceito de IDH aqui não é usado para determinar riqueza, mas de complexidade econômica, conforme o Observatory of Economic Complexity (OEA), organizado pelo MIT.

Ruanda e Botsuana ainda não se firmaram como países desenvolvidos, não enfrentam problemas globais como a transição demográfica, queda de produtividade e envelhecimento da população e cresceram sob forte intervenção militar e um Estado de vanguarda, mas liberais disseram que foi livre-mercado.

Canadá cometeu genocídio contra seus povos indígenas e roubaram suas terras, e ainda se beneficiam das terras roubadas dos indígenas, e várias igrejas perto de territórios indígenas foram queimadas após descobrirem que uma igreja assassinava crianças indígenas e enterrava no terreno ao lado. Ao todo, mais de mil valas foram descobertas. Em sua colonização, o Canadá tentou provocar um genocidio nas populações indígenas, e para isso, aplicou leis que permitiam ao Estado passar uma espécie de “tutela” de crianças indígenas para as igrejas católicas. Toda criança indígena era tirada de sua família e levada à internatos. Essas crianças tiradas de suas tribos chegavam aos internatos e tinham seus cabelos cortados (o cabelo longo era sagrado para eles). Faziam trabalhos forçado, passavam fome e faleciam de exaustão e por doenças. Isso sem contar os abusos sexuais. Elas desapareciam para sempre. Segundo as fontes dos jornais do Canadá, não se sabe quem está por trás da queimas de igrejas. Mas provavelmente não é mesmo um ato de revolta, mas sim pessoas da própria igreja tentando apagar as poucas provas e documentos sobre o genocidio que provocaram.

Inglaterra ainda tem colônias até hoje, expropriou riqueza de todos os continentes do mundo e em momentos de crise, invoca o 'keynesianismo'.

Japão era um império fascista na Ásia, tem uma economia bem planejada e também foi alvo do Plano Marshall.

Qual é o país capitalista que se sustenta sem a miséria dos outros? Como diz a frase: "De quantos pobres é feito um rico?"
