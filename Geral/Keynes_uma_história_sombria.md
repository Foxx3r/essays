# Keynes: uma história sombria
Keynes viajava para contratar crianças prostitutas, recomendando lugares "onde 'cama e menino' também não fossem caros".

Keynes disse que os judeus têm "instintos profundamente enraizados que são antagônicos e, portanto, repulsivos aos europeus".

Keynes odiava os soviéticos por “alguma bestialidade na natureza russa - ou nas naturezas russa e judia quando, como agora, eles são aliados”.

Em 1936, Keynes disse que sua teoria era mais adequada para o sistema nazista totalitário da Alemanha do que para uma sociedade livre.

Keynes era presidente da Associação Eugenista Britânica, apoiador do imperialismo britânico na Índia e odiava os trabalhadores - defendia que os trabalhadores deveriam parar de se reproduzir, pois ela era muito "bêbada e ignorante" para manter seus números baixos. Para Keynes, uma das causas das depressões é que os trabalhadores, por meio dos sindicatos, tornavam o preço dos salários rígidos e não permitiam o ajuste que traria a economia ao equilíbrio.
>Posso ser influenciado pelo que me parece justiça e bom senso; mas a guerra de classes me encontrará ao lado da burguesia educada.<br/>
>– <cite>Keynes</cite>

Mais detalhes em Keynes: A Critical Life.
