# Desvantagens do sistema monetário
Quais são alguns dos efeitos prejudiciais do sistema monetário?

Existem muitas desvantagens de usar esse velho método de troca para bens e serviços. Irei considerar aqui apenas alguns e deixar você aumentar esta lista por si mesmo.
1. O dinheiro é apenas uma interferência entre o que uma pessoa precisa e o que ela pode obter. Não é de dinheiro que as pessoas precisam, é de acesso aos recursos.
2. O uso de dinheiro resulta em estratificação social e elitismo baseados primeiramente em disparidades econômicas.
3. As pessoas não são iguais sem iguais poderes de compra.
4. A maioria das pessoas são escravas de empregos dos quais não gostam porque precisam de dinheiro.
5. Há uma tremenda corrupção, ganância, crime, fraude e muito mais por causa da necessidade de dinheiro.
6. A maioria das leis são decretadas em benefício das corporações, que tem dinheiro suficiente para praticar lobby, ou persuadir oficiais do governo a criarem leis que servem aos seus interesses.
7. Aqueles que controlam o poder aquisitivo possuem maior influência.
8. O dinheiro é usado para controlar o comportamento daqueles com poder de compra limitado.
9. Bens como alimentos são às vezes destruídos para manter os preços altos; quando as coisas são escassas os preços sobem.
10. Há um tremendo desperdício de material e abuso dos recursos disponíveis nas mudanças superficiais no design de produtos da moda todo ano a fim de se criar mercados contínuos para os fabricantes.
11. Há uma tremenda degradação ambiental devido ao custo elevado de métodos melhores de tratamento de lixo.
12. A Terra está sendo saqueada por lucro.
13. Os benefícios da tecnologia são somente distribuídos para aqueles com poder de compra suficiente.
14. E o mais importante, quando o fator principal da corporação é o lucro, as decisões em todas as áreas são feitas não em benefício das pessoas e do meio ambiente, mas primariamente para a aquisição de riqueza, propriedade e poder.

Alternativas ao sistema capitalista:
- Projeto cibernético do Chile (cybersyn, de Allende)
- Technocracy Inc. da década de 1930
- Teoria do equilíbrio material soviético
- Os zapatistas no México (historicamente e hoje)
- Ideias recentes sobre economias baseadas em recursos
- Confederalismo Democrático da Rojava na Síria
