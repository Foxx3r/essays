# Hitler não era um grande estrategista
Não, Hitler não era um "grande estrategista militar", era tão burro que os Aliados cancelaram seu plano de assassinato (operação Foxley) pois com Hitler era mais fácil de derrubar a Alemanha. Ele era emocionalmente instável, se alguma coisa desse errado no governo, ele nem fazia declarações públicas. Ele era meio narcisista e só saia da cama por causa da cocaína, sabe o tipo mesquinho que não se contenta com nada? Mas tudo bem, vamos aos fatos:
1. Hitler tinha uma organização militar confusa.
2. Mesmo com chefes da SS, marinha, aeronáutica e forças armadas, Hitler quem tomava a decisão final, e muitas vezes isso burocratizava o sistema e impedia as forças militares de fazerem operações conjuntas.
3. Hitler constantemente caia em armadilhas criadas pelos aliados (como a criada por Eisenhower para resolver o impasse na Normandia).
4. Hitler na França agiu brilhantemente para conquistar território, mas contra a URSS, ele tentava matar o máximo de eslavo possível o que culminou em sabotagem por parte dos civis/partisans e consequentemente, mais mortes para o Império Nazista.
5. Ele não tinha um centro de espionagem eficiente, os britânicos literalmente conseguiam tornar tudo espião alemão em um agente duplo.
6. Na ocupação da Renânia, no Anchluss, sobretudo na questão da Tchecoslováquia, ele estava em grande desvantagem, e ganhou todas sem disparar um tiro. Ele estava em desvantagem até na invasão da França. O Fall Gelb foi um triunfo da logística do começo ao fim. Um resultado surpreendente até para os alemães. Ninguém esperava que a França seria derrotada em 6 semanas.

Para leituras mais aprofundadas, recomendo o [Arte Da Guerra](https://en.wikipedia.org/wiki/The_Art_of_War) de [Sun Tzu](https://en.wikipedia.org/wiki/Sun_Tzu).
