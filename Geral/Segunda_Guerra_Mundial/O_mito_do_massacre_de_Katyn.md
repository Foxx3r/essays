# O mito do massacre de Katyn
O massacre de Katyn, segundo as versões mais comuns, foi um crime de guerra soviético na floresta polonesa Katyn em 1940. Os números de mortos às vezes diverge bastante, mas giram em torno de 12.000 e 22.000.

Primeiramente que nem foram os soviéticos, foram os nazistas, e o [Tribunal de Nuremberg](https://fr.wikipedia.org/wiki/Proc%C3%A8s_de_Nuremberg) e as comissões norte-americanas sabiam disso, nos anos 50. A teoria que os soviéticos cometeram o massacre começa na década de 80 (após a crise do petróleo, o avanço do neoliberalismo, destruição do Estado de bem-estar social (keynesianismo) o que implicou em aumento do movimento fascista e de extrema-direita) insurgente na extrema-direita nacionalista polonesa. Nos anos 90, já estava difundido no senso comum, ainda mais depois dos relatórios de Gorbachev e Yeltsin.

Vamos aos fatos que provam que esses relatórios/documentos de Gorbachev e Yeltsin eram falsos:
1. Stalin nunca assinava um documento com "Stalin".
2. Stalin nunca assinava e NÃO PRECISAVA assinar papéis já assinados pelo Comitê Central.
3. As letras de Stalin estavam exageradamente grandes, como se eles quisessem mostrar pata todo mundo que foi Stalin quem assinou.
4. Beria assinou os papéis com uma máquina de escrever de 1969. O massacre ocorreu em 1943. Os documentos foram liberados em 1990.
5. Não há assinaturas de Beria nos documentos de execução, como se o massacre tivesse sido realizado sem o consentimento das autoridades soviéticas.

Agora vamos ao método de como os poloneses foram mortos:
- Em família
- Fazendo fila
- Tiro na nuca

Todos métodos da SS. Não só isso, todas as fontes sobre Katyn vem de 4 relatórios: Amtliches material (alemão), Comissão Burdenko (soviética), relatórios de Gorbachev/Yeltsin e escavações recentes do governo polonês e ucraniano (financiado pela fundação "Memória às vítimas do comunismo"). Goebbels na época viu como uma grande oportunidade para espalhar propaganda anti-soviética, apoiado pelo governo polonês no exílio (que era cheio de colaboradores da invasão nazista e fascistas assumidos). Foi formada então a Amtliches material, para fazer relatório do que houve em Katyn, e mesmo com Goebbels dizendo para suprimirem a informação, a Amtliches material divulgou que em seu estudo, TODAS as balas eram de origem alemãs.

Já a comissão Burdenko percebeu que os alemães cortaram os bolsos, mas mesmo assim deixaram coisas para trás, inclusive cartas de 40 e 41. A autópsia realizada por médicos alemães que deram depoimento no julgamento de Nuremberg também revelou que os corpos que estavam enterrados, já estavam mortos há pelo menos 1 ano ou 1 ano e meio, o que acaba com toda a cronologia alemã de Vincenzo Mario Palmieri, um médico italiano e simpatizante e colaborador fascista, que fala que foram suprimidos do relatório por algum motivo, que a expectativa do tempo de morte estava errada, era entre 2-3 anos.

Quando Gorbachev levou os documentos para a Polônia, percebeu-se que os arquivos estavam classificados de forma errada, alguns documentos estavam classificados como super secretos mesmo sem a necessidade. Em 2009, Putin estava querendo vender gás para o governo polonês, então ele assume o massacre de Katyn (que ele sabe que é mentira). Logo, surgiu o depoimento de 3 membros da NKVD confirmando uma operação em Katyn, mas eles nunca sequer trabalharam na região.

Recentemente, houveram escavações em Volodymyr-Volynsky, financiadas pelo governo polonês e pela fundação "Memória às vítimas do comunismo", e o governo fez bastante propaganda sobre o caso, até que as pessoas começaram a perceber que o distintivo de policiais da escavação tinham o nome da região quando ela mudou de nome em outubro de 40, mas ele morreu em maio. Depois disso, misteriosamente as escavações foram canceladas. A identificação de corpos também mostrou que algumas pessoas ali deveriam estar há 1200 km, e o caso todo é no mínimo estranho.

Goebbels, em seu diário em 1943 (após estar claro que os soviéticos iriam varrer a Alemanha por toda a Europa até Berlim), disse:
>Infelizmente, teremos que renunciar a Katyn. Os bolchevistas, sem dúvida, logo vão "descobrir" que nós matamos 12 mil militares poloneses. Este episódio nos causará problemas no futuro. Os soviéticos farão o possível para descobrir quantas covas existirem para então nos culpar disso.

Também, o famoso investigador do caso Katyn, Viktor Ilyukhin, morreu de forma misteriosa. Ele havia descoberto que:
1. Culpar o NKVD era de interesse da Alemanha em 1943, quando os poloneses lutavam ativamente contra Hitler, e os alemães e queriam apresentar os bolcheviques como monstros;
2. A suposta autoria pelo NKVD estaria em contradição com a formação do Exército de Anders, por meio do qual prisioneiros de guerra poloneses na União Soviética foram agrupados militarmente e saíram da URSS pela fronteira com o Irã para juntar-se ao Exército Britânico para combater os nazistas na Itália;
3. Houve uma filmagem de oficiais poloneses pelos alemães no outono de 1941, após a ocupação da região de Smolensk pela Wehrmacht;
4. Os poloneses foram executados com armas alemãs;
5. Muitas das vítimas tinham as mãos amarradas com fio de papel, que não era produzido na URSS;
6. A autenticidade do documento assinado por Beria é questionável, pois testes efetuados a pedido do historiador C. Strygina, constatariam que as três primeiras páginas foram produzidas em uma máquina de escrever, e a última página, na qual existem apenas cinco linhas de texto, bem como a suposta assinatura L. Beria, foi produzida por outra máquina de escrever de 1969.

Fontes:
- [The "Official" version of Katyn massacre disproven?](https://msuweb.montclair.edu/~furrg/research/furr_katyn_preprint_0813.pdf)
- [Katyn: 49 signs of falsification of “Closed package no. 1”](https://mythcracker.wordpress.com/2010/07/14/katyn-49-signs-of-falsification-of-%E2%80%9Cclosed-package-no-1%E2%80%9D/)
- [The Katyn Massacre - by Prof. Ella Rule](http://stalinsociety.net/?p=103)
- [The truth about Katyn - report of Special Commission for Ascertaining and Investigating the Circumstances of the Shooting of Polish Officer Prisoners by the German-Fascist Invaders in the Katyn Forest](http://www.revolutionarydemocracy.org/archive/katyn.htm)
- [Evidence that the Katyn Massacre was committed by the Germans](https://espressostalinist.com/the-real-stalin-series/katyn/)
- [Falsification about Nazi aims, then and now](http://www.cpcml.ca/Tmlw2013/W43034.HTM)
- [Yeltsin’s regime falsified Katyn documents](https://www.youtube.com/watch?v=8TgqDVrkREw&app=desktop)
