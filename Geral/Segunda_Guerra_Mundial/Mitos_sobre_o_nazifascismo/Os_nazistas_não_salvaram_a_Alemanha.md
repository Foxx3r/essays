# Os nazistas não salvaram a Alemanha
Este é um dos favoritos dos apologistas fascistas em todos os lugares. No entanto, na realidade, a economia nazista foi um tremendo fracasso e levou a enormes reduções nos padrões de vida do povo alemão. De acordo com um [estudo](https://www.sciencedirect.com/science/article/pii/S1570677X02000023) publicado na revista Economics and Human Biology:
>Os resultados implicam que a Alemanha experimentou um aumento substancial nas taxas de mortalidade na maioria dos grupos etários em meados da década de 1930, mesmo em relação aos de 1932, o pior ano da Grande Depressão. Além disso, a altura das crianças - um indicador da qualidade da nutrição e saúde - em geral estagnou entre 1933 e 1938, mas aumentou significativamente durante a década de 1920. A perseguição, por si só, não explica um desenvolvimento tão adverso no bem-estar biológico; os segmentos não perseguidos da população alemã também foram afetados.

Esses problemas foram o resultado direto da política econômica nazista:
>A razão para este desenvolvimento adverso foi causado pelo fato de que os gastos militares aumentaram às custas de medidas de saúde pública. Além disso, as importações de alimentos foram reduzidas e os preços de muitos produtos agrícolas controlados. Há ampla evidência de que esse conjunto de políticas econômicas teve um efeito adverso sobre a saúde e o estado nutricional da população.

Fontes:
- [University of Barcelona | Against the Mainstream: Nazi Privatization in 1930s Germany](http://www.ub.edu/graap/nazi.pdf)
- [Jewish Telegraphic Agency | Goldmann Warns Against "Distortions" in Charges Against USSR](https://www.jta.org/1965/06/11/archive/goldmann-warns-against-distortions-in-jewish-charges-against-russia)
