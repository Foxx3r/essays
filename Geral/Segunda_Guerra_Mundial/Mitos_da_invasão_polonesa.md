# Mitos da invasão polonesa
Lev Sotskov desclassificou [documentos](https://www.irishtimes.com/news/russia-declassifies-secret-documents-on-nazi-soviet-pact-1.723486) soviéticos que mostram que Stalin tinha 1 milhão de soldados prontos para atacar Hitler pela Polônia, 2 semanas antes do começo da guerra, mas os britânicos e franceses não responderam. 1 semana depois, Stalin foi obrigado a assinar o pacto Molotov-Ribbentrop para ganhar tempo. O pacto Molotov-Ribbentrop não dizia nada sobre repartir a Polônia, os soviéticos apenas disseram "não passem dessa linha" e manteram certa influência na área, pois eles precisavam de um país nem que fosse fascista no meio, para que Hitler não chegasse às fronteiras soviéticas.

A França era dominada por comunistas até 1938 e ter um apoio da URSS acabaria com toda a Europa Ocidental, caindo nas mãos dos comunistas. Mas em 1937, a frente popular perdeu seu poder. Na época do Acordo de Munique, os comunistas já estavam fora do governo.

Em 17 de Setembro, foi concluído que o governo polonês havia deixado de existir. Molotov perguntou para o diplomata polonês nesse mesmo dia: "Onde está seu governo?" e ele disse: "Eu não sei onde está meu governo". A [Liga Das Nações](https://en.wikipedia.org/wiki/League_of_Nations) já não reconhecia a Polônia como um Estado. A Polônia Oriental naquele momento era um "país" sem governo. Stalin percebeu que então Hitler tinha como violar o pacto de forma legal de 2 formas, então ele ordena que as tropas anexassem a Polônia e organizassem um novo governo.

O supremo comandante polonês [Rydz-Smigly](https://pl.wikipedia.org/wiki/Edward_%C5%9Amig%C5%82y-Rydz) disse para as tropas polonesas não combaterem os soviéticos, e sim os nazistas. No mesmo dia, o presidente [Ignacy Moscicki](https://en.wikipedia.org/wiki/Ignacy_Mo%C5%9Bcicki) admitiu que a Polônia não tinha mais governo, na Romênia. E o governo romeno também disse o mesmo.

Como resultado, a Romênia e nem a França declararam guerra à URSS, já que eles tinham um pacto de aliança com a Polônia. E nem a Inglaterra exigiu a retirada das tropas, nem a Liga Das Nações declararam que a URSS invadiu um Estado Membro. E todos os países da época aceitaram a neutralidade soviética na guerra.

Fontes:
- [The USSR and Germany Never had a common plan to split Poland (by Finnish Bolshevik)](https://youtu.be/kp5CU3OB2UU)
- [Grover Furr - did the Soviet Union invade Poland in September 1939?](https://www.youtube.com/watch?v=n9z_PlnAf5s)
- [Did the Soviet Union invade Poland in September 1939?](https://msuweb.montclair.edu/~furrg/research/mlg09/did_ussr_invade_poland.html)
- [Another View of Stalin - “Stalin and the Anti-Fascist War - the German-Soviet Pact”](https://stalinsocietypk.files.wordpress.com/2013/05/another-view-of-stalin1.pdf)
