# Em defesa do SUS
O SUS (Sistema Único de Saúde) é sim um serviço gratuito, pois o imposto que você paga não é tido como uma contraprestação do serviço de saúde e a função do seu imposto não é arcar com um serviço do governo e sim se destinar aos cofres públicos. Ninguém, quando se dirige à um hospital público é questionado se pagou os impostos ou não.
>Gratuitos: é o negócio jurídico em que apenas uma parte aufere vantagem ou benefício. Nessa modalidade, outorga-se vantagem a uma das partes sem exigir contraprestação da outra, como exemplo a doação pura e o comodato.

O orçamento da União não é unicamente obtido por impostos, mas mesmo se fosse, em 2020 apenas 3,63% dele foi usado para a área da Saúde, ou seja, em teoria, 3,63% dos impostos pagos financiariam a saúde pública (O que dá em média 400 reais anualmente para alguém que ganhe mil reais por mês), mas isso é falso, pois como disse anteriormente, o orçamento obtido pela União não vem unicamente de impostos, mas sim recebimentos de indenizações/juros, venda de imóveis e títulos públicos, prestação de serviços etc.

Em média, é calculado que o brasileiro pague R$25,00 por mês para uso do SUS, que engloba a Fiocruz, Anvisa, INCA, ANS, Funasa e outros.

Supondo que você paga pelo SUS, se você pegar os planos de saúde mais baratos no mercado privado, eles saem 5 vezes mais caros que o que o SUS oferece para você e não oferecem, por exemplo, tratamentos mais caros ou transplantes, na qual o SUS realiza, gratuitamente 95% dos mesmos, tendo o maior sistema público de transplantes do mundo.
>Atualmente, 95% dos procedimentos são realizados pelo Sistema Único de Saúde (SUS), tornando o país referência mundial no campo dos transplantes e maior sistema público do mundo.
