# Bolsonaro e seu militarismo
A decisão do Bolsonaro de aumentar o investimento militar não é "exaltar armas". É só o melhor lugar para desvio de verba. Assim como nos EUA, investimento em defesa pode ir para literalmente qualquer coisa. Pessoalmente, acho o Chomsky extremamente ingênuo nas análises dele e de socialista americano eu prefiro mil vezes o Parenti. Mas o Chomsky fala muito claramente que o investimento em defesa dos EUA serve para se investir socialmente em inovações tecnológicas que depois irão ser apropriadas pela iniciativa privada dos EUA. Foi assim com a ARPANet, foi assim com tela de toque, foi assim com smartphones, foi assim com o ENIAC, foi assim com virtualmente todas as tecnologias que usamos hoje.

Como o Brasil é um país dependente (Ruy Mauro Marini ftw), é óbvio que esse dinheiro não vai ser usado de maneira tão... inovadora, por falta de uma palavra melhor. Vai ser só desvio de verba vulgar mesmo. Vai servir para construir casa para a familícia, para o cassinão da especulação financeira, para ampliar grilagem de latifúndio, para encher o bolso de pensionista militar.

E o pior é que a direita, com o fascínio ao militarismo, vai ver isso com bons olhos. Os EUA ao menos conseguiram arrumar uma desculpa, com o estado de guerra permanente que eles arrumaram e que se sentem justificados com o discurso de "os EUA têm muitos inimigos, então precisam continuar investindo em defesa".

Esse discurso é inclusive reproduzido por missionários religiosos de igrejas americanas aqui no Brasil. Eu já experimentei isso em primeira mão: discuti com um missionário mórmon sobre esse investimento em defesa ser algo completamente fraudulento, e ele repetiu a cartilha previsível de que os EUA têm muitos inimigos. Isso só demonstra como a religião é usada como ferramenta de propagação dos interesses do império.

E aí é óbvio que existe uma necessidade em uma educação emancipatória para reduzir a possibilidade de radicalização feita por esse tipo de entidade religiosa. O que, por sinal, é o que a China na verdade faz com os uigures (a história de campos de concentração é uma completa fabricação inventada por ONGs americanas e mídias ocidentais).

Educação essa que, obviamente, o Bolsonaro também está reduzindo o investimento. É incrível como coisas aparentemente tão desconexas se conectam em uma análise materialista da nossa atual totalidade histórica.

Não existe direita não-bolsonarista. Foi a direita “tradicional” e “civilizada” que em 2014 lançou suspeição sobre a reeleição da Dilma. Foram eles também que consideraram “uma escolha muito difícil” entre Haddad e Bolsonaro. A direita é a mãe do fascismo e do bolsonarismo.
