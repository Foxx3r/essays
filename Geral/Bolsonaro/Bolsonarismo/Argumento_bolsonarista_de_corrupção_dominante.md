# Argumento bolsonarista de corrupção dominante
Esse papo de corrupção ou traição dominante é parte da ideologia liberal, pois nega essencialmente a origem política e material das coisas. Ambos se tratam de um discurso moralista: abstratiza tudo ao redor do ser - negando até a alienação, relação de poder envolvida - e posiciona via achismo ou expectativa individual (que pode ser fruto de imposição ou não). Isso causa - e é a base principal para sustentar pobreza teórica - imobilismo e expiação de culpa na classe trabalhadora.

Na nossa perspectiva como classe, "corrompido" e "traído" são sentimentos e não fatos. Os fatos são contradição, alienação e erros de análise ou estratégia. Me dá um nó na garganta quando eu vejo comunista produzindo material carregando essas noções, com a justificativa de que "só assim o povo entende".

Não devo nem dizer que a origem disso é preconceito contra pobre, que sustenta também aquela crença de que "o povo é tolo e culpado pelo fascismo se desenvolvendo", não é? Só me passa uma coisa pela cabeça, o que devia ser óbvio: nosso dever é desmanchar essas coisas e não sustentar elas, seus patetas!
