# A favor da liberação das drogas
Os produtores e vendedores de drogas se beneficiam da proibição e fazem lobby para mantê-la, e mesmo que não fizessem, a estrutura do tráfico é feita ao redor do fato de que é proibido pelo Estado. Toda a infraestrutura usada para vender drogas hoje é estruturada como é porque é proibido. Se fosse liberado (como a maconha está sendo liberada pelo mundo), toda a cadeia de produção mudaria (como está mudando).

Vício é questão de saúde pública, não é questão criminal. Ao liberar as drogas, você abre espaço para desincentivar o uso de drogas (como Portugal fez com sucesso), obtendo o monopólio das drogas, evitando com que crianças sejam expostas a drogas, e podendo inclusive dar descontos caso a pessoa esteja fazendo terapia contra o uso de drogas.
