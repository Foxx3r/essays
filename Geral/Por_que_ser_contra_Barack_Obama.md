# Por que ser contra Barack Obama?
Depois de ganhar o Prêmio Nobel da Paz, ele lançou uma média de 30 mil bombas/ano durante sua presidência, principalmente em países muçulmanos, e deportou 2.5 milhões de imigrantes não-documentados. Somente em 2016, jogou 26.171 bombas no Oriente Médio e Norte da África, mais de 3.000 em relação ao ano anterior, levou o país com o maior IDH da África à um IDH de um país feudal, e reinstituiu a escravidão na Líbia. Os países bombardeados incluem Síria, Iraque, Iêmen, Afeganistão, Paquistão, Líbia e Somália.

Obama começou uma campanha de assassinatos por drones no Oriente Médio e na África, em que 90% dos mortos não eram os alvos pretendidos, mas sim civis inocentes, incluindo mulheres, crianças e cidadãos americanos. Os ataques com drones são usados pelos militares e pela CIA para caçar e matar pessoas que a administração Obama considerou - por meio de processos secretos, sem acusação ou julgamento - dignas de execução. Ele autorizou 10 vezes mais ataques de drones do que Bush.

![](images/the_chain_of_command.png)

Em 3 de outubro de 2015, Obama ordenou que um canhão da Força Aérea dos Estados Unidos AC-130U atacasse o Kunduz Trauma Center operado pelos Médicos sem Fronteiras, no norte do Afeganistão. O ataque matou 42 pessoas e feriu outras 30. O ataque aéreo constitui um crime de guerra (ataques a hospitais são considerados crimes de guerra), e é a primeira vez em que um ganhador do Prêmio Nobel da Paz (Obama) bombardeou e matou outro (Médicos sem Fronteiras). A CNN e o New York Times obscureceram deliberadamente a responsabilidade dos EUA pelo atentado, com a manchete: "Os EUA são culpados depois que a bomba atinge um hospital afegão".

Em 2010, o presidente Obama ordenou que a CIA assassinasse um cidadão americano no Iêmen, Anwar al-Awlaki, apesar de nunca ter sido acusado de nenhum crime, matando-o com um ataque de drones em setembro de 2011. Duas semanas depois, um ataque separado de drones da CIA no Iêmen matou seu filho americano de 16 anos, Abdulrahman, junto com o primo de 17 anos do menino e vários outros iemenitas inocentes.

Em uma demonstração previsível de bipartidarismo, em janeiro de 2017, Trump ordenou uma greve dos SEALs e relatórios do Iêmen rapidamente revelaram que 30 pessoas foram mortas, incluindo 10 mulheres e crianças. Entre os mortos: a filha de 8 anos de Anwar Awlaki, irmã do jovem de 16 anos morto por Obama.

Obama escalou a guerra no Afeganistão, destacando mais 30 mil soldados em 2009.

Obama admitiu apoiar e armar neo-nazistas e outros grupos ultranacionalistas como o batalhão Azov na Ucrânia para derrubar seu ex-governo pró-Rússia. A Ucrânia agora tem um governo pró-EUA que glorifica os nazistas com ultranacionalismo, anti-semitismo, pogroms neonazistas e ataques a grupos LGBT.

Obama e Hillary Clinton fraudaram as eleições de 2010 no Haiti, ameaçando o então presidente de remover a ajuda financeira dos EUA, a menos que seu candidato da OEA, Michel Martelly, ganhasse a eleição, depois de forçar outros candidatos populares a desistir. Esta é uma continuação da "Operação defender a democracia", na qual as tropas dos EUA invadiram o Haiti para derrubar o presidente democraticamente eleito Jean-Baptiste Aristide em 1991, que ainda está no exílio. Obama fez lobby para reduzir o salário mínimo do Haiti para $0,30/hora.

Obama continuou apoiando Hosni Mubarak, o presidente egípcio que ajudou os EUA a lutar contra Saddam, dando a ele $2 bilhões todo ano. Depois que Mubarak foi denunciado por corrupção na Primavera Árabe de 2009, os EUA entraram em negociações com Suleiman, um colaborador da CIA e vice-presidente chefe de inteligência de Mubarak conhecido por torturar pessoas em nome dos EUA. Os EUA então concederam a ele asilo e tratamento médico.

Obama continuou a apoiar Israel e seu genocídio aos palestinos, deixando um presente de despedida de sua presidência a Israel de $38 bilhões em ajuda militar nos próximos 10 anos. “O primeiro-ministro Netanyahu e eu estamos confiantes de que o novo MOU fará uma contribuição significativa para a segurança de Israel no que continua sendo uma vizinhança perigosa”, escreveu Obama.

Obama finalmente atingiu o objetivo dos EUA de derrubar Gaddafi e desestabilizar a Líbia. De 2011 em diante, começou a conduzir uma extensa campanha de bombardeio (> 110 mísseis de cruzeiro tomahawk) nas Guerras Civis da Líbia de 2011 e 2014. Isso inclui 7.700 ataques aéreos, resultando em 30.000 -100.000 mortes. Cidades legalistas foram bombardeadas até se transformarem em escombros e etnicamente limpas, e o país está um caos quando milícias islâmicas treinadas e armadas pelo Ocidente tomam território e instalações de petróleo e disputam o poder. A milícia Misrata, treinada e armada por forças especiais ocidentais, é uma das mais violentas e poderosas do mundo. O país desmoronou em um caos de crianças soldados, escravidão e violência entre grupos extremistas, uma repetição obsessiva do que o imperialismo dos EUA deixou seu rastro na Síria, Afeganistão e Iraque.

Em uma entrevista com o ex-porta-voz de Gaddafi sobre a derrubada da Líbia pelos EUA. Quando pressionado, Obama afirmou que "não administrar as consequências da derrubada de Gaddafi" foi "o maior erro de sua presidência".

Em 2009, Obama e Hillary Clinton ajudaram no golpe militar do líder de esquerda democraticamente eleito Zelaya de Honduras, que os EUA viram como uma ameaça aos interesses comerciais dos EUA. E-mails vazados descobriram que Hillary estava negociando um acordo com os líderes do golpe, para garantir que Zelaya seria impedido de retornar ao poder, e Obama bloqueou uma proposta de resolução que exigia o retorno de Zelaya. Depois do golpe, pelo menos 116 ativistas ambientais anti-EUA foram assassinados pelo regime pró-EUA. Os EUA agora enfrentam imigração maciça dos mesmos países que desestabilizaram, e então os deportam em um ciclo interminável de opressão. Obama disse que a deportação de imigrantes hondurenhos enviará uma mensagem de que "eles não serão bem-vindos a este país".

Obama concedeu imunidade aos torturadores da CIA de Bush Jr.

Obama expandiu as bases de drones na África Ocidental, incluindo no Níger.

Um e-mail vazado revelou que o banco americano Citigroup escolheu a dedo o gabinete de Obama, em 2008, no início da crise financeira. Mesmo antes de tomar posse, ele faz com que o Citibank examine seu gabinete em particular.

Obama recusou-se a processar qualquer banqueiro ou executivo responsável pela crise de 2008. Seu procurador-geral declarou: "Alguns bancos são grandes demais para serem processados".

Obama deu um resgate de $85 bilhões para os gigantes automotivos GM e Chrysler quando eles o solicitaram em 2009.

Em uma gala de elite para executivos ricos do petróleo, Obama disse a eles para "apenas dizer obrigado", por transformar os Estados Unidos no maior produtor de petróleo do mundo e torná-los todos mais ricos.

Obama aprovou uma expansão de 117 milhões de toneladas de produção de carvão, indignando os ativistas do clima.

Obama voltou atrás em sua promessa de campanha por um sistema de saúde de pagador único e removeu a opção pública de seu ACA (uma proposta em grande parte tirada de Mitt Romney e escrita por magnatas da indústria de saúde), que concedeu subsídios maciços para capacitar a indústria de seguro saúde parasitária (~1,2 trilhão de dólares), forçando aqueles que não têm condições de pagar pelo seguro uma taxa anual de $700 por não tê-lo (chamada de "provisão de responsabilidade compartilhada"). Seu ACA é essencialmente um enorme subsídio governamental diretamente nos bolsos das empresas privadas de saúde, juntamente com um mandato que forçou as pessoas a pagar essas empresas também. Pelo menos 1/4 dos trabalhadores americanos estão presos na economia de gig, e muitos mais estão empregados por menos do que o horário integral sem seguro.

Obama não fez nada a respeito do aumento da dívida de empréstimos estudantis (agora >$1T, um aumento de 86% durante sua gestão), recusando-se a instituir programas de perdão ou permitindo que os empréstimos estudantis fossem cancelados por meio de falência.

Obama federalizou o sistema em que o governo agora lucra imensamente com os juros dos empréstimos que faz diretamente aos estudantes e com a inadimplência. Terceirização da cobrança de dívidas federais de empréstimos estudantis a empresas privadas de empréstimos com um lucro enorme.

Obama presidiu a maior crise de execuções hipotecárias da história dos Estados Unidos, mas nada fez a respeito.

A era Obama foi uma das maiores quedas na classe trabalhadora e na riqueza dos negros na história: o valor da casa diminuiu em ~$17k entre 2007 e 2016. Suas políticas de habitação levaram milhões a perder suas casas. Enquanto os bancos de Wall street receberam $29 trilhões em resgates, $75 bilhões em alívio foram reservados para execuções hipotecárias e assistência hipotecária. Em vez de ser pago às famílias, era pago aos agentes hipotecários, e os serviços encontraram maneiras de embolsar o dinheiro e continuar as execuções: ao final do programa, menos de 20% dos fundos foram usados, e a maioria desistiu do o programa devido a execuções hipotecárias. O governo Obama se recusou a processar a fraude, ou qualquer um dos responsáveis pela crise financeira de 2008.

Obama visitou Flint Michigan durante sua crise de envenenamento por água com chumbo, para apoiar a não resposta do governador e fazer o controle de danos para o partido democrático, ultrajando os cidadãos. Obama recusou-se a dar ajuda federal para a cidade, ou chamar o estado de emergência. Em um gesto altamente ofensivo durante uma reunião na prefeitura, ele minimizou a situação fingindo beber a água da torneira envenenada. Eventualmente, autorizou a FEMA a dar $5 milhões à cidade, enquanto as estimativas sugeriam que pelo menos $100 milhões eram necessários. 100 mil cidadãos, incluindo 8 a 10 mil crianças, foram intoxicados por chumbo como resultado da negligência do estado.

Obama passa seu jet-ski pós-presidência com bilionários, ganhando ~400 mil para cada palestra em Wall Street, comprando uma enorme propriedade multimilionária e fazendo documentários Netflix anti-China. Seu patrimônio líquido aumentou 3.000% após a presidência.

Obama deportou mais de 3 milhões de imigrantes durante sua presidência. Deportou mais de 400 mil pessoas somente em 2012. Ajudou a aumentar o ICE e o sistema de detenção em massa de imigrantes dos EUA.
>Até agora, Trump deportou menos pessoas do que seu antecessor, o presidente Barack Obama. De acordo com as estatísticas do ICE, 256.085 pessoas foram deportadas no ano fiscal de 2018, contra 226.119 remoções no ano fiscal de 2017.
>
>Isso ainda é significativamente menor do que o número de pessoas deportadas durante o ano fiscal de 2012, quando o governo Obama deportou mais de 400.000 pessoas.

Atualmente, o ICE, a polícia encarregada de fiscalizar a imigração, opera mais de 500 campos de prisioneiros, mantendo mais de 34.000 indocumentados considerados "estrangeiros", 20.000 dos quais não têm condenações criminais, no sistema de detenção de imigração dos EUA. Os campos incluem trabalho forçado (muitas vezes com contratos de empresas privadas), más condições, falta de direitos (uma vez que os indocumentados não são considerados cidadãos) e deportações forçadas, muitas vezes dividindo famílias. Os detidos muitas vezes são detidos por um ano sem julgamento, com procedimentos judiciais antiquados adiando as datas dos tribunais por meses, encorajando muitos a aceitar a deportação imediata na esperança de poder retornar mais rápido do que o tribunal pode chegar a uma decisão, mas perdendo o status legal, em um sistema cruel de coerção. Após a criação do DHS e do ICE, o orçamento para fiscalização da imigração dobrou de $6,2 bilhões em 2002 para $12,5 bilhões em 2006 com Obama.

Obama ampliou a guerra contra as drogas, prendendo minorias em taxas muito maiores do que nunca, historicamente. Obama não deu apoio à questão das vidas dos negros, aos protestos de Ferguson ou aos protestos do duto de acesso à Dakota, permitindo que as autoridades locais reprimissem brutalmente esses movimentos. A Guerra às drogas, uma política de detenção e encarceramento visando minorias, iniciada por Nixon, criou ao longo dos anos um sistema monstruoso de encarceramento em massa, resultando na prisão de 1,5 milhão de pessoas a cada ano, com os Estados Unidos tendo o maior número de prisioneiros per capita de qualquer nação. Um em cada cinco negros americanos vai passar um tempo atrás das grades devido às leis sobre as drogas. A guerra criou uma subclasse permanente de pessoas empobrecidas que têm poucas oportunidades educacionais ou de trabalho por terem sido punidas por delitos de drogas, em um ciclo vicioso de opressão.

Obama autorizou o uso de equipamentos militares pela polícia.

O complexo industrial carcerário dos Estados Unidos cresceu maciçamente e a rápida expansão da população carcerária dos Estados Unidos à influência política de empresas privadas de prisões e empresas que fornecem bens e serviços para agências penitenciárias do governo. Esses grupos incluem corporações que contratam trabalho prisional, empresas de construção, fornecedores de tecnologia de vigilância, empresas que operam serviços de alimentação em prisões e instalações médicas, empresas privadas de liberdade condicional, advogados e grupos de lobby que os representam. Grupos ativistas como a Organização Nacional para a Reforma das Leis da Maconha (NORML) argumentaram que o complexo industrial carcerário está perpetuando uma crença errônea de que a prisão é uma solução eficaz para problemas sociais como falta de moradia, desemprego, dependência de drogas, doença mental, e analfabetismo.

Em 12 de abril de 2015, Freddie Carlos Gray Jr., um homem negro de 25 anos, foi preso pelo Departamento de Polícia de Baltimore por possuir o que a polícia alegou ser um canivete ilegal segundo a lei de Baltimore. Enquanto era transportado em uma van da polícia, vários policiais o seguraram, pressionando sua medula espinhal, após o que ele entrou em coma e morreu, gerando uma série de protestos em Baltimore; a tropa de choque respondeu com violência e convocou a guarda nacional para ajudar contra os "bandidos", como foram rotulados por Obama em uma entrevista coletiva. Depois que os protestos foram reprimidos, os policiais foram julgados separadamente e todos foram declarados inocentes.

Depois que o ATF foi pego em um escândalo de venda de armas para cartéis de drogas, Obama defendeu-o e se recusou a processá-lo.

Obama autorizou o crescimento massivo do sistema de vigilância dos EUA em todo o mundo.

Desde 2012, o exército dos EUA tem uma campanha estatal de astroturfing financiada para manipular a opinião pública online e espalhar propaganda pró-EUA por meio de sockpuppets financiados pelo governo, chamada Operação Earnest Voice.

O vazamento de telegramas diplomáticos dos EUA de 2010 por Chelsea Manning revelou uma política abrangente de usar embaixadores dos EUA como espiões, apoiando ditaduras, espionando a ONU, fortalecendo as empresas americanas no exterior e interrompendo as negociações de desarmamento nuclear. O escopo desses vazamentos atinge todos os países com os quais os EUA têm um relacionamento.

Obama perseguiu implacavelmente e prendeu denunciantes, acusando mais do que todos os presidentes anteriores juntos.

Em 2010, Chelsea Manning foi presa sob o ato de espionagem por uma série de vazamentos que envergonharam o governo dos EUA, incluindo o ataque aéreo de 12 de julho de 2007 em Bagdá, documentos da Guerra do Afeganistão, vazamento de documentos da Guerra do Iraque, vazamento de cabos diplomáticos dos EUA e arquivos da Baía de Guantánamo. O vazamento foi, nas palavras de Manning: "possivelmente um dos documentos mais significativos de nosso tempo, removendo a névoa da guerra e revelando a verdadeira natureza da guerra assimétrica do século 21".

Obama incentivou o FBI a usar informantes para se infiltrar nas comunidades muçulmanas dos EUA, bloqueou sua exposição ao expandir a definição de segredos de estado.

Fontes:
- [The Guardian | America dropped 26,171 bombs in 2016. What a bloody end to Obama's reign](https://www.theguardian.com/commentisfree/2017/jan/09/america-dropped-26171-bombs-2016-obama-legacy)
- [NBC news | U.S. Bombed Iraq, Syria, Pakistan, Afghanistan, Libya, Yemen, Somalia in 2016](https://www.nbcnews.com/news/world/u-s-bombed-iraq-syria-pakistan-afghanistan-libya-yemen-somalia-n704636)
- [Independent | Map shows where President Barack Obama dropped his 20,000 bombs](https://www.independent.co.uk/news/world/americas/us-president-barack-obama-bomb-map-drone-wars-strikes-20000-pakistan-middle-east-afghanistan-a7534851.html)
- [The New York Times | C.I.A. to Expand Use of Drones in Pakistan](https://www.nytimes.com/2009/12/04/world/asia/04drones.html?smid=tw-share)
- [The Washington Post | U.S. airstrike kills more than 150 at Somalia terrorist camp, military says](https://www.washingtonpost.com/news/checkpoint/wp/2016/03/07/u-s-drone-strike-kills-more-than-150-in-somalia/)
- [The Intercept | The Drone Papers](https://theintercept.com/drone-papers/)
- [The Intercept | Manhunting in the Hindu Kush](https://theintercept.com/drone-papers/manhunting-in-the-hindu-kush/)
- [Wikipedia | Civilian casualties from U.S. drone strikes](https://en.wikipedia.org/wiki/Civilian_casualties_from_U.S._drone_strikes)
- [The Bureau of Investigative Journalism | Obama’s covert drone war in numbers: ten times more strikes than Bush](http://web.archive.org/web/20170215211531/https://www.thebureauinvestigates.com/2017/01/17/obamas-covert-drone-war-numbers-ten-times-strikes-bush/)
- [Wikipedia | Kunduz hospital airstrike](https://en.wikipedia.org/wiki/Kunduz_hospital_airstrike)
- [The Intercept | CNN and the NYT Are Deliberately Obscuring Who Perpetrated the Afghan Hospital Attack](https://theintercept.com/2015/10/05/cnn-and-the-nyt-are-deliberately-obscuring-who-perpetrated-the-afghan-hospital-attack/)
- [Salon | Confirmed: Obama authorizes assassination of U.S. citizen](https://www.salon.com/2010/04/07/assassinations_2/)
- [The Intercept | Obama Killed a 16-Year-Old American in Yemen. Trump Just Killed His 8-Year-Old Sister.](https://theintercept.com/2017/01/30/obama-killed-a-16-year-old-american-in-yemen-trump-just-killed-his-8-year-old-sister/)
- [The Guardian | Anwar al-Awlaki killed in Yemen - as it happened](https://www.theguardian.com/world/blog/2011/sep/30/anwar-al-awlaki-yemen-live)
- [Salon | The killing of Awlaki's 16-year-old son](https://www.salon.com/2011/10/20/the_killing_of_awlakis_16_year_old_son/)
- [Reuters | Commando dies in U.S. raid in Yemen, first military op OK'd by Trump](https://www.reuters.com/article/uk-usa-yemen-qaeda-idUKKBN15D094?edition-redirect=uk)
- [NPR | Obama Gambles On Afghan Escalation](https://www.npr.org/templates/story/story.php?storyId=120991044)
- [Ron Paul Institute for Peace and Prosperity | Obama Admits US Role in Ukraine Overthrow](http://www.ronpaulinstitute.org/archives/peace-and-prosperity/2015/february/02/obama-admits-us-role-in-ukraine-overthrow/)
- [Global Research | Washington Was Behind Ukraine Coup: Obama admits that US “Brokered a Deal” in Support of “Regime Change”](https://www.globalresearch.ca/washington-was-behind-ukraine-coup-obama-admits-that-us-brokered-a-deal-in-support-of-regime-change/5429142)
- [Bellingcat | How to Mainstream Neo-Nazis: A Lesson from Ukraine’s New Government](https://www.bellingcat.com/news/uk-and-europe/2019/10/21/how-to-mainstream-neo-nazis-a-lesson-from-ukraines-new-government/)
- [The Nation | Neo-Nazis and the Far Right Are On the March in Ukraine](https://www.thenation.com/article/archive/neo-nazis-far-right-ukraine/)
- [Time | Another Presidential Ouster Brews at the Haiti Polls](http://content.time.com/time/world/article/0,8599,2046634,00.html)
- [Wikipedia | Operation Uphold Democracy](https://en.wikipedia.org/wiki/Operation_Uphold_Democracy)
- [The Nation | WikiLeaks Haiti: Let Them Live on $3 a Day](https://www.thenation.com/article/archive/wikileaks-haiti-let-them-live-3-day/)
- [The Intercept | After Feigning Love for Egyptian Democracy, U.S. Back To Openly Supporting Tyranny](https://theintercept.com/2014/10/02/feigned-american-support-egyptian-democracy-lasted-roughly-six-weeks/)
- [Wikipedia | Omar Suleiman (politician)](https://en.wikipedia.org/wiki/Omar_Suleiman_(politician))
- [Reuters | U.S., Israel sign $38 billion military aid package](https://www.reuters.com/article/us-usa-israel-statement-idUSKCN11K2CI)
- [Wikipedia | Second Libyan Civil War](https://en.wikipedia.org/wiki/Second_Libyan_Civil_War)
- [RT | Libya was destroyed due to Western leaders’ lust to continue exploiting Africa’s riches – Gaddafi spokesman](https://www.rt.com/news/470321-libya-gaddafi-spokesman-interview/)
- [The Guardian | Barack Obama says Libya was 'worst mistake' of his presidency](https://www.theguardian.com/us-news/2016/apr/12/barack-obama-says-libya-was-worst-mistake-of-his-presidency)
- [Common Dreams | The US Role in the Honduras Coup and Subsequent Violence](https://www.commondreams.org/views/2016/03/15/us-role-honduras-coup-and-subsequent-violence)
- [Document Cloud | Peanut Gallery](https://www.documentcloud.org/documents/2157083-peanut-gallery.html)
- [Reuters | Pressure mounts on Honduras to end coup crisis](https://www.reuters.com/article/us-honduras/pressure-mounts-on-honduras-to-end-coup-crisis-idUSN287369520090929)
- [Global Witness | How Many More?](https://www.globalwitness.org/en/campaigns/environmental-activists/how-many-more/)
- [The Conversation | How US policy in Honduras set the stage for today’s migration](https://theconversation.com/how-us-policy-in-honduras-set-the-stage-for-todays-migration-65935)
- [Reuters | U.S. says deportation of Honduran children a warning to illegal migrants](https://www.reuters.com/article/us-usa-immigration/u-s-says-deportation-of-honduran-children-a-warning-to-illegal-migrants-idUSKBN0FK29N20140715)
- [The Guardian | Obama's justice department grants final immunity to Bush's CIA torturers](https://www.theguardian.com/commentisfree/2012/aug/31/obama-justice-department-immunity-bush-cia-torturer)
- [Daily Mail Online | Obama sends 100 troops to build new drone base in West Africa to target Al Qaeda](https://www.dailymail.co.uk/news/article-2283346/Obama-builds-new-drone-base-West-Africa-combat-spread-Al-Qaeda.html)
- [Observer | Obama Makes First Appearance in Wikileaks, Receives Admin List From Big Banker](https://observer.com/2016/10/obama-makes-first-appearance-in-wikileaks-receives-admin-list-from-big-banker/)
- [WikiLeaks | Podesta Emails](https://wikileaks.org/podesta-emails/emailid/32870)
- [Huffpost | Eric Holder Admits Some Banks Are Just Too Big To Prosecute](https://www.huffpost.com/entry/eric-holder-banks-too-big_n_2821741)
- [Wikipedia | Effects of the 2008–2010 automotive industry crisis on the United States](https://en.wikipedia.org/wiki/Effects_of_the_2008–2010_automotive_industry_crisis_on_the_United_States)
- [The Real News Network | Obama Tells Wall Street To Thank Him For Making Them So Much Money](https://therealnews.com/obama-tells-wall-street-to-thank-him-for-making-so-much-money-at-elite-gala-with-top-bush-reagan-official)
- [YouTube | As Climate Warms, US Approves 117 Million Ton Coal Mine Expansion - The Ring Of Fire](https://www.youtube.com/watch?v=IlmT_UVxKNI)
- [Insurance Information Institute | Facts + Statistics: Industry overview](https://www.iii.org/fact-statistic/facts-statistics-industry-overview)
- [HealthInsurance.org | What is the individual mandate?](https://www.healthinsurance.org/glossary/individual-mandate/)
- [The Guardian | The gig is up: America’s booming economy is built on hollow promises](https://www.theguardian.com/commentisfree/2019/jun/02/gig-economy-us-trump-uber-california-robert-reich)
- [The Hill | President Obama's horrible, terrible legacy on student loans](https://thehill.com/blogs/congress-blog/education/279512-president-obamas-horrible-terrible-legacy-on-student-loans)
- [Mother Jones | The Incredible, Rage-Inducing Inside Story of America’s Student Debt Machine](https://www.motherjones.com/politics/2018/08/debt-student-loan-forgiveness-betsy-devos-education-department-fedloan/)
- [The New York Times | Debt Collectors Cashing In on Student Loans](https://www.nytimes.com/2012/09/09/business/once-a-student-now-dogged-by-collection-agencies.html)
- [Foreclosed | The Obama Presidency Was A Disaster For Middle-class Wealth In The United States](https://www.peoplespolicyproject.org/wp-content/uploads/2017/12/Foreclosed.pdf)
- [Jacobin | How Obama Destroyed Black Wealth](https://jacobinmag.com/2017/12/obama-foreclosure-crisis-wealth-inequality)
- [YouTube | Janitor Obama war on Flint Michigan](https://www.youtube.com/watch?app=desktop&v=GVv3n18_TGs)
- [The Street | Barack Obama Is Now Among 10 Highest-Paid Public Speakers](https://www.thestreet.com/investing/highest-paid-public-speakers-14315669)
- [Evening Standard | Barack and Michelle Obama net worth 2020: How much is the former US President worth along with his wife?](https://www.standard.co.uk/insider/celebrity/barack-and-michelle-obama-net-worth-2020-how-much-is-the-former-us-president-worth-along-with-his-wife-a4178561.html)
- [Axios | Trump isn't matching Obama deportation numbers](https://www.axios.com/immigration-ice-deportation-trump-obama-a72a0a44-540d-46bc-a671-cd65cf72f4b1.html)
- [Politico | Biden under fire for mass deportations under Obama](https://www.politico.com/story/2019/07/12/biden-immigration-2020-1411691)
- [CNN | 35 arrested in Trump-touted ICE operation that targeted 2,000](https://lite.cnn.com/en/article/h_1626e3440cea85fbf66e92338b1bf564)
- [Wikipedia | Immigration detention in the United States](https://en.wikipedia.org/wiki/Immigration_detention_in_the_United_States#Criticisms)
- [CNN | America's shameful 'prison camps'](https://edition.cnn.com/2015/07/23/opinions/reyes-immigration-detention/)
- [Wikipedia | U.S. Immigration and Customs Enforcement](https://en.wikipedia.org/wiki/U.S._Immigration_and_Customs_Enforcement#Corporate_contracts)
- [Migration Police Institute | Immigration Enforcement in the United States: The Rise of a Formidable Machinery](https://www.migrationpolicy.org/research/immigration-enforcement-united-states-rise-formidable-machinery)
- [Wikipedia | War on drugs](https://en.wikipedia.org/wiki/War_on_drugs)
- [Mint Press News | White House To Reauthorize Military Equipment For Police](https://www.mintpressnews.com/white-house-reauthorize-military-equipment-police/218714/)
- [Wikipedia | Prison–industrial complex](https://en.wikipedia.org/wiki/Prison–industrial_complex)
- [Wikipedia | United States incarceration rate](https://en.wikipedia.org/wiki/United_States_incarceration_rate#Growth)
- [Wikipedia | Private prison](https://en.wikipedia.org/wiki/Private_prison)
- [Wikipedia | Private probation](https://en.wikipedia.org/wiki/Private_probation)
- [Wikipedia | National Organization for the Reform of Marijuana Laws](https://en.wikipedia.org/wiki/National_Organization_for_the_Reform_of_Marijuana_Laws)
- [Wikipedia | Death of Freddie Gray](https://en.wikipedia.org/wiki/Death_of_Freddie_Gray)
- [Wikipedia | 2015 Baltimore protests](https://en.wikipedia.org/wiki/2015_Baltimore_protests)
- [Los Angeles Times | Full Coverage: ATF’s Fast and Furious scandal](https://www.latimes.com/nation/atf-fast-furious-sg-storygallery.html?page=2)
- [Wikipedia | Global surveillance disclosures (2013–present)](https://en.wikipedia.org/wiki/Global_surveillance_disclosures_(2013–present))
- [The Guardian | Revealed: US spy operation that manipulates social media](https://www.theguardian.com/technology/2011/mar/17/us-spy-operation-social-networks)
- [Wikipedia | Operation Earnest Voice](https://en.wikipedia.org/wiki/Operation_Earnest_Voice)
- [Wikipedia | United States diplomatic cables leak](https://en.wikipedia.org/wiki/United_States_diplomatic_cables_leak)
- [Wikipedia | Contents of the United States diplomatic cables leak](https://en.wikipedia.org/wiki/Contents_of_the_United_States_diplomatic_cables_leak)
- [The San Diego Union-Tribune | Before Trump’s crackdown on leaks, Obama went after 10 leakers, journalists](https://www.sandiegouniontribune.com/opinion/the-conversation/sd-before-trump-obama-prosecuted-leaks-20170804-htmlstory.html)
- [The Guardian | Obama's war on whistleblowers leaves administration insiders unscathed](https://www.theguardian.com/us-news/2015/mar/16/whistleblowers-double-standard-obama-david-petraeus-chelsea-manning)
- [YouTube | Ellsberg: Obama's War on Whistleblowers](https://www.youtube.com/watch?v=x-iT51XSTgE)
- [YouTube | Obama: Snowden was no patriot.](https://www.youtube.com/watch?v=wS9TXJqxkSQ)
- [YouTube | Alex Gibney on Obama admin. and whistleblowers](https://www.youtube.com/watch?v=YIWjon88lWc)
- [Wikipedia | Chelsea Manning](https://en.wikipedia.org/wiki/Chelsea_Manning)
- [Wikipedia | July 12, 2007, Baghdad airstrike](https://en.wikipedia.org/wiki/July_12,_2007,_Baghdad_airstrike)
- [Wikipedia | Afghan War documents leak](https://en.wikipedia.org/wiki/Afghan_War_documents_leak)
- [Wikipedia | Iraq War documents leak](https://en.wikipedia.org/wiki/Iraq_War_documents_leak)
- [Wikipedia | Guantanamo Bay files leak](https://en.wikipedia.org/wiki/Guantanamo_Bay_files_leak)
- [Color Lines | Obama's Unprecedented Use of State Secrets to Defend Religious Profiling](https://www.colorlines.com/articles/obamas-unprecedented-use-state-secrets-defend-religious-profiling)
