# A teoria generativa-transformacional
Chomsky é um cabeça dura. Daniel Everett desprovou as teorias dele de forma convincente fazendo estudos com os índios brasileiros, e ele foi soberbo, não reconheceu que os estudos dele foram superados. Daniel Everett demonstrou que as primeiras línguas não necessariamente eram recursivas, pois os índios isolados que ele estudou não tinham recursão no idioma deles.

Porém o idioma deles é completamente funcional, para a realidade deles, esses índios são bastante inteligentes e bem sintonizados à realidade. Eles não tem crença em divindades, aliás.

Mas o que é recursividade na linguística? Vou mostrar um exemplo: "O fato de que Joãozinho pular a cerca tenha irritado o cachorro do vizinho, causou grande estresse à sua mãe". Essa frase é recursiva: ela tem uma frase dentro da outra. A frase que está dentro é: "Joãozinho pular a cerca tenha irritado o cachorro".

A teoria gerativa-transformacional do Chomsky implica que o ser humano só pode se tornar consciente da linguagem no momento em que ele aprende pensamentos recursivos, mas as evidências apontam que o ser humano pode desenvolver a linguagem na ausência de pensamentos recursivos ou auto-referentes, e ser induzido à aprender isso depois, através da observação da natureza

Para você conseguir entender essa frase, você precisa ser capaz de formar pensamento recursivo. Se você não tivesse aprendido isso durante seu desenvolvimento intelectual, um mundo de ideias seriam impossíveis para você. O problema é: é necessário ter pensamento recursivo para desenvolver a linguagem? Ou você consegue se tornar falante e adquirir um alto grau de consciência própria, ambiental e social, sem os pensamentos recursivos?

As evidências sugerem que sim, você pode. Isso implica que no passado, nós imaginamos errado a linha do tempo da evolução da linguagem humana.
