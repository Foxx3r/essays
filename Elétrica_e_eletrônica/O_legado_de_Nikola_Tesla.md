# O legado de Nikola Tesla
Já pararam pra pensar no quanto Tesla é um dos maiores gênios da humanidade e tem seu nome manchado por Elon Musk?

A forma mais eficiente de transportar energia elétrica é pelo sistema trifásico de corrente alternada, Tesla provou isso matematicamente para nós e criou toda a base de cálculo e teórica desse sistema.

Tesla também inventou o motor de indução, que é o modelo de motor de corrente alternada mais eficiente e o mais usado no mundo, se nós temos ventiladores, motobombas, aspiradores de pó, liquidificadores, serras elétricas, lixadeiras, exaustores, etc hoje, é graças ao Tesla. Tesla criou o mundo da elétrica como conhecemos hoje, devemos muito à ele.

Tesla queria que a energia fosse transmitida pelo ambiente. Hoje vemos a mesma tecnologia ser usada em dispositivos de carregamentos dos celulares mais modernos. O sonho dele era inviável com a tecnologia da época, mesmo hoje seria muito caro, para a ideia de Tesla funcionar, seria preciso fazer a Terra de catodo e usar o efeito de capacitância do ar, mas isso causa uma ionização muito grande no solo onde o catodo é instalado. As distribuidoras hoje têm grandes problemas com ionização de aterramento, justamente pelo movimento de cargas na terra. Elas tratam o solo com um gel especial e perfuram o solo bem fundo com barras de cobre galvanizado, mas sempre precisam substituir, porque a enorme energia que passa por essas barras destrói o metal. É mais prático, seguro e econômico passar a energia por cabos metálicos do que enviar pelo ar.

Ele conseguiu fazer umas conclusões acerca das teorias matemáticas e geometrias das formas e energias. Alguns campos nunca sequer foram explorados, mesma teoria das montadoras de carros que não queriam o carro movido a água. Algumas empresas não queriam algumas tecnologias dele porque não teriam lucro algum.

A água é mais barata que a gasolina, mas para ela servir como combustível precisa de um catalizante de eletrólise e de uma bateria elétrica que vai precisar de recarga externa de vez em quando. O motor movido à água é mais um motor elétrico com passos extras, do que movido à água de fato, para usar a água precisa fazer eletrólise. Dá para modificar sua moto para aceitar água como combustível, é só adaptar o motor para funcionar com gás, então você adiciona a caixa de eletrólise e pronto, o hidrogênio aciona o motor e o escapamento solta vapor de água. Mas tem que recarregar a bateria de vez em quando, pois ela mais perde que ganha, devido à eletrólise. Quanto ao risco, ele é bem pequeno, não dá para explodir, a caixa de eletrólise só faz o suficiente para preencher a câmara do pistão.

E não se confunda, Nikola Tesla não era croata. A ideia de igualar etnia com nacionalidade é muito ocidental, dos estados modernos coloniais (que não são estados-nações). Para a Europa, Ásia e para boa parte da África, não se confundem. Uma coisa é o seu povo; outra é o estado soberano que emite o seu passaporte.

Um exemplo que eu gosto de dar é o da Madre Teresa de Calcutá:
- Nasceu em território da atual Macedônia do Norte
- Em uma comunidade albanesa-kosovar
- Em família de etnia aromena (não-romena: aromena)
- Radicou-se na Índia
