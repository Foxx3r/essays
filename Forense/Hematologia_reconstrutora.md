# Hematologia reconstrutora
Com a hematologia forense, podemos descobrir o tipo de arma do crime, número de golpes, origem do sangue, velocidade, ângulo, posição e movimentos da vítima e agressor, ordem cronológica dos ferimentos, horário do crime e se a morte foi imediata ou não.

A hematologia forense analítica se preocupa em identificar o sangue, enquanto a hematologia forense reconstrutora se preocupa em reconstruir a cena do crime.

Devido à degradação das hemácias e oxidação da hemoglobina, o sangue se encontra em estado ferroso. Analisando o plasma, pode-se identificar se o indivíduo estava sob efeitos de drogas, envenenado, etc, e os testes imunológicos revelam se o indivíduo tem alguma doença, e se possível, identificar o indivíduo geneticamente usando a amostra sanguínea, dado que os glóbulos brancos são células nucleadas, portanto, fonte de DNA.

A seguir, temos testes de orientação. O teste de Kastle-Mayer é um teste presuntivo (ou seja, trata-se da identificação da presença de sangue) baseado na oxidação da fenolftalína, onde uma pequena amostra questionada é coletada em um swab umedecido sobre o qual se adiciona uma gota do Reativo de Kastle-Meyer (uma solução alcalina e incolor de fenolftalína – nome dado à fenolfitaleína em sua forma reduzida). Uma gota de água oxigenada é adicionada em sequência e a coloração rósea aponta resultado positivo. Ao aplicar o teste, o sangue fica rosado na hora (se não for imediato, não é sangue), no entanto, depende de vários fatores-ambiente, portanto, não é muito confiável, mas ele é um teste rápido.

O luminol pode identificar manchas de sangue mesmo após anos da ocorrência do crime; ele é um reagente luminescente.

Há outros métodos, como benzidina (conhecido por Adler-Ascarelli) e do verde malaquita. No entanto, esses dois são pouco utilizados por possuírem elevada toxicidade.

Os testes de certeza confirmam se a substância é mesmo sangue, e se o sangue é de origem humana. Existem 3 tipos de testes de certeza:

Cristais de Teichman: são formados quando uma solução de brometo de potássio, iodeto de potássio e cloreto de potássio dissolvidos em ácido acético glacial reage com a hemoglobina mediante aquecimento formando os cristais.

Cristais de Takayama: o grupo prostético da hemoglobina tem a propriedade de se ligar a moléculas nitrogenadas, como amônia, nicotina e piridina, formando complexos chamados de hemocromogênio cuja estrutura é característica em coloração e forma de cristal. A reação consiste em uma hidrólise alcalina, separando o grupo prostético da globina.

Mecanismos imunológicos: são reações envolvendo anticorpos contra componentes moleculares do sangue humano. Neste caso, o resultado positivo não apenas confirma a presença de sangue, como atesta que o sangue é de origem humana. Por este motivo, são por vezes chamados de teste de origem humana.

As manchas recentes são vermelhas e úmidas, passando de vermelho-castanho a castanho-escuro; manchas bem secas apresentam aspecto fendilhado, com escamas brilhantes e aspecto esverdeado.

A idade da mancha depende de dois fatores: cor e o grau de solubilidade. A cor determina a idade da mancha, mas deve-se levar em conta elementos como a umidade, temperatura, putrefação, intensidade de luz e agentes químicos que influenciam.

A forma da mancha pode determinar a altura, ângulo e força do ataque. Se a mancha é um escorrimento, o sangue se apresenta sob a forma de poças, harco ou filetes decorrentes de grande perda de sangue.

A questão da quantidade de manchas é colocada quando se alega como causa da morte hemorragia externa, ou também para se ter uma ideia da debilitação do ferido, pois pela contagem das gotas encontradas, avaliação do tamanho, bem como pelo cálculo do volume das poças e coágulos, é possível se chegar à quantidade de sangue perdido. Salpicos indicam que uma segunda força atuou contra o derramamento do sangue, além da gravidade.

Através da distribuição topográfica, é possível dizer se houve deslocamento do cadáver, se a vítima caminhou após o ferimento, por onde andou, que posição estava, se praticou certos atos antes ou após o ferimento. Pode determinar, também, os atos do criminoso, bem como se estava ferido.

A seguir, a projeção de gotas:<br/>
Forma circular = baixa altura<br/>
Forma estrelada, com bordos irregulares = altura aproximada de 40cm<br/>
Forma estrelada com bordos denteados, gotas satélites = altura superior a 125 cm<br/>
Gotículas = altura superior a 2m

Para esconder manchas de sangue, deve-se usar alvejante; se a sala for feita de aço inoxidável, deve-se usar limpador a vapor; limpar a área com raiz-forte ou matéria fecal animal pode também atrapalhar o processo de luminescência; se o material no qual o sangue penetrou for madeira, não há jeito de remover, tendo em vista que o sangue penetra na madeira.
