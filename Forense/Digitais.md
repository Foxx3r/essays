# Digitais
Datiloscopia é um ramo da papiloscopia (e portanto da dermatoglifia) que estuda a identificação humana através das digitais (principalmente a terceira falange, ou falange digital).

Mas existem outros ramos, como quiroscopia (identificação pelas palmas das mãos), podoscopia (identificação pela sola dos pés), poroscopia (identificação pelos poros digitais) e cristascopia (identificação pelas cristas papilares), sendo realizado em agentes vivos e agentes cadavéricos.

A datiloscopia civil identifica pessoas para fins civis, como expedição de documentos. A datiloscopia criminal identifica pessoas para fins de investigação criminal.

A albodatiloscopia é uma área científica que estuda as linhas brancas (falhas) da impressão digital. Embora haja controvérsias sobre o que causa isso, alguns dizem que é devido a velhice, outros devido a temperatura úmida ou manuseio de produtos químicos. Essas linhas são sempre mutáveis, elas crescem, desaparecem e mudam de forma, mas podendo assumir algumas características imutáveis ao longo do tempo. A albodatiloscopia também estuda a forma geométrica dessas linhas e suas classificações.

O que temos certeza é que não é causado por cicatrizes, pois estas linhas brancas estão em uma camada diferente da pele. Também verificou-se que as linhas brancas aparecem em crianças, jovens, adultos e velhos e é comum nos portadores de doenças alérgicas, radiodermite, hanseníase bem como nas mãos de lavadeiras, professoras, feirantes de legumes, pedreiro, copeiro ou em conseqüência das doenças renais. Porém, essas linhas não coçam, não infeccionam, não doem e aparecem como um sulco largo e isolado, formam imagens quadriculadas, cruzadas ou reticuladas combinadas entre si, constituindo uma figura estranha e superposta ao desenho existente. As linhas brancas não se originam da solução de continuidade das saliências papilares e algumas desaparecem com os anos, sem deixar nenhum vestígio, e em outras a persistência é duradoura.

A impressão digital é inversa ao desenho digital (desenho de fato das digitais na derme).

As digitais são imutáveis.

As digitais se formam no 6° mês de gestação, por causa do líquido amniótico, pressão intra-uterina e a compressão dos vasos sanguíneos na região dos dedos.

Impressões digitais que não têm delta (triângulo) são chamadas de arco. Impressões digitais com delta na direita são chamadas de presilha interna. Se estiver à esquerda, é uma presilha externa. Se tiver mais de um delta, é um verticilo (e para determinar a direção, deve-se saber onde está o núcleo). Esse ê o chamado sistema de Vucetich.

Quando não é suficiente comparar digitais, conta-se as linhas a partir da presilha externa ou do verticilo, uma linha imaginária chamada linha de Galton.

A seguir, tipos de impressões digitais encontradas nas cenas de crime.

Moldadas: quando as impressões são encontradas em materiais que permitem a modelagem em baixo relevo (massa de fixar vidro por exemplo), tendo o perito que fotografá-la aplicando à mesma uma luz oblíqua para produzir sombra nos sulcos do molde, revelando assim o desenho formado pelas cristas papilares e ou poderá moldar tal impressão com material apropriado como silicone ou gesso, por exemplo.

Visíveis ou entintadas: quando as impressões são deixadas visíveis no local por ter o agente manuseado substâncias como tinta, sangue, graxa, sujeira etc. sendo fácil sua localização tendo no entanto o perito que fotografá-las para o estudo que se fizer necessário.

Latentes: São as impressões mais comuns por serem produzidas por substâncias segregadas pelo próprio corpo do agente como suor e gorduras.

Para revelar impressões latentes, é necessário o emprego de agentes químicos e técnicas especiais para manuseá-los. Para identificar as digitais, deve-se aplicar pó na camada, que adere à água (99% da composição do suor dos sulcos intrapapilares), ou usar vapor de iodo, que revela a digital como uma substância marrom, além do método da ninidrina, que demora pelo menos 10 dias para revelar. Existem outros diversos métodos (nitrato de prata, diazafluorenona, etc).

Negro de Fumo: é um termo genérico usado para identificar uma ampla variedade de materiais carbonáceos finamente divididos. É um dos materiais mais utilizados na revelação de impressões latentes encontradas em suportes de fundo claro, sendo aplicado por pulverização ou por meio de um pincel. Suas micro partículas aderem na mancha deixada pelas cristas papilares banhadas de suor e ou óleo revelando o desenho das papilas dérmicas.

Carbonato de chumbo: é um pó branco extremamente fino. É um dos materiais mais utilizados na revelação de impressões latentes encontradas em suportes de fundo escuro e transparentes, sendo aplicado por pulverização ou por meio de um pincel. Suas micro partículas aderem na mancha deixada pelas cristas papilares banhadas de suor e ou óleo revelando o desenho das papilas dérmicas.

Nitrato de prata: material altamente corrosivo. Causa queimaduras a qualquer área de contato. Pode ser fatal se engolido. Danoso se for inalado. É utilizado na revelação de impressões latentes deixadas em papel. Em contato com o cloreto de sódio (suor), produz cloreto de prata e nitrato de sódio, sendo que o cloreto de prata é indissolúvel permitindo assim que após a lavagem do papel, todo o material seja removido, exceto o desenho deixado pela mancha de suor, sendo este revelado com a exposição à luz, revelando assim o desenho deixado pelas cristas papilares.

Vapor de iodo: material altamente corrosivo. Causa queimaduras a qualquer área de contato. Pode ser fatal se engolido. Danoso se for inalado. É utilizado na revelação de impressões latentes deixadas em papel. O vapor de iodo reage com os ácidos graxos deixados no contato das cristas papilares com o papel revelando assim o desenho papilar.

Vapor de super-cola: um dos mais recentes métodos de detecção de impressões digitais é o vapor de cola (ou vapor de cianocrilato – Super Bonder). O material é exposta ao vapor de cianocrilato por alguns minutos. A digital aparece em leves contornos brancos visíveis a olho nu.

O grafite, o talco, a poeira e outros tantos pós extremamente finos, mais uma série de substâncias, podem ser utilizados para revelar impressões latentes.

Se você encontrou mais de 12 pontos característicos em uma impressão, você já pode saber quem é a pessoa.

Se encontrarem minhas digitais em uma cena de crime, posso ser processado? Sendo direto, não. O trabalho do datiloscopista é apenas isolar e identificar as digitais. O trabalho do perito criminal é analisar a materialidade das provas, se elas são relevantes, quem pode ser o suspeito e a classificação das provas. Só há risco de você ser processado caso você atrapalhe a coleta de evidências.

A datiloscopia é uma área muito complexa e que envolve vários processos químicos, além de ser uma área complexa que envolve inúmeras características que um perito deve ou não decorar – geralmente peritos decoram os padrões mais comuns, e caso encontre algum do qual não se lembra, olha no livro e classifica.
