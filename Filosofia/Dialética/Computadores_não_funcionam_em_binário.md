# Computadores não funcionam em binário
Se eu tentasse fazer um análogo computacional desta questão, seria o seguinte: em vez de relacionar 0 e 1 com 'falso' e 'verdadeiro', suponha que os chamássemos de 'divergir' e 'parar'. Agora, codificando todos os dados como bits, temos um sistema computacional em que cada bloco de construção (dados) foi associado a uma parada definitiva ou não, e podemos apenas testar o bit para decidir qual é. Mas, o problema de parada para máquinas de Turing com este esquema de nomenclatura permanece indecidível. Por quê?

As respostas são semelhantes. Mesmo que você possa descrever uma máquina de Turing como um monte de bits que você chamou de "parar" e "divergir", o problema da parada é sobre o que a máquina descrita faz, não uma simples agregação dos bits em sua descrição. A relação da lógica clássica com os bits tem a ver com a primeira ter (no caso mais simples) dois "valores de verdade". Assim, você pode descrever esses dois valores com estados de um bit, e dada uma fórmula de cálculo proposicional finita e bits para os valores-verdade das variáveis, você pode calcular um bit para o valor de verdade de toda a proposição. Também acontece que o bit calculado para P ∨ ¬P desse modo é independentemente do bit para P.

No entanto, isso não é o que é feito quando damos significados computacionais para fórmulas lógicas intuicionistas. As fórmulas não são interpretadas como bits, mas como especificações para máquinas ou programas. E quais programas atendem à especificação não é determinado por quais bits eles são construídos, mas pelo que eles fazem. Em alguns casos, isso pode ser muito simples, como:
- Todo programa atende à especificação de verdade, ou ⊤.
- Nenhum programa atende à especificação para falso, ou ⊥.

Mas há exemplos complexos que nos remete diretamente para a interpretação Brouwer-Heyting-Kolmogorov:
- Um programa para A ∨ B deve produzir 0 junto com um programa para A, ou 1 junto com um programa para B.
- Um programa para A → B deve aceitar um programa para A e produzir um programa para B.
- Um programa para ∀n:N.P(n) deve aceitar um número natural k e produzir uma máquina para P(k).

É importante ressaltar que isso significa que uma máquina que atende à especificação para P ∨ ¬P, deve decidir P, onde P não é apenas um bit, mas uma especificação para máquinas. E a lógica é rica o suficiente para codificar programas computacionais indecidíveis como especificações de máquina. Assim, como existem problemas computacionais indecidíveis, não há esperança de que o princípio P ∨ ¬P se mantenha em geral. Por exemplo, ∀n.P(n) ∨ ¬P(n) poderia ser uma máquina que decide o problema da parada, porque P(n) é uma codificação de "a enésima máquina (sob alguma numeração) para".

A versão bit das coisas pode ser incorporada à estrutura de especificação. Para proposições que correspondem a problemas decidíveis, a especificação corresponde a alguma outra expressão envolvendo bits, e as máquinas que decidem o problema responderão de acordo com o valor de verdade clássico representado pelo bit. Mas, claro, isso só funciona para problemas/proposições que são decidíveis.
