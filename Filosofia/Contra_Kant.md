# Contra Kant
Rand chamava Kant de "o homem mais malvado do mundo", pois ele queria universalizar a moral.

Para a ética de Kant, qualquer princípio de ação deve poder ser universalizado antes que se possa considerá-lo um princípio de moralidade. O objetivo de Kant é que qualquer princípio moral verdadeiro se aplica a todos, tanto quanto teoremas da matemática, porque a razão é assim.

Mas também não é o maior erro de Kant. Os iluministas achavam que tinham que substituir toda a filosofia por uma nova (vide Hume e o ceticismo radical de filósofos da época), incluindo Kant, que ao invés de retornar aos princípios antigos mais sólidos, fez essa mesma coisa. Ele também achava que a mente humana constituía a realidade como conhecemos.

Kant acreditava que poderia subjetivizar todo o conhecimento e experiência enquanto ainda mantinha a objetividade e a universalidade, postulando que toda subjetividade é a mesma, nós "criamos" o mundo que vivenciamos, mas fazemos isso de maneira universal e objetiva porque nossa razão é a mesma em todos os lugares, então, sempre "criamos" o mundo de acordo com os mesmos princípios da razão.

Mas para sabermos realmente se isso é verdade, precisaríamos saber se a razão realmente constitui o mundo, mas isso seria impossível, pois como isso seria anterior ao mundo constituído, não poderíamos saber, estaria além das capacidades da Razão humana (tipo o Big Bang), então ninguém pode dizer se é verdade, nem mesmo Kant, e Kant diz simplesmente para não pensarmos nisso...

Ou seja, o pensamento de Kant parece ser epistemologicamente autodestrutivo, embora não ontologicamente.

A teoria do conhecimento de Kant afirma saber coisas que sua própria teoria diz serem impossíveis, que sua própria teoria do conhecimento está correta, uma vez que sua teoria do conhecimento descreve os estados epistêmicos das coisas do mundo numenal, não apenas do mundo fenomenal.
>Para traçar um limite para o pensamento, devemos ter que pensar os dois lados desse limite (devemos, portanto, ser capazes de pensar o que não pode ser pensado).<br/>
>– <cite>Wittgenstein</cite>

Essa quote acima é uma versão do "O pior argumento do mundo" de Stove.

Kant diz que Vontade é Razão, mas Schopenhauer e Nietzsche corrigem isso. Na verdade, nem mesmo Descartes - pai do racionalismo - sequer argumentou que vontade=razão. Kant foi o primeiro iluminista a fazê-lo.

Visto que, por um lado, "bom" tem um significado substantivo e, por outro lado, tem o sentido iluminista de "questões de autonomia", pode haver um "tipo de" bem que não é bom. Ele é bom porque é autônomo, mas não é bom porque não é bom: o mal caótico. Kant cometeu um erro fatal em relação à autonomia.

Ele estava certo ao dizer que um agente totalmente bom se vincula livremente à lei e pela lei. Ele errou ao dizer que o agente totalmente bom legisla a lei para si mesmo. O erro de Kant é "para ser autônomo, é preciso ser legislador". Isso é um erro, pois a pessoa é totalmente autodeterminada quando adere à lei moral. A liberdade de uma pessoa não é comprometida por aderir livremente à lei natural, não mais do que por acreditar na verdade. O bom e o verdadeiro são aspectos externos que podem e devem ser internalizados.

Teorias anteriores que se diziam sintéticas a priori mais tarde passaram a ser consideradas analíticas. Kant pensava que a geometria euclidiana era sintética, mas então a descoberta da geometria não-euclidiana e o advento de formalizações rigorosas dos axiomas de Euclides por Tarski e Hilbert levaram a maioria dos filósofos a concluir que era analítica. Da mesma forma, Kant apelidou a aritmética de sintética, mas então Frege fez um argumento convincente de que ela era analítica, fornecendo fundamentos lógicos para o assunto (com a ajuda da axiomatização da aritmética de Peano).
