# Conhecimento
Conhecimentos podem ser destruídos, portanto são mutáveis. A imaterialidade implica em imutabilidade, se é mutável não é imaterial e por sua vez é material. Todavia, conhecimentos são materiais. Por ser material, não pode ser transcendente, portanto não pode sair do campo material. Pelo material só poder ser por meio material (sentidos internos + externos), não existe conhecimento sem estar mesclado com alguma experiência. Não tem como uma pessoa em estado vegetativo conhecer a verdade de uma proposição por exemplo.

O que é eterno é o objeto do conhecimento, por exemplo, 8+8 vai ser 16 em qualquer lugar do mundo (usando sistema decimal, rs, para não vir nenhum ANCAP dizendo que 8+8 em base-12 é 14), isso é inegável, mas isso é uma coisa, o conhecimento é outro. O conhecimento é um dado da realidade e nesse dado você tem o conteúdo - ele é eterno - mas não é isso que é o conhecimento, conhecimento é algo que você tem no seu cérebro - suas memórias - e você pode perder isso. Mesmo que você pegue o silogismo de todo A é A, todo A é B, relações proposicionais, etc. você sempre estará pegando dados de experiências que você teve.

Os ANCAPs dizem que existe um conhecimento que transcende a matéria justamente porque eles confundem o conhecimento com o objeto do conhecimento. Então eles caem no mesmo problema da física moderna: criar modelos abstratos na mente para depois partir para a realidade, e como eles nunca viram nada parecido na realidade, cometem enormes erros fulcrais.

Todo conhecimento está mesclado com uma forma de experiência, o que muda é se ele é pela razão superior (dedução) ou inferior (indução). Muitos isolam a experiência apenas aos sentidos externos (olfato, paladar, visão, audição e tato), mas também existem os internos (imaginação, cognição, etc).

Se existe um "Direito Natural" (jusnaturalismo), por que ele precisa ser mediado? E se o capitalismo é natural, por que ele só se desenvolveu há 400 anos atrás? Se o modo atual de organização da sociedade é natural, por que não foi a forma de desenvolvimento de toda sociedade que existiu no planeta?

Liberais dizem que o marxismo não é ciência, mas o mesmo escapa de análises objetivas, concretas e materiais da realidade, chamando tudo o que ele não pode explicar de "natural".

Conhecimentos se dão por neuroplasticidade. Não existe conhecimento desprovido de empiria (estimulo dos sentidos) e também não principiam do intelecto. Leslie Lemke com 14 anos tocou, com perfeição, o Concerto n.º 1 para piano de Tchaikovsky e é autista Savant grave. Já foi provado na neurobiologia que o aprendizado envolve alteração cerebral. Para que o aprendizado ocorra, o cérebro precisa de condições nas quais seja capaz de mudar em resposta a estímulos (neuroplasticidade) e de produzir novos neurônios (neurogênese). Envolve também o recrutamento de várias regiões do cérebro. Essas regiões estão associadas aos sentidos internos como memória, controle da vontade e níveis mais elevados de funcionamento cognitivo. Não existe nenhuma forma de aprendizado que seja independente dessa empiria.

Como se dá a alteração cerebral? É justamente nesse processo que se dá o conhecimento, com ressonância magnética dá para ter total precisão disso. Você pode argumentar dizendo que o conhecimento não deriva todo da experiência por conta da ação do intelecto, mas também não pode dizer que é no intelecto que ela se forma.

Isso não significa que o conhecimento é moldado somente disso, existe todo um processo de aprendizagem, que quanto mais complexo, mais aprimorado vai ser o resultado (conhecimento), e nos humanos até coisas imateriais atuam como o intelecto. Mas não é isso que causa ele.

Uma pequena analogia:

Há um copo d'água, eu coloco um sachê (pó) para fazer suco. Mexo, mexo, apenas balançando um copo (sem usar colher). Resultado: Copo com um suco mal feito. Agora eu pego uma colher e mexo dentro. Resultado: Copo com um suco bem feito. 

Copo = Sentidos externos<br/>
Água = Sentidos internos<br/>
Sachê em Pó = Informações que vão entrar<br/>
Suco = Produto final (conhecimento)<br/>
Colher = Intelecto

Nos animais como não tem intelecto, o conhecimento é imperfeito, nos humanos é perfeito, porém, como vê na analogia, Agir é diferente de Produzir, o intelecto age na formação do conhecimento, mas é na empiria (estimulo dos sentidos) que ele é produzido.

Note, a priori pode tanto significar necessidade lógica como "independe dos sentidos", o primeiro existe, o segundo não. Estou falando justamente do segundo. A priori é o conhecimento por necessidade lógica, ele existe, o que não existe é conhecimento independente de algum grau dos sentidos, porem isso não anula em nada o apriorismo.

E mais, conhecimento não é falseável, porque conhecimento é informação testada e consolidada. Informação é falseável, conhecimento não. Conhecimento no máximo é refinável.

Leituras mais aprofundadas:
- https://gsi.berkeley.edu/gsi-guide-contents/learning-theory-research/neuroscience/
- https://www.youtube.com/watch?v=_nWMP68DqHE
