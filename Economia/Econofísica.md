# Econofísica
Os estudos empíricos da distribuição da renda pessoal nas sociedades capitalistas são interessantes. O mais alto, o regime de lucro-renda é normalmente ajustado a uma distribuição de Pareto (ou poder), enquanto o mais baixo, o regime de renda-salário, que representa a grande maioria da população, é normalmente ajustado a uma distribuição log-normal, mas recentemente alguns pesquisadores relatam que uma distribuição exponencial (Boltzmann-Gibbs) descreve melhor o regime mais baixo. A existência de dois regimes de renda é uma consequência direta das duas principais fontes de renda nas sociedades capitalistas, ou seja, salários e lucros, e a distribuição geral da renda parece ser uma mistura de duas distribuições qualitativamente diferentes.

Lange em sua introdução à econométrica, mostra que na Polônia socialista, toda renda se tornava uma distribuição log-normal, e argumenta que a distribuição da lei de potência é encontrada mais normalmente em países capitalistas.

Como demonstra Ian Wright em Implicit Microfoundations for Macroeconomics, uma grande economia de mercado tem um grande número de graus de liberdade com coordenação de micronível fraca. Em tais circunstâncias, a natureza de "partícula" dos indivíduos determina mais fortemente os resultados de nível macro em comparação com sua natureza "mecânica". Uma ampla gama de fenômenos macroeconômicos pode, portanto, ser explicada sem recorrer a suposições detalhadas sobre a racionalidade individual.

O modelo de "arquitetura social fechada" é um exemplo particular desta abordagem metodológica. Ele replica muitas das distribuições macroeconômicas relatadas das economias capitalistas, como:
1. A distribuição de Laplace do crescimento da empresa.
2. A distribuição da lei da potência dos tamanhos das empresas.
3. A distribuição log-normal das falências das empresas.
4. A distribuição exponencial da expectativa de vida da empresa.
5. A distribuição log-normal do PIB destendido.
6. A distribuição exponencial da duração das recessões.
7. A distribuição log-normal-Pareto da renda.
8. A distribuição exponencial-Pareto de riqueza.
9. Participações estáveis, mas flutuantes, na renda nacional.

O modelo também prevê uma forma funcional para a distribuição empírica da taxa de lucro industrial. Os resultados são fortes evidências a favor da proposição de que microfundamentos implícitos, em vez de explícitos, captam melhor as relações essenciais entre os níveis micro e macro da economia. A exploração de modelos macroeconômicos de base estocástica com microfundamentos implícitos, portanto, nos apresenta uma oportunidade de desenvolver teorias mais parcimoniosas com maior poder explicativo.

Fontes:
- [New evidence for the power-law distribution of wealth](https://www.sciencedirect.com/science/article/abs/pii/S0378437197002173)
- [Exchanges in complex networks: Income and wealth distributions](https://www.researchgate.net/publication/1939428_Exchanges_in_complex_networks_Income_and_wealth_distributions)
- [Applications of physics to economics and finance: Money, income, wealth, and the stock market](https://arxiv.org/abs/cond-mat/0307341)
- [Physics of Personal Income](https://link.springer.com/chapter/10.1007/978-4-431-66993-7_38)
- [Income Distribution and Stochastic Multiplicative Process with Reset Event](https://www.researchgate.net/publication/225269073_Income_Distribution_and_Stochastic_Multiplicative_Process_with_Reset_Event)
- [Implicit Microfoundations for Macroeconomics](http://www.economics-ejournal.org/economics/journalarticles/2009-19/)
- [Income distribution dynamics: a classical perspective](https://scholar.google.at/citations?view_op=view_citation&hl=th&user=m74xM1AAAAAJ&citation_for_view=m74xM1AAAAAJ:UeHWp8X0CEIC)
- [A Two Factor Model of Income Distribution Dynamics](https://www.semanticscholar.org/paper/A-Two-Factor-Model-of-Income-Distribution-Dynamics-Nirei-Souma/f7f3707a0118cef4c535f50c37d9bc8c6ab8d229)
- [Maximum entropy formalism, fractals, scaling phenomena, and 1/f noise: a tale of tails](https://www.vosesoftware.com/riskwiki/Maximumentropy.php)
- An entropy-utility model for the size distribution of income
- [Statistical mechanics of money, income and wealth: a short survey](https://arxiv.org/abs/cond-mat/0001432)
- [Temporal evolution of the "thermal" and "superthermal" income classes in the USA during 1983–2001](https://iopscience.iop.org/article/10.1209/epl/i2004-10330-3/meta)
