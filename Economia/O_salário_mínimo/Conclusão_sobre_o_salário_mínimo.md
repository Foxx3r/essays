# Conclusão sobre o salário mínimo
Tendo examinado as evidências, parece aparente que os aumentos do salário mínimo têm pouco ou nenhum efeito negativo sobre o emprego (com quaisquer impactos existentes concentrados nos setores comercializáveis). Além disso, eles parecem ter um impacto fortemente positivo sobre a renda real, redução da pobreza, saúde pública, desigualdade e uma série de outros resultados. O aumento do salário mínimo não é apenas uma política extremamente popular; também está bem fundamentado nas evidências.

Fontes:
- [U.S Government Accountability Office | Low-Wage Workers: Poverty and Use of Selected Federal Social Safety Net Programs Persist among Working Families](https://www.gao.gov/products/gao-17-677)
- [U.S Government Accountability Office | Federal Social Safety Net Programs: Millions of Full-Time Workers Rely on Federal Health Care and Food Assistance Programs](https://www.gao.gov/products/gao-21-45)
