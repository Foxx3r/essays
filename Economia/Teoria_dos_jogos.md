# Teoria dos jogos
Teoria dos jogos estuda disputas entre partes onde há uma compensação financeira. Os jogadores escolhem isoladamente suas estratégias e avaliam os resultados racionalmente depois. 

Os teoremas fundamentais são a destruição do liberalismo e o utilitarismo. Todas as disputas realistas têm pontos de equilíbrio, um nome enganoso. Um ponto de equilíbrio é um ponto onde o jogo estagnou, nenhum jogador tem motivação para mudar de estratégia sem cooperação. Uma violação dos pressupostos utilitários/liberais é que muitas vezes o jogo sem moderador converge para um ponto de estagnação que é péssimo para todos os envolvidos, mas se fica lá por medo de piorar ainda mais.

Outro exemplo são jogos repetidos e autorregulação. É possível mostrar que para todo jogo existe um % máximo de inflação para o qual autorregulação funciona, além disso os jogadores sentem incentivo para quebrar as regras para capitalizar agora e lidar com sanções depois. A gente vê isso no marketing brasileiro.

O equilíbrio de Nash acontece quando você não pode mais usar uma estratégia para o bem próprio, você só tem motivação para mudar para uma estratégia cooperativista sem tirar vantagem sobre ninguém.
