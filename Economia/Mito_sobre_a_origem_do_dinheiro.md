# Mito sobre a origem do dinheiro
Você alguma vez já ouviu falar que sociedades antigas tinham sua economia baseadas em [escambo](https://pt.wikipedia.org/wiki/Escambo) e que essa forma de troca era muito ineficiente e daí surgiu o dinheiro? Pois bem, é mentira.

Foi ao contrário, o escambo surgiu após a criação do dinheiro. Os povos pré-colombianos consideravam-se uma família (como a maioria das sociedades "primitivas"), consideravam o povo inteiro como uma família única, porque a produção era compartilhada entre TODOS, a propriedade era compartilhada entre TODOS.

O "mito do escambo" surgiu com a tese científica de Adam Smith no qual dizia achar (isso mesmo, "eu acho" em uma tese científica...) que o dinheiro vinha do escambo, mas estudos antropológicos e sociológicos atuais descobriram que o escambo vinha de mercados com economias falidas. No entanto, sobre a criação do dinheiro, há várias hipóteses e controvérsias.

Esse mito é mantido até hoje, porque deslegitimizaria o discurso positivista burguês.

Para mais informações https://www.theatlantic.com/business/archive/2016/02/barter-society-myth/471051/
