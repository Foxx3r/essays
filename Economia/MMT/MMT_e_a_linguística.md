# MMT e a linguística
![](images/MMT_linguistics.png)

Uma visão geral dos pressupostos que sustentam a linguística cognitiva e a filosofia cognitiva pode ajudar a explicar por que tem sido difícil montar um desafio coerente às visões macroeconômicas ortodoxas. Para isso, temos que entender 3 coisas básicas:
1. A mente está inerentemente incorporada.
2. O pensamento é principalmente inconsciente.
3. Os conceitos abstratos são amplamente metafóricos.

A personificação se relaciona com as atividades cotidianas, como agarrar algo com as mãos ou ficar em pé e andar. Relacionamos essas ações físicas por meio do uso de metáforas para tentar compreender o mundo ao nosso redor.

Por exemplo, entendemos conceitos associados a 'mais' ou 'menos' em termos de uma direção 'para cima' ou 'para baixo' como consequência de nossa experiência física de interação com nosso ambiente.

Metáforas relacionadas com a direção também são empregadas em nosso raciocínio sobre estar feliz ou triste (como em 'aquele foi um filme lindo' ou 'estou me sentindo mal') e sucesso ou fracasso (como em 'eles estão subindo na vida' ou 'eles caíram em desgraça'). Pensamos na quantidade em termos de direção, de modo que 'mais' seja conceitualizado como uma tendência ascendente e 'menos' como uma tendência descendente. Em relação ao nosso pensamento geral sobre dinheiro, 'mais' é normalmente bom e 'menos' é tipicamente ruim.

A ideia de que um déficit fiscal maior pode ser algo desejável é, portanto, contra-intuitivo em um sentido muito básico. O desenvolvimento de uma pedagogia econômica e a compreensão das oportunidades políticas decorrentes de uma compreensão mais completa dos termos macroeconômicos precisam ser comunicados de uma forma que explore nossa capacidade de interpretar as mesmas informações de maneiras diferentes. Palavras são importantes. Desde quando você era bebê, seus pais e outros responsáveis ​​contavam histórias para ensiná-lo a se comportar. Palavras e lições que você aprendeu na infância ficam com você. Eles estão impressos em seu cérebro e podem ser facilmente acionados. Se alguém proferir a palavra 'bruxa', isso provavelmente irá desencadear uma velha memória de uma das histórias que lhe contaram quando criança.

Às vezes, você não consegue tirar essas memórias da mente. Não pense em um elefante! Por mais que tente, você pensará em um elefante. Faça o que fizer, não pense no Dumbo! A maioria de vocês, vai pensar no personagem do filme da Disney. Essas grandes orelhas caídas grudam na sua mente e tornam difícil pensar em qualquer outra coisa. Há mais nisso do que simplesmente associar uma palavra (elefante) a uma visão (a imagem de Dumbo que você lembra da infância).

A ciência moderna nos ensina que os humanos pensam usando histórias, muitas delas aprendidas há muito tempo quando crianças. No processo de pensamento (cognição), você adapta essas velhas histórias a novas situações para dar sentido ao mundo.

Os anunciantes usam a moderna ciência da cognição para invocar associações entre os produtos que estão tentando vender e as redes neurais que já foram implantadas em seu cérebro. Os políticos também usam a ciência para acionar associações boas e más para ganhar votos. Por exemplo, um político que está concorrendo com uma plataforma 'dura com o crime' se refere indiretamente a um caso notório para despertar o medo nas mentes dos eleitores.

Por exemplo, a palavra 'dívida' é necessariamente um termo carregado com associações de imprudência, ônus e inadimplência. Depois que as redes neurais associadas a essas histórias são ativadas, é extremamente difícil evitar colocar conotações negativas na palavra e o conceito de 'dívida'. Em geral, os economistas conservadores têm sido muito mais cientes da ciência da cognição e a têm usado com sucesso para incorporar amplamente sua ideologia ao discurso econômico, inclusive na sala de aula e na esfera da formulação de políticas.

Caso queiram se aprofundar, recomendo a [palestra](https://www.youtube.com/watch?v=EuzCmRDi3vw) de Shenker-Osorio (e seus livros).
