# Déficits/superávits fiscais são ruins/bons
Os déficits fiscais não são bons nem ruins. Em termos contábeis, equivalem ao superávit não-governamental. Em termos comportamentais, são exigidos quando as intenções de gasto do setor não-governamental são insuficientes para garantir a plena utilização dos recursos produtivos disponíveis.

O contexto é importante porque o resultado fiscal é um veículo para atingir as metas socioeconômicas, e não um fim em si mesmo. Da mesma forma, os superávits fiscais não são bons nem ruins e podem ser prejudiciais em algumas circunstâncias. Para uma nação com fortes exportações líquidas, serviços públicos de qualidade e níveis de renda nacional suficientes para sustentar os desejos de poupança do setor privado, um superávit pode ser necessário para conter a demanda agregada nominal e evitar a inflação.

Portanto, existem dois problemas em usar o termo “déficit fiscal”. Em primeiro lugar, embora muitos falem como se o déficit fiscal fosse uma escolha de política do governo, na verdade o resultado fiscal é determinado pelo estado da atividade geral e está em grande parte fora do controle do governo. Se o gasto privado for fraco, o déficit normalmente aumentará à medida que a receita tributária diminuir. Os movimentos do saldo fiscal são, portanto, ambíguos. Um determinado resultado fiscal pode ocorrer porque o governo tomou decisões discricionárias de política fiscal para manter o pleno emprego, dadas as decisões de gasto e poupança do setor não governamental. Ou porque os gastos não-governamentais caíram e os estabilizadores automáticos levaram a um declínio da receita tributária líquida de transferências e a um aumento do desemprego. Quando o gasto privado entra em colapso e o déficit aumenta, a resposta correta é aumentar o gasto público líquido discricionário, não cortá-lo. O governo não faz orçamento para um déficit fiscal. Em vez disso, o resultado de um déficit (e como o governo deve reagir ao resultado orçamentário) depende do desempenho da economia. Em segundo lugar, o termo tem uma conotação negativa porque um déficit significa um déficit, que embora seja preciso no sentido contábil, é altamente enganoso no contexto da contribuição positiva que um déficit dá à riqueza financeira líquida do setor não governamental. Os déficits do governo são a única fonte de ativos financeiros líquidos para o setor não-governamental. Todas as transações entre agentes do setor não governamental são líquidas a zero. Essa realidade contábil significa que, se o setor não governamental quiser economizar na moeda de emissão, o governo terá de estar em déficit. Os saldos setoriais derivados das contas nacionais generalizam esse resultado e mostram que o déficit/superávit governamental é sempre igual ao superávit/déficit não-governamental. Os superávits fiscais destroem a riqueza privada, forçando o setor privado a liquidar sua riqueza para obter dinheiro e destruir a liquidez (débito em contas de reserva), que é deflacionário. Com um déficit externo, os superávits fiscais resultam em níveis crescentes de dívida do setor doméstico privado e não podem representar uma estratégia de crescimento sustentável de longo prazo.

Em última análise, a decisão do setor privado doméstico de aumentar sua poupança líquida e reduzir seus níveis de dívida irá interagir com o entrave fiscal decorrente dos superávits e forçar a economia à recessão. O componente cíclico do orçamento empurrará o déficit de volta para um déficit (ruim). Compreender o contexto em que ocorre um determinado resultado fiscal é crucial para uma avaliação fundamentada da adequação da posição fiscal. O fato de que um déficit fiscal permite que o setor não governamental economize em geral e um superávit fiscal destrói a riqueza não governamental deve ser compreendido e promovido.

Eu acho engraçado quem reduz os autores somente às prescrições que eventualmente surgem da concepção que eles têm da realidade e ignoram todo o resto. Como "ah, do Keynes eu aproveito a parte da emissão de moeda e do Hayek aquela parte da renda minima".

Sempre que alguém fala algo assim comigo, eu imagino que a pessoa nunca leu nada e que esses autores escrevem tipo um manual de instruções tipo:

**Keynes: Teoria Geral do Juro, do Emprego e da Moeda**

**Capitulo 1: Como resolver crises**
1. Imprima dinheiro
2. Faça x
3. Faça y

**Capitulo 2: Como reduzir a pobreza**
1. Faça z

Tenho certeza que acham que é isso que tem nos livros. Nada de interpretações da realidade, cosmovisões, epistemologias, reflexões. Apenas o que o autor quer fazer e ponto.
