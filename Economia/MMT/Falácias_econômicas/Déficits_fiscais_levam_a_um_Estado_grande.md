# Déficits fiscais levam a um estado grande
Os déficits fiscais podem refletir qualquer tamanho de governo. Mesmo os pequenos governos precisarão incorrer em déficits contínuos se houver um desejo dentro do setor não governamental de economizar em geral e o objetivo da política for manter os níveis de pleno emprego da renda nacional. A teoria econômica não especifica um tamanho ótimo de governo. O apelo por um governo menor reflete uma postura puramente ideológica, sem base na teoria econômica.

O tamanho do governo refletirá as preferências da população pelo fornecimento público de bens e serviços e infraestrutura. O tamanho do governo é uma escolha política e não uma necessidade econômica. Mesmo os pequenos governos normalmente incorrem em déficits contínuos para manter o pleno emprego.
