# Suporte empírico
Anwar Shaikh, Minqi Li e Paul Cockshott mais recentemente tem vários trabalhos demonstrando a validade empírica da TVT, assim como Ricardo já o fazia.

Desde o surgimento do marxismo no século 19, a maioria dos marxistas aderiu à teoria do valor-trabalho. Enquanto os economistas convencionais geralmente descartam a TVT como desatualizada e irrelevante, seus proponentes argumentam que ele foi apoiado por evidências empíricas recentes.

Um [artigo](https://ideas.repec.org/p/new/wpaper/2003.html) recente da New School for Social Research:
>A alegada refutação da teoria do valor-trabalho foi parte integrante do ataque marginalista contra a análise clássica e marxista. No entanto, a análise estatística das relações preço-valor possibilitada pelos dados disponíveis desde o final do século 20 sugere uma força empírica considerável da teoria do valor-trabalho.

Alguns dos primeiros trabalhos empíricos sobre este tópico vêm de uma [publicação](https://www.academia.edu/2733055/The_transformation_from_Marx_to_Sraffa) de 1984 do economista Anwar Shaikh, em que ele argumentou que "tanto para os preços de produção quanto para os preços de mercado, cerca de 93% das variações transversais e intertemporais nestes os preços podem ser explicados pelas variações correspondentes nos valores [de trabalho]."

Uma [publicação](https://www.jstor.org/stable/23597084?seq=1) de 1997 por Shaikh desenvolve este argumento ainda mais, com referência adicional à evidência empírica:
>Ao longo dos anos de insumos e produtos, descobrimos que em média os valores do trabalho se desviam dos preços de mercado em apenas 9,2% e que os preços de produção (calculados com taxas de lucro observadas) se desviam dos preços de mercado em apenas 8,2%.

Seguindo o trabalho inicial de Shaikh, um [artigo](https://www.jstor.org/stable/23597084?seq=1) de 1987 no Cambridge Journal of Economics examinou os desvios entre as razões do valor do trabalho e os preços de produção, chegando à conclusão de que:
>Os preços relativos de produção são determinados principalmente pelas taxas do valor do trabalho (...) Os efeitos agregados dos desvios entre os preços de produção e as razões do valor do trabalho são pequenos, de modo que pode-se aceitar como empiricamente válida a proposição de Marx de que a soma dos lucros é igual à mais-valia total.

Um [artigo](https://www.jstor.org/stable/23598125?seq=1) de 1989 no Cambridge Journal of Economics analisou dados dos Estados Unidos, descobrindo que:
>O valor-trabalho e os preços de produção para a economia dos Estados Unidos no período pós-Segunda Guerra Mundial eram notavelmente próximos uns dos outros, bem como dos preços de mercado. (...) Enquanto a presença de bens de capital heterogêneos e proporções fixas desferiu golpes fatais no conceito neoclássico de capital físico agregado, a quase linearidade das curvas de salário-lucro real parece apoiar a teoria do valor do trabalho como uma prática poderosa usada para analisar e compreender o caráter global da produção e do crescimento nas economias capitalistas.

Em um [artigo](https://academic.oup.com/cje/article-abstract/21/4/545/1729729) de 1997, publicado no Cambridge Journal of Economics, os economistas Cockshott e Cottrell forneceram o que alegaram ser o suporte empírico para a lei do valor, entendida como a proposição de que o tempo de trabalho incorporado é conservado nas trocas de mercadorias:
>Os preços de mercado estão bem correlacionados com a soma do conteúdo de mão-de-obra direta e indireta. É possível produzir correlações igualmente boas tomando a soma do conteúdo x direto e indireto, onde x é alguma entrada diferente do tempo de trabalho? Repetimos a análise para eletricidade, ferro e aço e petróleo e mostramos que a resposta é não. As altas correlações no caso do tempo de trabalho não são, portanto, um artefato estatístico.

Um [artigo](https://academic.oup.com/cje/article-abstract/26/3/359/1711465) de 2002 no Cambridge Journal of Economics examinou o tempo de trabalho e o preço de mercado na economia grega, encontrando mais evidências para apoiar o valor empírico da TVT:
>Nossos resultados sobre a proximidade de valores e preços medidos por seu desvio absoluto e correlação, a forma das curvas de salário-lucro, o poder preditivo dos valores do trabalho sobre os preços de mercado em comparação com outras 'bases de valor' e a comparação do categorias fundamentais marxianas, quando estimadas em termos de valor e preço, fornecem suporte adicional para a força empírica da teoria do valor-trabalho.

Um [artigo](https://www.semanticscholar.org/paper/Labour-value-and-equalisation-of-profit-rates-%3A-a-Zachariah/10f8ac8fa8bdd02c222cac4a14caea8b6cc9ded3#paper-header) de 2006, publicado na Indian Development Review, examinou dados de 18 países, entre 1968 e 2000, argumentando que os dados apoiam a conexão empírica entre tempo de trabalho e preço de mercado:
>Os resultados são amplamente consistentes; o valor-trabalho e os preços de produção dos produtos da indústria estão altamente correlacionados com seu preço de mercado. O poder preditivo é comparado a bases de valores alternativas. Além disso, o suporte empírico para a equalização da taxa de lucro, conforme assumido pela teoria dos preços de produção, é fraco.

Um [artigo](https://content.csbs.utah.edu/~mli/Renmin%20Summer%202017/Tsoulfidis%20and%20Rieu-v19n3_275.pdf) de 2006 no Seoul Journal of Economics estendeu esta análise à economia coreana, argumentando:
>A economia coreana exibe semelhanças com uma série de outras economias no que diz respeito à proximidade dos valores de trabalho (...) Esse resultado dá suporte adicional à teoria do valor-trabalho como uma ferramenta analítica para a compreensão das leis do movimento das economias modernas.

Um [artigo](https://academic.oup.com/cje/article-abstract/37/5/1107/1679189) de 2013 do Cambridge Journal of Economics examinou dados da economia alemã, descobrindo que as evidências confirmam os principais resultados de estudos anteriores sobre valores de trabalho e preços de mercado. O autor argumenta que isso fornece suporte para a visão marxista geral da exploração:
>Embora a maioria dos autores contemporâneos negue a relevância dos valores do trabalho para explicar os preços, há boas razões para argumentar que a lei do valor está correta em um sentido estocástico. Sem entrar em detalhes, isso implica que os famosos postulados invariantes marxistas são justificáveis de maneira semelhante. O lucro, portanto, não deve estar relacionado ao produto marginal do capital, mas ao trabalho explorado.

Um [artigo](https://journals.sagepub.com/doi/full/10.1177/0486613419849674) de 2020 na Review of Radical Political Economics estende esta análise à economia chinesa, descobrindo:
>A teoria dos preços baseada na tradição marxista ou clássica pode explicar amplamente os preços de mercado observados nos setores produtivos capitalistas na China.

Além disso, o [artigo](https://ideas.repec.org/p/new/wpaper/2003.html) de 2020 acima mencionado da New School for Social Research diz que:
>A aplicação empírica mais abrangente de sua classe e [generaliza] os resultados que foram estabelecidos na literatura relevante. (...) A análise de um grande conjunto de dados de 42 países e 15 anos revela apenas desvios pequenos e estáveis e, portanto, dá suporte à análise política econômica clássica.
