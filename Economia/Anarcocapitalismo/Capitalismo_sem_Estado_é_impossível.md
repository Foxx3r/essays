# Capitalismo sem Estado é impossível
Você precisa da moeda por exemplo. De onde ela vem?

Moeda qualquer um pode criar. Mas imagina que existam 7 moedas diferentes rodando dentro da sociedade, quem define qual vale mais? Isso ficaria uma bagunça, mas aí você cria mais para resolver isso e tenta imprimir mais dela do que os outros. a abundância do seu produto já desvalorizou ele, já teria mais moeda do que uso para ela, ou seja, o Estado funciona centralizando isso e centralizando a moeda única em determinada região.

A liquidez mede a facilidade que um bem pode ser transformado em dinheiro. Tendo várias moedas na mesma economia servindo as mesmas funções e com valores de troca diferentes, isso ficaria uma bagunça completa. Imagina vender algo e ganhar em várias moedas diferentes? Usa a de maior liquidez? Você acaba de recentralizar a moeda para que tenha fluidez econômica, basicamente o que o Estado faz.

Outro exemplo: a laranja precisou ser transportada por uma estrada, quem pavimentou? O produtor precisou de tecnologia, quem financiou a compra com empréstimos e criou a tecnologia necessária? Sem contar que trocas não é mercado, comércio existe desde antes do capitalismo.

Uma empresa privada cria estradas e agora obtém pedágios sobre ela, o preço de todas as coisas vai nas alturas porque se não houver um monopólio, toda vez que a estrada for de outra empresa, você paga um novo pedágio. Imagina pagar pedágio para travessar uma rua de carro? O Estado funciona com esse monopólio. Toda pesquisa base do mundo é feita pelo Estado porque elas não dão renda imediata, as empresas compram a tecnologia para desenvolver e capitalizar algo que já veio quase pronto de alguma universidade do Estado. O povo junta fundos (crowdfunding) e alguém faz determinado serviço, parabéns você acaba de criar o imposto.

Para mais informações, consulte a secção de Direito e a secção O socialismo funciona sim, na parte onde mostra-se como as tecnologias atuais não teriam sido criadas sem a ajuda do Estado.

