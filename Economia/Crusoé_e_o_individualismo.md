# Crusoé e o individualismo
Robinson Crusoé tem um individualismo metodológico anacrônico, nessa concepção subjetivista. Só intervém a relação entre o homem e as coisas, não sendo perturbada pelas relações sociais entre os homens e o indivíduo isolado da natureza, excluindo o problema da exploração e das relações hierárquicas entre os homens.

Porém a verdadeira história de Robinson Crusoé é também uma história de conquista, escravidão, roubo, assassinato e força. Ela descreve como um inglês do século XVII reuniu capital e organizou uma força de trabalho para trabalhar para ele, no Brasil e no Caribe. Robinson é retratado como um indivíduo auto-suficiente, mas muito da verdadeira história, mesmo depois do naufrágio, mostra-o como um homem dependente, pertencendo a um todo maior e sempre esperando pela ajuda e cooperação dos outros. A natureza social da produção acaba sendo a verdadeira mensagem de sua história.

Não há nenhum paradoxo nisto, pois ao capitalismo pertencem tanto a produção das relações sociais da História mais altamente desenvolvidas como a produção do indivíduo solitário.

Crusoé produz somente para o consumo e não para a troca, então:
1. Não há escassez
2. O trabalho por si não tem valor

A força propulsora do capitalismo, a paixão por acumular, que desapareceu quando ele ficou sozinho. Seu isolamento voluntário é medo e desconfiança de outra pessoa, que é nada menos do que a alienação do individualismo possessivo, típico da sociedade capitalista. Não foram os atributos pessoais que deram a Robinson e a outros aventureiros europeus sua força frente aos não europeus colonizados, mas a força do conhecimento transformado em equipamentos.

Ele está sempre lutando com o problema de subordinar os escalões inferiores e tentando elevar-se acima do outro. Suas relações sociais são antagônicas e ele sabe disso, por mais que prefira inventar histórias sobre si mesmo. 
