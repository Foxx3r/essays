# Curiosidades econômicas
Wittgenstein era amigo de Sraffa e Gramsci e professor de Alan Turing.

Marcos Lisboa, antes de ser ortodoxo, fez doutorado na UFRJ, estudando em especial teorias neo-ricardianas. Após contato com com professores do IMPA, mudou completamente de lado e tornou-se ortodoxo puro sangue.

Veblen, um dos pais da economia instucionalista, tinha o costume de passar com 10 ou A qualquer o aluno que o pedisse. Basicamente, bastava pedir para ter 10 que ele dava essa nota aos alunos.

Marx quando escrevia O Capital, dedicou-se ao estudo de Cálculo para ver se os recursos matemáticos o ajudariam em seus insights sobre economia. No entanto, não conseguiu extrair muitas noções de lá. Outros autores acharam na álgebra linear formas de matematizar Marx.

Douglass North, a mando do governo dos EUA, veio ao Brasil ajudar Celso Furtado com um plano econômico. Furtado, que não era colonizado, não deu a mínima pras ideias do North e o último ficou chateadissimo.

Sraffa escreveu uma crítica tão forte a teoria do preços do Hayek que o mesmo por uns 10 anos diminuí drasticamente sua produção acadêmica e só voltou a ser relevante quando saiu de Cambridge  e foi lecionra numa universidade estadunidense.
