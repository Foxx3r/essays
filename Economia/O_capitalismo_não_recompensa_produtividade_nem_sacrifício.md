# O capitalismo não recompensa produtividade nem sacrifício
Uma das mentiras mais difundidas sobre o capitalismo é como o capitalismo recompensa os trabalhadores.

O capitalismo não recompensa:
- Produtividade
- Valor
- Trabalho árduo, esforço, sacrifício ou duração do trabalho

Como iremos provar, essas mentiras são apenas propaganda sem base na realidade.

Primeiro, vamos começar com como os salários realmente funcionam. Como a maioria dos preços no capitalismo, os salários (preço do trabalho) são determinados pelos mercados. Os mercados são um sistema de alocação em que os preços refletem o poder de barganha relativo entre compradores e vendedores de bens e serviços.

Os mercados incentivam compradores e vendedores a maximizarem o benefício transacional para si próprios. Com os mercados, os compradores querem que o preço seja o mais baixo possível e os vendedores querem que o preço seja o mais alto possível. Mas abaixo de um determinado preço (piso), uma transação não vale mais a pena para o vendedor (ou seja, eles perderiam dinheiro com esse preço). E acima de um determinado preço (teto), uma transação não vale mais a pena para o comprador (ou seja, pode desistir da compra ou encontrar alternativas).

No entanto, entre o preço mínimo e o preço máximo, o preço real dos bens e serviços nas transações de mercado tende para quem tem o maior poder de barganha. Quem pode se afastar do negócio com mais facilidade? Quem pode encontrar uma alternativa facilmente? Ou quem está mais desesperado? Na verdade, com os mercados, entre os preços mínimos e máximos, a maior parte do benefício de cada transação vai para a parte que já tem o maior poder de barganha.

Os economistas já entenderam essa verdade há muito tempo. O Movimento do Comércio Justo nasceu em grande parte como resultado disso. O comércio com parceiros comerciais desesperados e assolados pela pobreza em um sistema de mercado oferece virtualmente todos os benefícios do comércio aos que já são poderosos. Exemplo: digamos que para um vendedor de um país pobre custar $2 para fazer o widget A, o preço mínimo é $2. Digamos que os compradores de países ricos estariam dispostos a pagar até $10 por Widget A (teto). O preço real ficará muito mais próximo de $2 do que de $10 devido às disparidades do poder de barganha.

O Movimento do Comércio Justo entendeu que no comércio internacional, os preços de mercado refletem o poder de barganha relativo entre compradores (ou seja, países ricos) e vendedores (ou seja, países mais pobres) e, como resultado, os preços são tão baixos que dão a maior parte do benefício para os compradores. O Movimento do Comércio Justo mostrou como e por que os mercados exasperam as disparidades de riqueza internacionalmente. As transações de mercado entre parceiros comerciais radicalmente desiguais (poder de barganha) ao longo do tempo aumentam as disparidades de riqueza devido à forma como os mercados funcionam.

Isso deve ser evidente, visto que vemos isso todos os dias. Pessoas desesperadas para sobreviver, à beira da fome ou da falta de moradia, têm pouco poder de barganha. Pessoas com riqueza, que podem adiar as transações com pouco efeito para si mesmas, têm maior poder de barganha.

Com os mercados, os preços sempre tendem a beneficiar a parte com maior poder de barganha. Como os salários são preços para diferentes tipos de trabalho comprados e vendidos em um sistema de mercado, eles seguem a mesma lógica de mercado: entre o preço mínimo e o preço máximo, quando os vendedores estão mais desesperados do que os compradores, os preços caem para mais perto do preço mínimo. Quando os compradores estão mais desesperados do que os vendedores, os preços sobem para mais perto do preço teto.
>No capitalismo, quem manda é o consumidor.

Sim, então o consumidor decide que não há problema os trabalhadores da empresa terem condições péssimas de trabalho desde que o preço do que ele quer comprar caia e fique tudo certo.

Como vimos, os preços nos mercados refletem o poder de barganha relativo entre compradores e vendedores. Uma vez que os salários no capitalismo são comprados e vendidos no mercado de trabalho, os salários também são definidos pelo poder de barganha relativo entre o empregador (comprador) e o trabalhador (vendedor). Os empregadores conhecem essa verdade. É por isso que eles odeiam os sindicatos. Os sindicatos fortalecem o poder de negociação dos trabalhadores de várias maneiras (poder de negociação coletiva, tornar os trabalhadores menos dispensáveis, etc), resultando em salários mais altos para os trabalhadores sindicalizados.

Uma das principais coisas ensinadas nos melhores programas de mestrado de negócios/administração de empresas é como diminuir o poder de barganha dos funcionários, reduzindo assim os salários e, assim, aumentando os lucros. Com os mercados, o poder de barganha é tudo.

Os empregadores sabem disso, os executivos sabem disso, os mestrados em negócios sabem disso. Os economistas sabem disso. No entanto, a propaganda é o oposto: trabalhe duro e você ganhará mais, seja mais produtivo e você ganhará mais, produza mais valor para seu empregador e você ganhará mais. Tudo mentira para as massas.

Agora que sabemos a verdade, é fácil refutar as mentiras.

**Os salários são baseados na produtividade**

Entre a década de 1950 e hoje, a produtividade do trabalhador aumentou várias vezes, mas os salários permaneceram estagnados. Correlação histórica zero entre produtividade e salários. A produtividade por trabalhador disparou, mas praticamente nenhum desses ganhos beneficiou os trabalhadores porque o poder de barganha do trabalhador permaneceu estável. A guerra unilateral contra os sindicatos nos últimos 50 anos contribuiu muito para manter baixo o poder de barganha dos trabalhadores.

A única vez que a produtividade aumenta os salários é se esse aumento da produtividade se traduzir em maior poder de barganha para os trabalhadores. Se você aumenta sua produtividade de uma forma que o torna mais indispensável para seu empregador, isso aumenta seu poder de barganha e seu salário. Portanto, não é o aumento da produtividade que fez com que os salários subissem; só se os trabalhadores conseguirem obter maior poder de barganha como resultado do aumento da produtividade é que os salários subirão.

Mentira 1 desmascarada: Nenhuma correlação entre produtividade e salários.

**Os salários são baseados no valor do seu trabalho**

Se por "valor" queremos dizer "valorizado pela sociedade", obviamente não é o caso, pois os trabalhadores em alguns dos produtos e serviços de maior valor social (ou seja, professores) recebem os salários mais baixos.

Se por valor queremos dizer valor que pode ser traduzido em lucros para os empregadores, isso obviamente também não é verdade, já que os trabalhadores de alguns dos setores mais lucrativos (ou seja, os trabalhadores da fábrica do iPhone) também recebem alguns dos salários mais baixos.

Independentemente dos níveis de lucro (desde que não haja perda), a correlação mais forte é entre os salários dos trabalhadores e o nível de poder de barganha do trabalhador nessa indústria, não os níveis de lucro e não o valor do trabalho.

Mentira 2 desmascarada

**Os salários são baseados no trabalho árduo, esforço, sacrifício ou duração do trabalho**

Ninguém trabalha 100x, ou 1.000x ou 10.000 vezes mais ou mais arduamente do que qualquer outra pessoa - mas alguns ganham 100x, 1.000x ou 10.000 vezes mais do que outros.

Se os salários fossem remotamente equitativos, o trabalhador médio, trabalhando a duração média com um esforço médio, receberia a renda média de aproximadamente $130.000 por ano! Obviamente, este não é o caso.

Mas e quanto à duração do trabalho? Para muitos trabalhadores, existe alguma correlação entre a duração do trabalho e a renda. Mas essa correlação é apenas por causa do salário mínimo, não por causa das forças de mercado. E grande parte da luta por um salário mínimo foi baseada no entendimento de que, se deixada para as forças normais do mercado, ou seja, o poder de barganha relativo entre trabalhadores e empregadores, os salários cairiam tanto que a maioria dos trabalhadores receberia salários de extrema pobreza (piso).

Assim, como mostramos, o capitalismo de mercado recompensa o poder de barganha, não a produtividade, não o valor e, certamente, não o trabalho árduo, o esforço, o sacrifício ou a duração do trabalho. E como o mercado recompensa apenas o poder de barganha, ele é inerentemente injusto.

O único critério justo e equitativo para salários é recompensar o trabalho árduo, o esforço, o sacrifício e a duração do trabalho. Os capitalistas mentem sobre isso porque soa justo, mas o capitalismo não é inerentemente justo. São os socialistas que exigem justiça e equidade.
