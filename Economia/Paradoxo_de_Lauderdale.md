# Paradoxo de Lauderdale
James Maitland, o 8º conde de Lauderdale, era um escocês que discutia dinheiro e riqueza. Os nerds de história o conhecem como o cara que duelou sem sangue com Benedict Arnold, mas, na verdade, ele desempenhou um papel fundamental na discussão da riqueza pública. Maitland especulou que existe uma correlação inversa entre a riqueza pública e a riqueza privada. Ele teorizou que a riqueza pública é "tudo o que o homem deseja que seja útil ou agradável para ele". Eu descrevo isso como coisas como ar, água, etc.

A riqueza privada é uma riqueza pública, que “existe em certo grau de escassez”. Porém, escassez é um termo carregado. O que é escasso? Ele usou o exemplo da água. Você pode caminhar até um corpo d'água e usá-lo como quiser. Tem pouco valor financeiro por si só. Se você acumular água... você pode transformá-la em riqueza. Nesse caso, foi criada riqueza privada. No entanto, foi criado pela apreensão e acumulação de riquezas públicas.

Ao roubar riqueza pública, você criou riqueza privada.

Maitland também escreveu sobre plantadores de tabaco na Virgínia do século XVIII. A Assembleia aprovou uma lei restringindo as plantações a 6.000 plantas de tabaco por escravo. Na passagem do ato, cada escravo em média cuidava de 10.000 plantas. Com esse ato, eles reduziram a oferta em 40%. A escassez fez com que os preços subissem por antecipação, mas eles ainda tinham que colher o excedente da oferta. Naturalmente, os preços caíram, como você esperava.

Estou brincando! Eles queimaram o suprimento extra para garantir que os preços não caíssem. O objetivo não era cultivar tabaco, era ganhar dinheiro!

O que isso me lembra? Habitação. Os desenvolvedores criam, criam e criam. No entanto, o objetivo do desenvolvedor não é fazer habitação. É para ganhar dinheiro. Então o que eles fazem? Eles queimam suprimentos. Eles vendem no exterior para investidores que nunca precisarão dele. Eles vendem em massa para investidores locais, que lentamente vendem ao público. Pegando um bem público, espaço e, aos poucos, vendendo-o de volta ao público como um bem escasso, eles conseguem gerar uma fortuna. Quanto mais você gasta em moradia, menos gasta na comunidade. Quanto menos pessoas gastam na comunidade, menos próspera ela se torna. Em outras palavras, ao monopolizar a oferta natural de riqueza pública, eles conseguiram obter grande riqueza privada.

O capitalismo subestima os recursos públicos, como ar/água/solo, porque eles são tão abundantes e supervaloriza tudo o que é privado e escasso. Um barril de petróleo é vendido por $80, mas as emissões desse petróleo não aparecem na conta de ninguém.

Fonte:
- [Jason Hickel | Degrowth: A Call For Radical Abundance](https://www.jasonhickel.org/blog/2018/10/27/degrowth-a-call-for-radical-abundance)
