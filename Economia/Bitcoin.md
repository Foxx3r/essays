# Bitcoin
Irei refutar 6 mitos sobre o qual o Bitcoin alega ter sido criado para resolver:
1. Substituto das moedas estatais modernas
2. Confiabilidade, escapando dos mecanismos corruptos estatais
3. Transparência, sem terceiros intermediando a transação ou rastreando-a
4. Segurança, garantida pela Blockchain e sua criptografia reforçada
5. Descentralização, para evitar monopolização e corrupção do poder
6. Viabilidade, sendo segura nos seus propósitos e utilizável para qualquer fim prático em escala global visando satisfazer as necessidades básicas dos indivíduos

**Substituto**

A ideia de que o Bitcoin substituirá o dólar ou o euro é baseada em um equívoco fundamental de por que o dinheiro do Estado circula (Knapp 1924, Wray 1998). As moedas dos estados circulam porque são a unidade que deve ser usada para pagar os impostos. Enquanto isso continuar, sua circulação será primária e o Bitcoin e similares permanecerão no nível de hedge especulativo, assim como o ouro ainda está. Em comparação com o sistema de banco de compensação existente, é terrivelmente ineficiente em termos de energia.

**Confiabilidade**

Stablecoins são moedas virtuais que sempre devem ter o mesmo valor em dólar real. As pessoas que usam criptomoedas no Day Trade geralmente desejam transferir seus tokens instáveis para moedas reais seguras (como o dólar) porque as flutuações do mercado tornam inseguro mantê-los. Porém, quando uma empresa transaciona em dólares, ela deve seguir as regras do banco que a detém e, por procuração, as regras que o governo dos Estados Unidos impõe ao banco. Se você está negociando criptomoeda, provavelmente não gosta dessas regras, pois provavelmente está fazendo algo obscuro.

O ecossistema de câmbio de criptomoedas tem grandes problemas para obter acesso aos serviços bancários normais. Portanto, insira Tether ou USDT, um dólar criptomoeda substituto que teoricamente tem o mesmo valor de um dólar, mas pode ser negociado sem seguir a regulamentação em dólares.

O modelo é simples, você dá à empresa um dólar real, eles armazenam esse dólar real e dão a você um Tether. Você troca o Tether pelo que quiser e, a qualquer momento, pode resgatar o Tether por um dólar. Simples o suficiente. É um argumento simples: tenha dólares, compre Tether, faça coisas duvidosas, resgate Tether, ganhe dólares. "O dólar virtual para arbitragem regulatória."

No entanto, isso depende de um ponto centralizado: a empresa que administra esse serviço precisa manter um registro contábil de todo o dinheiro que entra e sai. Cada dólar entrado tem que ser combinado com um Tether emitido, cada Tether resgatou um dólar real que flui para fora. Os dólares reais mantidos na chamada "reserva" devem ser exatamente iguais ao número de dólares de Tether emitidos.

Se não for esse o caso (ou seja, Tethers sem lastro), uma certa porcentagem dos detentores dessa moeda não pode realmente resgatar porque o dinheiro não está lá. O que está claro é que a empresa supostamente emitiu $59 bilhões de dólares virtuais. Nessa escala, é altamente improvável que qualquer banco em todo o mundo tenha essas reservas em nome de uma empresa de criptomoeda. Nem mesmo os bancos mais duvidosos assumiriam esse nível insano de risco.

Onde está o dinheiro? A maioria dos jornalistas financeiros especulou que a empresa está envolvida em alguma trapaça contábil opaca onde, em vez de equiparar os Tethers com a entrada de dólares, eles produzem grandes quantidades de Tethers que não são respaldados por nada.

Agora, a empresa que emite esses produtos é notoriamente opaca e abriu uma loja no paraíso fiscal das Ilhas Virgens Britânicas para evitar qualquer regulamentação e obrigações de relatórios. Então, realmente não sabemos muito. O que sabemos vem de processos judiciais e jornalistas investigativos.

Em 2020, o procurador-geral de Nova York investigou a entidade norte-americana associada à negociação do Tether e, de fato, encontrou uma grande quantidade de falsas declarações em suas descobertas. Nas [palavras](https://ag.ny.gov/press-release/2021/attorney-general-james-ends-virtual-currency-trading-platform-bitfinexs-illegal) do Procurador-Geral:
>As afirmações da Tether de que sua moeda virtual sempre foi totalmente respaldada por dólares eram uma mentira. Essas empresas obscureceram o verdadeiro risco que os investidores enfrentavam e eram operadas por entidades não licenciadas e não regulamentadas que lidavam com os cantos mais sombrios do sistema financeiro. Tether fez declarações falsas sobre o apoio do stablecoin 'Tether' e sobre o movimento de centenas de milhões de dólares entre as duas empresas para encobrir a verdade sobre perdas massivas.

Finalmente obtivemos as divulgações exigidas pelo tribunal do que está realmente nas reservas. E não é de surpreender que, quando o cofre é aberto, o dinheiro não esteja realmente lá. O que vemos é muito "papel comercial", que é uma forma de dívida de curto prazo, um empréstimo sem garantia de empresa para empresa. É contabilizado pelo valor de face do empréstimo, mas na realidade esse valor depende do risco de crédito da outra contraparte do empréstimo. Se a contraparte não é boa para o valor do papel, então não vale a pena, apenas um truque de contabilidade. E não sabemos quem são as outras partes.

Cada Tether é apoiado por uma pilha gigante de IOUs para estranhos, e isso vale exatamente o que você pensa. Apenas 2,9% da tesouraria do Tether está na verdade em dólares reais mantidos por um banco. Portanto, para cada 1 USDT há $0,03 dólares reais.

As corretoras do planeta estão mantendo em média 3% do valor captado no caixa de câmbio. Se todos os donos de bitcoin quisessem vender seus Bitcoins agora, não haveria dinheiro para o câmbio. Melhor dizendo, elas precisariam honrar os tokens de dólar da conversão do bitcoin, mas os tokens de dólar que sustentam a precificação do bitcoin são insolventes, e é uma pirâmide porque os grandes acumuladores de Bitcoin fazem grandes movimentações para criar oscilações de preço, vender parte das suas moedas e ganhar dinheiro dos investidores pequenos. É como se os mais ricos estivessem cobrando um imposto do mercado de Bitcoin.

Bem, isso é um grande problema porque, em volume, o Tether é de longe a criptomoeda mais negociada em todo o mundo. Surpreendendo todos os outros tokens por uma margem significativa. Também a criptomoeda mais negociada para [Bitcoin](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3195066). 80% do volume de Bitcoin é onde uma das partes está trocando Tether por Bitcoin. Se o preço do Bitcoin é cotado em dólares e vendido em Tether, esse preço não reflete o vasto diferencial risco/valor entre os dois.

É amplamente suspeito que muitas bolsas estão recebendo grandes entregas de Tethers sem lastro, usando-os para lavar o comércio com eles próprios para Bitcoin a fim de aumentar a demanda e, assim, inflar sinteticamente o preço. Isso é ilegal em outros mercados.

Uma parte significativa da formação de preços do Bitcoin é, portanto, cotada em dólares, mas paga em dólares USDT que são realmente garantidos por três centavos. O que tornaria a maior parte da formação de preço do bitcoin totalmente sintética. O que leva à óbvia verdade inconveniente que a maioria das pessoas que olha para o espaço das criptomoedas acaba entendendo.

A maioria das bolsas está subcapitalizada e nunca será capaz de pagar nem mesmo uma pequena fração de seus clientes em dólares reais. Os mercados criptográficos não são significativamente diferentes dos esquemas Ponzi. As entradas de dólares de curto prazo são usadas para pagar as saídas de curto prazo e a coisa toda permanece flutuando desde que não haja muitos saques. Quando esse dia chegar, ela implodirá. Parece inevitável que em algum momento no futuro próximo todos os que possuem Tethers verão todas as suas propriedades desaparecerem. Eles passarão de 1,0x o dólar para 0,0x o dólar, provavelmente em um período muito curto.

O valor da maior parte das criptomoedas está medido não em dólar, mas em Tether, uma criptomoeda que se diz lastreada em dólar. Todas as criptomoedas negociadas em Tether valem pelo menos uns 70% menos do que afirmam valer.

Você pode escolher qualquer stablecoin e todas vão te enganar, porque stablecoin é o mesmo princípio que levou à crise das tulipas. Ninguém pode te garantir que uma stablecoin vale o que vale. É só fé no "livre mercado". O valor do Bitcoin é fortemente manipulado para especulação, usando um complexo esquema de stablecoins falsas. O valor do Bitcoin não opera em câmbio real, é um valor falso. Não há como o Bitcoin se tornar estável porque não há como o Bitcoin parear com o fluxo de mercadorias no mercado, tanto porque a emissão é limitada quanto porque não existe uma entidade regulando o enxugamento de moeda acumulada.

Se eu te contasse o quanto o mercado de Bitcoin é manipulável... O que mais tem é baleia e agente estatal operando em Bitcoin, para arrancar dinheiro dos peixinhos pequenos. As baleias injetam dinheiro na rede aos pouquinhos, esperando os trouxas comprarem Bitcoin o suficiente, daí quando os coeficientes estão perfeitos, eles tiram tudo de uma vez, causando essa queda. Compram os Bitcoins baratos e lavam o dinheiro deles, daí começa tudo outra vez. É um mercado totalmente manipulado. Só vale a pena investir em Bitcoin se você tiver mais de $10.000.

Vamos falar sobre como esquemas de pirâmide, como o Bitcoin, historicamente explodiram e os danos públicos que acontecem quando isso acontece.

Um esquema em pirâmide é um tipo de fraude em que os investimentos são solicitados ao público sob o pretexto (implícito ou explícito) de oferecer elevados retornos sobre o seu investimento. Normalmente, os retornos vão muito além dos mercados normais. O molho secreto que faz tudo girar é que os retornos são pagos aos primeiros investidores a partir dos fundos recebidos daqueles que investem mais tarde. De uma perspectiva contabilística, o esquema é insolvente desde o início, os ativos excedem o passivo e uma vez que o esquema não tem fluxo de caixa, a não ser novos investidores, o esquema requer constante recrutamento e promoção para sustentar a ilusão. 

Soa familiar? Os investidores posteriores são atraídos por anedotas seletivas sobre os retornos dos primeiros investidores, ignorando (muitas vezes devido à ofuscação) os macrofluxos de dinheiro de todo o esquema. Eventualmente, quando investidores tardios tentam tirar o seu dinheiro das bolsas ou corretores, descobrem a verdade sobre o esquema ser insolvente. Os operadores do esquema geralmente fogem com os fundos remanescentes e tudo isso rapidamente se desmorona deixando um rasto de vítimas.

O melhor exemplo histórico dessa tendência em escala é na Albânia em 1997, e essa história tem premonições assustadoras para o futuro colapso do mercado de criptomoedas. A Albânia saiu de um sistema comunista em 1991 diretamente para uma economia de livre-mercado, com muito poucas instituições para proteger ou regular o novo mercado. Efetivamente, é a economia puramente libertária com que a maioria dos criptocultistas sonham. Um paraíso greenfield para os golpistas.

Surgiram vários grandes "fundos" que prometeram pegar o investimento de retalho e utilizar os fundos para investir noutras empresas e infra-estruturas em desenvolvimento na nação. Prometiam grandes rendimentos com baixo risco, e o público mergulhou nestes esquemas sem muita diligência.

Muitos destes esquemas foram diretamente geridos por funcionários governamentais e prometeram servir como prestadores de serviços financeiros rudimentares na economia em desenvolvimento que não dispunham de muitos dos serviços encontrados em outras partes da Europa. Isso atingiu o seu auge em 1997, quando mais de 10% do PIB nacional foi bloqueado por esses esquemas. E nessa época os esquemas estavam fazendo promessas de rendimentos verdadeiramente absurdos de 40% ao mês para atrair as últimas vítimas.

As pessoas que estão pessoalmente investidas em um esquema em pirâmide lutarão com unhas e dentes para o legitimar. Elas querem acreditar. Os vigaristas não vêem fatos, eles vendem emoção. E os investidores ingênuos racionalizarão post-hoc quase tudo o que lhes promete dinheiro grátis. Não é de surpreender que esses esquemas nada mais fossem do que esquemas de pirâmide velados que continuaram solicitando mais e mais dinheiro público até que finalmente quebraram a economia inteira, o governo entrou em colapso e o país logo caiu na anarquia.

Durante toda esta era, o governo corrupto permaneceu em silêncio sobre as burlas (tal como a SEC e o governo federal fazem hoje em dia), acrescentando combustível ao meme que os esquemas em pirâmide eram legítimos. Durante toda essa era, o governo corrupto permaneceu em silêncio sobre os golpes (assim como a SEC e o governo federal fazem hoje), adicionando combustível ao meme de que os esquemas de pirâmide eram legítimos.

A história da Albânia é um presságio sobre o que pode acontecer quando um público obcecado por riquezas rápidas e sem vínculos com a atividade econômica real persegue esquemas que são simplesmente impossíveis economicamente. As manias especulativas acabam sempre mal para a maioria das pessoas porque são insustentáveis.

No "criptomercado" de hoje não existe mais um estigma associado a esquemas de pirâmide. O seu cão é um símbolo de escolha porque quer que ele suba, e é praticamente tudo o que há para ele. É pump and dump nu, já não apoiado por qualquer narrativa de criação de valor.

A moral desta história é que um governo competente, uma regulamentação forte e um público financeiramente instruído são as únicas forças que podem conter o frenesi antes que os esquemas de pirâmide prejudiquem as economias e os mercados. Precisamos de mais pessoas com a audácia de chamar esquemas de cripto-pirâmide pelo que eles realmente são: fraude de investimento economicamente insustentável que prejudica pessoas inocentes e destrói vidas.

**Transparência**

Bitcoin foi criado para não precisar de terceiros intermediando a transação. Mas qual é o terceiro que cobra mais de 10 dólares por transação? 10 dólares é a taxa de transação atual do Bitcoin. Quem acha que Bitcoin é viável como moeda cotidiana ou está iludido ou está querendo te iludir.

Dogecoin e Nano resolvem o problema porque não são alvos de especulação tão forte quanto bitcoin, porém, se a viabilidade de uma moeda é inversamente proporcional com o quanto ela é usada por especuladores, ela não é uma alternativa econômica viável. Se por um lado Dogecoin é uma moeda deflacionária em relação ao Real Brasileiro (você perderia dinheiro na especulação), por outro lado o preço do Dogecoin tem uma variação pequena, então dá para armazenar por curtos períodos de tempo e fazer compras.

Criador do DogeCoin, Jackson Palmer, no [Twitter](twitter.com/ummjackson/status/1415353984617914370):
>Depois de anos estudando isso, acredito que a criptomoeda é uma tecnologia hiper-capitalista inerentemente de direita construída principalmente para amplificar a riqueza de seus proponentes por meio de uma combinação de evasão fiscal, supervisão regulatória reduzida e escassez artificialmente aplicada.
>
>Apesar das alegações de “descentralização”, a indústria de criptomoedas é controlada por um poderoso cartel de figuras ricas que, com o tempo, evoluíram para incorporar muitas das mesmas instituições vinculadas ao sistema financeiro centralizado existente que supostamente pretendiam substituir.
>
>A indústria de criptomoedas alavanca uma rede de conexões comerciais duvidosas, influenciadores comprados e meios de comunicação pagos para perpetuar um funil cult de "enriquecimento rápido", projetado para extrair dinheiro novo dos financeiramente desesperados e ingênuos.
>
>A exploração financeira sem dúvida existia antes da criptomoeda, mas a criptomoeda é quase criada com o propósito de tornar o funil de lucratividade mais eficiente para os que estão no topo e menos protegido para os vulneráveis.
>
>Criptomoeda é como pegar as piores partes do sistema capitalista de hoje (por exemplo, corrupção, fraude, desigualdade) e usar software para limitar tecnicamente o uso de intervenções (por exemplo, auditorias, regulamentação, tributação) que servem como proteções ou redes de segurança para a pessoa média.
>
>Perdeu a senha da sua conta poupança? Sua culpa. Foi vítima de um golpe? Sua culpa. Bilionários manipulando mercados? Eles são gênios.
>
>Este é o tipo de criptomoeda perigosa do capitalismo "livre para todos", infelizmente, foi arquitetado para facilitar desde o seu início.
>
>Por essas razões, simplesmente não saio mais do meu caminho para me envolver em discussões públicas sobre criptomoeda. Não se alinha com minha política ou sistema de crenças, e não tenho energia para tentar discutir isso com aqueles que não estão dispostos a se envolver em uma conversa fundamentada.
>
>Eu aplaudo aqueles com energia para continuar fazendo as perguntas difíceis e aplicando as lentes do ceticismo rigoroso a que toda tecnologia deve estar sujeita. As novas tecnologias podem tornar o mundo um lugar melhor, mas não quando desvinculadas de sua política inerente ou de suas consequências sociais.

Bitcoin tem alguns problemas em privacidade, mas tem alguns mecanismos bem interessantes para reforçá-los, como CoinJoin (porém é caro demais). Lembrando que Bitcoin é rastreável pois sua Blockchain é aberta (mostra origem->destino e quantidade) para todo mundo. A Blockchain do Bitcoin é transparente (por enquanto, Greg Maxwell por exemplo é um dos devs trabalhando em CT - Confidential Transactions). No Monero, você não sabe o endereço de quem está enviando (Ring signatures), nem de quem está recebendo (Stealth addresses) e nem a quantidade (RingCT).

**Segurança**

A atual criptografia do Bitcoin, ECC (curvas elípticas, no caso é uma Koblitz, a secp256k1: `y² = x³ + 7`) não é resistente a computação quântica. Porém, não é fácil encontrar colisões para SHA-256. É possível, mas não é fácil. Porém, o algoritmo de Grover pode diminuir a complexidade de funções de hashing (no caso, SHA-256).

![](images/btc_ecc.png)

Nós temos as seguintes criptografias:
- Curva Koblitz: `y² = x³ + b`
- Secp256k1 (Bitcoin): `y² = x³ + 7`
- Complexidade Rho de Pollard (Scheme: EcDSA): `log2 √(πn/12) = 127.03`

A Koblitz tem parâmetros bem especificados, e por causa disso, tem algumas propriedades interessantes, como XOR para adição de dois pontos, por exemplo. Um pequeno ponto polêmico é sobre o EcDSA: o nonce é gerado aleatoriamente, ou seja, se haver algum problema na função `get_random` (como aconteceu no PS3), e pode haver problemas se o mesmo nonce for utilizado em mensagens diferentes. Uma solução para isso seria o RFC 6979 (que tira a aleatoriedade - coloca um valor determinístico utilizando HMAC-SHA256). Nem todas as wallets de Bitcoin utilizam EcDSA com valor determinístico, então é algo para se dar uma olhada antes de escolher.

Agora, temos as seguintes criptografias:
- Curva Montgomery: `by² = x³ + ax² + x`
- Curve25519 (Ethereum, Scheme: EdDSA): `y² = x³ + 48662x² + x`

A Curve25519 utilizada na Ed25519 é uma curva NIST, ou seja, ela foi modificada para atender às standards do governo americano, e dependendo do caso, os parâmetros podem ser mal escolhidos para fins de backdoor (como aconteceu com as curvas com `a=-3`), então é algo polêmico.

Sobre o EdDSA, o nonce é gerado com HMAC-SHA512 - é determinístico -, e o Curve25519 teria uma “vantagem em desempenho” por causa da curva Twisted Edward.

A fórmula da Twisted Edward (`a≠1`) é a seguinte: `ax² + y² = 1 + dx²y²`. Ela é biracionalmente equivalente a curva de Montgomery, eis o porque da “otimização”.

Resumindo, por causa dessa história de NIST a=-3, “otimização” e “desempenho”, eu ficaria com o pé atrás de utilizar a Curve25519 - se eu fosse escolher, utilizaria secp256k1 com HMAC-SHA256 no EcDSA.

**Descentralização**

Nós precisamos reconhecer que o Bitcoin falhou em seu processo de descentralização. As duas maiores exchanges de Bitcoin cada uma têm 5 vezes o valor da terceira colocada. A tendência de centralização de recursos no Bitcoin é tão grande quanto, senão maior, que numa economia normal, mas sem impostos para equilibrar.

Tratar a alocação e distribuição de moedas como um problema puramente matemático, sem a possibilidade de interferência é a negação da luta de classes no sistema financeiro. Como tal, favorece uma classe apenas: a classe que detém o capital.

**Viabilidade**

Várias exchanges caíram e perderam o dinheiro de seus clientes, por diversas vezes. Isso é algo incomparável com qualquer coisa que tenha acontecido com o dólar. Se uma exchange não consegue garantir a transação segura, Bitcoin não vale a pena. Simplesmente não existe iniciativa em se resolver o problema de segurança das exchanges de Bitcoin, exceto a de um camarada, mas infelizmente ainda está longe de ser concretizado. A CoinDesk resolve um problema de reverter transação indesejada, mas pode-se perder sua chave, como várias exchanges já perderam.

A falta de uma autoridade central sobre os dados torna impossível a recuperação de carteiras (o que seria equivalente a uma conta bancária). Imagine que acontece um incêndio na sua casa e você perde o computador e o backup da sua carteira Bitcoin. Tudo o que você tem de bitcoin é perdido para sempre. Em contraposição, no sistema bancário, se você perder o cartão (que é o acesso à sua conta), você pode provar sua identidade para o banco (que é a autoridade central sobre os dados) e recuperar o acesso à conta. Acho que não preciso explicar que isso torna inviável o uso de criptomoedas como mecanismo financeiro de massas.

Além disso, há outro problema: o Bitcoin é deflacionário. Deflação não é somente "produtos mais baratos". Deflação, no contexto de falta de liquidez na economia, significa menos dinheiro circulando. Se tem menos dinheiro circulando, tem menos gente com dinheiro, menos gente consumindo, menos capacidade produtiva bruta. A deflação não é uma causa de um possível aumento no consumo dos pobres, a deflação é uma consequência da falta de consumo dos pobres. Se deflação significasse que os pobres têm mais acesso aos bens de consumo, nesse raciocínio a essa altura mais gente deveria estar conseguindo comprar mais coisas com bitcoin, já que bitcoin é uma moeda deflacionária. Mas ela é deflacionária justamente porque a quantidade de bitcoins é limitada e está concentrada na mão de relativamente poucas pessoas. A causa da deflação é a concentração, a deflação não é a causa de nada.

Bitcoin tem um tempo de compensação de transação alto. Se alguém te envia Bitcoins e a rede estiver com muito tráfego, pode levar horas para os Bitcoins chegarem na sua carteira. Já aconteceu da rede levar um dia para confirmar transferências de Bitcoin devido à sobrecarga de transações, e toda aplicação tem tempo mínimo de retirada. Malmo afirma que em 2015 demorou 5.000 vezes mais eletricidade para processar uma transação de Bitcoin do que para o pagamento de um visto. Então, como tecnologia para ser usada no dia a dia do pagamento de xícaras de café ou sacos de arroz, ela é inviável. O grande custo de energia é uma consequência necessária da descentralização. A validação da cadeia de blocos deve ser computacionalmente difícil para evitar trapaças, e essa dificuldade computacional se traduz em uso de energia. A ineficiência da mineração do Bitcoin não está no hardware, está no software. Fazer o hardware ser mais eficiente só irá fazer com que mais hardware seja comprado enquanto for economicamente viável, o que no fim dá no mesmo consumo de energia.

Investimentos em Bitcoin não dão retorno imediato e não têm boa liquidez. Bitcoin não tem uma liquidez muito boa, mas as corretoras fazem malabarismo. A corretora pode oferecer "saque instantâneo" do bolso dela, e esperar a compensação acontecer no sistema dela depois. Como você vai ter algo para tirar sem receber na carteira? Bitcoin não é um tipo de conta-poupança. Compensa mais enviar dinheiro via Pix ou transferência bancária do que Bitcoin, a não ser que você tenha uma loja de drogas online. Mas existem criptomoedas melhores, como Monero, Etherium, etc.

A forma da retirada do dinheiro é decidida no contrato com a corretora. Ela pode te exigir saque mínimo, cobrar taxa de saque, exigir dias para o tempo de investimento mínimo, etc. Você pode aplicar o dinheiro na corretora e deixar investido no fundo, "D+15", "D+7" significa que você precisa esperar no mínimo 15 ou 7 dias corridos para sacar, se quiser seu dinheiro de volta, e não se engane muito pela taxa grande, você pode perder todo seu dinheiro do dia para a noite.

**A alternativa socialista ao Bitcoin**

O real não tem o mesmo valor para todas as regiões do Brasil, porque em diferentes regiões a cesta de mercadorias tem custos materiais (produção, transporte, manutenção) diferentes, e temos muitos problemas de precificação por conta disso. Uma moeda única em escala global amplificaria demais o problema da precificação e inflação.

Blockchain existe desde 1960 e já foi usada em muitos softwares antes de inventarem bitcoin. Blockchain é só uma tecnologia de replicação e sincronia de logs, MySQL usa isso, por exemplo. Dentro do nosso banco central (e dentro de cada banco, na verdade) existe um livro-razão gigante registrando cada centavo que saiu de uma conta e entrou em outra. A Blockchain do Bitcoin faz a mesma coisa. Sem ela, não seria possível ter certeza de que a sua carteira tem X Bitcoins, você poderia digitar qualquer número e seria um caos.

O sistema centralizado oferece vantagens que o descentralizado é incapaz de oferecer: estabilidade e câmbio real são as duas mais importantes. Se existisse um banco central com reserva física de moedas estrangeiras, com poder de emitir e destruir bitcoins, cobrando imposto, o Bitcoin poderia estabilizar seu preço e ser convertido em câmbio real pelas outras moedas do mundo.

Bitcoin é um enorme esquema de pirâmide, porque é artificialmente inflacionado usando stablecoins sem paridade real com os ativos que elas representam. Com uma moeda centralizada, existe uma entidade capaz de colocar dinheiro no mercado ou tirar, de acordo com as variações de demanda de mercadorias, criando a paridade.

A moeda centralizada digital é mais prática e barata do que a descentralizada, porque a liquidação de transferência pode ser instantânea e sem taxas (Pix é uma prova disso), é mais segura, porque o tipo de manipulações que as baleias fazem à vontade com bitcoins são proibidas na bolsa de valores, é mais estável, porque temos um grande banco controlando as oscilações do mercado (o câmbio é real, o Banco Central não esconde nem falsifica suas reservas), e pode ser usada anonimamente, é só usar dinheiro em espécie. Também dá pra incluir contratos inteligentes em uma moeda digital centralizada, e qualquer pessoa pode fazer um contrato de próprio punho, o juiz só precisa conseguir ler.

Pix é só um meio de transferência. Quando você deposita dinheiro em uma conta bancária, aquele dinheiro é manipulado de forma digital. O que não está pronto ainda é dinheiro digital fora de uma conta no banco. A China está testando isso agora mesmo. O plano é o dinheiro digital depender apenas do Banco Central, sem precisar de conta bancária, ou melhor, sem precisar de nada além de um RG para ter dinheiro digital. Você só precisaria ter um celular e um RG para usar seu dinheiro.

O nosso maior problema é quem manda no banco central e no governo. Resolvido esse problema, nossa moeda pode voltar a ser forte. A solução para a inflação é uma economia planificada, com pleno emprego, estoques cheios, barrigas alimentadas e dinheiro no bolso das pessoas, a inflação se manteria muito próxima de zero.

**Vantagens do Bitcoin - e sua dupla face desvantajosa**

Quero reconhecer aqui alguns dos argumentos a favor das criptomoedas que foram apresentados por defensores pró-socialistas.

A relação entre o desperdício energético e o uso e desenvolvimento de fontes renováveis é interessante; a transmissão de energia normalmente impõe restrições à infraestrutura, mas a mineração [incentiva o desenvolvimento em regiões remotas](https://slate.com/technology/2021/05/jack-dorsey-bitcoin-square-climate-white-paper-clean-energy.html) por si só.

Existe uma luta dentro da comunidade de criptomoedas para mudar de mecanismos de consenso de “prova de trabalho” que consomem muita energia para uma “prova de participação” ainda mais abstrata. Este constitui um campo de pesquisa interessante, mas pode realmente piorar as questões endêmicas de “[enriquecimento do mais rico](https://thedefiant.io/rich-getting-richer-in-pos-chains-by-chainflows-chris-remus/)” (a prova de participação consome muito menos energia que a prova de trabalho, porém, ainda não há números precisos, mas a quantidade necessária de processamento para se gerar uma prova é factualmente muito menor, o que necessariamente vai implicar num consumo menor, porém, a prova de participação funciona através da confiança dos maiores detentores de capital dentro da blockchain, e recompensando-os por isso, ou seja, a prova de participação necessariamente implica em uma concentração de renda cada vez maior). Além disso, a "ineficiência" (inegavelmente gasta muita energia por transação, onde de acordo com relatório do site MoneySuperMarket, cada transação consome 1173 kWh de eletricidade, e isso é mais que o consumo mensal de uma casa nos EUA (867 kWh)) dos algoritmos de prova de trabalho não é um acidente, é projetada. Quando o hardware se torna mais energeticamente eficiente, a rede impõe uma dificuldade maior e deixa os desafios mais difíceis. A única resposta são algoritmos de prova de participação, mas mesmo esta prova tem seus contras. Isso só demonstra que criptomoedas são um conceito inerentemente quebrado que não resolvem problemas do mundo real e criam problemas piores.

No entanto, grande parte dos sistemas citados aqui se devem por causa de uma Blockchain distribuídas. Blockchains não distribuídas trazem alguns benefícios, como garantia de unicidade e atomicidade das transações. A própria moeda Petro incorpora algumas tecnologias da Blockchain.

Além disso, tecnologias como o Digital Yuan da China (plataforma de blockchain para acelerar o desenvolvimento do sistema de trens de carga entre a China e Europa) e o Petro da Venezuela visam permitir que entidades comerciais contornem o sistema SWIFT, o que prejudica a capacidade do império dos EUA de impor sanções esmagadoras.

**Apesar dos problemas, é utópico o Bitcoin no socialismo?**

O Bitcoin não poderia ser usado como uma alternativa moderna aos vale-trabalhos.

Pode-se argumentar que o conteúdo de energia deve, em certo sentido, ser refletido na precificação, mas dado que a precificação puramente baseada na energia não leva em consideração os insumos de trabalho, isso claramente não constituiria uma realização do sistema de vale-trabalho de Marx. Mesmo que se aceite o preço da energia para bens de consumo, não está claro como poderíamos determinar a remuneração dos trabalhadores. No caso dos vales-trabalho de Marx, eles estão diretamente ligados ao conteúdo de trabalho dos bens de consumo e ao trabalho contribuído pelos trabalhadores. O mesmo não pode ser dito do Bitcoin. Embora seja possível calcular um equivalente em bitcoin de produtos por meio dos conteúdos de energia relativa, o mesmo não é possível para mão de obra contribuída. Isso significa que esta proposta não fornece resposta à questão essencial de quanto tempo um trabalhador terá de trabalhar para poder pagar o Nissan Leaf EV, por exemplo.

Os vale-trabalhos de Marx não exigem trabalho para serem produzidos. Ou pelo menos não tanto quanto o trabalho que pretendem representar. Eles são simplesmente tokens dados para confirmar que um trabalhador contribuiu com uma certa quantidade de tempo de trabalho. Na verdade, os vale-trabalhos não precisam levar a mesma quantidade de tempo de trabalho para serem produzidos. Na verdade, se o fizessem, isso seria um desperdício significativo de trabalho que não iria para a produção de objetos com valor de uso direto. Os vale-trabalhos são melhores em comparação com um ingresso de teatro. Um ingresso de teatro representa o direito de poder assistir a uma apresentação. Para que seja esse o caso, não é necessário que o próprio tíquete exija uma quantidade significativa de esforço ou recursos para ser produzido. Tudo o que deve ser garantido é que não pode ser facilmente falsificado, a fim de evitar fraudes.

Para as pessoas interessadas em lavagem de dinheiro, evasão fiscal e negócios com drogas ilegais, o Bitcoin tem seus atrativos, mas como modelo de pagamento em um sistema socialista, a infraestrutura fornecida por chip, pin cards e leitores de cartão existentes é mais promissora. A diferença essencial entre os vales-trabalho e as notas bancárias propostos por Marx é que os vales-trabalho não circulam. Eles são creditados às pessoas pelo trabalho realizado e cancelados quando um trabalhador compra algo em lojas públicas. Se você leu o livro socialista utópico de Edward Bellamy, "Looking Backward", pode encontrar um relato de como isso funcionava com a tecnologia do século XIX. Bellamy imaginou uma América socialista em 2000. As pessoas tinham cartões de crédito sociais, como os cartões perfurados recentemente inventados por Hollerith para processamento de dados. No início de cada mês, você recebe um novo cartão com seus créditos marcados. Você ia a lojas públicas para comprar coisas e a caixa registrava fisicamente os créditos de seu cartão enquanto os lia. A mercadoria foi então enviada para sua casa por tubos pneumáticos.

A própria ideia do cartão de crédito deriva, na verdade, dessa literatura socialista utópica. Mas, ao contrário dos cartões de crédito capitalistas do ano 2000 nos EUA, não haveria maneira de realizar comércio privado com o sistema de Bellamy. Os seus créditos são intransferíveis, uma vez que, no momento em que os utiliza, tornam-se pequenos fragmentos de papel inúteis no depósito de resíduos da caixa. Nenhuma atividade no mercado negro privado é possível sem alguma forma de circulação de dinheiro.

Uma economia socialista em 2030 não teria que reverter para os cartões de crédito de papel. Basta, uma vez que os bancos, os meios de produção e distribuição são de propriedade pública, alterar o software que os bancos utilizam. Em vez de euros ou libras sendo transferidos de sua conta para a conta da Tesco, o software simplesmente cancelaria seu crédito de trabalho. A loja, sendo administrada publicamente, não seria um negócio com fins lucrativos, portanto, não precisaria ser creditada em dinheiro. A loja não compraria mercadorias de um atacadista porque os depósitos e fábricas de onde as mercadorias vinham também seriam administrados publicamente. Em consequência, não haveria transferência de propriedade entre a fábrica, armazém e supermercado e, portanto, não haveria necessidade de uma cadeia de pagamentos. Ainda teriam que ser coletadas estatísticas para ver quantas horas de trabalho as pessoas estavam gastando com flocos de milho ou biscoitos de trigo, etc., para garantir que as fábricas públicas alocassem as quantidades correspondentes de recursos para fazer esses produtos. Os mesmos registros, em conjunto com o controle de estoque, seriam usados para detectar furtos. Mas não haveria dinheiro e não haveria necessidade da elaborada proteção contra trapaças mútuas que o Bitcoin usa. O Bitcoin, longe de ser um modelo para uma economia cooperativa, na verdade simboliza um capitalismo cão come cão, onde ninguém coopera e ninguém confia em ninguém.
