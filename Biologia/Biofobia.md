#;Biofobia
Primeiro de tudo, o que é a biofobia? A noção conhecida como biofobia foi criada segundo a alegação de que, ao aderirem à perspectiva da tábula rasa, com o comportamento humano sendo regido pela história e pela sociedade, os cientistas sociais acabem não levando em conta que a espécie humana também possua em seu comportamento padrões passíveis de compreensão complementar pela ótica da teoria da evolução.

Um trio de sociólogos, para averiguar a existencia da biofobia, desenvolveram uma pesquisa, disponibilizando um questionário de 11 perguntas a 155 outros sociólogos, onde estes, depois de reportarem suas identidades políticas (conservador, moderado, progressista ou radical, com esse último significando algo próximo de "extrema-esquerda"), tinham que marcar se achavam conclusões típicas da biologia comportamental plausíveis ou implausíveis; as perguntas do questionário eram se concordavam com as seguintes conclusões:
1. Evoluímos uma tendência a preferir alimentos com alto teor calórico.
2. O medo tão facilmente desenvolvido de aranhas e cobras reside numa resposta inata de aversão a predadores.
3. A percepção da beleza física possui componentes correlacionados à capacidade reprodutiva.
4. O ciúme carrega influências evolucionárias significantes.
5. Homens são naturalmente mais inclinados à poligamia.
6. Homens cometem mais crimes que as mulheres por motivações naturais.
7. Homens naturalmente consomem mais pornografia.
8. Homens tentam controlar a sexualidade feminina graças aos benefícios evolucionários ligados ao comportamento sexual promíscuo e de posse.
9. Algumas pessoas têm, geneticamente, um potencial de inteligência maior do que outras.
10. A sexualidade tem bases biológicas.
11. Graças a possuírem cérebros diferentes, homens e mulheres diferem em suas capacidades cognitivas.

Embora os achados, em comparação às pesquisas mais antigas, indiquem que existe uma simpatia crescente pela biologia comportamental humana (e um cenário mais dividido e complexo do que os críticos dão a entender), a aceitação de cada um dos pontos refletiu a sensibilidade política que lhes era inerente.

No ponto 1, um assunto pouco polêmico, a aceitação foi de 59%, enquanto que, no ponto 8, um assunto muito polêmico, a aceitação foi de 22%. Ou seja, a receptividade às explicações biológicas caiu conforme o aumento da sensibilidade política.

![](images/biophobia_table.jpeg)

Os últimos 3 pontos tiveram a aceitação de, respectivamente, 81%, 70% e 42%. Apesar da surpreendente aceitação dos pontos 9 e 10 ir contra a hipótese de biofobia, talvez haja uma explicação ainda mais simples: já que para a direita conservadora a homossexualidade não é natural e sim um pecado, faz-se necessário, pensa-se, defender que a orientação sexual tenha bases biológicas.

É uma exceção confirmando a regra vista dentre os pontos 1-8, onde a simpatia se mostrou inversamente proporcional à carga política. Além do mais, no ponto 11 o padrão também se repetiu e a aceitação foi a menor das três, dando um peso ainda maior à hipótese de biofobia. Tirando que cerca de apenas 29% dos 543 convidados aceitou participar da pesquisa (com uma parcela tendo inclusive se recusado de maneira pejorativa, o que é um possível indicativo de que talvez a aversão à biologia seja maior), algumas coisas ficaram mais claras com os resultados.

Ao que parece, na psicologia política existe, da esquerda à direita, um tipo de viés cognitivo que se sobressai quando os fatos só são vistos como fatos quando confirmam a ideologia do grupo - ou, pelo menos, quando não atentam contra ela. Frequentemente a direita conservadora tem tentado buscar argumentos baseados na evolução contra a esquerda, e mais frequente ainda é a esquerda tachar (com razão) como pseudociências o criacionismo bíblico e a "teoria do design inteligente", posições tão comuns à direita salientando que temos uma origem compartilhada com os outros animais graças à seleção natural. E isso enquanto, por outro lado, afirmam aos quatro cantos que "não existe natureza humana, os únicos instintos que temos é o instinto de sobrevivência e o de reprodução", ignorando, por má fé e desconhecimento, que é impossível que os instintos que dispuseram toda a vida na Terra não exerçam nenhuma influência no comportamento social humano.

O que não significa que seja um fenômeno genético. As relações humanas, assim como apontaram Marx e Engels, são relações e fenômenos biológicos, pois o motor destas relações é o da sobrevivência do organismo humano. Mas estas não são relações determinadas pelos genes.

Outro grande exemplo de biofobia, foram as críticas feitas ao biólogo Edward Wilson, que ao escrever o seu livro "Sociobiologia: A Nova Síntese" tentando trazer uma aproximação entre as ciencias biológicas e naturais, sofreu grande ataque de uma parte expressiva de biólogos da época chamados de "biólogos dialéticos" que tinham uma percepção marxista da ciência e afirmavam de modo categórico a inexistência da natureza humana.

Até hoje, antropólogos mais aversos à teoria sociobiólogica se baseam em teses infundadas para críticar a teoria, usando argumentos como o de Sahlins, que afirmava que a falta da universalidade de conhecimentos matemáticos como o de fração na humanidade seria uma evidência que as equações da sociobiologia para explicar parte do nosso comportamento são falhos. O irônico é que tais equações explicam de modo bem sucedido o comportamento de outros animais como abelhas e formigas que com toda certeza também não compreendem o conceito de fração.
