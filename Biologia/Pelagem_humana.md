# Pelagem humana
Só não temos pelos nas palmas da mão e do pé e entre os dedos, no resto do corpo todo temos mas são finos demais. Resquício da nossa herança de mamíferos.

O ser humano ficar pelado foi uma grande vantagem evolutiva, para diminuir o consumo energético do corpo. O corpo do ser humano gasta a metade da energia por Kg do que o da maioria dos mamíferos. Manter pelagem crescendo o tempo todo gasta muita energia e recursos materiais do corpo. Eles não crescem por graça divina, tem um trabalho celular intenso para construir o pelo.

O ser humano foi otimizado para economizar bastante energia, porém, procrastinação não foi adquirida através da evolução para economizar energia para o cérebro. Todos os animais procrastinam. Procrastinação é uma emoção, como tristeza ou alegria. Ela te protege de desperdiçar energia valiosa com algo que você percebe não valer a pena (todas as emoções tem um propósito, aliás, elas não existem à toa).

E o ser humano é um animal muito prejudicado no design. As poucas vantagens que temos em relação aos outros animais é: eficiência energética, inteligência superior, ser o animal que mais caminha longas distâncias  no planeta, e ser capaz de tacar pedras ou esfaquear animais maiores que si com precisão. Foi essa otimização que nos permitiu hoje ter um cabeção consumindo 100 watts sozinho.

Os outros animais têm bem mais musculatura, pelagens e pelancas que protegem o abdômen, ossos mais densos, espinhos, carapaças, garras, e isso gasta muita energia e sobra pouco para o cérebro. Nós cortamos todas as defesas físicas e concentramos a esmagadora maior parte da nossa energia no cérebro. Ficamos com um corpinho ridículo de frágil, pelado, mas temos computador.

Nossas pernas longas e músculos densos concentrados nas pernas também são uma grande vantagem evolutiva. Ao invés de focar em musculatura nos braços, focamos em musculatura nas pernas, então quando acaba a comida em um lugar, nós podemos ir para outro mesmo se estiver há 10 Km. Podemos caçar por distâncias muito maiores que os outros caçadores grandes e pesados.

Nós ficamos pelados e frágeis, mas nossa inteligência criou sapatos, roupas, armas, etc. Nossa tremenda eficiência energética nos permitiu nos adaptar à todos os climas da Terra. Em climas quentes nós podemos simplesmente beber mais água, nosso corpo esquenta pouco; em climas frios nós podemos comer mais, mas menos por Kg do que os outros animais, e continuar aquecidos, sem falar nas roupas. É um design de termoregulagem muito versátil.

Infelizmente as vezes dá problema com fungos ou bactérias nas partes mais delicadas do corpo que os animais com corpo mais quente não tem, mas inventamos a higiene, que é até melhor (e mais recentemente, a medicina).
