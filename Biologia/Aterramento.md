#;Aterramento
Na elétrica, aterramento é o estado de um circuito que está conectado eletricamente ao planeta Terra. A Terra neutraliza toda tensão elétrica, levando o circuito em contato com ela ao potencial zero (zero volts).

Os corpos dos animais terrestres evoluíram para estar em contato elétrico com a Terra na maior parte do tempo. Isso é importante porque nós geramos muita eletricidade estática, devido a forma que nosso corpo funciona. Entrar em aterramento melhora imediatamente a circulação sanguínea e permite o fígado neutralizar radicais livres com maior eficiência.

Na verdade, a Terra como um todo possui uma carga elétrica tremenda, e troca uma quantidade absurda de carga elétrica à cada segundo com a atmosfera. Porém, localmente, na crosta terrestre ela fornece lacunas para todos os nossos elétrons livres, então nós atingimos o potencial zero.
