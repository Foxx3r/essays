#;Como resolver cólicas?
Geralmente quando a mulher tem muita cólica na menstruação, é porque o útero dela não está baixo o bastante e os restos do endométrio estão entrando nas trompas. Se as trompas ficarem preenchidas e o corpo não conseguir expulsar, esse tecido vai apodrecer ali e causar uma inflamação grave chamada de endometriose, daí elas precisam ir no ginecologista fazer uma lavagem intrauterina.

As mulheres as vezes precisam fazer banhos de acento para limpar pele do útero que não saiu totalmente na menstruação. Os ginecologistas ensinam isso para elas quando acham necessário.
