# O que é vida?
Eu responderia que é um sistema computacional de malha aberta auto-referenciado com fluxo de informação retro-alimentado. Fogo dentro dessa explicação não é considerado vida (como na biologia tradicional), porque para o fogo acontecer, só é preciso existir calor, combustível e comburente. O fogo só cria 1 desses 4 citados, ele não se replica, é só uma reação química, nada demais.

O combustível é oxidado-reduzido pelo comburente com a facilitação do calor e sua estrutura química arrebenta, liberando luz e mais calor, esse calor faz uma cascata que mantém o combustível queimando, é só isso.

Já sistemas físicos computacionais auto-referenciados com fluxo de alimentação retro-alimentado são muito mais sofisticados do que o fogo:
1. Eles são sistemas computacionais: eles realizam computação.
2. Eles são auto-referenciados: eles carregam informação detalhada sobre si mesmos, mesmo que não tenham a capacidade de se auto-replicar.
3. Eles são sistemas computacionais abertos: realizam uma constante troca de informação e energia com o ambiente.
4. O fluxo de informação que esses sistemas sustentam é retro-alimentado: eles realizam funções de controle dependendo das informações recebidas do ambiente e ajustam a informação que estão emitindo para o ambiente.

Ou seja, os seres vivos são dispositivos que realizam computação e manipulam energia e matéria para continuarem funcionando. Nossas células literalmente executam programas que estão codificados no DNA. Todas elas possuem dispositivos de computação no núcleo que são acionados por vários tipos de estruturas de sensores, e de acordo com os programas genéticos, elas ativam outras estruturas de uma forma coordenada. Por exemplo, para construir novas organelas, reconstruir a membrana de revestimento, se mover no ambiente.
